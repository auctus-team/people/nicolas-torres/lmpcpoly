#!/usr/bin/env python

import numpy as np
import pinocchio as pin
import rospy
import tf.transformations
from franka_msgs.msg import FrankaState
from geometry_msgs.msg import Pose, PoseStamped
from interactive_markers.interactive_marker_server import (
    InteractiveMarkerFeedback, InteractiveMarkerServer)
from sensor_msgs.msg import JointState
from visualization_msgs.msg import (InteractiveMarker,
                                    InteractiveMarkerControl, Marker)

nodename = 'test_marker.py'
marker_pose = PoseStamped()
pose_pub = None


class Limits:
    def __init__(self, name='noname', min=-np.inf, max=np.inf):
        self.min, self.max = min, max
        self.name = name

    def clip(self, val, bias=0):
        res = np.clip(val, bias + self.min, bias + self.max)
        # print('test_marker::{}: {:.2f} < {:.2f} < {:.2f} == {:.2f}'.format(
        #     self.name, bias + self.min, val, bias + self.max, res))
        return res


class PositionLimits:
    def __init__(self):
        self.x, self.y, self.z = Limits('x'), Limits('y'), Limits('z')


position_limits = PositionLimits()
position_limits.x = Limits('x', 0.1, 0.6)
position_limits.y = Limits('y', -0.45, .3)
position_limits.z = Limits('z', 0.10, 0.65)


def publisher_callback(msg, link_name):
    marker_pose.header.frame_id = link_name
    marker_pose.header.stamp = rospy.Time(0)
    pose_pub.publish(marker_pose)


def process_feedback(feedback):
    if feedback.event_type == InteractiveMarkerFeedback.POSE_UPDATE:
        marker_pose.pose.position.x = position_limits.x.clip(
            feedback.pose.position.x, link0_p[0])
        marker_pose.pose.position.y = position_limits.y.clip(
            feedback.pose.position.y, link0_p[1])
        marker_pose.pose.position.z = position_limits.z.clip(
            feedback.pose.position.z, link0_p[2])
        marker_pose.pose.orientation = feedback.pose.orientation
    server.applyChanges()


def makeBox(msg):
    marker = Marker()

    # marker.type = Marker.SPHERE
    marker.type = Marker.CUBE
    marker.scale.x = msg.scale * 0.45
    marker.scale.y = msg.scale * 0.45
    marker.scale.z = msg.scale * 0.45
    marker.color.r = 0.5
    marker.color.g = 0.5
    marker.color.b = 0.5
    marker.color.a = 0.3

    return marker


def SE3ToPosemsg(SE3):
    msg = Pose()
    p = SE3.translation
    msg.position.x, msg.position.y, msg.position.z = p[0], p[1], p[2]
    q = pin.Quaternion(SE3.rotation)
    msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w = q.x, q.y, q.z, q.w
    return msg


def wait_for_initial_pose():
    joints = rospy.wait_for_message(
        '/panda/franka_state_controller/joint_states', JointState, timeout=30)

    q = joints.position
    robot_description = rospy.get_param('/panda/robot_description')
    ###################################################
    model = pin.buildModelFromXML(robot_description)
    q = np.hstack([q, np.zeros(model.nv - len(q))])
    data = model.createData()
    # for some reason this seg faults on forwardKinematics
    # ----------------------------------
    # import tempfile
    # tmp = tempfile.NamedTemporaryFile()
    # with open(tmp.name, 'w') as f:
    #     f.write(robot_description)
    # robot=pin.RobotWrapper.BuildFromURDF(tmp.name)
    # model = robot.model
    # data = robot.data
    ###################################################
    # print('dir(model)', dir(model))
    # print('nframes:', model.nframes)
    rospy.loginfo('test_marker.py:: model.names: %s', [i for i in model.names])
    # frame_id = model.getFrameId(model.names[-1])
    # frame_id = 'panda_joint8'
    frame_id = model.nframes - 1
    rospy.logwarn('%s: marker model frame_id: %s', nodename, frame_id)
    pin.forwardKinematics(model, data, q=np.array(q))
    current_pose = pin.updateFramePlacement(model, data, frame_id)

    msg = SE3ToPosemsg(current_pose)
    marker_pose.pose.orientation = msg.orientation
    marker_pose.pose.position = msg.position


if __name__ == '__main__':
    rospy.init_node(nodename)
    rospy.loginfo('%s: starting...', nodename)

    rospy.loginfo('%s: rospy.wait_for_message joint_states...', nodename)
    wait_for_initial_pose()

    rospy.loginfo('%s: tf.TransformListener()', nodename)
    listener = tf.TransformListener()
    global link0_p
    rospy.loginfo('%s: listener.lookupTransform(world, panda_link0)', nodename)
    rate = rospy.Rate(10)
    link0_p = None
    retries = 100
    while link0_p is None and retries > 0:
        try:
            link0_p, _ = listener.lookupTransform('world', 'panda_link0',
                                                  rospy.Time(0))
            rospy.logwarn('%s: panda_link0 pos: %s', nodename, link0_p)
        except (tf.LookupException, tf.ConnectivityException):
            continue
        retries -= 1
        rate.sleep()
    if link0_p is None:
        rospy.logfatal(
            '%s: listener.lookupTransform(world, panda_link0) FAILED',
            nodename)

    rospy.loginfo('%s: link0_p: %s (%s retries left)', nodename, link0_p,
                  retries)

    pose_pub = rospy.Publisher('/interactive_marker',
                               PoseStamped,
                               queue_size=10)
    rospy.loginfo('%s: publisher initialized', nodename)
    server = InteractiveMarkerServer('equilibrium_pose_marker')
    int_marker = InteractiveMarker()
    link_name = 'world'
    int_marker.header.frame_id = link_name
    int_marker.scale = 0.3
    int_marker.name = 'equilibrium_pose'
    int_marker.description = ('the robot will follow once activated')
    int_marker.pose = marker_pose.pose
    # run pose publisher
    rospy.Timer(rospy.Duration(0.005),
                lambda msg: publisher_callback(msg, link_name))

    # insert a box
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = 'rotate_x'
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 1
    control.orientation.y = 0
    control.orientation.z = 0
    control.name = 'move_x'
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = 'rotate_y'
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 1
    control.orientation.z = 0
    control.name = 'move_y'
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = 'rotate_z'
    control.interaction_mode = InteractiveMarkerControl.ROTATE_AXIS
    int_marker.controls.append(control)
    control = InteractiveMarkerControl()
    control.orientation.w = 1
    control.orientation.x = 0
    control.orientation.y = 0
    control.orientation.z = 1
    control.name = 'move_z'
    control.interaction_mode = InteractiveMarkerControl.MOVE_AXIS
    int_marker.controls.append(control)

    control = InteractiveMarkerControl()
    control.always_visible = True
    control.interaction_mode = InteractiveMarkerControl.MOVE_3D
    int_marker.controls.append(control)
    control.markers.append(makeBox(int_marker))

    server.insert(int_marker, process_feedback)

    rospy.loginfo('%s: marker created, server.applyChanges()', nodename)
    server.applyChanges()

    rospy.logwarn('%s: READY', nodename)
    rospy.spin()
