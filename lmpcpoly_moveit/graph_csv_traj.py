from datetime import datetime
import traceback
from pdb import pm
import matplotlib.pyplot as plt
import matplotlib as mpl
import osqp
import pinocchio as pin

import sys
import os

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

# mpl.rcParams['font.family'] = 'sans-serif'
# mpl.rcParams['font.sans-serif'] = ['Arial']
mpl.rcParams['font.family'] = 'Avenir'
mpl.rcParams['text.usetex'] = True
# mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['font.size'] = 20
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 5

import numpy as np

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp
import pinocchio as pin
import csv

################################################################
# trajectory generation


def lin_from(xlist):
    # return [x.linear for x in xlist]
    return [pin.exp(x).translation for x in xlist]


def ang_from(xlist):
    return [x.angular for x in xlist]


def norm_from_vechist(vechist):
    '''
    turns list of pin.Motion into a numpy vector of norms
    '''
    return np.array([np.linalg.norm(v) for v in vechist])


def vec_from_motlist(vec, row=None, ncut=None):
    '''
    turns list of pin.Motion into a numpy vector
    '''
    if ncut is None:
        ncut = len(vec)
    if row is None:
        return np.array([v.vector for v in vec[:ncut]])
    return np.array([v.vector[row] for v in vec[:ncut]])


def se3err(x1, x2):
    '''
    NOTE: not a real se3 result, linear comp contains translation
    '''
    return pin.Motion(
        np.hstack([
            pin.exp(x1).translation - pin.exp(x2).translation,
            x1.angular - x2.angular
        ]))


def se3skew(se3):
    '''
    se3 skew for matricial multiplication
    '''
    return np.vstack([
        np.hstack([pin.skew(se3.angular),
                   pin.skew(se3.linear)]),
        np.hstack([np.zeros((3, 3)), pin.skew(se3.angular)])
    ])


def brse3(x1, x2):
    '''
    se3 bracket
    '''
    br = pin.Motion.Zero()
    br.linear = pin.skew(x1.angular) @ x2.linear - pin.skew(
        x2.angular) @ x1.linear
    br.angular = pin.skew(x1.angular) @ x2.angular
    return br


def dexp(x0):
    '''
    Computes the linearization matrix at some point x0
    '''
    br = se3skew(x0)
    return (np.eye(6)  # n=0
            + br / 2  # n=1
            + br @ br / 12  # n=2
            )


def me2(x0, x, dx):
    '''
    Using the ME2 linearization at some point x0, compute the value
    of applying an increment dx to x.
    '''
    return x + pin.Motion(dexp(x0) @ dx)


##################################################################
hist_t_mpcv = []
hist_pose_mpcv = []
hist_err_mpcv = []
hist_posed_mpcv, hist_posedd_mpcv = [], []

ref_t = []
ref_se3 = []
ref_se3d = []

ref_se3_reconstructed = []
ref_se3d_reconstructed = []

#######################################################
#######################################################


def exists(filename):
    if not os.path.isfile(filename):
        print('Error: file not found\nfile: {}'.format(filename))
        return False
    return True


#######################################################
homedir = os.path.expanduser('~')
generic_path = os.path.join(homedir, '.ros', 'lmpcv1_{}{}.csv')
state_refpath = generic_path.format('ref', 'x')
input_refpath = generic_path.format('ref', 'u')
state_respath = generic_path.format('result', 'x')
input_respath = generic_path.format('result', 'u')

input_file_found = exists(input_refpath)


def strVec(vec):
    return ' '.join(['{:.2f}'.format(x) for x in vec])


def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))


def so3lim(mot):
    if np.linalg.norm(mot.angular) >= np.pi:
        mot.angular = -mot.angular
    return mot


x0 = stringToMotion(
    '0.848715     0.710175    -0.333284      2.90236     -1.20235 -0')
xf = stringToMotion(
    '0.398456 -0.376723   0.33332  -2.90245   1.20224        -0')

x0exp = pin.exp(x0)
xfexp = pin.exp(xf)

print('x0', x0)
print('exp(x0)', pin.exp(x0))
print('xf', xf)
print('exp(xf)', pin.exp(xf))

delta = pin.SE3.Interpolate(x0exp, xfexp, 0)
deltalog = pin.log(delta)

print('delta', deltalog)
print('exp(delta)', delta)

# sys.exit(0)

reference_loaded = False
if exists(state_refpath):
    print(state_refpath, 'found')
    with open(state_refpath, 'r') as reff:
        vecreader = csv.reader(reff, delimiter=',')
        for row in vecreader:
            ref_t.append(float(row[0]))
            vec = pin.Motion(np.array(row[1:]).astype(float))
            # ref_se3.append(so3lim(vec))
            ref_se3.append(vec)

    for kk, tt in enumerate(ref_t):
        alpha = kk / len(ref_t)
        posekref_reconstructed = pin.log(
            pin.SE3.Interpolate(x0exp, xfexp, alpha))
        ref_se3_reconstructed.append(x0exp.act(posekref_reconstructed))
        print('alpha', alpha)
        if kk < 10:
            print('recon:', strVec(posekref_reconstructed.vector))
            print(' file:', strVec(ref_se3[kk].vector))
    reference_loaded = True

if exists(input_refpath):
    print(input_refpath, 'found')
    with open(input_refpath, 'r') as reff:
        vecreader = csv.reader(reff, delimiter=',')
        for row in vecreader:
            if not reference_loaded:
                ref_t.append(float(row[0]))
            vec = pin.Motion(np.array(row[1:]).astype(float))
            ref_se3d.append(vec)

result_loaded = False
if exists(state_respath):
    print(state_respath, 'found')
    with open(state_respath, 'r') as refs:
        vecreader = csv.reader(refs, delimiter=',')
        for row in vecreader:
            hist_t_mpcv.append(float(row[0]))
            vec = pin.Motion(np.array(row[1:]).astype(float))
            hist_pose_mpcv.append(vec)
    result_loaded = True

if exists(input_respath):
    print(input_respath, 'found')
    with open(input_respath, 'r') as refs:
        vecreader = csv.reader(refs, delimiter=',')
        for row in vecreader:
            if not reference_loaded:
                hist_t_mpcv.append(float(row[0]))
            vec = pin.Motion(np.array(row[1:]).astype(float))
            hist_posed_mpcv.append(vec)

###################################################################


def plot_cart(figno, title, baseunit, labels, col_offset=0):
    columns = 3
    # rows = 2
    rows = 1
    if input_file_found:
        # rows = 3
        rows = 2
    fig, ax = plt.subplots(rows, columns, num=figno, sharex='col')
    if rows == 1:
        ax = ax[None]
    alpha = 0.4
    alpha_zero = 0.3
    color_me2 = 'blue'
    color_mpcv = 'green'
    color_mpcv_substraction = 'orange'
    color_mpcvprop = 'red'
    label_mpcvel = 'mpcv_prop'
    label_mpcvel_comp = 'mpcv_comp'
    label_me2 = 'me2'
    lw = 3
    pose0_marker_size = 30

    def myplot(tnp, ax, vector, lbl, clr, alp, ls='solid'):
        ax.plot(tnp[:vector.shape[0]],
                vector,
                label=lbl,
                linestyle=ls,
                linewidth=lw,
                color=clr,
                alpha=alp)
        ax.scatter(
            tnp[:vector.shape[0]],
            vector,
            color=clr,
            marker='x',
            # clip_on=False,
            s=5,
            alpha=alp + .2)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)

    def plot_dashed_zero_sized(tnp, motlist, ax, label='ref', color='black'):
        vector = vec_from_motlist([pin.Motion.Zero() for x in motlist], col)
        ax.plot(tnp[:vector.shape[0]],
                vector,
                label=label,
                linestyle='dashed',
                linewidth=lw,
                color=color,
                alpha=alpha_zero)

    fig.suptitle(title)
    for col in np.arange(columns):
        axis = ax[0, col]
        idx = col + col_offset
        print('col:', col, 'idx:', idx)
        if col == 0:
            axis.set_ylabel('[{}]'.format(baseunit))
        axis.set_title(coords[col])
        ##########################################################
        # XYZ

        if ref_se3:
            myplot(ref_t,
                   ax=axis,
                   vector=vec_from_motlist(ref_se3, idx),
                   lbl='ref',
                   ls='dashed',
                   clr='black',
                   alp=alpha)

            # myplot(ref_t,
            #     ax=axis,
            #     vector=vec_from_motlist(ref_se3_reconstructed, idx),
            #     lbl='ref-recons',
            #     ls='dashed',
            #     clr='blue',
            #     alp=alpha)

        if hist_t_mpcv:
            myplot(hist_t_mpcv,
                   ax=axis,
                   vector=vec_from_motlist(hist_pose_mpcv, idx),
                   lbl=label_mpcvel_comp,
                   clr=color_mpcv,
                   alp=alpha)
        ##########################################################
        # axis = ax[1, col]
        # if col == 0:
        #     axis.set_ylabel('[{}]'.format(baseunit))
        # axis.set_title('err' + coords[col])
        # axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        # # XYZ Error
        # plot_dashed_zero_sized(ref_t, ref_se3, ax=axis)

        # myplot(ref_t,ax=axis,
        #        vector=norm_from_vechist(ang_from(ref_se3)),
        #        lbl='ref ang norm',
        #        clr='black',
        #        alp=alpha)

        # myplot(ax=axis,
        #        vector=vec_from_motlist(hist_err_me2, idx),
        #        lbl=label_me2,
        #        clr=color_me2,
        #        alp=alpha)
        # myplot(ax=axis,
        #        vector=vec_from_motlist(hist_err_mpcv, idx),
        #        lbl=label_mpcvel_comp,
        #        ls='solid',
        #        clr=color_mpcv,
        #        alp=alpha)
        if col == (columns - 1):
            axis.legend(
                shadow=True,
                fancybox=True,
                frameon=False,
                loc='upper left',
                bbox_to_anchor=(.85, .9),
            )

        ##########################################################
        # VELOCITIES
        if input_file_found:
            # axis = ax[2, col]
            axis = ax[1, col]
            if col == 0:
                axis.set_ylabel('[{}/s]'.format(baseunit))
            axis.set_title(r'$\dot{{{coord}}}$'.format(coord=coords[col]))
            axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
            # non error plots
            plot_dashed_zero_sized(ref_t, ref_se3d, ax=axis)
            myplot(ref_t,
                   ax=axis,
                   vector=vec_from_motlist(ref_se3d, idx),
                   lbl='ref',
                   ls='dashed',
                   clr='black',
                   alp=alpha)

        if hist_posed_mpcv:
            myplot(hist_t_mpcv,
                   ax=axis,
                   vector=vec_from_motlist(hist_posed_mpcv, idx),
                   lbl=label_mpcvel_comp,
                   clr=color_mpcv,
                   alp=alpha)

        ##########################################################
        # ACCELERATIONS
        # axis = ax[3, col]
        # if col == 0:
        #     axis.set_ylabel(r'[{}/$s^2$]'.format(baseunit))
        # axis.set_title(
        #     r'$\ddot{{{coord}}}$'.format(coord=coords[col]))
        # axis.ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        # plot_dashed_zero_sized(hist_posedd_exp, ax=axis)
        # # NON error plots
        # toplot_mpcv_posedd = vec_from_motlist(hist_posedd_mpcv, idx)
        # myplot(ax=axis,
        #        vector=vec_from_motlist(hist_posedd_exp, idx),
        #        lbl='ref',
        #        ls='dashed',
        #        clr='black',
        #        alp=alpha)
        # # toplot_mpcv_posedd = vec_from_motlist(hist_posedd_exp, idx) - vec_from_motlist(hist_posedd_mpcv, idx)
        # myplot(ax=axis,
        #        vector=toplot_mpcv_posedd,
        #        lbl=label_mpcvel,
        #        ls='solid',
        #        clr=color_mpcv,
        #        alp=alpha)

        # posedd_max = np.max(toplot_mpcv_posedd)
        # posedd_min = np.min(toplot_mpcv_posedd)
        # factor = 1.1
        # axis.set_ylim(bottom=posedd_min * factor, top=posedd_max * factor)
        # axis.set_xlabel('[s]')


coords = ['x', 'y', 'z']
labels = ['ref', 'mpc']
colors = ['blue', 'green']

plot_cart(figno=0, title='position', baseunit='m', labels=labels, col_offset=0)
plot_cart(figno=1,
          title='angular',
          baseunit='rad',
          labels=labels,
          col_offset=3)

##################################################################
plt.show()
