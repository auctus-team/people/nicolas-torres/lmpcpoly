import pinocchio as pin
import numpy as np


def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))


poser = stringToMotion(
    '0.8173444   0.60327331 -0.11307027  2.51256546 -1.07595846  0.05600359')
posel = stringToMotion(
    '0.10615261 -1.00983103  0.37681411 -2.6008344   1.04078695  0.36898001')

print('poser:', poser)
print('posel:', posel)
print('th poser:', np.linalg.norm(poser.angular))
print('th posel:', np.linalg.norm(posel.angular))

thetar = np.linalg.norm(poser.angular)
thetal = np.linalg.norm(poser.angular)


def Ginv(so3):
    '''
    lynch & park pg 106
    '''
    th, w = thaxis(so3)
    wskew = pin.skew(w)
    return np.eye(3) / th- wskew / 2 + (
        1 / th- (1 / np.tan(th/ 2)) / 2) * (wskew @ wskew)

def log3(R):
    '''
    lynch & park pg 87
    '''
    tr = R.trace()
    # print('R.trace:', tr)
    th = np.arccos((tr - 1) / 2)
    # print('th:', th)
    return th * pin.unSkew((R - R.T) / (2 * np.sin(th)))


def log6(T):
    '''
    lynch & park pg 106
    should be the same as pinocchio, outside of singularities
    '''
    so3 = log3(T.rotation)
    th, w = thaxis(so3)

    pos = T.translation
    return pin.Motion(th * Ginv(so3) @ pos, so3)

def closestlog6(T, T0):
    '''
    implements the log6 without th<=2pi
    '''
    se3, _  = closestlog6th(T, T0)
    return se3

# def one_turn_pair(T, T0):
#     so3 = pin.log(T.rotation)
#     th, w = thaxis(so3)

#     so30 = pin.log(T0.rotation)
#     th0, w0 = thaxis(so30)

#     if th-th0 > np.pi:


#     return pin.log(T), pin.log(T0)
def closerlog6_positive(T, T0):
    '''
    implements the log6 without th<=2pi
    '''
    reflog = pin.log(T0)

    sol1 = pin.log(T)
    so3 = pin.log(T.rotation)
    th, w = thaxis(so3)

    th2 = th + 2 * np.pi
    so32 = th2 * w
    sol2 = pin.Motion(th2 * Ginv(so32) @ pos, so32)

    def distance(ref, val):
        return np.linalg.norm(pin.log(pin.exp(ref).actInv(pin.exp(val))).linear)
        # return np.linalg.norm(ref-val)

    dist = np.linalg.norm(sol2-reflog)
    print('th:', th, 'th2:', th2, 'thref:', np.linalg.norm(reflog.angular))
    print('dist sol2: ', distance(reflog, sol2), 'dist sol1', distance(reflog, sol1))
    if distance(reflog, sol2) > distance(reflog, sol1):
        print('--> chose sol2 <--')
        return sol2
    print('--> chose sol1 <--')
    return sol1


def closestlog6th(T, T0):
    '''
    implements the log6 without th<=2pi
    '''

    # so3 = log3(T.rotation)
    so3 = pin.log(T.rotation)
    th, w = thaxis(so3)

    pos = T.translation
    sol1 = (pin.Motion(th * Ginv(so3) @ pos, so3), th)

    th2 = th + 2 * np.pi
    so32 = th2 * w
    sol2 = (pin.Motion(th2 * Ginv(so32) @ pos, so32), th2)

    th3 = th - 2 * np.pi
    so33 = th3 * w
    sol3 = (pin.Motion(th3 * Ginv(so33) @ pos, so33), th3)

    t0log = pin.log(T0)

    res = sol1
    # sols = [sol1, sol3]
    sols = [sol1, sol2]

    def distance_metric(val, ref):
        # return np.linalg.norm(pin.log(pin.exp(val.angular).T@pin.exp(ref.angular)))
        # return np.linalg.norm(val.angular - ref.angular)
        return np.linalg.norm(val-ref)

    for i, tupl in enumerate(sols):
        sol = tupl[0]
        dist = distance_metric(sol, t0log)
        # print('sol:', sol)
        print('i:', i, 'norm:', dist)
        if dist > distance_metric(res[0], t0log):
            print('--> chose i:', i, '<--')
            res = tupl

    return res

def thaxis(so3):
    th = np.linalg.norm(so3)
    return th, so3/th

def G(so3):
    '''
    lynch & park pg 105
    '''
    th, w = thaxis(so3)
    skew = pin.skew(w)
    return th*np.eye(3) + (1-np.cos(th))*skew + (th-np.sin(th))*skew@skew

def exp3(so3):
    '''
    lynch & park pg 84
    Rodrigues formula
    '''
    th, w = thaxis(so3)
    skew = pin.skew(w)
    return np.eye(3) + np.sin(th)*skew + (1-np.cos(th))*skew@skew

def exp6(se3):
    '''
    lynch & park pg 105
    '''
    th, w = thaxis(se3.angular)
    return pin.SE3(exp3(se3.angular), G(th*w)@se3.linear/th)

def exp6ref(val, th):
    '''
    compute exp6 but take advantage of the th used to know quadrant
    (if th is negative, then it needs to be put in [0, 2pi])
    '''
    _, wval = thaxis(val.angular)
    if th<0:
        wval*=-1
    # print('th original:', th_original)
    # print('     th val:', thval)
    # print('th orig-val:', th_original-thval)
    # lin_original = -th_original*val.linear/thval
    # return pin.SE3(exp3(val.angular), G(th_original*wval)@lin_original/th_original)
    return pin.SE3(exp3(val.angular), G(th*wval)@val.linear/th)



def print_line():
    print('-' * 30)


poserexp = pin.exp(poser)
poselexp = pin.exp(posel)
#########################################################################################
#########################################################################################
# LOG3 and EXP3

print_line()
print('EXP3 compare to pinocchio')
print('poser.angular:', poser.angular)
th, w  = thaxis(poser.angular)
print('th poser.angular:', th)
print('w poser.angular:', w)
pinexp = pin.exp(poser.angular)
print('pin.exp(R):\n', pinexp)
# myexp3 = exp3(poser.angular)
th2piang = (th-2*np.pi)*w
print('th2piang:', th2piang)
myexp3 = exp3(th2piang)
print('myexp3(R):\n', myexp3)
print('dR to th2piang (should be 0):\n', pin.log(myexp3.T@pinexp))

drl = poser.angular -posel.angular
print('poser->posel:', drl, 'norm:', np.linalg.norm(drl))
drlmod = th2piang-posel.angular
print('poser(mod)->posel:', drlmod, 'norm:', np.linalg.norm(drlmod))
print_line()


print_line()
print('LOG3 compare to pinocchio')
print('pin.log(R):', pin.log(poserexp.rotation))
mylog3 = log3(poserexp.rotation)
assert np.isclose(np.linalg.norm(mylog3-poser.angular), 0), 'LOG3 dR is not zero'

print('mylog3(R):', mylog3)
mylog3exp = pin.exp(mylog3)
print('mylog3exp:\n', mylog3exp)
assert np.isclose(np.linalg.norm(pin.log(mylog3exp.T @ poserexp.rotation)), 0), 'EXP3 dR is not zero'
print_line()
print_line()
print_line()

print_line()
print('LOG6 check linear components correspond')
print('poserexp:\n', poserexp)
print('poser.linear:', poser.linear)
pos = poserexp.translation
th = np.linalg.norm(poser.angular)
print('poser Ginv lin:', th * Ginv(poser.angular) @ pos)
print_line()


#########################################################################################
#########################################################################################
# LOG6 and EXP6

print_line()
mylog6r = log6(poserexp)
assert np.isclose(np.linalg.norm(mylog6r-poser), 0), 'log6 failed dX is not zero'
mylog6l = log6(poselexp)
assert np.isclose(np.linalg.norm(mylog6l-posel), 0), 'log6 failed dX is not zero'

mylog6rmyexp = exp6(mylog6r)
mylog6lmyexp = exp6(mylog6l)
assert np.isclose(np.linalg.norm(pin.log(mylog6rmyexp.actInv(poserexp)).vector), 0), 'exp6 failed dX is not zero'
assert np.isclose(np.linalg.norm(pin.log(mylog6lmyexp.actInv(poselexp)).vector), 0), 'exp6 failed dX is not zero'

#########################################################################################
#########################################################################################
# LOG6 and EXP6

# test it finds the same log, closest to itself
closer_pos_poser = closerlog6_positive(poserexp, poserexp)
assert np.isclose(np.linalg.norm(pin.log(pin.exp(closer_pos_poser).actInv(poserexp)).vector), 0), 'closer log6 failed dX is not zero'
closer_pos_posel = closerlog6_positive(poselexp, poselexp)
assert np.isclose(np.linalg.norm(pin.log(pin.exp(closer_pos_posel).actInv(poselexp)).vector), 0), 'closer log6 failed dX is not zero'

# cross test
closer_pos_poser_froml = closerlog6_positive(poserexp, poselexp)
assert np.isclose(np.linalg.norm(pin.log(pin.exp(closer_pos_poser_froml).actInv(poserexp)).vector), 0), 'closer log6 failed dX is not zero'
closer_pos_posel_fromr = closerlog6_positive(poselexp, poserexp)
assert np.isclose(np.linalg.norm(pin.log(pin.exp(closer_pos_posel_fromr).actInv(poselexp)).vector), 0), 'closer log6 failed dX is not zero'
