#pragma once

#include "pinocchio/fwd.hpp" // SE3
#include "pinocchio/multibody/data.hpp"
#include "pinocchio/multibody/model.hpp"

// #include <cstddef>
#include <ros/node_handle.h>

// MoveIt
// #include <actionlib/server/simple_action_server.h>
// #include <moveit_trajectory_interface/TrajectoryAction.h>
// #include <moveit/kinematic_constraints/utils.h>
// #include <moveit/move_group_interface/move_group_interface.h>
// #include <moveit/planning_interface/planning_interface.h>
// #include <moveit/planning_pipeline/planning_pipeline.h>
// #include <moveit/planning_scene_monitor/planning_scene_monitor.h>
// #include <moveit/robot_model_loader/robot_model_loader.h>
// #include <moveit/robot_state/conversions.h>
// #include <moveit/trajectory_processing/time_optimal_trajectory_generation.h>
// #include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/RobotTrajectory.h>
// #include <moveit_msgs/PlanningScene.h>

#include <geometry_msgs/PoseArray.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/TwistStamped.h>
//
#include <realtime_tools/realtime_publisher.h>

#include <moveit_trajectory_interface/MPCConfigure.h>
#include <moveit_trajectory_interface/MPCFollow.h>

// #include <nav_msgs/Path.h>
#include <chrono>
#include <lmpcpoly/mpcacc.hpp>
#include <lmpcpoly/mpcacc_tan.hpp>
#include <lmpcpoly/polycap_extended.hpp>
#include <mutex>
#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64.h>
#include <std_srvs/Empty.h>

// polytopes
#include <jsk_recognition_msgs/PolygonArray.h>
#include <sensor_msgs/PointCloud.h>
#include <visualization_msgs/Marker.h>

// #include <std_srvs/Empty.h>
#include <atomic>
#include <string>

class MoveItTrajectory {
public:
  MoveItTrajectory();

  void init(std::string planning_group,
            std::string robot_description_param_name, Eigen::VectorXd q_init,
            std::vector<std::string> joint_names = std::vector<std::string>());

  bool load_robot_model(
      std::string robot_description,
      std::vector<std::string> joint_names = std::vector<std::string>());

  void update_initial_pose();

  void setControlledFrame(std::string controlled_frame);

  // void abort_objective();
  void set_objective(const Eigen::VectorXd start, const Eigen::VectorXd end);
  /**
   * \brief
   * Get the desired Pose
   */
  Eigen::Affine3d getCartesianPose();

  /**
   * \brief
   * Get the desired Twist
   */
  Eigen::Matrix<double, 6, 1> getCartesianVelocity();

  /**
   * \brief
   * Get the desired Acceleration
   */
  Eigen::Matrix<double, 6, 1> getCartesianAcceleration();

  Eigen::VectorXd getJointAcceleration();

  Eigen::VectorXd getJointVelocity();

  Eigen::VectorXd getJointConfiguration();

  Eigen::VectorXd getJointAccelerationAtTime(double time);

  Eigen::VectorXd gains();

  Eigen::VectorXd kd_factors();

  double ff_acc_factor();

  bool computeNewTrajectory(moveit_msgs::RobotTrajectory path);

  void updateTrajectory(const Eigen::VectorXd &q, const Eigen::VectorXd &qd,
                        const Eigen::VectorXd &qdd);

  void publish_trajectory();

  double getTimeProgress();

  bool GoalReached();

  bool isInitialized();

  std::string getRobotName();

  void sortJointState(moveit_msgs::RobotTrajectory &msg);

  double control_period();

  bool
  toggle_follow_flag(moveit_trajectory_interface::MPCFollow::Request &req,
                     moveit_trajectory_interface::MPCFollow::Response &res);

  bool configure_mpc(moveit_trajectory_interface::MPCConfigure::Request &req,
                     moveit_trajectory_interface::MPCConfigure::Response &res);
  bool
  get_configure_mpc(moveit_trajectory_interface::MPCConfigure::Request &req,
                    moveit_trajectory_interface::MPCConfigure::Response &res);

  void follow_target_pose(const geometry_msgs::PoseStamped &posestamped);

  void publish_polytope(
      realtime_tools::RealtimePublisher<sensor_msgs::PointCloud>
          &vertices_publisher,
      realtime_tools::RealtimePublisher<jsk_recognition_msgs::PolygonArray>
          &polygon_publisher,
      const Eigen::MatrixXd A, const lmpcpoly::VectorLimits qd,
      const std::string frame_id,
      const Eigen::Vector3d position = Eigen::Vector3d::Zero(),
      const double scaling = 5);

private:
  ros::NodeHandle node_handle;
  // Model information
  pinocchio::Model model; /*!< @brief pinocchio model*/
  pinocchio::Data data;   /*!< @brief pinocchio data*/
  std::string controlled_frame = "";

  // Trajectory parameters
  double t_traj = 0.0;
  Eigen::Matrix3d epsR;
  pinocchio::SE3 initial_pose;
  Eigen::Affine3d desired_pose;
  pinocchio::SE3 target_pose;
  pinocchio::SE3 follow_pose;
  Eigen::Matrix<double, 6, 1> desired_velocity;
  Eigen::Matrix<double, 6, 1> desired_acceleration;
  Eigen::Matrix<double, 6, 1> desired_jerk;
  Eigen::VectorXd joint_configuration;
  Eigen::VectorXd joint_velocity;
  Eigen::VectorXd joint_acceleration;
  std::atomic<bool> trajectory_is_built;
  std::atomic<bool> follow_flag;

  lmpcpoly::Delay horizon_publish_delay;
  lmpcpoly::Delay polytope_publish_delay;

  // PUBLISHERS
  realtime_tools::RealtimePublisher<sensor_msgs::PointCloud>
      acc_vertices_publisher;
  realtime_tools::RealtimePublisher<jsk_recognition_msgs::PolygonArray>
      acc_polygon_publisher;
  realtime_tools::RealtimePublisher<std_msgs::Float64> mpctime_publisher;
  realtime_tools::RealtimePublisher<std_msgs::Float64> kinenergy_publisher;
  realtime_tools::RealtimePublisher<std_msgs::Float64>
      desired_kinenergy_publisher;
  realtime_tools::RealtimePublisher<std_msgs::Float64> kinenergyd_publisher;
  double kin_energy_prev = 0;
  realtime_tools::RealtimePublisher<geometry_msgs::PoseArray> horizon_publisher;
  realtime_tools::RealtimePublisher<sensor_msgs::JointState>
      jointspace_limit_distance_publisher;

  ros::Publisher home_pose_publisher, initial_pose_publisher;

  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      bias_acceleration_publisher, bias_jerk_publisher;
  Eigen::VectorXd current_bias_acceleration_local;
  Eigen::VectorXd current_bias_acceleration_lwa;
  Eigen::VectorXd current_bias_jerk_local;
  Eigen::VectorXd current_bias_jerk_lwa;

  realtime_tools::RealtimePublisher<geometry_msgs::PoseStamped>
      current_pose_publisher, desired_pose_publisher, target_pose_publisher;

  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      velocity_limits_publisher, acceleration_limits_publisher;

  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      desired_error_publisher, target_error_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_velocity_publisher, desired_velocity_publisher;
  realtime_tools::RealtimePublisher<visualization_msgs::Marker>
      desired_velocity_arrow_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_acceleration_publisher, desired_acceleration_publisher;
  realtime_tools::RealtimePublisher<geometry_msgs::TwistStamped>
      current_jerk_publisher, desired_jerk_publisher;

  // moveit_trajectory_interface::progress trajectory_progress;
  bool is_initialized = false;
  std::string planning_group;
  std::string robot_description_param_name;
  bool joints_are_unordered = false;

  double current_mpcresult_starttime;
  // std::unique_ptr<lmpcpoly::Horizon> current_horizon = nullptr;
  // std::future<std::unique_ptr<lmpcpoly::Horizon>> future_horizon;
  Eigen::VectorXd current_q;
  Eigen::VectorXd current_qd;
  Eigen::MatrixXd current_OperIner;
  Eigen::VectorXd current_qdd;
  pinocchio::SE3 current_pose;
  Eigen::VectorXd current_twist;
  Eigen::VectorXd current_twist_acc;
  Eigen::VectorXd current_twist_jerk;
  pinocchio::Data::Matrix6x Jlwa;
  pinocchio::Data::Matrix6x Jlocal;
  Eigen::MatrixXd Minv;
  Eigen::MatrixXd JMinvlwa;
  Eigen::MatrixXd JMinvlocal;
  Eigen::MatrixXd g;

  // lmpcpoly::VectorLimits tau_lims;
  lmpcpoly::VectorLimits q_lims;
  lmpcpoly::VectorLimits qd_lims;
  lmpcpoly::VectorLimits qdd_lims;
  lmpcpoly::VectorLimits qdd_lims_firststep;
  lmpcpoly::VectorLimits qddd_lims;

  lmpcpoly::RobotPolytope pol_q_pos;
  lmpcpoly::RobotPolytope pol_qd_pos;
  lmpcpoly::RobotPolytope pol_qdd_pos;
  lmpcpoly::RobotPolytope pol_qdd_firststep_pos;
  lmpcpoly::RobotPolytope pol_qddd_pos;
  lmpcpoly::RobotPolytope pol_q_tan;
  lmpcpoly::RobotPolytope pol_qd_tan;
  lmpcpoly::RobotPolytope pol_qdd_tan;
  lmpcpoly::RobotPolytope pol_qdd_firststep_tan;
  lmpcpoly::RobotPolytope pol_qddd_tan;

  Eigen::VectorXd qdd_max;
  Eigen::VectorXd qddd_max;

  unsigned int H, ndt;
  std::unique_ptr<lmpcpoly::MPCAcc> mpcprob;
  std::unique_ptr<lmpcpoly::MPCAccTangent> mpcprobtang;

  std::mutex pin_mutex;
  std::mutex current_horizon_mutex;
  unsigned int vel_scaling_iter = 0;
  double vel_scaling = 0.3;

  double filter_w0 = 200;
  double mpc_compute_time = 0.1;
  double mpc_compute_time_warn = 0.1;
  lmpcpoly::LPFilter lptwist;

  inline static lmpcpoly::Delay input_zeroaccmsg_delay = lmpcpoly::Delay(1);

  double _control_period = 1e-3;
  lmpcpoly::MPCParams mpc_solveparams;

  std::atomic<bool> tangent_space = false;
  bool verbose = true;
  bool limit_jerk = false;
  bool polytope_with_bias = false;

  ros::ServiceServer follow_service;
  ros::ServiceServer configure_mpc_service;
  ros::ServiceServer get_configure_mpc_service;

  ros::Subscriber follow_subscriber;
  lmpcpoly::Delay follow_update_delay;

  lmpcpoly::FakeVelocityScaling velscaling;
  double kp_linear = 500;
  double kp_angular = 1000;
  double kd_factor_linear = 2;
  double kd_factor_angular = 2;
  double ff_acc_factor_ = 1;
  double Emax = 0.1;
  double tau_factor = 1;
  double qd_factor = 1;
  double qdd_factor = 1;
  double qddd_factor = 1;
  int constraints_mode = 0;
};
