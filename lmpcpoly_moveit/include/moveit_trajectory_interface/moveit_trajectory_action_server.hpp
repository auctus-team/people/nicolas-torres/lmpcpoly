#pragma once

#include <actionlib/server/simple_action_server.h>
#include <moveit_trajectory_interface/TrajectoryAction.h>
#include <moveit_trajectory_interface/moveit_trajectory.hpp>
#include <ros/console.h>
#include <ros/ros.h>

class TrajectoryAction {
public:
  TrajectoryAction(std::string name,
                   std::shared_ptr<MoveItTrajectory> trajectory);
  void
  executeCB(const moveit_trajectory_interface::TrajectoryGoalConstPtr &goal);

protected:
  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<moveit_trajectory_interface::TrajectoryAction>
      as_; // NodeHandle instance must be created before this line. Otherwise
           // strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  moveit_trajectory_interface::TrajectoryFeedback feedback_;
  moveit_trajectory_interface::TrajectoryResult result_;
  std::shared_ptr<MoveItTrajectory> trajectory;
};
