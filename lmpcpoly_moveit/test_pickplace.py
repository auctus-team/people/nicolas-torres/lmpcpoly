import moveit_commander
import pinocchio as pin
import numpy as np
import time
import sys
import rospy
import actionlib
import moveit_msgs.msg
import geometry_msgs.msg

from moveit_trajectory_interface.srv import *
from moveit_trajectory_interface.msg import *

def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))

moveit_commander.roscpp_initialize(sys.argv)
rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
robot = moveit_commander.RobotCommander('/panda/robot_description', ns='panda')
scene = moveit_commander.PlanningSceneInterface(ns='panda')

group_name = 'panda_arm'
move_group = moveit_commander.MoveGroupCommander(
    group_name, robot_description='/panda/robot_description', ns='panda')

# Instantiate clients
trajectory_action_client = actionlib.SimpleActionClient(
    '/panda/moveit_trajectory_interface/trajectory_action',
    moveit_trajectory_interface.msg.TrajectoryAction,
)
print('waiting for client')
trajectory_action_client.wait_for_server()
print('got client')

display_trajectory_publisher = rospy.Publisher(
    '/move_group/display_planned_path',
    moveit_msgs.msg.DisplayTrajectory,
    queue_size=20,
)

# We can get the name of the reference frame for this robot:
planning_frame = move_group.get_planning_frame()
print('============ Planning frame: %s' % planning_frame)

# We can also print the name of the end-effector link for this group:
eef_link = move_group.get_end_effector_link()
print('============ End effector link: %s' % eef_link)

# We can get a list of all the groups in the robot:
group_names = robot.get_group_names()
print('============ Available Planning Groups:', robot.get_group_names())

# Sometimes for debugging it is useful to print the entire state of the
# robot:
print('============ Printing robot state')
print(robot.get_current_state())
print('')

# print('============ joint configuration goal')
# # We get the joint values from the group and change some of the values:
# tau = 2*np.pi
# joint_goal = move_group.get_current_joint_values()
# joint_goal[0] = 0
# joint_goal[1] = -tau / 8
# joint_goal[2] = 0
# joint_goal[3] = -tau / 4
# joint_goal[4] = 0
# joint_goal[5] = tau / 6  # 1/6 of a turn
# joint_goal[6] = 0

# # The go command can be called with joint values, poses, or without any
# # parameters if you have already set the pose or joint target for the group
# move_group.go(joint_goal, wait=True)


# # Calling ``stop()`` ensures that there is no residual movement
# move_group.stop()
print('sleep 1s...')
time.sleep(1)
def GeomMsgToSE3(msg):
    curq = pin.Quaternion(
        np.array([
            msg.orientation.x, msg.orientation.y, msg.orientation.z,
            msg.orientation.w
        ]))
    pos = np.array([msg.position.x, msg.position.y, msg.position.z])
    return pin.SE3(curq.matrix(), pos)


def SE3ToGeomMsg(SE3):
    msg = geometry_msgs.msg.Pose()
    p = SE3.translation
    msg.position.x, msg.position.y, msg.position.z = p[0], p[1], p[2]
    q = pin.Quaternion(SE3.rotation)
    msg.orientation.x, msg.orientation.y, msg.orientation.z, msg.orientation.w = q.x, q.y, q.z, q.w
    return msg


def move_to_SE3(Pose):
    pose_goal = SE3ToGeomMsg(Pose)
    print('goal:\n', pose_goal.position)

    # curpose_msg = move_group.get_current_pose().pose
    # pose_goal = geometry_msgs.msg.Pose()
    # pose_goal.orientation = curpose_msg.orientation
    # pose_goal.position.x = 0.4
    # pose_goal.position.y = 0.1
    # pose_goal.position.z = 0.4

    move_group.set_pose_target(pose_goal)
    print(type(move_group))
    print('go!...')
    # plan = move_group.go(wait=True)
    plan = move_group.go()
    # move_group.stop()
    # move_group.clear_pose_targets()

    # time to converge
    timeout = 10  # [s], max wait
    ang_eps = 3 * (np.pi / 180)  # [rad]
    lin_eps = 3 * 1e-3
    pose = GeomMsgToSE3(move_group.get_current_pose().pose)
    delay = .1
    err = pin.log(pose.actInv(Pose))
    err_lin = np.linalg.norm(pin.exp(err).translation)
    err_ang = np.linalg.norm(err.angular)
    # while err_lin > lin_eps and err_ang > ang_eps:
    while err_lin > lin_eps:
        if timeout > 0:
            rospy.sleep(delay)
            timeout -= delay
        else:
            print('Error: timeout but robot did not reach goal')
            print('err lin: {:.2f}m'.format(err_lin))
            print('err ang: {:.2f}rad | {:.2f}deg'.format(
                err_ang, err_ang * 180 / np.pi))
            print('goal:\n', pose_goal.position)
            print('current:\n', move_group.get_current_pose().pose.position)
            break
        pose = GeomMsgToSE3(move_group.get_current_pose().pose)
        err = pin.log(pose.actInv(Pose))
        err_lin = np.linalg.norm(pin.exp(err).translation)
        err_ang = np.linalg.norm(err.angular)
        # print('remaining...{:2.f}s'.format(timeout))

    # time to wait after converge/fail
    # rospy.sleep(.2)
    # rospy.sleep(1)
    rospy.sleep(1.5)
    # rospy.sleep(2)


move_group.set_planning_time(10)
move_group.clear_path_constraints()

# dphix = pin.rpy.rotate('x', -np.pi*1e-4)
dphixn = pin.rpy.rotate('x', -np.pi / 8)  # right
dphixp = pin.rpy.rotate('x', np.pi / 8)
# dphiy = pin.rpy.rotate('y', np.pi*1e-4)
dphiy = np.eye(3)
# z08 = np.array([0.4, 0.1, .8])
# z01 = np.array([0.4, 0.1, .1])
dz = np.array([0, 0, .1])

start_pose_msg = move_group.get_current_pose().pose
print('='*10)
print('============ start_pose:\n', start_pose_msg)
start_pose = GeomMsgToSE3(start_pose_msg)
print('log(start_pose):', pin.log(start_pose).vector)

# TODO: hardcoded starting pose
# start_pose_log = stringToMotion('0.81813072  0.73563557 -0.32793589  2.87568122 -1.19176581 -0.10189045')
# start_pose_log = stringToMotion('-2.80151853e-01 -2.37375025e+00 -5.97704303e-01 -2.90240112e+00 1.20206308e+00 -2.49410062e-05')
start_pose_log = stringToMotion('0.84912  0.710302 -0.333397 2.90247 -1.20214 -0.000522939')
start_pose = pin.exp(start_pose_log)


print('start_pose:', pin.log(start_pose).vector)
print('start_pose.position:', start_pose.translation)
print('='*10)
# orinv = pin.rpy.rotate('x', 0.98*np.pi)
# orinv = dphixn @ dphiy @ start_pose.rotation
orinv = start_pose.rotation
orinvr = dphixn @ start_pose.rotation
orinvl = dphixp @ start_pose.rotation
or_inspecr = pin.rpy.rotate('x', -np.pi / 1.8) @ start_pose.rotation
or_inspecl = pin.rpy.rotate('x', np.pi / 1.8) @ start_pose.rotation
# print('orinv:\n', orinv)
# print('start.rotation:\n', start_pose.rotation)
# start_pose = pin.SE3(orinv, start_pose.translation)
start_poser = pin.SE3(orinvr, start_pose.translation)
start_posel = pin.SE3(orinvl, start_pose.translation)
inspectionr = pin.SE3(or_inspecr, start_pose.translation+ np.array([0, .1, -.2]))
inspectionl = pin.SE3(or_inspecl, start_pose.translation+ np.array([0, -.1, -.2]))

obj1 = pin.SE3(
    pin.rpy.rotate('x', -np.pi / 12) @ start_pose.rotation,
    start_pose.translation + np.array([-0.15, 0.2, -.4]))
obj2 = pin.SE3(
    pin.rpy.rotate('x', -np.pi / 6) @ start_pose.rotation,
    start_pose.translation + np.array([-0.2, -0.2, -.3]))
obj3 = pin.SE3(
    pin.rpy.rotate('x', -np.pi / 3) @ start_pose.rotation,
    start_pose.translation + np.array([-0.3, -0.5, -.1]))
obj4 = pin.SE3(
    pin.rpy.rotate('x', np.pi * .5) @ start_pose.rotation,
    start_pose.translation + np.array([-0.4, -0.3, 0]))
# obj4 = pin.SE3(
#     pin.rpy.rotate('z', 10 * (np.pi / 180)) @ pin.rpy.rotate(
#         'y', -50 * (np.pi / 180)) @ pin.rpy.rotate('x', 50 *
#                                                    (np.pi / 180)) @ np.eye(3),
#     # start_pose.translation + np.array([-.6, -0.5, .2]))
#     start_pose.translation + np.array([-0.3, -0.5, -.1]))
objz = pin.SE3(
    start_pose.rotation,
    start_pose.translation + np.array([0,0, -.3]))
objl = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.6, 0.5, -0.2]))
    # start_pose.translation + np.array([-.05, -0.3, -.35]))
    start_pose.translation + np.array([-.05, -0.15, -.1]))
print('objl:', objl)

objr = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.1, 0.5, -0.2]))
    # start_pose.translation + np.array([-.1, 0.3, -.35]))
    start_pose.translation + np.array([-.1, 0.15, -.1]))


far_objl = pin.SE3(objl)
# far_objl.rotation = orinvl
# far_objl.translation = far_objl.translation + np.array([0, -.2, 0])
far_objl.translation = far_objl.translation + np.array([0.05, -.1, -0.1])
far_objr = pin.SE3(objr)
# far_objr.rotation = orinvr
# far_objr.translation = far_objr.translation + np.array([0, .2, 0])
far_objr.translation = far_objr.translation + np.array([0, .1, .1])

far_inspectionr = pin.SE3(pin.rpy.rotate('y', -np.pi / 8)@pin.rpy.rotate('x', -np.pi / 4) @ start_pose.rotation, objr.translation)
far_inspectionl = pin.SE3(inspectionl.rotation, far_objl.translation+ np.array([0, .1, .1]))

video1=pin.SE3(objl)
video1.translation+= np.array([0.05, -.1, -0.1])
video2=pin.SE3(inspectionr)
# video2.rotation = pin.rpy.rotate('y', -np.pi / 8)@pin.rpy.rotate('x', -np.pi / .8) @ start_pose.rotation
video2.rotation = pin.rpy.rotate('y', -np.pi / 8)@start_pose.rotation
video2.translation += np.array([0.1, 0, .1])
video3=pin.SE3(objr)
video3.rotation = pin.rpy.rotate('x', -np.pi / 1.8) @ start_pose.rotation
video3.translation += np.array([0.05, .1, .1])


# print('start_poser:', pin.log(start_poser).vector)
# print('start_posel:', pin.log(start_posel).vector)
# print('start_poser:', start_poser)
# print('start_posel:', start_posel)

# poses = [obj1, start_pose, obj2, start_pose]
# poses = [start_poser, obj1]
poses = [start_poser, start_posel]
poses = [start_poser, obj1, start_poser, start_posel, obj2, start_posel, obj3, obj4]
poses = [start_poser, obj1, obj2, obj3, obj4]
poses = [start_poser, obj1, obj2, obj3]
# poses = [start_poser, obj1, obj2]
poses = [objl, objr]
# poses = [objz, start_pose]
# poses = [far_objl, far_objr]
# poses = [far_inspectionl, far_inspectionr]
poses = [objl, inspectionr, objr, inspectionl]
# poses = [objl, inspectionr, objr]
# poses = [video1, video2, video3]
# poses = [inspectionr, objl]
# poses = [inspectionr, objr]

# def_pos_tol, def_orien_tol = move_group.get_goal_position_tolerance(), move_group.get_goal_orientation_tolerance()

# print('   position tolerance:', def_pos_tol)
# print('orientation tolerance:', def_orien_tol)

i = 0
while True:
    if i >= len(poses):
        i = 0
    print('============ move, i:', i)
    # if poses[i] == far_objr:
    #     print('non default tolerances')
    #     move_group.set_goal_position_tolerance(1.8)
    #     move_group.set_goal_orientation_tolerance(1)
    # else:
    #     print('DEFAULT tolerances')
    #     move_group.set_goal_position_tolerance(def_pos_tol)
    #     move_group.set_goal_orientation_tolerance(def_orien_tol)
    move_to_SE3(poses[i])
    i += 1
