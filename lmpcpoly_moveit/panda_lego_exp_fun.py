#!/usr/bin/env python3
from pickle import FALSE, TRUE
import string
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
import geometry_msgs.msg
from math import pi, sin, cos
from std_msgs.msg import String
from moveit_commander.conversions import pose_to_list
from moveit_trajectory_interface.srv import *

import franka_gripper.msg
import tf


class MoveGroupPythonIntefaceTutorial(object):
    """MoveGroupPythonIntefaceTutorial"""

    def __init__(self):
        super(MoveGroupPythonIntefaceTutorial, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
        # rospy.wait_for_service('/panda/velocity_qp/getNewCartesianPath')

        ## Instantiate a `RobotCommander`_ object. This object is the outer-level interface to
        ## the robot:
        robot = moveit_commander.RobotCommander("/panda/robot_description",
                                                ns="panda")

        ## Instantiate a `PlanningSceneInterface`_ object.  This object is an interface
        ## to the world surrounding the robot:
        scene = moveit_commander.PlanningSceneInterface(ns="panda")

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to one group of joints.  In this case the group is the joints in the Panda
        ## arm so we set ``group_name = panda_arm``. If you are using a different robot,
        ## you should change this value to the name of your robot arm planning group.
        ## This interface can be used to plan and execute motions on the Panda:
        group_name = "panda_arm"
        group = moveit_commander.MoveGroupCommander(
            group_name,
            robot_description="/panda/robot_description",
            ns="panda")

        # Available planners : AnytimePathShortening, BFMT, BKPIECE, BiEST, BiTRRT, EST, FMT, KPIECE,
        # LBKPIECE, LBTRRT, LazyPRM, LazyPRMstar, PDST, PRM, PRMstar, ProjEST, RRTConnect, RRT, RRTstar,
        # SBL, SPARS, SPARStwo, STRIDE, TRRT
        group.set_planner_id("RTTstar")

        open_gripper_publisher = rospy.Publisher(
            '/panda/franka_gripper/move/goal',
            franka_gripper.msg.MoveActionGoal,
            queue_size=20)

        close_gripper_publisher = rospy.Publisher(
            '/panda/franka_gripper/grasp/goal',
            franka_gripper.msg.GraspActionGoal,
            queue_size=20)

        # We can get the name of the reference frame for this robot:
        planning_frame = group.get_planning_frame()
        print("============ Reference frame: %s" % planning_frame)

        # We can also print the name of the end-effector link for this group:
        eef_link = group.get_end_effector_link()
        print("============ End effector: %s" % eef_link)

        # We can get a list of all the groups in the robot:
        group_names = robot.get_group_names()
        print("============ Robot Groups:", robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(robot.get_current_state())
        print("")

        print("============ Printing group joint values")
        print(group.get_current_joint_values())
        print("")

        print("============ Printing group pose")
        print(group.get_current_pose())
        print("")

        print("============ Printing planner name")
        print(group.get_planner_id())
        print("")

        # Misc variables
        self.box_name = ''
        self.robot = robot
        self.scene = scene
        self.group = group

        self.planning_frame = planning_frame
        self.eef_link = eef_link
        self.group_names = group_names

        self.open_gripper_publisher = open_gripper_publisher
        self.close_gripper_publisher = close_gripper_publisher

    def add_obstacles(self):
        """
    It adds table and walls to the scene.
    """

        table_pose = geometry_msgs.msg.PoseStamped()
        table_pose.header.frame_id = "panda_link0"
        table_pose.pose.position.x = 0
        table_pose.pose.position.y = 0
        table_pose.pose.position.z = -0.01

        self.scene.add_box("table", table_pose, size=(2, 2, 0.01))

        wall1_pose = geometry_msgs.msg.PoseStamped()
        wall1_pose.header.frame_id = "panda_link0"
        wall1_pose.pose.position.x = -0.45
        wall1_pose.pose.position.y = -0.2
        wall1_pose.pose.position.z = 0.5

        self.scene.add_box("wall1", wall1_pose, size=(0.01, 1.55, 1))

        wall2_pose = geometry_msgs.msg.PoseStamped()
        wall2_pose.header.frame_id = "panda_link0"
        wall2_pose.pose.position.x = 0.225
        wall2_pose.pose.position.y = 0.4
        wall2_pose.pose.position.z = 0.5

        self.scene.add_box("wall2", wall2_pose, size=(1.6, 0.01, 1))

    def remove_obstacles(self):
        """
    > Remove the table and walls from the scene
    """

        self.scene.remove_world_object("table")
        self.scene.remove_world_object("wall1")
        self.scene.remove_world_object("wall2")

    def go_to_pose(self, x, y, z, w=-pi / 4, cartesian=FALSE):
        """
    > The function takes as input the desired position and orientation of the end-effector and moves
    the robot to that position

    :param x: The x coordinate of the pose
    :param y: The y coordinate of the pose
    :param z: The height of the end effector above the ground
    :param w: the angle of the gripper in the x-y plane
    :param cartesian: If true, the robot will move in a straight line between the current pose and the
    new pose. If false, the robot will move in a curved path
    """

        self.group.clear_pose_targets()
        pose_goal = self.group.get_current_pose().pose

        # We need to define a waypoint to define linear trajectory
        if cartesian:
            waypoint = []
            waypoint.append(copy.deepcopy(pose_goal))

        pose_goal.position.x = x
        pose_goal.position.y = y
        pose_goal.position.z = z

        # Force gripper to be parallele to floor
        quaternion = tf.transformations.quaternion_from_euler(pi, 0, w)

        pose_goal.orientation.x = quaternion[0]
        pose_goal.orientation.y = quaternion[1]
        pose_goal.orientation.z = quaternion[2]
        pose_goal.orientation.w = quaternion[3]

        # Define linear cartesian trajectory
        # ! Warning ! you can't ask for orientation with this methods
        ### When usign this method + asking the robot to not move, there is a small offset ###
        ### Check if this is also the case when you ask for a postion ###
        if cartesian:
            waypoint.append(copy.deepcopy(pose_goal))

            plan, ratio = self.group.compute_cartesian_path(
                waypoint, 0.01, 0.0)
            self.group.execute(plan)

        # Not linear trajectory
        else:
            self.group.set_pose_target(pose_goal)
            self.group.go()

    def open_gripper(self):
        """
    It publishes a message to the gripper to open it
    """

        move_cmd = franka_gripper.msg.MoveActionGoal()
        move_cmd.goal.width = 0.08
        move_cmd.goal.speed = 0.1

        self.open_gripper_publisher.publish(move_cmd)

    def close_gripper(self, f=10.0):
        """
    It publishes a message to the gripper to close it

    :param f: the force to apply to the gripper
    """

        grsp_cmd = franka_gripper.msg.GraspActionGoal()

        grsp_cmd.goal.width = 0.031
        grsp_cmd.goal.epsilon.inner = 0.005
        grsp_cmd.goal.epsilon.outer = 0.005
        grsp_cmd.goal.speed = 0.1
        grsp_cmd.goal.force = f

        self.close_gripper_publisher.publish(grsp_cmd)

        # force to wait a little bit
        rospy.sleep(1)

    def pick_2step(self, x, y, z):
        """
    > The function `pick_2step` takes as input the x, y, and z coordinates of the lego brick, and then
    moves the robot to the x, y, z coordinates of the lego brick, opens the gripper, moves the robot
    down to the lego brick, closes the gripper, and then moves the robot up a little bit.

    :param x: x coordinate of the lego
    :param y: the y coordinate of the lego
    :param z: the height of the lego
    """
        # Go on lego
        print("Go above lego")
        self.go_to_pose(x, y, z + 0.05)

        # Open gripper
        print("Open gripper")
        self.open_gripper()
        # Go down
        print("Close approach to lego")
        self.go_to_pose(x, y, z, cartesian=TRUE)

        # Close gripper
        print("Close gripper")
        self.close_gripper()

        # Go a little up
        print("Move upper")
        self.go_to_pose(x, y, z + 0.05, cartesian=TRUE)

    def place_2step(self, x, y, z):
        """
    > The function `place_2step` takes as input the x, y, and z coordinates of the place location, and
    then moves the robot to the place location in two steps: first, it moves the robot to a point
    above the place location, then it moves the robot to the place location and opens the gripper, and
    finally it moves the robot back to the point above the place location

    :param x: x coordinate of the place location
    :param y: the y coordinate of the place location
    :param z: the height of the lego piece
    """

        print("Go above place")
        self.go_to_posel(x, y, z + 0.05)

        print("Plug lego")
        self.go_to_pose(x, y, z - 0.01, cartesian=TRUE)

        print("Open gripper")
        self.open_gripper()

        print("Go above place")
        self.go_to_pose(x, y, z + 0.05, cartesian=TRUE)


# mg = MoveGroupPythonIntefaceTutorial()
