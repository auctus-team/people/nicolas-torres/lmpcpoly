/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2012, Willow Garage, Inc.
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of Willow Garage nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

#include "moveit_trajectory_interface/moveit_trajectory.hpp"

#include "pinocchio/algorithm/aba.hpp"
#include "pinocchio/algorithm/frames.hpp"
#include "pinocchio/algorithm/jacobian.hpp"
#include "pinocchio/algorithm/joint-configuration.hpp"
#include "pinocchio/algorithm/kinematics.hpp"
#include "pinocchio/algorithm/model.hpp"
#include "pinocchio/algorithm/rnea.hpp"
#include "pinocchio/parsers/urdf.hpp"
#include <eigen3/Eigen/src/Geometry/Transform.h>
#include <eigen_conversions/eigen_msg.h>
// #include "pinocchio/fwd.hpp"
// #include "pinocchio/multibody/model.hpp"
//

// polytopes
#include <geometry_msgs/Point32.h>
#include <geometry_msgs/PolygonStamped.h>

#include <cmath>
#include <iostream>
#include <ros/ros.h> // ros::param, etc..

// #include <cstddef>
// #include <fstream>
// #include <iomanip>
#include <lmpcpoly/methods.hpp>
#include <lmpcpoly/polycap_extended.hpp>
#include <ros/console.h>
#include <sstream>
#include <vector>

#define NODE_NAME "/lmpcpoly_moveit"
#define REALTIME
#ifndef REALTIME
#define publock(publisher, instructions)                                       \
  {                                                                            \
    publisher.lock();                                                          \
    instructions;                                                              \
    publisher.unlockAndPublish();                                              \
  }
#else
#define publock(publisher, instructions)                                       \
  if (publisher.trylock()) {                                                   \
    instructions;                                                              \
    publisher.unlockAndPublish();                                              \
  }
#endif

constexpr double pi = 3.14159265358979323846;
// constexpr double delta_compute_time = 0.015; // [s]
constexpr double delta_compute_time = 0.0015; // [s]

struct WarnTock {

  WarnTock(std::string name, double delay_s)
      : name(name), delay_s(delay_s), start(lmpcpoly::precise_now()) {}

  ~WarnTock() {
    double total = lmpcpoly::tock_ms(start) / 1000;
    if (total > delay_s) {
      ROS_ERROR_STREAM_THROTTLE(0.1, name << " time excedded (" << total
                                          << "s > " << delay_s << "s)");
    }
  }

  std::string name;
  double delay_s;
  std::chrono::time_point<std::chrono::steady_clock> start;
};

template <typename T>
bool is_in_vector(const std::vector<T> &vector, const T &elt) {
  return vector.end() != std::find(vector.begin(), vector.end(), elt);
}

bool getRosParamEigen(const std::string &param_name,
                      Eigen::VectorXd &param_data, bool verbose = false) {
  std::vector<double> std_param;
  if (!ros::param::get(param_name, std_param)) {
    return false;
  }

  if (std_param.size() == (long unsigned int)param_data.size()) {
    for (int i = 0; i < param_data.size(); ++i)
      param_data(i) = std_param[i];
    if (verbose)
      ROS_INFO_STREAM(param_name << " : " << param_data.transpose());
    return true;
  }

  ROS_FATAL_STREAM("Wrong matrix size for param "
                   << param_name
                   << ". Check the Yaml config file. Killing ros");
  ros::shutdown();
  return false; // shouldn't ever reach this but avoids -Wreturn-type
}

void MoveItTrajectory::publish_polytope(
    realtime_tools::RealtimePublisher<sensor_msgs::PointCloud>
        &vertices_publisher,
    realtime_tools::RealtimePublisher<jsk_recognition_msgs::PolygonArray>
        &polygon_publisher,
    const Eigen::MatrixXd A, const lmpcpoly::VectorLimits qd,
    const std::string frame_id, const Eigen::Vector3d position,
    const double factor) {

  WarnTock temp("publish_polytope()", 0.0001);
  // lmpcpoly::print_mat("A", A);
  // lmpcpoly::print_mat("qd.min", qd.min);
  // lmpcpoly::print_mat("qd.max", qd.max);
  Eigen::MatrixXd extremes =
      lmpcpoly::vertices_from_lmi(A.topRows(3), qd.min, qd.max);
  lmpcpoly::TrianglePolygon tripol =
      lmpcpoly::point_cloud_to_triangle_polygon(extremes);

  auto rosnow = ros::Time::now();

  // build & publish vertices message
  publock(
      vertices_publisher,
      sensor_msgs::PointCloud &vert_msg = vertices_publisher.msg_;
      vert_msg.header.stamp = rosnow; vert_msg.header.frame_id = frame_id;
      vert_msg.points.resize(tripol.vertices.cols());
      for (unsigned int coli = 0; coli < tripol.vertices.cols(); ++coli) {
        geometry_msgs::Point32 point;
        point.x = factor * tripol.vertices(0, coli) + position(0);
        point.y = factor * tripol.vertices(1, coli) + position(1);
        point.z = factor * tripol.vertices(2, coli) + position(2);
        vert_msg.points[coli] = point;
      });

  // build & publish polygon message

  publock(
      polygon_publisher,
      jsk_recognition_msgs::PolygonArray &polarray_msg = polygon_publisher.msg_;
      polarray_msg.header.stamp = rosnow;
      polarray_msg.header.frame_id = frame_id;
      polarray_msg.polygons.resize(tripol.faces.size());
      std::vector<double> likelihoods; likelihoods.push_back(0.1);
      likelihoods.push_back(0.5); likelihoods.push_back(1);
      for (const auto &likelihood
           : likelihoods) {
        for (long unsigned int facei = 0; facei < tripol.faces.size();
             ++facei) {
          auto const &pol_mat = tripol.faces[facei];
          geometry_msgs::PolygonStamped polstamped_msg;
          polstamped_msg.header.stamp = rosnow;
          polstamped_msg.header.frame_id = frame_id;
          for (unsigned int coli = 0; coli < 3; ++coli) {
            geometry_msgs::Point32 point;
            point.x = factor * pol_mat(0, coli) / likelihood + position(0);
            point.y = factor * pol_mat(1, coli) / likelihood + position(1);
            point.z = factor * pol_mat(2, coli) / likelihood + position(2);
            polstamped_msg.polygon.points.push_back(point);
          }
          polarray_msg.likelihood.push_back(likelihood);
          polarray_msg.polygons[facei] = polstamped_msg;
        }
      });
}

MoveItTrajectory::MoveItTrajectory()
    : trajectory_is_built{false}, follow_flag{false},
      horizon_publish_delay(0.1),  //
      polytope_publish_delay(0.2), //
      H(6), ndt(40),               //
      follow_update_delay(0.01)    //
{}

bool MoveItTrajectory::toggle_follow_flag(
    moveit_trajectory_interface::MPCFollow::Request &req
    __attribute__((unused)),
    moveit_trajectory_interface::MPCFollow::Response &res
    __attribute__((unused))) {
  if (follow_flag.load()) {
    trajectory_is_built.store(false);
    follow_flag.store(false);
    ROS_WARN_STREAM("turn OFF following");
    follow_subscriber.shutdown();
    update_initial_pose();
  } else {
    target_pose = current_pose;
    follow_flag.store(true);
    trajectory_is_built.store(true);
    ROS_WARN_STREAM("turn ON following");

    ros::NodeHandle node_handle(NODE_NAME);
    follow_subscriber = node_handle.subscribe(
        req.topic, 1000, &MoveItTrajectory::follow_target_pose, this);
  }
  return true;
}

bool MoveItTrajectory::configure_mpc(
    moveit_trajectory_interface::MPCConfigure::Request &req,
    moveit_trajectory_interface::MPCConfigure::Response &res
    __attribute__((unused))) {

  follow_update_delay.delay_s = req.marker_update_delay;

  Eigen::Vector3d vmax_linear =
      req.max_velocity_linear * Eigen::Vector3d::Ones();
  Eigen::Vector3d amax_linear =
      req.max_acceleration_linear * Eigen::Vector3d::Ones();
  Eigen::Vector3d jmax_linear = req.max_jerk_linear * Eigen::Vector3d::Ones();
  Eigen::VectorXd vmax_tan(6);
  vmax_tan << vmax_linear, req.max_velocity_angular * Eigen::Vector3d::Ones();
  Eigen::VectorXd amax_tan(6);
  amax_tan << amax_linear,
      req.max_acceleration_angular * Eigen::Vector3d::Ones();
  Eigen::VectorXd jmax_tan(6);
  jmax_tan << jmax_linear, req.max_jerk_angular * Eigen::Vector3d::Ones();

  ROS_WARN_STREAM("configure_mpc()::vmax:" << vmax_tan.transpose());
  ROS_WARN_STREAM("configure_mpc()::amax:" << amax_tan.transpose());
  ROS_WARN_STREAM("configure_mpc()::jmax:" << jmax_tan.transpose());

  mpcprob->con_v.update_box_max(vmax_linear);
  mpcprob->con_a.update_box_max(amax_linear);
  mpcprob->con_j.update_box_max(jmax_linear);

  mpcprobtang->con_v.update_box_max(vmax_tan);
  mpcprobtang->con_a.update_box_max(amax_tan);
  mpcprobtang->con_j.update_box_max(jmax_tan);

  // TODO add update wreg and wterminal for mpcprob
  mpc_solveparams.w_a = req.mpc_w_reg;
  mpc_solveparams.w_terminal = req.mpc_w_terminal;
  mpcprobtang->update_wreg(mpc_solveparams.w_a);
  mpcprobtang->update_wterminal(mpc_solveparams.w_terminal);
  Emax = req.Emax;
  tangent_space = req.tangent_mode;
  limit_jerk = req.limit_jerk_horizon;
  polytope_with_bias = req.polytope_with_bias;
  constraints_mode = req.constraints_mode;
  tau_factor = req.tau_factor;
  qd_factor = req.qd_factor;
  qdd_factor = req.qdd_factor;
  qddd_factor = req.qddd_factor;

  kp_linear = req.kp_linear;
  kp_angular = req.kp_angular;
  kd_factor_linear = req.kd_factor_linear;
  kd_factor_angular = req.kd_factor_angular;
  ff_acc_factor_ = req.ff_acc_factor;

  ROS_WARN_STREAM("configure_mpc()::w_a:" << req.mpc_w_reg);
  ROS_WARN_STREAM("configure_mpc()::w_terminal:" << req.mpc_w_terminal);
  ROS_WARN_STREAM("configure_mpc()::kp_linear:" << req.kp_linear);
  ROS_WARN_STREAM("configure_mpc()::kp_angular:" << req.kp_angular);
  ROS_WARN_STREAM("configure_mpc()::kd_factor_linear:" << req.kd_factor_linear);
  ROS_WARN_STREAM(
      "configure_mpc()::kd_factor_angular:" << req.kd_factor_angular);
  ROS_WARN_STREAM("configure_mpc()::ff_acc_factor:" << req.ff_acc_factor);
  ROS_WARN_STREAM("configure_mpc()::tau_factor:" << req.tau_factor);
  ROS_WARN_STREAM("configure_mpc()::qd_factor:" << req.qd_factor);
  ROS_WARN_STREAM("configure_mpc()::qdd_factor:" << req.qdd_factor);
  ROS_WARN_STREAM("configure_mpc()::qddd_factor:" << req.qddd_factor);
  ROS_WARN_STREAM("configure_mpc()::constraints_mode:" << req.constraints_mode);
  ROS_WARN_STREAM("configure_mpc()::tangent_mode:" << (int)req.tangent_mode);
  ROS_WARN_STREAM(
      "configure_mpc()::limit_jerk_horizon:" << (int)req.limit_jerk_horizon);
  ROS_WARN_STREAM(
      "configure_mpc()::polytope_with_bias:" << (int)req.polytope_with_bias);

  return true;
}

bool MoveItTrajectory::get_configure_mpc(
    moveit_trajectory_interface::MPCConfigure::Request &req
    __attribute__((unused)),
    moveit_trajectory_interface::MPCConfigure::Response &res) {

  res.marker_update_delay = follow_update_delay.delay_s;

  if (tangent_space) {
    res.max_velocity_linear = mpcprobtang->con_v.firststep_box_max()(0);
    res.max_acceleration_linear = mpcprobtang->con_a.firststep_box_max()(0);
    res.max_jerk_linear = mpcprobtang->con_j.firststep_box_max()(0);
  } else {
    res.max_velocity_linear = mpcprob->con_v.firststep_box_max()(0);
    res.max_acceleration_linear = mpcprob->con_a.firststep_box_max()(0);
    res.max_jerk_linear = mpcprob->con_j.firststep_box_max()(0);
  }

  res.max_velocity_angular = mpcprobtang->con_v.firststep_box_max()(3);
  res.max_acceleration_angular = mpcprobtang->con_a.firststep_box_max()(3);
  res.max_jerk_angular = mpcprobtang->con_j.firststep_box_max()(3);

  res.mpc_w_reg = mpc_solveparams.w_a;
  res.mpc_w_terminal = mpc_solveparams.w_terminal;

  res.kp_linear = kp_linear;
  res.kp_angular = kp_angular;

  res.kd_factor_linear = kd_factor_linear;
  res.kd_factor_angular = kd_factor_angular;
  res.ff_acc_factor = ff_acc_factor_;

  res.Emax = Emax;
  res.constraints_mode = constraints_mode;
  res.tangent_mode = tangent_space;
  res.limit_jerk_horizon = limit_jerk;
  res.polytope_with_bias = polytope_with_bias;
  res.tau_factor = tau_factor;
  res.qd_factor = qd_factor;
  res.qdd_factor = qdd_factor;
  res.qddd_factor = qddd_factor;

  return true;
}

Eigen::VectorXd MoveItTrajectory::gains() {
  Eigen::VectorXd vec(6);
  vec << kp_linear * Eigen::Vector3d::Ones(),
      kp_angular * Eigen::Vector3d::Ones();
  return vec;
}

Eigen::VectorXd MoveItTrajectory::kd_factors() {
  Eigen::VectorXd vec(6);
  vec << kd_factor_linear * Eigen::Vector3d::Ones(),
      kd_factor_angular * Eigen::Vector3d::Ones();
  return vec;
}

double MoveItTrajectory::ff_acc_factor() { return ff_acc_factor_; }

void MoveItTrajectory::follow_target_pose(
    const geometry_msgs::PoseStamped &posestamped) {

  if (follow_update_delay.passed()) {
    Eigen::Isometry3d eig;
    tf::poseMsgToEigen(posestamped.pose, eig);
    // target_pose = pinocchio::SE3(eig.rotation(), eig.translation());
    follow_pose = pinocchio::SE3(eig.matrix());

    if (follow_flag.load()) {
      if (verbose) {
        ROS_WARN_STREAM_THROTTLE(0.5, "MoveItTrajectory::follow_target_pose()");
        ROS_WARN_STREAM_THROTTLE(
            0.5, lmpcpoly::str_mat("cube p", eig.translation()));
        ROS_WARN_STREAM_THROTTLE(0.5, "update Pose");
      }
      target_pose = follow_pose;
    }
    follow_update_delay.refresh();
  }
}
void MoveItTrajectory::update_initial_pose() {
  initial_pose = current_pose;
  auto rosnow = ros::Time::now();
  geometry_msgs::Pose xmsg;
  Eigen::Affine3d x;
  x.matrix() = initial_pose.toHomogeneousMatrix();
  tf::poseEigenToMsg(x, xmsg);
  geometry_msgs::PoseStamped msg;
  msg.header.stamp = rosnow;
  msg.header.frame_id = "world";
  msg.pose = xmsg;
  initial_pose_publisher.publish(msg);
}
void MoveItTrajectory::init(std::string planning_group,
                            std::string robot_description_param_name,
                            Eigen::VectorXd q_init,
                            std::vector<std::string> joint_names) {
  if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME,
                                     ros::console::levels::Info)) {
    ros::console::notifyLoggerLevelsChanged();
  }

  this->planning_group = planning_group;
  this->robot_description_param_name = robot_description_param_name;
  // ROS_WARN("Received new trajectory");
  // The :planning_interface:`MoveGroupInterface` class can be easily
  // setup using just the name of the planning group you would like to control
  // and plan for.
  std::string robot_description;
  ros::param::get(robot_description_param_name, robot_description);
  load_robot_model(robot_description, joint_names);
  // ros::NodeHandle node_handle("/" + model.name);
  ros::NodeHandle node_handle(NODE_NAME);
  current_q = q_init;
  current_qd = Eigen::VectorXd::Zero(q_init.rows());
  current_qdd = Eigen::VectorXd::Zero(q_init.rows());

  if (this->controlled_frame.empty()) {
    controlled_frame = model.frames[model.nframes - 1].name;
  }

  // compute neutral pose
  Eigen::VectorXd q_mid =
      model.lowerPositionLimit +
      (model.upperPositionLimit - model.lowerPositionLimit) / 2;
  pinocchio::forwardKinematics(model, data, q_mid);
  pinocchio::updateFramePlacements(model, data);
  pinocchio::SE3 home_pose = data.oMf[model.getFrameId(controlled_frame)];

  // compute initial pose
  pinocchio::forwardKinematics(model, data, q_init);
  pinocchio::updateFramePlacements(model, data);

  Jlocal.resize(6, model.nv);
  Jlocal.setZero();
  pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame),
                              pinocchio::ReferenceFrame::LOCAL, Jlocal);
  Jlwa.resize(6, model.nv);
  Jlwa.setZero();
  pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame),
                              pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED,
                              Jlwa);

  pinocchio::computeMinverse(model, data, current_q);
  data.Minv.triangularView<Eigen::StrictlyLower>() =
      data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
  Minv = data.Minv;
  JMinvlocal = Jlocal * Minv;
  JMinvlwa = Jlwa * Minv;
  g = pinocchio::computeGeneralizedGravity(model, data, current_q);

  // --------------------------------------------------------------
  // Eigen::VectorXd b =
  //     pinocchio::nonLinearEffects(model, data, current_q, current_qd);
  // Eigen::VectorXd tau_min = -tau_factor * model.effortLimit - (b - g);
  // Eigen::VectorXd tau_max = tau_factor * model.effortLimit - (b - g);
  // --------------------------------------------------------------
  Eigen::VectorXd tau_min = -tau_factor * model.effortLimit;
  Eigen::VectorXd tau_max = tau_factor * model.effortLimit;
  // --------------------------------------------------------------
  // tau_lims = lmpcpoly::VectorLimits{tau_min.cwiseMin(tau_max),
  //                                   tau_min.cwiseMax(tau_max)};
  qdd_max.resize(7);
  qdd_max << 15, 7.5, 10, 12.5, 15, 20, 20;
  qddd_max.resize(7);
  qddd_max << 7500, 3750, 5000, 6250, 7500, 10000, 10000;

  qdd_lims =
      lmpcpoly::VectorLimits{-qdd_factor * qdd_max, qdd_factor * qdd_max};
  pol_qdd_pos.update(Jlwa.topRows(3), qdd_lims);
  pol_qdd_tan.update(Jlocal, qdd_lims);

  qddd_lims =
      lmpcpoly::VectorLimits{-qddd_factor * qddd_max, qddd_factor * qddd_max};
  pol_qddd_pos.update(Jlwa.topRows(3), qddd_lims);
  pol_qddd_tan.update(Jlocal, qddd_lims);

  Eigen::VectorXd qdd0_max(current_qdd + qddd_lims.max * _control_period);
  Eigen::VectorXd qdd0_min(current_qdd + qddd_lims.min * _control_period);
  qdd_lims_firststep = lmpcpoly::VectorLimits{qdd_lims.min.cwiseMax(qdd0_min),
                                              qdd_lims.max.cwiseMin(qdd0_max)};
  pol_qdd_firststep_pos.update(Jlwa.topRows(3), qdd_lims_firststep);
  pol_qdd_firststep_tan.update(Jlocal, qdd_lims_firststep);

  // --------------------------------------------------------------
  Eigen::VectorXd qd_min = -qd_factor * model.velocityLimit;
  Eigen::VectorXd qd_max = qd_factor * model.velocityLimit;
  // Eigen::VectorXd qd_min =
  //     (-qd_factor * model.velocityLimit - current_qd) / (ndt * 1e-3);
  // Eigen::VectorXd qd_max =
  //     (qd_factor * model.velocityLimit - current_qd) / (ndt * 1e-3);
  qd_lims =
      lmpcpoly::VectorLimits{qd_min.cwiseMin(qd_max), qd_min.cwiseMax(qd_max)};
  pol_qd_pos.update(Jlocal.topRows(3), qd_lims);
  pol_qd_tan.update(Jlocal, qd_lims);
  // --------------------------------------------------------------
  q_lims = lmpcpoly::VectorLimits{model.lowerPositionLimit - current_q,
                                  model.upperPositionLimit - current_q};
  pol_q_pos.update(Jlwa.topRows(3), q_lims);
  pol_q_tan.update(Jlocal, q_lims);
  // --------------------------------------------------------------

  target_pose = data.oMf[model.getFrameId(controlled_frame)];
  follow_pose = target_pose;
  current_pose = data.oMf[model.getFrameId(controlled_frame)];
  desired_pose.matrix() = current_pose.toHomogeneousMatrix();

  desired_velocity = Eigen::MatrixXd::Zero(6, 1);
  desired_acceleration = Eigen::MatrixXd::Zero(6, 1);
  desired_jerk = Eigen::MatrixXd::Zero(6, 1);

  joint_configuration.resize(model.nv);
  joint_velocity.resize(model.nv);
  joint_acceleration.resize(model.nv);

  // PUBLISHERS
  acc_polygon_publisher.init(node_handle, "polytope_acceleration_polygon", 1);
  acc_vertices_publisher.init(node_handle, "polytope_acceleration_vertices", 1);
  mpctime_publisher.init(node_handle, "mpc_compute_time", 1);
  kinenergy_publisher.init(node_handle, "kinetic_energy", 1);
  desired_kinenergy_publisher.init(node_handle, "desired_kinetic_energy", 1);
  kinenergyd_publisher.init(node_handle, "kinetic_energyd", 1);
  horizon_publisher.init(node_handle, "horizon", 1);
  jointspace_limit_distance_publisher.init(node_handle,
                                           "jointspace_limit_distance", 1);

  bias_acceleration_publisher.init(node_handle, "bias_acceleration", 1);
  bias_jerk_publisher.init(node_handle, "bias_jerk", 1);

  desired_error_publisher.init(node_handle, "desired_error", 1);
  target_error_publisher.init(node_handle, "target_error", 1);
  current_velocity_publisher.init(node_handle, "current_velocity", 1);
  desired_velocity_publisher.init(node_handle, "desired_velocity", 1);
  desired_velocity_arrow_publisher.init(node_handle, "desired_velocity_arrow",
                                        1);
  velocity_limits_publisher.init(node_handle, "velocity_limits", 1);
  acceleration_limits_publisher.init(node_handle, "acceleration_limits", 1);
  current_acceleration_publisher.init(node_handle, "current_acceleration", 1);
  desired_acceleration_publisher.init(node_handle, "desired_acceleration", 1);
  current_jerk_publisher.init(node_handle, "current_jerk", 1);
  desired_jerk_publisher.init(node_handle, "desired_jerk", 1);

  initial_pose_publisher = node_handle.advertise<geometry_msgs::PoseStamped>(
      "initial_pose", 1, true);
  update_initial_pose();

  home_pose_publisher =
      node_handle.advertise<geometry_msgs::PoseStamped>("home_pose", 1, true);
  auto rosnow = ros::Time::now();
  {
    geometry_msgs::Pose xmsg;
    Eigen::Affine3d x;
    x.matrix() = home_pose.toHomogeneousMatrix();
    tf::poseEigenToMsg(x, xmsg);
    geometry_msgs::PoseStamped msg;
    msg.header.stamp = rosnow;
    msg.header.frame_id = "world";
    msg.pose = xmsg;
    home_pose_publisher.publish(msg);
  }

  current_pose_publisher.init(node_handle, "current_pose", 1);
  target_pose_publisher.init(node_handle, "target_pose", 1);
  desired_pose_publisher.init(node_handle, "desired_pose", 1);

  const double eps = 1e-3;
  Eigen::VectorXd ux = Eigen::VectorXd::Zero(3);
  ux << 1, 0, 0;
  Eigen::VectorXd uy = Eigen::VectorXd::Zero(3);
  uy << 0, 1, 0;
  Eigen::VectorXd uz = Eigen::VectorXd::Zero(3);
  uz << 0, 0, 1;
  epsR = Eigen::AngleAxisd(eps, uz).matrix() *
         Eigen::AngleAxisd(eps, uy).matrix() *
         Eigen::AngleAxisd(eps, ux).matrix();

  std::string param_name;
  const std::string root_name = "/" + model.name + NODE_NAME;
  auto param_path = [&root_name](const std::string &name) {
    return root_name + "/" + name;
  };

  ROS_WARN_STREAM("controlled_frame:" << controlled_frame);

  // *******************************************
  param_name = "H";
  double Htemp;
  if (node_handle.getParam(param_path(param_name), Htemp)) {
    H = static_cast<unsigned int>(Htemp);
    ROS_WARN_STREAM("Load " << param_name << ":" << H << " steps");
  }

  std::string solver = "qpoases";
  param_name = "solver";
  if (node_handle.getParam(param_path(param_name), solver)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << solver);
  }

  double ndt_temp;
  param_name = "ndt";
  if (node_handle.getParam(param_path(param_name), ndt_temp)) {
    ndt = static_cast<unsigned int>(ndt_temp);
    ROS_WARN_STREAM("Load " << param_name << ":" << ndt
                            << " (step inside horizon hdt = ndt*dt)");
  }

  auto load_positive_double_rosparam = [&node_handle, &param_path](
                                           std::string param_name, double &var,
                                           std::string units = "") {
    if (node_handle.getParam(param_path(param_name), var)) {
      if (var <= 0) {
        ROS_FATAL_STREAM(param_name << "parameter must be higher than 0, value:"
                                    << var);
      }
      ROS_WARN_STREAM("Load " << param_name << ": " << var);
    } else {
      ROS_WARN_STREAM("Could not find parameter "
                      << param_name << ", will use default values: " << var
                      << units);
    }
  };
  load_positive_double_rosparam("Emax", Emax);
  load_positive_double_rosparam("tau_factor", tau_factor);
  load_positive_double_rosparam("mpc_wreg", mpc_solveparams.w_a);
  load_positive_double_rosparam("mpc_terminal", mpc_solveparams.w_terminal);
  load_positive_double_rosparam("mpc_compute_time", mpc_compute_time, "s");
  load_positive_double_rosparam("mpc_compute_time_warn", mpc_compute_time_warn,
                                "s");

  load_positive_double_rosparam("kp_linear", kp_linear);
  load_positive_double_rosparam("kp_angular", kp_angular);
  load_positive_double_rosparam("kd_factor_linear", kd_factor_linear);
  load_positive_double_rosparam("kd_factor_angular", kd_factor_angular);
  load_positive_double_rosparam("ff_acc_factor", ff_acc_factor_);

  param_name = "limit_jerk";
  if (node_handle.getParam(param_path(param_name), limit_jerk)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << limit_jerk);
  }

  param_name = "tangent_space";
  bool temp_tangent = tangent_space;
  if (node_handle.getParam(param_path(param_name), temp_tangent)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << temp_tangent);
  }
  tangent_space = temp_tangent;

  param_name = "verbose";
  if (node_handle.getParam(param_path(param_name), verbose)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << verbose);
  }

  mpcprob.reset(new lmpcpoly::MPCAcc(H, ndt * _control_period, mpc_solveparams,
                                     solver, mpc_compute_time, limit_jerk));
  mpcprob->verbose = verbose;

  mpcprobtang.reset(new lmpcpoly::MPCAccTangent(H, ndt * _control_period,
                                                mpc_solveparams, solver,
                                                mpc_compute_time, limit_jerk));
  mpcprobtang->verbose = verbose;

  // TODO
  // ROS_WARN_STREAM("mpc compute time:" << mpcprob->compute_time_limit() <<
  // "s");
  ROS_WARN_STREAM("mpc horizon duration:" << mpcprob->horizon.duration()
                                          << "s");

  // *******************************************

  param_name = "filter_w0";
  if (node_handle.getParam(param_path(param_name), filter_w0)) {
    ROS_WARN_STREAM("Load " << param_name << ":" << filter_w0 << "Hz");
  }
  ROS_WARN_STREAM("filter cutoff freq (" << param_name << "):" << filter_w0
                                         << "Hz");
  if (filter_w0 < 0) {
    ROS_WARN_STREAM("filter_w0<0, disabling filter");
  }

  Eigen::VectorXd p_max = Eigen::VectorXd::Ones(6);
  Eigen::VectorXd v_max = Eigen::VectorXd::Ones(6);
  Eigen::VectorXd a_max = Eigen::VectorXd::Ones(6);
  Eigen::VectorXd j_max = Eigen::VectorXd::Ones(6);

  auto load_vec_rosparam = [&param_path](std::string param_name,
                                         Eigen::VectorXd &vec) {
    if (!getRosParamEigen(param_path(param_name), vec)) {
      ROS_WARN_STREAM("Could not find parameter "
                      << param_name << ", will use default values: "
                      << lmpcpoly::str_mat(param_name, vec));
    }
    ROS_WARN_STREAM("Load " << param_name << ": " << vec.transpose());
  };

  load_vec_rosparam("p_max", p_max);
  load_vec_rosparam("v_max", v_max);
  load_vec_rosparam("a_max", a_max);
  load_vec_rosparam("j_max", j_max);

  const auto &pmax_linear = p_max.segment<3>(0);
  const auto &vmax_linear = v_max.segment<3>(0);
  const auto &amax_linear = a_max.segment<3>(0);
  const auto &jmax_linear = j_max.segment<3>(0);
  mpcprob->con_p.update_box_max(pmax_linear);
  mpcprob->con_v.update_box_max(vmax_linear);
  mpcprob->con_a.update_box_max(amax_linear);
  mpcprob->con_j.update_box_max(jmax_linear);

  mpcprobtang->con_p.update_box_max(p_max);
  mpcprobtang->con_v.update_box_max(v_max);
  mpcprobtang->con_a.update_box_max(a_max);
  mpcprobtang->con_j.update_box_max(j_max);

  // fake velocity scaling parameters
  auto load_double_rosparam =
      [&param_path, &node_handle](std::string param_name, double &dou) {
        if (!node_handle.getParam(param_path(param_name), dou)) {
          ROS_WARN_STREAM("Could not find parameter "
                          << param_name << ", will use default value: " << dou);
        }
        ROS_WARN_STREAM("Load " << param_name << ": " << dou);
      };

  load_double_rosparam("vel_scaling_max", velscaling.max);
  load_double_rosparam("vel_scaling_min", velscaling.min);
  load_double_rosparam("vel_scaling_step", velscaling.step);
  load_double_rosparam("vel_scaling_delay", velscaling.delay.delay_s);

  // SERVICES
  follow_service = node_handle.advertiseService(
      "/start_mpc", &MoveItTrajectory::toggle_follow_flag, this);
  configure_mpc_service = node_handle.advertiseService(
      "/configure_mpc", &MoveItTrajectory::configure_mpc, this);
  get_configure_mpc_service = node_handle.advertiseService(
      "/get_configure_mpc", &MoveItTrajectory::get_configure_mpc, this);

  is_initialized = true;
}

bool MoveItTrajectory::isInitialized() { return is_initialized; }

std::string MoveItTrajectory::getRobotName() { return model.name; }

bool MoveItTrajectory::load_robot_model(std::string robot_description,
                                        std::vector<std::string> joint_names) {
  pinocchio::Model temp_model;
  pinocchio::urdf::buildModelFromXML(robot_description, temp_model, false);
  if (!joint_names.empty()) {
    std::vector<pinocchio::JointIndex> list_of_joints_to_keep_unlocked_by_id;
    for (std::vector<std::string>::const_iterator it = joint_names.begin();
         it != joint_names.end(); ++it) {
      const std::string &joint_name = *it;
      if (temp_model.existJointName(joint_name))
        list_of_joints_to_keep_unlocked_by_id.push_back(
            temp_model.getJointId(joint_name));
    }

    // Transform the list into a list of joints to lock
    std::vector<pinocchio::JointIndex> list_of_joints_to_lock_by_id;
    for (pinocchio::JointIndex joint_id = 1;
         joint_id < temp_model.joints.size(); ++joint_id) {
      const std::string joint_name = temp_model.names[joint_id];
      if (is_in_vector(joint_names, joint_name))
        continue;
      else {
        list_of_joints_to_lock_by_id.push_back(joint_id);
      }
    }

    // Build the reduced temp_model from the list of lock joints
    Eigen::VectorXd q_rand = pinocchio::randomConfiguration(temp_model);
    model = pinocchio::buildReducedModel(temp_model,
                                         list_of_joints_to_lock_by_id, q_rand);
  }
  // TODO: so joint_names can in fact be empty or this should be a critical
  // error?
  data = pinocchio::Data(model);

  return true;
}

void error_stream(std::string msg) { ROS_ERROR_STREAM(msg); }

void MoveItTrajectory::updateTrajectory(const Eigen::VectorXd &q,
                                        const Eigen::VectorXd &qd,
                                        const Eigen::VectorXd &qdd) {
  WarnTock temp2("MoveItTrajectory::updateTrajectory", 1);
  std::chrono::time_point<std::chrono::steady_clock> start_updatetrajectory =
      lmpcpoly::precise_now();

  if (velscaling.scale()) {
    if (verbose) {
      ROS_WARN_STREAM_THROTTLE(0.5, "vel scale:" << velscaling.current);
    }
    mpcprobtang->con_v.update_box_max(velscaling.current *
                                      mpcprobtang->con_v.firststep_box_max());
    mpcprob->con_v.update_box_max(velscaling.current *
                                  mpcprob->con_v.firststep_box_max());
  }

  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    current_q = q;
    current_qd = qd;
    current_qdd = qdd;

    pinocchio::computeJointJacobians(
        model, data, current_q); // also calls forwardKinematics?
    pinocchio::framesForwardKinematics(
        model, data, current_q); // also calls forwardKinematics?
    pinocchio::computeJointJacobiansTimeVariation(model, data, current_q,
                                                  current_qd);
    // pinocchio::computeJointKinematicHessians(model, data, current_q);

    // pinocchio::forwardKinematics(model, data, current_q, current_qd,
    //                              current_qdd);
    // pinocchio::updateFramePlacements(model, data);

    current_pose = data.oMf[model.getFrameId(controlled_frame)];

    pinocchio::ReferenceFrame frame =
        pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED;
    auto rosnow = ros::Time::now();
    std::string frame_id = "world";
    if (tangent_space) {
      frame_id = "local";
      frame = pinocchio::ReferenceFrame::LOCAL;
    }

    Jlocal.setZero();
    pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame),
                                pinocchio::ReferenceFrame::LOCAL, Jlocal);
    Jlwa.setZero();
    pinocchio::getFrameJacobian(model, data, model.getFrameId(controlled_frame),
                                pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED,
                                Jlwa);

    Eigen::MatrixXd Jd_local(Eigen::MatrixXd::Zero(6, model.nq)),
        Jd_lwa(Eigen::MatrixXd::Zero(6, model.nq));
    // Jdd(Eigen::MatrixXd::Zero(6, model.nq));
    pinocchio::getFrameJacobianTimeVariation(
        model, data, model.getFrameId(controlled_frame),
        pinocchio::ReferenceFrame::LOCAL, Jd_local);
    pinocchio::getFrameJacobianTimeVariation(
        model, data, model.getFrameId(controlled_frame),
        pinocchio::ReferenceFrame::LOCAL_WORLD_ALIGNED, Jd_lwa);
    current_bias_acceleration_local = Jd_local * current_qd;
    current_bias_acceleration_lwa = Jd_lwa * current_qd;
    // pinocchio::getFrameJacobianTimeVariation(
    //     model, data, current_q, model.getFrameId(controlled_frame),
    //     pinocchio::ReferenceFrame::LOCAL, Jd);
    // current_bias_jerk_local;
    current_bias_jerk_local = 2 * Jd_local * current_qdd;
    current_bias_jerk_lwa = 2 * Jd_lwa * current_qdd;

    pinocchio::computeMinverse(model, data, current_q);
    data.Minv.triangularView<Eigen::StrictlyLower>() =
        data.Minv.transpose().triangularView<Eigen::StrictlyLower>();
    Minv = data.Minv;
    // lmpcpoly::print_mat("Minv", Minv);
    JMinvlocal = Jlocal * Minv;
    JMinvlwa = Jlwa * Minv;

    current_OperIner = (JMinvlocal * Jlocal.transpose()).inverse();

    g = pinocchio::computeGeneralizedGravity(model, data, current_q);
    // --------------------------------------------------------------
    // Eigen::VectorXd b =
    //     pinocchio::nonLinearEffects(model, data, current_q, current_qd);
    // Eigen::VectorXd tau_min = -tau_factor * model.effortLimit - (b - g);
    // Eigen::VectorXd tau_max = tau_factor * model.effortLimit - (b - g);
    // --------------------------------------------------------------
    Eigen::VectorXd tau_min = -tau_factor * model.effortLimit;
    Eigen::VectorXd tau_max = tau_factor * model.effortLimit;
    // --------------------------------------------------------------
    // tau_lims = lmpcpoly::VectorLimits{tau_min.cwiseMin(tau_max),
    //                                   tau_min.cwiseMax(tau_max)};

    qdd_lims =
        lmpcpoly::VectorLimits{-qdd_factor * qdd_max, qdd_factor * qdd_max};
    pol_qdd_pos.update(Jlwa.topRows(3), qdd_lims);
    pol_qdd_tan.update(Jlocal, qdd_lims);

    qddd_lims =
        lmpcpoly::VectorLimits{-qddd_factor * qddd_max, qddd_factor * qddd_max};
    pol_qddd_pos.update(Jlwa.topRows(3), qddd_lims);
    pol_qddd_tan.update(Jlocal, qddd_lims);

    Eigen::VectorXd qdd0_max(current_qdd + qddd_lims.max * _control_period);
    Eigen::VectorXd qdd0_min(current_qdd + qddd_lims.min * _control_period);
    qdd_lims_firststep = lmpcpoly::VectorLimits{
        qdd_lims.min.cwiseMax(qdd0_min), qdd_lims.max.cwiseMin(qdd0_max)};

    pol_qdd_firststep_pos.update(Jlwa.topRows(3), qdd_lims_firststep);
    pol_qdd_firststep_tan.update(Jlocal, qdd_lims_firststep);

    // --------------------------------------------------------------
    Eigen::VectorXd qd_min = -qd_factor * model.velocityLimit;
    Eigen::VectorXd qd_max = qd_factor * model.velocityLimit;
    // Eigen::VectorXd qd_min =
    //     (-qd_factor * model.velocityLimit - current_qd) / (ndt * 1e-3);
    // Eigen::VectorXd qd_max =
    //     (qd_factor * model.velocityLimit - current_qd) / (ndt * 1e-3);
    qd_lims = lmpcpoly::VectorLimits{qd_min.cwiseMin(qd_max),
                                     qd_min.cwiseMax(qd_max)};
    pol_qd_pos.update(Jlwa.topRows(3), qd_lims);
    pol_qd_tan.update(Jlocal, qd_lims);

    // --------------------------------------------------------------
    q_lims = lmpcpoly::VectorLimits{model.lowerPositionLimit - current_q,
                                    model.upperPositionLimit - current_q};
    pol_q_pos.update(Jlwa.topRows(3), q_lims);
    pol_q_tan.update(Jlocal, q_lims);
    // --------------------------------------------------------------

    auto frameid = model.getFrameId(controlled_frame);
    current_twist =
        pinocchio::getFrameVelocity(model, data, frameid, frame).toVector();

    if (current_twist_acc.rows() == 0) {
      current_twist_acc = Eigen::VectorXd::Zero(6);
    }

    auto temp_acc =
        // pinocchio::getFrameAcceleration(
        pinocchio::getFrameClassicalAcceleration(model, data, frameid, frame)
            .toVector();

    current_twist_jerk = (temp_acc - current_twist_acc) / _control_period;
    current_twist_acc = temp_acc;

    // PUBLISH TWIST for VEL, ACC, JERK
    publock(current_velocity_publisher, geometry_msgs::Twist twistmsg;
            current_velocity_publisher.msg_.header.frame_id = frame_id;
            tf::twistEigenToMsg(current_twist, twistmsg);
            current_velocity_publisher.msg_.header.stamp = rosnow;
            current_velocity_publisher.msg_.twist = twistmsg;);
    publock(current_acceleration_publisher, geometry_msgs::Twist twistmsg;
            current_acceleration_publisher.msg_.header.frame_id = frame_id;
            tf::twistEigenToMsg(current_twist_acc, twistmsg);
            current_acceleration_publisher.msg_.header.stamp = rosnow;
            current_acceleration_publisher.msg_.twist = twistmsg;);
    publock(current_jerk_publisher, geometry_msgs::Twist twistmsg;
            current_jerk_publisher.msg_.header.frame_id = frame_id;
            tf::twistEigenToMsg(current_twist_jerk, twistmsg);
            current_jerk_publisher.msg_.header.stamp = rosnow;
            current_jerk_publisher.msg_.twist = twistmsg;);
  }
  current_twist =
      lptwist.filter(current_twist, filter_w0, mpcprob->T); // low-pass filter

  pinocchio::SE3 temp_des_pose(initial_pose);
  pinocchio::Motion temp_desired_twist(pinocchio::Motion::Zero());
  pinocchio::Motion temp_desired_twistacc(pinocchio::Motion::Zero());
  pinocchio::Motion temp_desired_twistjerk(pinocchio::Motion::Zero());
  if (trajectory_is_built.load()) {
    if (verbose) {
      ROS_WARN_STREAM("****************************");
      auto t_horizon = mpcprob->horizon.t_horizon(t_traj);

      ROS_WARN_STREAM("RECOMPUTE HORIZON: t_horizon:"
                      << t_horizon << "s  t_traj:" << t_traj);
      ROS_WARN_STREAM("compute_time_limit:" << mpcprob->compute_time_limit_s
                                            << "s  delta_compute_time:"
                                            << delta_compute_time << "s");
    }

    if (tangent_space) {

      {
        auto rosnow = ros::Time::now();
        publock(bias_acceleration_publisher, geometry_msgs::Twist twistmsg;
                bias_acceleration_publisher.msg_.header.frame_id = "local";
                tf::twistEigenToMsg(current_bias_acceleration_local, twistmsg);
                bias_acceleration_publisher.msg_.header.stamp = rosnow;
                bias_acceleration_publisher.msg_.twist = twistmsg;);
        publock(bias_jerk_publisher, geometry_msgs::Twist twistmsg;
                bias_jerk_publisher.msg_.header.frame_id = "local";
                tf::twistEigenToMsg(current_bias_jerk_local, twistmsg);
                bias_jerk_publisher.msg_.header.stamp = rosnow;
                bias_jerk_publisher.msg_.twist = twistmsg;);
      }

      lmpcpoly::InitialConditionsTangent initial;
      initial.t = t_traj;
      initial.pose0 = current_pose;
      initial.v0 = pinocchio::Motion(current_twist);
      initial.a0 = pinocchio::Motion(current_twist_acc);
      if (verbose) {
        ROS_WARN_STREAM(lmpcpoly::str_mat("initial.v0", initial.v0).c_str());
        ROS_WARN_STREAM(lmpcpoly::str_mat("initial.a0", initial.a0).c_str());
      }

      {
        WarnTock temp("mpcprobtang->solve", mpc_compute_time_warn);

        // mpcprobtang->con_p.update_mode(constraints_mode);
        // mpcprobtang->con_p.update_firststep_polytope(pol_q_tan.polytope_lmi());
        // mpcprobtang->con_p.update_horizon_polytope(pol_q_tan.polytope_lmi());

        mpcprobtang->con_v.update_mode(constraints_mode);
        mpcprobtang->con_v.update_firststep_polytope(pol_qd_tan.polytope_lmi());
        mpcprobtang->con_v.update_horizon_polytope(pol_qd_tan.polytope_lmi());

        mpcprobtang->con_a.update_mode(constraints_mode);
        Eigen::VectorXd a0_max(initial.a0 +
                               mpcprobtang->con_j.firststep_box_max() *
                                   _control_period);
        Eigen::VectorXd a0_min(initial.a0 -
                               mpcprobtang->con_j.firststep_box_max() *
                                   _control_period);
        mpcprobtang->con_a.update_firststep_box_max(a0_min, a0_max);
        mpcprobtang->con_a.update_firststep_polytope(
            pol_qdd_firststep_tan.polytope_lmi(current_bias_acceleration_local,
                                               polytope_with_bias));
        mpcprobtang->con_a.update_horizon_polytope(pol_qdd_tan.polytope_lmi(
            current_bias_acceleration_local, polytope_with_bias));

        if (limit_jerk) {
          mpcprobtang->con_j.update_mode(constraints_mode);
          mpcprobtang->con_j.update_firststep_polytope(
              pol_qddd_tan.polytope_lmi(current_bias_jerk_local,
                                        polytope_with_bias));
          mpcprobtang->con_j.update_horizon_polytope(pol_qddd_tan.polytope_lmi(
              current_bias_jerk_local, polytope_with_bias));
        }
        mpcprobtang->solve(initial, target_pose);

        // -----------------------------------------------------------------
        // -----------------------------------------------------------------
        // -----------------------------------------------------------------
        // max kin energy stuff (not working)
        // acc_con.A = Eigen::MatrixXd::Zero(mpcprobtang->H, mpcprobtang->nz);

        // auto deltaEfunc = [](double T, const Eigen::VectorXd &v0,
        //                      const Eigen::VectorXd &a0,
        //                      const Eigen::MatrixXd &OperIner) {
        //   // return ((T * T / 2) * a0.transpose() + T * v0.transpose()) *
        //   // OperIner;
        //   return T * T * v0.transpose() * OperIner;
        // };
        // acc_con.A.block(0, 0, 1, 6) = deltaEfunc(_control_period,
        // initial.v0,
        //                                          initial.a0,
        //                                          current_OperIner);

        // Eigen::MatrixXd A = Eigen::MatrixXd::Zero(1, mpcprobtang->nz);
        // A.block(0, 0, 1, 6) = deltaEfunc(mpcprobtang->T, initial.v0,
        // initial.a0,
        //                                  current_OperIner);
        // for (unsigned int kk = 1; kk < mpcprobtang->H; ++kk) {
        //   acc_con.A.block(kk, 0, 1, mpcprobtang->nz) =
        //       acc_con.A.block(kk - 1, 0, 1, mpcprobtang->nz) +
        //       lmpcpoly::block_rcircshift(A, 6 * kk);
        // }
        // // acc_con.A.conservativeResize(mpcprobtang->H + 2,
        // mpcprobtang->nz);
        // // acc_con.A.block(mpcprobtang->H, 0, 2,
        // mpcprobtang->nz).setZero();
        // // acc_con.A.block(mpcprobtang->H, 0, 1, 6) =
        // //     acc_con.A.block(0, 0, 1, 6) * mpcprobtang->ss.Bv;
        // // acc_con.A.block(mpcprobtang->H + 1, 0, 1, 6) =
        // //     acc_con.A.block(1, 0, 1, 6) * mpcprobtang->ss.Bv;
        // // acc_con.A.block(mpcprobtang->H + 1, 6, 1, 6) =
        // //     acc_con.A.block(1, 6, 1, 6) * mpcprobtang->ss.Bv;
        // // lmpcpoly::print_mat("acc_con.A", acc_con.A);

        // double current_E =
        //     initial.v0.transpose() * current_OperIner * initial.v0;
        // // double Emax_margin = 0.9 * Emax;
        // double dEbound = Emax - current_E;
        // double w_a_temp = mpc_solveparams.w_a;
        // double temp_Emax = Emax;
        // if (dEbound < 0) {
        //   dEbound = std::numeric_limits<double>::infinity();
        //   temp_Emax = std::numeric_limits<double>::infinity();
        //   // w_a_temp = 100 * mpc_solveparams.w_a;
        // }

        // if (current_E < 0) {
        //   std::cout << "negative current_E:" << current_E << std::endl;
        // }

        // // acc_con.A = T * initial.v0.transpose() * current_OperIner;
        // // acc_con.b.min = Eigen::VectorXd::Zero(1);
        // // double lb = -std::numeric_limits<double>::infinity();
        // // double lb = -1e-3;
        // // double lb = 0;
        // double lb = -current_E;
        // // double lb = -Emax;
        // acc_con.b.min = lb * Eigen::VectorXd::Ones(mpcprobtang->H);
        // // acc_con.b.max = dEbound * Eigen::VectorXd::Ones(mpcprobtang->H);
        // acc_con.b.max = dEbound * Eigen::VectorXd::Ones(mpcprobtang->H);

        // // acc_con.b.min.conservativeResize(mpcprobtang->H + 2);
        // // acc_con.b.min.segment(mpcprobtang->H, 2) = //
        // // Eigen::VectorXd::Zero(2);
        // //     -temp_Emax * Eigen::VectorXd::Ones(2);
        // // acc_con.b.max.conservativeResize(mpcprobtang->H + 2);
        // // acc_con.b.max.segment(mpcprobtang->H, 2) =
        // //     temp_Emax * Eigen::VectorXd::Ones(2);

        // // lmpcpoly::print_mat("acc_con.b.min", acc_con.b.min);
        // // lmpcpoly::print_mat("acc_con.b.max", acc_con.b.max);
        // std::cout << "dE (A*a0):" << (acc_con.A * initial.a0)[0]
        //           << " < dEb:" << dEbound << std::endl;

        // // Eigen::VectorXd amax_tan(100 * Eigen::Vector3d::Ones(6));
        // // mpcprobtang->update_amax(amax_tan);

        // // mpcprobtang->update_wreg(mpc_solveparams.w_a, current_OperIner);
        // mpcprobtang->update_wreg(w_a_temp, current_OperIner);
        // -----------------------------------------------------------------
        // -----------------------------------------------------------------
        // -----------------------------------------------------------------
      }

      temp_des_pose = mpcprobtang->horizon.p_at(t_traj + _control_period);
      temp_desired_twist = mpcprobtang->horizon.v_at(t_traj + _control_period);
      temp_desired_twistacc =
          mpcprobtang->horizon.a_at(t_traj + _control_period);
      temp_desired_twistjerk =
          mpcprobtang->horizon.j_at(t_traj + _control_period);
    } else {

      {
        auto rosnow = ros::Time::now();
        publock(bias_acceleration_publisher, geometry_msgs::Twist twistmsg;
                bias_acceleration_publisher.msg_.header.frame_id = "world";
                tf::twistEigenToMsg(current_bias_acceleration_lwa, twistmsg);
                bias_acceleration_publisher.msg_.header.stamp = rosnow;
                bias_acceleration_publisher.msg_.twist = twistmsg;);
        publock(bias_jerk_publisher, geometry_msgs::Twist twistmsg;
                bias_jerk_publisher.msg_.header.frame_id = "world";
                tf::twistEigenToMsg(current_bias_jerk_lwa, twistmsg);
                bias_jerk_publisher.msg_.header.stamp = rosnow;
                bias_jerk_publisher.msg_.twist = twistmsg;);
      }

      lmpcpoly::InitialConditions initial;
      initial.t = t_traj;
      initial.p0 = current_pose.translation();
      initial.v0 = pinocchio::Motion(current_twist).linear();
      initial.a0 = pinocchio::Motion(current_twist_acc).linear();
      if (verbose) {
        ROS_WARN_STREAM(lmpcpoly::str_mat("initial.p0", initial.p0).c_str());
        ROS_WARN_STREAM(lmpcpoly::str_mat("initial.v0", initial.v0).c_str());
        ROS_WARN_STREAM(lmpcpoly::str_mat("initial.a0", initial.a0).c_str());
      }

      {
        WarnTock temp("mpcprob->solve", mpc_compute_time_warn);

        // mpcprob->con_p.update_mode(constraints_mode);
        // mpcprob->con_p.update_firststep_polytope(pol_q_pos.polytope_lmi());
        // mpcprob->con_p.update_horizon_polytope(pol_q_pos.polytope_lmi());

        mpcprob->con_v.update_mode(constraints_mode);
        mpcprob->con_v.update_firststep_polytope(pol_qd_pos.polytope_lmi());
        mpcprob->con_v.update_horizon_polytope(pol_qd_pos.polytope_lmi());

        mpcprob->con_a.update_mode(constraints_mode);
        Eigen::VectorXd a0_max(initial.a0 + mpcprob->con_j.firststep_box_max() *
                                                _control_period);
        Eigen::VectorXd a0_min(initial.a0 - mpcprob->con_j.firststep_box_max() *
                                                _control_period);
        mpcprob->con_a.update_firststep_box_max(a0_min, a0_max);
        mpcprob->con_a.update_firststep_polytope(
            pol_qdd_firststep_pos.polytope_lmi(
                current_bias_acceleration_lwa.head(3), polytope_with_bias));
        mpcprob->con_a.update_horizon_polytope(pol_qdd_pos.polytope_lmi(
            current_bias_acceleration_lwa.head(3), polytope_with_bias));

        if (limit_jerk) {
          mpcprob->con_j.update_mode(constraints_mode);
          mpcprob->con_j.update_firststep_polytope(pol_qddd_pos.polytope_lmi(
              current_bias_jerk_lwa.head(3), polytope_with_bias));
          mpcprob->con_j.update_horizon_polytope(pol_qddd_pos.polytope_lmi(
              current_bias_jerk_lwa.head(3), polytope_with_bias));
        }
        mpcprob->solve(initial, target_pose.translation());
      }
      temp_des_pose.translation() =
          mpcprob->horizon.p_at(t_traj + _control_period);
      temp_desired_twist.linear() =
          current_pose.rotation() *
          mpcprob->horizon.v_at(t_traj + _control_period);
      temp_desired_twistacc.linear() =
          current_pose.rotation() *
          mpcprob->horizon.a_at(t_traj + _control_period);
      temp_desired_twistjerk.linear() =
          current_pose.rotation() *
          mpcprob->horizon.j_at(t_traj + _control_period);
    }
  }

  desired_pose.matrix() = temp_des_pose.toHomogeneousMatrix();
  desired_velocity = temp_desired_twist;
  desired_acceleration = temp_desired_twistacc;
  desired_jerk = temp_desired_twistjerk;

  if (desired_velocity.norm() > 1000) {
    ROS_ERROR_STREAM("ERROR velocity too high");
    ROS_ERROR_STREAM(lmpcpoly::str_mat("v", mpcprobtang->horizon.v));
    ROS_ERROR_STREAM(lmpcpoly::str_mat("a", mpcprobtang->horizon.a));
  }

  // TODO: use realtime or rely on this?
  // https://github.com/frankaemika/franka_ros/blob/8b8d1c2bc12c2ce8cb9587ce37b185421895fec5/franka_control/src/franka_control_node.cpp#L122
  t_traj += _control_period;

  publock(mpctime_publisher, mpctime_publisher.msg_.data =
                                 lmpcpoly::tock_ms(start_updatetrajectory););
  auto debugtock = lmpcpoly::tock_ms(start_updatetrajectory);
  if (debugtock > 1.) {
    std::cout << "debug tock_ms > 1ms:" << debugtock << "ms" << std::endl;
  }
  if (debugtock > 100) {
    ROS_ERROR_STREAM("WOT HAPPEN");
  }

  double kin_energy =
      current_twist.transpose() * current_OperIner * current_twist;
  double desired_kin_energy =
      desired_velocity.transpose() * current_OperIner * desired_velocity;
  publock(kinenergy_publisher, kinenergy_publisher.msg_.data = kin_energy;);
  publock(desired_kinenergy_publisher,
          desired_kinenergy_publisher.msg_.data = desired_kin_energy;);
  publock(kinenergyd_publisher,
          kinenergyd_publisher.msg_.data = (kin_energy - kin_energy_prev);
          kin_energy_prev = kin_energy;);

  // ============================================================================
  // DISABLE PUBLISH POLYTOPE
  // if (horizon_publish_delay.passed()) {
  //   Eigen::Vector3d polytope_position = Eigen::Vector3d::Zero();
  //   // std::string frame_id = "panda_link8";
  //   std::string frame_id = controlled_frame;
  //   Eigen::MatrixXd A = Jlocal;
  //   // Eigen::MatrixXd A = Jlwa;
  //   if (!tangent_space) {
  //     polytope_position = current_pose.translation();
  //     frame_id = "world";
  //     A = Jlwa;
  //   }

  //   double factor = 0.15;
  //   publish_polytope(acc_vertices_publisher, acc_polygon_publisher, A,
  //   qd_lims,
  //                    frame_id, polytope_position, factor);

  //   // double dt = 0.1;
  //   // double factor = dt * dt / 2;
  //   // publish_polytope(acc_vertices_publisher, acc_polygon_publisher,
  //   JMinv,
  //   //                  tau_lims, frame_id, polytope_position, factor);

  //   polytope_publish_delay.refresh();
  // }
  // ============================================================================
  publish_trajectory();
}

Eigen::Affine3d MoveItTrajectory::getCartesianPose() { return desired_pose; }

Eigen::Matrix<double, 6, 1> MoveItTrajectory::getCartesianVelocity() {
  return desired_velocity;
}

Eigen::Matrix<double, 6, 1> MoveItTrajectory::getCartesianAcceleration() {
  return desired_acceleration;
}

void MoveItTrajectory::setControlledFrame(std::string controlled_frame) {
  this->controlled_frame = controlled_frame;
}

void MoveItTrajectory::sortJointState(moveit_msgs::RobotTrajectory &msg) {
  // This function is used to sort the joint comming from the /joint_state
  // topic They are order alphabetically while we need them to be order as in
  // the URDF file With use the joint_names variable that defined the joints
  // in the correct order
  moveit_msgs::RobotTrajectory sorted_joint_state = msg;
  std::vector<std::string> joint_names_from_urdf;
  for (pinocchio::JointIndex joint_id = 1;
       joint_id < (pinocchio::JointIndex)model.njoints; ++joint_id) {
    joint_names_from_urdf.push_back(model.names[joint_id]);
  }
  if (joint_names_from_urdf != msg.joint_trajectory.joint_names) {
    for (unsigned int num_points = 0;
         num_points < msg.joint_trajectory.points.size(); num_points++) {
      int i = 0;
      for (auto ith_unsorted_joint_name : msg.joint_trajectory.joint_names) {
        int j = 0;
        for (auto jth_joint_name : joint_names_from_urdf) {
          if (ith_unsorted_joint_name.find(jth_joint_name) !=
              std::string::npos) {
            sorted_joint_state.joint_trajectory.points[num_points]
                .positions[j] =
                msg.joint_trajectory.points[num_points].positions[i];
            //  sorted_joint_state.joint_trajectory.joint_names[j] =
            //  msg.joint_trajectory.joint_names[i];
            break;
          } else {
            j++;
          }
        }
        i++;
      }
    }
  }
  msg = sorted_joint_state;
}
// void MoveItTrajectory::abort_objective() {
//   set_objective(current_q, current_q);
// }
void MoveItTrajectory::set_objective(const Eigen::VectorXd start,
                                     const Eigen::VectorXd end) {
  std::lock_guard<std::mutex> guard(current_horizon_mutex);
  ROS_WARN("current_horizon_mutex locked...");
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // MPC
  ///////////////////////////////////////////////////////////////
  // 1
  desired_acceleration.setZero();
  desired_velocity.setZero();
  desired_jerk.setZero();

  pinocchio::SE3 starting_posee;
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    pinocchio::forwardKinematics(model, data, start);
    pinocchio::updateFramePlacements(model, data);
    starting_posee = data.oMf[model.getFrameId(controlled_frame)];
    // auto from = pinocchio::log6(starting_posee).toVector();
    desired_pose.matrix() = starting_posee.toHomogeneousMatrix();

    pinocchio::forwardKinematics(model, data, end);
    pinocchio::updateFramePlacements(model, data);
    target_pose = data.oMf[model.getFrameId(controlled_frame)];
  }

  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
  ROS_WARN("     NEW TRAJECTORY REQUEST");
  lmpcpoly::print_mat("  from", starting_posee.translation());
  lmpcpoly::print_mat("target", target_pose.translation());

  ROS_WARN("waiting for planifier first result...");
  if (tangent_space) {
    lmpcpoly::InitialConditionsTangent initial;
    initial.t = 0;
    initial.pose0 = starting_posee;
    initial.v0 = pinocchio::Motion(current_twist);
    initial.a0 = pinocchio::Motion(current_twist_acc);
    if (verbose) {
      ROS_WARN_STREAM(lmpcpoly::str_mat("initial.v0", initial.v0).c_str());
      ROS_WARN_STREAM(lmpcpoly::str_mat("initial.a0", initial.a0).c_str());
    }

    bool presolve = true;
    {
      WarnTock temp("mpcprobtang->solve", mpc_compute_time_warn);
      mpcprobtang->solve(initial, target_pose, presolve);
    }
  } else {
    lmpcpoly::InitialConditions initial;
    initial.t = 0;
    initial.p0 = starting_posee.translation();
    initial.v0 = pinocchio::Motion(current_twist).linear();
    initial.a0 = pinocchio::Motion(current_twist_acc).linear();
    {
      WarnTock temp("mpcprob->solve", mpc_compute_time_warn);
      mpcprob->solve(initial, target_pose.translation());
    }
  }

  ROS_WARN("DONE!");

  ROS_WARN(" NEW TRAJECTORY COMPUTED");
  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
  ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");

  t_traj = 0.0;
  trajectory_is_built.store(true);
}

bool MoveItTrajectory::computeNewTrajectory(moveit_msgs::RobotTrajectory path) {
  if (follow_flag.load()) {
    return true;
  }

  if (path.joint_trajectory.points.size() == 0) {
    ROS_WARN("Empty trajectory not doing anything");
    return false;
  }
  trajectory_is_built.store(false);
  sortJointState(path);

  std::vector<Eigen::VectorXd> waypoints;

  std::stringstream ss;
  ss << std::endl << "---------------------------------------" << std::endl;
  ss << "model.nv:" << model.nv << std::endl;
  ss << "  points:" << path.joint_trajectory.points.size() << std::endl;
  for (unsigned int point = 0; point < path.joint_trajectory.points.size();
       point++) {
    Eigen::VectorXd waypoint(model.nv);
    for (unsigned int i = 0; i < waypoint.size(); ++i) {
      waypoint[i] = path.joint_trajectory.points[point].positions[i];
    }
    waypoints.push_back(waypoint);
  }
  ss << "start configuration:" << waypoints[0].transpose() << std::endl;
  auto finalwp = path.joint_trajectory.points.size() - 1;
  ss << "end   configuration:" << waypoints[finalwp].transpose() << std::endl;
  ss << "---------------------------------------" << std::endl;
  ROS_WARN(ss.str().c_str());

  Eigen::VectorXd q;
  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    q = current_q;
  }
  set_objective(q, waypoints[finalwp]);
  // NOTE: waypoints[0] would work fine except when bypassing gazebo
  // simulation set_objective(waypoints[0], waypoints[finalwp]);
  return true;
}

bool MoveItTrajectory::GoalReached() {
  std::lock_guard<std::mutex> guard(pin_mutex);
  pinocchio::SE3 tipMdes;
  pinocchio::forwardKinematics(model, data, current_q);
  pinocchio::updateFramePlacements(model, data);

  tipMdes = data.oMf[model.getFrameId(controlled_frame)].actInv(target_pose);
  Eigen::Matrix<double, 6, 1> err = pinocchio::log6(tipMdes).toVector();

  const auto &lin = err.head<3>();
  const auto &ang = err.tail<3>();
  if ((lin.norm() < 1e-3) && (ang.norm() < 1e-3)) {
    return true;
  }
  return false;
}

void MoveItTrajectory::publish_trajectory() {

  // throttle pubishing horizon
  if (horizon_publish_delay.passed() && trajectory_is_built.load()) {
    publock(
        horizon_publisher,
        horizon_publisher.msg_.header.stamp = ros::Time::now();
        horizon_publisher.msg_.header.frame_id = "world";

        if (tangent_space) {
          auto pose_vec = mpcprobtang->horizon.sample_horizon();
          horizon_publisher.msg_.poses.resize(pose_vec.size());
          for (unsigned int i = 0; i < pose_vec.size(); ++i) {
            Eigen::Affine3d posek;
            posek.matrix() = pose_vec[i].toHomogeneousMatrix();
            geometry_msgs::Pose posek_msg;
            tf::poseEigenToMsg(posek, posek_msg);
            horizon_publisher.msg_.poses[i] = posek_msg;
          }
        } else {
          auto pos_vec = mpcprob->horizon.sample_horizon();
          horizon_publisher.msg_.poses.resize(pos_vec.size());
          for (unsigned int i = 0; i < pos_vec.size(); ++i) {
            pinocchio::SE3 pinpose(initial_pose.rotation(), pos_vec[i]);
            Eigen::Affine3d posek;
            posek.matrix() = pinpose.toHomogeneousMatrix();
            geometry_msgs::Pose posek_msg;
            tf::poseEigenToMsg(posek, posek_msg);
            horizon_publisher.msg_.poses[i] = posek_msg;
          } //
        });
    horizon_publish_delay.refresh();
  }

  auto rosnow = ros::Time::now();
  // VEL AND ACC
  // Publish robot desired velocity
  std::string frame_id = "world";
  // local world aligned adj
  Eigen::Matrix<double, 6, 6> adjlwa =
      pinocchio::SE3(current_pose.rotation(), Eigen::Vector3d::Zero())
          .toActionMatrixInverse();
  Eigen::Matrix<double, 6, 6> adj = adjlwa;
  if (tangent_space) {
    frame_id = "local";
    adj = Eigen::MatrixXd::Identity(6, 6);
  }

  publock(desired_velocity_publisher, geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(adj * desired_velocity, twistmsg);
          desired_velocity_publisher.msg_.header.stamp = rosnow;
          desired_velocity_publisher.msg_.header.frame_id = frame_id;
          desired_velocity_publisher.msg_.twist = twistmsg;);
  publock(
      desired_velocity_arrow_publisher,
      Eigen::VectorXd vel = adjlwa * desired_velocity;
      visualization_msgs::Marker &marker =
          desired_velocity_arrow_publisher.msg_;
      marker.header.stamp = rosnow; marker.header.frame_id = "world";
      marker.type = visualization_msgs::Marker::ARROW; marker.points.resize(2);
      marker.id = 0; marker.ns = "desired_velocity_arrow";
      marker.scale.x = 0.01; marker.scale.y = 0.1; marker.scale.z = .0;
      marker.action = visualization_msgs::Marker::ADD; {
        geometry_msgs::Point point;
        point.x = current_pose.translation()(0);
        point.y = current_pose.translation()(1);
        point.z = current_pose.translation()(2);
        marker.points[0] = point;
      } {
        geometry_msgs::Point point;
        point.x = current_pose.translation()(0) + vel(0);
        point.y = current_pose.translation()(1) + vel(1);
        point.z = current_pose.translation()(2) + vel(2);
        marker.points[1] = point;
      } marker.color.r = 0.5f;
      marker.color.g = 0.0f; marker.color.b = 0.5f; marker.color.a = 0.7f;
      // marker.lifetime = ros::Duration();
  );
  // Publish robot desired acceleration
  publock(desired_acceleration_publisher, geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(adj * desired_acceleration, twistmsg);
          desired_acceleration_publisher.msg_.header.stamp = rosnow;
          desired_acceleration_publisher.msg_.header.frame_id = frame_id;
          desired_acceleration_publisher.msg_.twist = twistmsg;);
  publock(desired_jerk_publisher, geometry_msgs::Twist twistmsg;
          tf::twistEigenToMsg(adj * desired_jerk, twistmsg);
          desired_jerk_publisher.msg_.header.stamp = rosnow;
          desired_jerk_publisher.msg_.header.frame_id = frame_id;
          desired_jerk_publisher.msg_.twist = twistmsg;);

  publock(
      velocity_limits_publisher, geometry_msgs::Twist twistmsg;
      Eigen::VectorXd vmax = mpcprobtang->con_v.firststep_box_max();
      if (!tangent_space) {
        vmax.topRows(3) = mpcprob->con_v.firststep_box_max();
      } tf::twistEigenToMsg(vmax, twistmsg);
      velocity_limits_publisher.msg_.header.stamp = rosnow;
      velocity_limits_publisher.msg_.header.frame_id = frame_id;
      velocity_limits_publisher.msg_.twist = twistmsg;);
  publock(
      acceleration_limits_publisher, geometry_msgs::Twist twistmsg;
      Eigen::VectorXd amax = mpcprobtang->con_a.firststep_box_max();
      if (!tangent_space) {
        amax.topRows(3) = mpcprob->con_a.firststep_box_max();
      } tf::twistEigenToMsg(amax, twistmsg);
      acceleration_limits_publisher.msg_.header.stamp = rosnow;
      acceleration_limits_publisher.msg_.header.frame_id = frame_id;
      acceleration_limits_publisher.msg_.twist = twistmsg;);

  {
    std::lock_guard<std::mutex> guard(pin_mutex);
    // pinocchio::forwardKinematics(model, data, current_q, current_qd);
    // pinocchio::updateFramePlacements(model, data);
    publock(
        jointspace_limit_distance_publisher,
        jointspace_limit_distance_publisher.msg_.header.frame_id =
            "joint-space";
        jointspace_limit_distance_publisher.msg_.header.stamp = rosnow;
        jointspace_limit_distance_publisher.msg_.position.resize(model.nv);
        jointspace_limit_distance_publisher.msg_.velocity.resize(model.nv);
        jointspace_limit_distance_publisher.msg_.effort.resize(model.nv);
        jointspace_limit_distance_publisher.msg_.name.resize(model.nv);

        Eigen::VectorXd pos(model.nv); Eigen::VectorXd vel(model.nv);
        Eigen::VectorXd acc(model.nv);
        pos = (model.upperPositionLimit - current_q)
                  .cwiseMin(current_q - model.lowerPositionLimit);
        vel = current_qd.array().abs() / model.velocityLimit.array();
        acc = current_qdd.array().abs() / qdd_max.array();
        for (int i = 0; i < model.nv; ++i) {
          jointspace_limit_distance_publisher.msg_.name[i] =
              std::string("joint") + std::to_string(i + 1);
          jointspace_limit_distance_publisher.msg_.position[i] = pos[i];
          jointspace_limit_distance_publisher.msg_.velocity[i] = vel[i];
          jointspace_limit_distance_publisher.msg_.effort[i] = acc[i];
        };);
  }
  // ERROR
  publock(desired_error_publisher, geometry_msgs::Twist twistmsg;
          Eigen::Vector3d err_pos =
              desired_pose.translation() - current_pose.translation();
          Eigen::Vector3d err_ang = pinocchio::log3(
              current_pose.rotation().transpose() * desired_pose.rotation());
          pinocchio::Motion err_twist(err_pos, err_ang);
          tf::twistEigenToMsg(err_twist.toVector(), twistmsg);
          desired_error_publisher.msg_.header.stamp = rosnow;
          desired_error_publisher.msg_.header.frame_id = "local";
          desired_error_publisher.msg_.twist = twistmsg;);

  publock(target_error_publisher, geometry_msgs::Twist twistmsg;
          Eigen::Vector3d err_pos =
              target_pose.translation() - current_pose.translation();
          Eigen::Vector3d err_ang = pinocchio::log3(
              current_pose.rotation().transpose() * target_pose.rotation());
          pinocchio::Motion err_twist(err_pos, err_ang);
          tf::twistEigenToMsg(err_twist.toVector(), twistmsg);
          target_error_publisher.msg_.header.stamp = rosnow;
          target_error_publisher.msg_.header.frame_id = "local";
          target_error_publisher.msg_.twist = twistmsg;);

  // POSES
  publock(current_pose_publisher, geometry_msgs::Pose xmsg; Eigen::Affine3d x;
          x.matrix() = current_pose.toHomogeneousMatrix();
          tf::poseEigenToMsg(x, xmsg);
          current_pose_publisher.msg_.header.stamp = rosnow;
          current_pose_publisher.msg_.header.frame_id = "world";
          current_pose_publisher.msg_.pose = xmsg;);
  publock(target_pose_publisher, geometry_msgs::Pose xmsg; Eigen::Affine3d x;
          x.matrix() = target_pose.toHomogeneousMatrix();
          tf::poseEigenToMsg(x, xmsg);
          target_pose_publisher.msg_.header.stamp = rosnow;
          target_pose_publisher.msg_.header.frame_id = "world";
          target_pose_publisher.msg_.pose = xmsg;);

  publock(desired_pose_publisher, geometry_msgs::Pose xmsg; Eigen::Affine3d x;
          x.matrix() = desired_pose.matrix(); tf::poseEigenToMsg(x, xmsg);
          desired_pose_publisher.msg_.header.stamp = rosnow;
          desired_pose_publisher.msg_.header.frame_id = "world";
          desired_pose_publisher.msg_.pose = xmsg;);
}

double MoveItTrajectory::control_period() { return _control_period; }

double MoveItTrajectory::getTimeProgress() {
  // pinocchio::forwardKinematics(model, data, current_q);
  // pinocchio::updateFramePlacements(model, data);

  // const pinocchio::SE3 current_to_ending =
  //     data.oMf[model.getFrameId(controlled_frame)].actInv(target_pose);
  // Eigen::Matrix<double, 6, 1> current_to_ending_se3 =
  //     pinocchio::log6(current_to_ending).toVector();

  // const pinocchio::SE3 start_to_ending =
  //     starting_pose.actInv(target_pose);
  // Eigen::Matrix<double, 6, 1> start_to_ending_se3 =
  //     pinocchio::log6(start_to_ending).toVector();

  // // auto progress = (current_to_ending_se3.array() /
  // start_to_ending_se3.array()).matrix().norm(); auto delta =
  // (start_to_ending_se3.norm() -
  //               current_to_ending_se3.norm());
  // if (delta < 0){
  //   delta = 0;
  // }
  //   auto progress = delta/ start_to_ending_se3.norm();

  // // ROS_INFO_STREAM_THROTTLE(.1, "current_to_ending: " <<
  // current_to_ending_se3.transpose());
  // // ROS_INFO_STREAM_THROTTLE(.1, "  start_to_ending: " <<
  // start_to_ending_se3.transpose()); ROS_INFO_STREAM_THROTTLE(.1,
  // "*******************************************"); ROS_INFO_STREAM_THROTTLE(
  //     .1, "delta:" << delta);
  // ROS_INFO_STREAM_THROTTLE(
  //     .1, "start_to_ending_se3.norm():" << start_to_ending_se3.norm());
  // ROS_INFO_STREAM_THROTTLE(.1,
  //                          "current_to_ending_se3.norm():"<<
  //                          current_to_ending_se3.norm());
  // ROS_INFO_STREAM_THROTTLE(
  //     .1, "         progress: "
  //             << (current_to_ending_se3.array() /
  //             start_to_ending_se3.array())
  //                    .matrix()
  //                    .transpose());
  // ROS_INFO_STREAM_THROTTLE(.1, "             norm: " << progress);
  // ROS_INFO_STREAM_THROTTLE(.1, "traj progress: " << std::setprecision(2)
  //                                                << progress * 100 << "%");
  // return progress;

  // double traj_duration = mpcprob->H * mpcprob->ndt * mpcprob->dt;
  double traj_duration = 1;
  if (t_traj < traj_duration) {

    double progress = t_traj / traj_duration;
    // std::stringstream ss;
    // ss << "traj progress: " << std::setprecision(2) << progress * 100 <<
    // "%"; ROS_INFO_THROTTLE(.1, ss.str());
    ROS_INFO_STREAM_THROTTLE(.1, "t_traj: " << t_traj << "s");
    ROS_INFO_STREAM_THROTTLE(.1, "traj progress: " << std::setprecision(2)
                                                   << progress * 100 << "%");
    return progress;
  }
  return 1;
}
