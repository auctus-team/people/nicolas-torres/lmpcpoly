#include "moveit_trajectory_interface/moveit_trajectory_interface.hpp"

TrajectoryInterface::TrajectoryInterface() {
  interface = std::shared_ptr<MoveItTrajectory>(new MoveItTrajectory);
  boost::thread t1(&TrajectoryInterface::action_thread, this, interface);
}

void TrajectoryInterface::action_thread(
    std::shared_ptr<MoveItTrajectory> interface) {
  while (!interface->isInitialized()) {
    ROS_DEBUG_STREAM_THROTTLE(1,
                              "Wait for trajectory manager to be initialized");
  }
  TrajectoryAction trajectory_action(
      "/" + interface->getRobotName() +
          "/moveit_trajectory_interface/trajectory_action",
      interface);
  while (ros::ok()) {
    // spin
  }
}
