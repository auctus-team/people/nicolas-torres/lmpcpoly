#include "moveit_trajectory_interface/moveit_trajectory_action_server.hpp"

TrajectoryAction::TrajectoryAction(std::string name,
                                   std::shared_ptr<MoveItTrajectory> trajectory)
    : as_(nh_, name, boost::bind(&TrajectoryAction::executeCB, this, _1),
          false),
      action_name_(name) {
  this->trajectory = trajectory;
  as_.start();
}

void TrajectoryAction::executeCB(
    const moveit_trajectory_interface::TrajectoryGoalConstPtr &goal) {
  // TODO: what's the point of this if there's a loop without delay
  // afterwards?
  ros::Rate r(1);
  bool success = trajectory->computeNewTrajectory(goal->path);

  while (trajectory->getTimeProgress() < 0.99) {
    // check that preempt (meaning cancelled) has not been requested by the
    // client
    if (as_.isPreemptRequested() || !ros::ok()) {
      ROS_INFO("%s: Preempted", action_name_.c_str());
      // set the action state to preempted
      as_.setPreempted();
      success = false;
      // trajectory->abort_objective();
      break;
    }
    feedback_.time_ratio = trajectory->getTimeProgress();
    // publish the feedback
    as_.publishFeedback(feedback_);
  }

  success = trajectory->GoalReached();

  if (success) {
    as_.setSucceeded(result_);
  } else {
    as_.setAborted(result_);
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("     ABORTED");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
    ROS_WARN("!!!!!!!!!!!!!!!!!!!!!!!!!!");
  }
}
