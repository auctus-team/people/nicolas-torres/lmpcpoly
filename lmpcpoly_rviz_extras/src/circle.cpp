#include <cstdio>
#include <cstdlib>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QSpinBox>

#include <QIcon>
#include <QStyle>

#include "aux.hpp"
#include "circle.hpp"

#include <ros/master.h>
#include <ros/node_handle.h>
#include <ros/ros.h>

#include <QApplication>
#include <QStyle>
#include <QToolButton>

#include <geometry_msgs/Quaternion.h>
#include <std_msgs/Header.h>

#include <cmath>
#include <ios>
#include <sstream>

// #include <eigen3/Eigen/src/Geometry/Transform.h>
// #include <eigen_conversions/eigen_msg.h>

namespace lmpcpoly_rviz_extras {

void Circle::add_custom_slider(std::string title, int slider_max,
                               int slider_interval, double current_value,
                               double real_max, double *msg_variable,
                               QVBoxLayout *layout) {
  QString qtitle(QString::fromStdString(title));
  QHBoxLayout *tmpH1 = new QHBoxLayout();
  QLabel *tmpLabelTitle = new QLabel(qtitle);
  QLabel *tmpLabel = new QLabel("0");
  QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
  tmpSlider->setMinimum(1);
  tmpSlider->setMaximum(slider_max);
  tmpSlider->setValue(proportional_int(slider_max, current_value, real_max));
  tmpSlider->setTickInterval(slider_interval);
  tmpSlider->setTickPosition(QSlider::TicksBelow);
  tmpH1->addWidget(tmpLabelTitle);
  tmpH1->addWidget(tmpSlider);
  tmpH1->addWidget(tmpLabel);
  layout->addLayout(tmpH1);
  auto updateLambda = [=]() {
    double realVal =
        proportional_value(slider_max, tmpSlider->value(), real_max);
    tmpLabel->setText(QString::number(realVal));
    *msg_variable = realVal;
  };
  updateLambda();
  QObject::connect(tmpSlider, &QSlider::valueChanged, this, [=]() {
    updateLambda();
    // this->configure_mpc_client.call(this->configure_msg);
  });
}

Circle::Circle(QWidget *parent) : rviz::Panel(parent) {
  pose1_publisher =
      nh.advertise<geometry_msgs::PoseStamped>("circle_pose1", 1, true);
  pose_circle_publisher =
      nh.advertise<geometry_msgs::PoseStamped>("circle_cyclic", 1, true);

  tf::StampedTransform transform;
  listener.waitForTransform("world", "panda_link0", ros::Time::now(),
                            ros::Duration(45));
  listener.lookupTransform("world", "panda_link0", ros::Time::now(), transform);

  auto *layout = new QVBoxLayout;
  layout->setAlignment(Qt::AlignHCenter);
  {
    int slider_max = 40000, slider_interval = 1;
    double current_value = 5, real_max = 30;
    QString qtitle(QString::fromStdString("T"));
    QHBoxLayout *tmpH1 = new QHBoxLayout();
    QLabel *tmpLabelTitle = new QLabel(qtitle);
    QLabel *tmpLabel = new QLabel("0");
    QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
    tmpSlider->setMinimum(1);
    tmpSlider->setMaximum(slider_max);
    tmpSlider->setValue(proportional_int(slider_max, current_value, real_max));
    tmpSlider->setTickInterval(slider_interval);
    tmpSlider->setTickPosition(QSlider::TicksBelow);
    tmpH1->addWidget(tmpLabelTitle);
    tmpH1->addWidget(tmpSlider);
    tmpH1->addWidget(tmpLabel);
    layout->addLayout(tmpH1);
    auto updateLambda = [=, this]() {
      double realVal =
          proportional_value(slider_max, tmpSlider->value(), real_max);
      tmpLabel->setText(QString::number(realVal));
      this->period = realVal;
    };
    updateLambda();
    QObject::connect(tmpSlider, &QSlider::valueChanged, this,
                     [=]() { updateLambda(); });
  }

  {
    int slider_max = 40000, slider_interval = 1;
    double current_value = .3, real_max = 1;
    QString qtitle(QString::fromStdString("R"));
    QHBoxLayout *tmpH1 = new QHBoxLayout();
    QLabel *tmpLabelTitle = new QLabel(qtitle);
    QLabel *tmpLabel = new QLabel("0");
    QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
    tmpSlider->setMinimum(1);
    tmpSlider->setMaximum(slider_max);
    tmpSlider->setValue(proportional_int(slider_max, current_value, real_max));
    tmpSlider->setTickInterval(slider_interval);
    tmpSlider->setTickPosition(QSlider::TicksBelow);
    tmpH1->addWidget(tmpLabelTitle);
    tmpH1->addWidget(tmpSlider);
    tmpH1->addWidget(tmpLabel);
    layout->addLayout(tmpH1);
    auto updateLambda = [=, this]() {
      double realVal =
          proportional_value(slider_max, tmpSlider->value(), real_max);
      tmpLabel->setText(QString::number(realVal));
      this->radio = realVal;
    };
    updateLambda();
    QObject::connect(tmpSlider, &QSlider::valueChanged, this,
                     [=]() { updateLambda(); });
  }

  {
    timer.reset(new QTimer);
    int msec = timer_delay * 1000;
    // timer->setSingleShot(true);
    connect(timer.get(), &QTimer::timeout, this, [=, this]() {
      publish_pose(pose_circle_publisher, pose1.pose, pose1.header.frame_id);
    });
    timer->start(msec);
  }

  // Vertical layout
  setLayout(layout);

  // create an interactive marker server on the topic namespace simple_marker
  server.reset(new interactive_markers::InteractiveMarkerServer(
      "circle_markers", "circle_server", true));

  std_msgs::Header header;
  header.frame_id = "world";
  header.stamp = ros::Time::now();
  pose1.header = header;

  tf::Quaternion quat(1, 0, 0, 0);
  {
    tf::Vector3 vec3(0.5, .3, 0.45);
    tf::Pose pose0_link0(quat, vec3);
    geometry_msgs::Pose pose_world;
    pose0_link0 = transform * pose0_link0;
    poseTFToMsg(pose0_link0, pose_world);
    publish_pose(pose1_publisher, pose_world, "world");
    pose1.pose = pose_world;
    server->insert(
        create_interactive_cube(pose_world, "1"),
        [=, this](const visualization_msgs::InteractiveMarkerFeedbackConstPtr
                      &feedback) {
          if (feedback->event_type ==
              visualization_msgs::InteractiveMarkerFeedback::POSE_UPDATE) {
            publish_pose(pose1_publisher, feedback->pose,
                         feedback->header.frame_id);
            this->pose1.header = feedback->header;
            this->pose1.pose = feedback->pose;
          }
        });
  }

  server->applyChanges();
}

void Circle::save(rviz::Config config) const { rviz::Panel::save(config); }

void Circle::load(const rviz::Config &config) { rviz::Panel::load(config); }
} // namespace lmpcpoly_rviz_extras

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(lmpcpoly_rviz_extras::Circle, rviz::Panel)
