#pragma once

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#include <rviz/panel.h>
#endif

#include <QComboBox>
#include <QPushButton>
#include <QTimer>
#include <QVBoxLayout>

#include <interactive_markers/interactive_marker_server.h>
#include <tf/transform_listener.h>

#include <geometry_msgs/PoseStamped.h>

class QLineEdit;
class QSpinBox;
class QLabel;

namespace lmpcpoly_rviz_extras {
class PickNPlace : public rviz::Panel {
  Q_OBJECT
public:
  explicit PickNPlace(QWidget *parent = 0);

  virtual void load(const rviz::Config &config);
  virtual void save(rviz::Config config) const;

public Q_SLOTS:

protected Q_SLOTS:
  void pushButtonCallback();

protected:
  void add_custom_slider(std::string title, int slider_max, int slider_interval,
                         double current_value, double real_max,
                         double *msg_variable, QVBoxLayout *layout);

  void add_custom_scientific_slider(std::string title, double current_value,
                                    double *msg_variable, QVBoxLayout *layout);

  void reset_timer();

  QPushButton *push_button;
  // The ROS node handle.
  ros::NodeHandle nh;

  double marker_delay = 10;
  std::unique_ptr<QTimer> timer;
  geometry_msgs::PoseStamped pose1;
  geometry_msgs::PoseStamped pose2;
  int current_marker = 1;

  QComboBox *topic_combo;

  ros::Publisher pose1_publisher;
  ros::Publisher pose2_publisher;
  ros::Publisher pose_picknplace_publisher;
  std::unique_ptr<interactive_markers::InteractiveMarkerServer> server;

  tf::TransformListener listener;
};

} // namespace lmpcpoly_rviz_extras
