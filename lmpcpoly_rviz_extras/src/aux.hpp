#pragma once

#include <geometry_msgs/Pose.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Quaternion.h>
#include <ros/ros.h>
#include <string>

#include <interactive_markers/interactive_marker_server.h>

namespace lmpcpoly_rviz_extras {
geometry_msgs::Quaternion quaternion(double x, double y, double z,
                                     double w = 1);

double proportional_value(long int max, long int val, double realMax);
long int proportional_int(long int max, double val, double realMax);
double proportional_value_min(long int min, long int max, long int val,
                              double realMin, double realMax);
long int proportional_int_min(long int min, long int max, double val,
                              double realMin, double realMax);
void publish_pose(ros::Publisher &publisher, geometry_msgs::Pose pose,
                  std::string frame_id);
void add_rotation_control(visualization_msgs::InteractiveMarker &int_marker,
                          geometry_msgs::Quaternion quat);
void add_move_control(visualization_msgs::InteractiveMarker &int_marker,
                      geometry_msgs::Quaternion quat);
void add_axis_full_control(visualization_msgs::InteractiveMarker &int_marker,
                           geometry_msgs::Quaternion quat);
visualization_msgs::InteractiveMarker
create_interactive_cube(geometry_msgs::Pose pose_world, std::string name);
visualization_msgs::InteractiveMarker
create_interactive_arrow(geometry_msgs::Pose pose_world, std::string name);

} // namespace lmpcpoly_rviz_extras
