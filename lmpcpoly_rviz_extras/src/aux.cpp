#include "aux.hpp"

// std::string run(const char *cmd) {
//   char buffer[128];
//   std::string result = "";
//   FILE *pipe = popen(cmd, "r");
//   if (!pipe)
//     throw std::runtime_error("popen() failed!");
//   try {
//     while (fgets(buffer, sizeof buffer, pipe) != NULL) {
//       result += buffer;
//     }
//   } catch (...) {
//     pclose(pipe);
//     throw;
//   }
//   pclose(pipe);
//   return result;
// }

namespace lmpcpoly_rviz_extras {
geometry_msgs::Quaternion quaternion(double x, double y, double z, double w) {
  geometry_msgs::Quaternion quat;
  quat.x = x;
  quat.y = y;
  quat.z = z;
  quat.w = w;
  return quat;
}

double proportional_value(long int max, long int val, double realMax) {
  double delta = (max - 1);
  return (val / delta) * realMax;
}

long int proportional_int(long int max, double val, double realMax) {
  double delta = (max - 1);
  return (val * delta) / realMax;
}

double proportional_value_min(long int min, long int max, long int val,
                              double realMin, double realMax) {
  double delta = (max - min);
  return (val / delta) * (realMax - realMin);
}

long int proportional_int_min(long int min, long int max, double val,
                              double realMin, double realMax) {
  double delta = (max - min);
  return (val * delta) / (realMax - realMin);
}

void publish_pose(ros::Publisher &publisher, geometry_msgs::Pose pose,
                  std::string frame_id) {
  geometry_msgs::PoseStamped msg;
  msg.header.stamp = ros::Time::now();
  msg.header.frame_id = frame_id;
  msg.pose = pose;
  publisher.publish(msg);
}

void add_rotation_control(visualization_msgs::InteractiveMarker &int_marker,
                          geometry_msgs::Quaternion quat) {
  visualization_msgs::InteractiveMarkerControl rotate_control;
  rotate_control.name = "rotate_x";
  // rotate_control.always_visible = true;
  rotate_control.orientation = quat;
  rotate_control.interaction_mode =
      visualization_msgs::InteractiveMarkerControl::ROTATE_AXIS;
  int_marker.controls.push_back(rotate_control);
}

void add_move_control(visualization_msgs::InteractiveMarker &int_marker,
                      geometry_msgs::Quaternion quat) {
  visualization_msgs::InteractiveMarkerControl move_control;
  move_control.name = "move_x";
  move_control.orientation = quat;
  move_control.interaction_mode =
      visualization_msgs::InteractiveMarkerControl::MOVE_AXIS;
  int_marker.controls.push_back(move_control);
}

void add_axis_full_control(visualization_msgs::InteractiveMarker &int_marker,
                           geometry_msgs::Quaternion quat) {
  add_rotation_control(int_marker, quat);
  add_move_control(int_marker, quat);
}

visualization_msgs::InteractiveMarker
create_interactive_cube(geometry_msgs::Pose pose_world, std::string name) {

  // create an interactive marker for our server
  visualization_msgs::InteractiveMarker int_marker;
  int_marker.header.frame_id = "world";
  int_marker.header.stamp = ros::Time::now();
  int_marker.scale = 0.3;
  int_marker.name = name;
  int_marker.pose = pose_world;

  // create a grey box marker
  visualization_msgs::Marker box_marker;
  box_marker.type = visualization_msgs::Marker::CUBE;
  box_marker.scale.x = 0.15;
  box_marker.scale.y = 0.15;
  box_marker.scale.z = 0.15;
  box_marker.color.r = 0.2;
  box_marker.color.g = 0.2;
  box_marker.color.b = 0.5;
  box_marker.color.a = 0.5;

  {
    visualization_msgs::InteractiveMarkerControl box_control;
    box_control.always_visible = true;
    box_control.interaction_mode =
        visualization_msgs::InteractiveMarkerControl::MOVE_3D;
    box_control.markers.push_back(box_marker);
    int_marker.controls.push_back(box_control);
  }

  add_axis_full_control(int_marker, quaternion(1, 0, 0));
  add_axis_full_control(int_marker, quaternion(0, 1, 0));
  add_axis_full_control(int_marker, quaternion(0, 0, 1));

  return int_marker;
}

visualization_msgs::InteractiveMarker
create_interactive_arrow(geometry_msgs::Pose pose_world, std::string name) {

  // create an interactive marker for our server
  visualization_msgs::InteractiveMarker int_marker;
  int_marker.header.frame_id = "world";
  int_marker.header.stamp = ros::Time::now();
  int_marker.scale = 0.3;
  int_marker.name = name;
  int_marker.pose = pose_world;

  // create a grey box marker
  visualization_msgs::Marker marker;
  marker.type = visualization_msgs::Marker::ARROW;
  marker.scale.x = 0.3;
  marker.scale.y = 0.05;
  marker.scale.z = 0.05;
  marker.color.r = 0.2;
  marker.color.g = 0.2;
  marker.color.b = 0.5;
  marker.color.a = 0.7;

  {
    visualization_msgs::InteractiveMarkerControl control;
    control.always_visible = true;
    control.interaction_mode =
        visualization_msgs::InteractiveMarkerControl::MOVE_3D;
    control.markers.push_back(marker);
    int_marker.controls.push_back(control);
  }

  add_rotation_control(int_marker, quaternion(1, 0, 0));
  add_move_control(int_marker, quaternion(1, 0, 0));
  add_rotation_control(int_marker, quaternion(0, 1, 0));
  add_rotation_control(int_marker, quaternion(0, 0, 1));

  return int_marker;
}

} // namespace lmpcpoly_rviz_extras
