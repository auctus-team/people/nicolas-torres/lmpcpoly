# from mpl_toolkits.mplot3d.art3d import Poly3DCollection
# from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt
import numpy as np
# from scipy.spatial import ConvexHull
np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)

###################################################################
## add mpcpp dir to path
import os
import sys

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
                            'build')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
sys.path.append(buildabspath + '.note')
print('import mpcpp...')
import pylmpcpoly as mpcpp

print('finished import mpcpp...')
###################################################################

import pycapacity.visual as pcv
import pycapacity.algorithms as pca

nq = 7
ncoords = 3
J = np.random.rand(ncoords, nq)
# J = np.eye(ncoords, nq)
print('J:\n', J)
# J = np.eye(ncoords+1, nq)
# J[3,:] = np.array([-0.57735, -0.57735, 0.57735])
qd = mpcpp.VectorLimits.Ones(nq)
print('qd.min:\n', qd.min.T)
print('qd.max:\n', qd.max.T)

########################################################
# # hp = mpcpp.HPS(qd, J, nu=ncoords, rows=3)
# hp = mpcpp.HPS(qd, J, nu=ncoords, rows=1)
# A = np.vstack([hp.A, -hp.A])
# b = np.hstack([hp.b.max, -hp.b.min])[None].T
########################################################
# A,b = pca.hyper_plane_shift_method(J,qd.min,qd.max)
########################################################
# hd = mpcpp.hps(J,qd)
# A,b = hd.H,hd.d
# b=b[None].T
########################################################
lmi = mpcpp.hps_lmi(J, qd.min, qd.max)
A, b = np.vstack([lmi.A, -lmi.A]), np.hstack([lmi.b.max, lmi.b.min])[None].T
########################################################
# normal = np.array([1, 1, -1])
# A = np.vstack([-np.eye(3), np.eye(3), 3*np.eye(3)])
# Aextra = np.vstack([
#     # np.array([1,1,-1]),
#     normal,
# ])
# A = np.vstack([
#     -np.eye(3), np.eye(3),
#     Aextra
# ])
# amax = 30
# plane_factor=0.1
# b = np.hstack([
#     amax*np.ones(A.shape[0]-Aextra.shape[0]),
#     plane_factor*amax*np.ones(Aextra.shape[0]),
# ])[None].T
########################################################
print('A:', A.shape, '\n', A)
print('b:', b.shape, '\n', b.T)

########################################################
vertex, face_indices = pca.hsapce_to_vertex(A, b)
faces = pca.face_index_to_vertex(vertex, face_indices)
fig = plt.figure()
# ax = fig.gca()
# ax = plt.axes(projection='3d')
# ax = fig.add_subplot(projection='3d')
#plot the polytope
ax = pcv.plot_polytope_faces(
    # ax=ax,
    plt=plt,
    faces=faces,
    alpha=0.3,
    face_color='lightsalmon',
    edge_color='orangered',
    label='polytope')
pcv.plot_polytope_vertex(ax=ax, vertex=vertex, color='gray')
########################################################

# ########################################################
# A,b = pca.hyper_plane_shift_method(J,qd.min,qd.max)
# vertex, face_indices = pca.hsapce_to_vertex(A, b)
# faces = pca.face_index_to_vertex(vertex, face_indices)
# pcv.plot_polytope_faces(
#     ax=ax,
#     plt=plt,
#     faces=faces,
#     alpha = 0.3,
#     face_color='yellow',
#     edge_color='red',
#     label='polytope')
# pcv.plot_polytope_vertex(ax=ax, vertex=vertex, color='blue')
# ########################################################

########################################################
# fig = plt.figure()
# ax = fig.add_subplot(111,projection='3d')
# lim = 30
# for p in np.arange(0, lim, lim/20):
#     ax.scatter(p, p, 0, color='green')
# xx, yy = np.meshgrid(np.arange(-lim,lim, lim/10), np.arange(-lim/2,lim/2, lim/10))
# # x + y -z = 15
# normal = np.array([1, 1, -1])
# z = (plane_factor*amax + normal[0]*xx +normal[1]*yy) / normal[2]
# z = (normal[0]*xx +normal[1]*yy) / normal[2]
# zorigin = 15/normal[2]
# print('zinter:', zorigin)
# ax.scatter(0, 0, zorigin, color='blue')
# # xy11 = 15 +
# ax.plot_surface(xx, yy, z, alpha=0.5)
########################################################

lim = 3
for p in np.arange(0, lim, lim / 10):
    ax.scatter(p, p, 0, color='gray')
ax.scatter(0, 0, 0, color='black')
ax.set_xlabel('X', fontsize=20)
ax.set_ylabel('Y', fontsize=20)
ax.set_zlabel('Z', fontsize=20)
plt.grid()

# ########################################################
# # H=3
# H = 50
# # dt = 20e-3
# dt = 50e-3
# initial = mpcpp.InitialConditions()
# # target_position = 0.1*np.ones(3)
# target_position = np.array([1.2, 1., 0])
# initial.p = np.array([0, -0.2, 0])
# # initial.v = np.array([3, -2, 0])
# initial.v = np.array([-1, 0, 0])
# # target_position = np.random.rand(3)
# # initial.p = np.random.rand(3)
# mpc = mpcpp.MPCAcc(
#     H,
#     dt,
#     solver='osqp',
#     # solver='qpoases',
#     # compute_time=15,
#     compute_time=100,
# )
# params = mpcpp.MPCParams(
#     w_terminal=1000,
#     w_a=1e-5,
# )
# mpc.solve(initial, target_position, params)
# p = mpc.hor.p.reshape(H, 3).T
# v = mpc.hor.v.reshape(H, 3).T
# a = mpc.hor.a.reshape(H, 3).T
# ax.plot(a[0,:], a[1,:], a[2,:], color='green')
# ax.scatter(a[0,:], a[1,:], a[2,:], color='green')
# # for k,ak in enumerate(a.T):
# #     print('k:', k, 'ak:', ak.T)
# ########################################################

plt.show()
