import matplotlib.pyplot as plt
import numpy as np
import os


def setup_latex():
    import matplotlib as mpl
    # mpl.rcParams['font.family'] = 'sans-serif'
    # mpl.rcParams['font.sans-serif'] = ['Arial']
    mpl.rcParams['font.family'] = 'Avenir'
    mpl.rcParams['text.usetex'] = True
    # mpl.rcParams['figure.autolayout'] = True
    mpl.rcParams['font.size'] = 14.5
    mpl.rcParams['axes.linewidth'] = 1
    mpl.rcParams['lines.markersize'] = 5
    mpl.rcParams['text.latex.preamble'] = [
        r'\usepackage{amsmath}', r'\usepackage{amssymb}'
    ]

    mpl.rcParams['font.family'] = 'serif'
    mpl.rcParams['font.serif'] = 'Computer Modern'
    # mpl.verbose.level = 'debug-annoying'


def lighten_color(color, amount=0.5):
    """
    ref: https://stackoverflow.com/a/49601444
    Lightens the given color by multiplying (1-luminosity) by the given amount.
    Input can be matplotlib color string, hex string, or RGB tuple.

    Examples:
    >> lighten_color('g', 0.3)
    >> lighten_color('#F034A3', 0.6)
    >> lighten_color((.3,.55,.1), 0.5)
    """
    import matplotlib.colors as mc
    import colorsys
    try:
        c = mc.cnames[color]
    except:
        c = color
    c = colorsys.rgb_to_hls(*mc.to_rgb(c))
    return colorsys.hls_to_rgb(c[0], 1 - amount * (1 - c[1]), c[2])


def ActTwist(homogeneous, twist):
    return pin.SE3(homogeneous).act(pin.Motion(twist)).vector


def savefig_if_notexists(fig, fpath):
    if not os.path.exists(fpath):
        print('saving {}...'.format(fpath))
        fig.savefig(
            fpath,
            dpi=300,
            bbox_inches="tight",
        )


def find_nearest(array, value):
    idx = np.searchsorted(array, value, side="left")
    if idx > 0 and (idx == len(array) or np.fabs(value - array[idx - 1]) <
                    np.fabs(value - array[idx])):
        return idx - 1
    else:
        return idx


def deriv_unit(unit, type=None):
    if type == 'd':
        unit = r'$\frac{{{unit}}}{{s}}$'.format(unit=unit)
    elif type == 'dd':
        unit = r'$\frac{{{unit}}}{{s^2}}$'.format(unit=unit)
    elif type == 'ddd':
        unit = r'$\frac{{{unit}}}{{s^3}}$'.format(unit=unit)
    unit = '[' + unit + ']'
    return unit


def deriv_title(title, type=None, supind=''):
    if supind != '':
        supind = '^{{{}}}'.format(supind)

    if type == 'd':
        title = r'$\dot{{{title}}}{supind}$'.format(title=title, supind=supind)
    elif type == 'dd':
        title = r'$\ddot{{{title}}}{supind}$'.format(title=title,
                                                     supind=supind)
    elif type == 'ddd':
        title = r'$\dddot{{{title}}}{supind}$'.format(title=title,
                                                      supind=supind)
    else:
        title = r'${title}{supind}$'.format(title=title, supind=supind)

    return title


class History:

    @staticmethod
    def fromHomogeneous(homogeneous_hist):
        hist = History(homogeneous_hist.hist[0][0:-1, 3])
        for t, elem in zip(homogeneous_hist.t[1:], homogeneous_hist.hist[1:]):
            hist.add(t, elem[0:-1, 3])
        return hist

    @staticmethod
    def Errorfrom(hist1, hist2):
        hist = History(hist1.hist[0] - hist2.hist[0])
        shortest = min(len(hist1.hist), len(hist2.hist))
        for k in np.arange(shortest):
            hist.add(hist1.t[k], hist1.hist[k] - hist2.hist[k])
        return hist

    @staticmethod
    def fromHomogeneous(homogeneous_hist):
        hist = History(homogeneous_hist.hist[0][0:-1, 3])
        for t, elem in zip(homogeneous_hist.t[1:], homogeneous_hist.hist[1:]):
            hist.add(t, elem[0:-1, 3])
        return hist

    def __init__(self, elem0, t0=0):
        self.t = [t0]
        self.hist = [elem0]

    def add(self, t, elem):
        self.t.append(t)
        self.hist.append(elem)

    def len(self):
        return len(self.t)

    def initial(self):
        return self.hist[0]

    def last(self):
        return self.hist[-1]

    def at_t(self, t):
        return self.hist[find_nearest(self.t, t)]

    def at(self, ind):
        return self.hist[ind]

    def tnp(self):
        return np.array(self.t)

    def np(self, row=None):
        if row is None:
            return np.vstack(self.hist)
        return np.hstack([x[row] for x in self.hist])

    def plot(self, ax, lbl, ls, clr, lw, alp, row, botvisible):
        plot1(ax, self.tnp(), self.np(row), lbl, ls, clr, lw, alp, botvisible)


def plot1(ax, t, vector, lbl, ls, clr, lw, alp, botvisible=False):
    if isinstance(ax, plt.Figure):
        ax = ax.gca()
    ax.plot(t[:vector.shape[0]],
            vector,
            label=lbl,
            linestyle=ls,
            linewidth=lw * 0.4,
            color=clr,
            alpha=alp)
    alp = alp + .2
    # if alp > 1:
    #     alp = 1
    # ax.scatter(
    #     t[:vector.shape[0]],
    #     vector,
    #     color=clr,
    #     marker='x',
    #     # clip_on=False,
    #     s=5,
    #     alpha=alp)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(botvisible)


def plot_dashed_zero(lw, t, vector, ax, label='ref', color='black', alpha=0.3):
    vector = [0 for x in t]
    ax.plot(t,
            vector,
            label=label,
            linestyle='dashed',
            linewidth=lw,
            color=color,
            alpha=alpha)


##################################################################
def save_xyz(position_real,
             position_desired,
             position_target,
             vel_desired,
             acc_desired=None,
             jerk_desired=None,
             single_coord=None,
             separate_coords=False,
             show_position_real_ruc=False,
             ruckig_list=[]):
    coords = ['x', 'y', 'z']
    coords_color = ['firebrick', 'forestgreen', 'steelblue']
    columns = 3
    if not separate_coords:
        columns = 1
    rows = 3

    ruckig_jerk_provided = ruckig_list and len(ruckig_list) > 4
    if jerk_desired is not None or ruckig_jerk_provided:
        rows = 4

    fig, axes = plt.subplots(
        rows,
        columns,
        num=0,
        # sharex='col',
        sharex='all',
        sharey='row',
        gridspec_kw={
            # 'hspace': 0.18,
            'hspace': 0.05,
            'wspace': 0
        },
        squeeze=True,
    )
    if columns == 1:
        axes = axes[None].T

    base_alpha = 0.99
    base_alpha_des = 0.7
    base_lw = 5
    legend_xy = (0.85, 1.15)
    legend_loc = 'best'
    handletextpad = 0.25
    labelspacing = -0.03

    to_iterate = np.arange(len(coords))
    if single_coord is not None:
        to_iterate = np.arange(1) + 1
    for icoord in to_iterate:
        coord = coords[icoord]
        coord_color = coords_color[icoord]
        coord_color_des = lighten_color(coord_color, .71)
        coord_color_desruc = lighten_color(coord_color_des, 1.7)
        # ax[row,i].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        print('i:', icoord)
        col = icoord
        if col > columns - 1:
            col = columns - 1
        pos_lw = base_lw
        ax = axes[0, col]
        ax.grid()
        if single_coord is not None or icoord == 0:
            ax.set_ylabel(deriv_unit('m', ''), rotation=0)
        # ax.set_xlabel('t [s]')
        position_real.plot(ax,
                           lbl=deriv_title(coord, ''),
                           ls='dashed',
                           clr=coord_color,
                           lw=pos_lw,
                           alp=base_alpha,
                           row=icoord,
                           botvisible=False)
        position_desired.plot(ax,
                              lbl=deriv_title(coord, supind='des'),
                              ls='solid',
                              clr=coord_color_des,
                              lw=pos_lw,
                              alp=base_alpha_des,
                              row=icoord,
                              botvisible=False)
        if ruckig_list:
            if show_position_real_ruc:
                ruckig_list[0].plot(ax,
                                    lbl=deriv_title(coord, supind='ruckig'),
                                    ls='solid',
                                    clr='r',
                                    lw=pos_lw * 0.8,
                                    alp=base_alpha_des,
                                    row=icoord,
                                    botvisible=False)
            ruckig_list[1].plot(ax,
                                lbl=deriv_title(coord, supind='ruckig'),
                                ls='solid',
                                clr=coord_color_desruc,
                                lw=pos_lw,
                                alp=base_alpha_des,
                                row=icoord,
                                botvisible=False)
        position_target.plot(ax,
                             lbl=coords[icoord] + '*',
                             ls='dashed',
                             clr='k',
                             lw=pos_lw * 0.7,
                             alp=0.7,
                             row=icoord,
                             botvisible=False)
        ax.legend(
            shadow=True,
            fancybox=True,
            frameon=False,
            loc=legend_loc,
            handletextpad=handletextpad,
            labelspacing=labelspacing,
            # bbox_to_anchor=legend_xy,
        )
        # savefig_if_notexists(ax, 'position.png')
        #####################################################################
        # velocity
        ax = axes[1, col]
        ax.grid()
        # ax[1,i].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        # ax.set_title(deriv_title(coords[i], 'd'))
        if single_coord is not None or icoord == 0:
            ax.set_ylabel(deriv_unit('m', 'd'), rotation=0)
        # ax.set_xlabel('t [s]')
        vel_desired.plot(ax,
                         lbl=deriv_title(coord, 'd', supind='des'),
                         ls='solid',
                         clr=coord_color_des,
                         lw=base_lw,
                         alp=base_alpha_des,
                         row=icoord,
                         botvisible=False)
        if ruckig_list:
            ruckig_list[2].plot(ax,
                                lbl=deriv_title(coord, 'd', supind='ruckig'),
                                ls='solid',
                                clr=coord_color_desruc,
                                lw=base_lw,
                                alp=base_alpha_des,
                                row=icoord,
                                botvisible=False)
        ax.legend(
            shadow=True,
            fancybox=True,
            frameon=False,
            loc=legend_loc,
            handletextpad=handletextpad,
            # bbox_to_anchor=legend_xy,
        )
        # savefig_if_notexists(ax, 'velocity.png')
        #####################################################################
        # acceleration
        ax = axes[2, col]
        ax.grid()
        # ax[2,i].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
        # ax.set_title(deriv_title(coords[i], 'dd'))
        if single_coord is not None or icoord == 0:
            ax.set_ylabel(deriv_unit('m', 'dd'), rotation=0)
        if jerk_desired is None:
            ax.set_xlabel('t [s]')
        acc_desired.plot(ax,
                         lbl=deriv_title(coord, 'dd', supind='des'),
                         ls='solid',
                         clr=coord_color_des,
                         lw=base_lw,
                         alp=base_alpha_des,
                         row=icoord,
                         botvisible=jerk_desired is None)
        if ruckig_list:
            ruckig_list[3].plot(ax,
                                lbl=deriv_title(coord, 'dd', supind='ruckig'),
                                ls='solid',
                                clr=coord_color_desruc,
                                lw=base_lw,
                                alp=base_alpha_des,
                                row=icoord,
                                botvisible=jerk_desired is None)
        ax.legend(
            shadow=True,
            fancybox=True,
            frameon=False,
            # loc=legend_loc,
            loc='upper right',
            handletextpad=handletextpad,
            # bbox_to_anchor=legend_xy,
            bbox_to_anchor=(1, 1.15),
        )
        # savefig_if_notexists(ax, 'acceleration.png')
        #####################################################################
        # jerk
        if rows == 4:
            ax = axes[3, col]
            ax.grid()
            # ax[row,i].set_title(deriv_title(coords[i], 'ddd'))
            # ax[row,i].ticklabel_format(axis='y', style='sci', scilimits=(0, 0))
            if single_coord is not None or icoord == 0:
                ax.set_ylabel(deriv_unit('m', 'ddd'), rotation=0)
            ax.set_xlabel('t [s]')
            if jerk_desired is not None:
                jerk_desired.plot(
                    ax,
                    # lbl='jerk ' + coords[i],
                    lbl=deriv_title(coord, 'ddd', supind='des'),
                    ls='solid',
                    clr=coord_color_des,
                    lw=base_lw,
                    alp=base_alpha_des,
                    row=icoord,
                    botvisible=True)

            if ruckig_jerk_provided:
                ruckig_list[4].plot(ax,
                                    lbl=deriv_title(coord,
                                                    'ddd',
                                                    supind='ruckig'),
                                    ls='solid',
                                    clr=coord_color_desruc,
                                    lw=base_lw,
                                    alp=base_alpha_des,
                                    row=icoord,
                                    botvisible=True)
            ax.legend(
                shadow=True,
                fancybox=True,
                frameon=False,
                loc=legend_loc,
                handletextpad=handletextpad,
                # bbox_to_anchor=legend_xy,
            )
            # savefig_if_notexists(ax, 'jerk.png')

    # tikzplotlib.save("traj_y.tex")

    fig.align_ylabels()
    savefig_if_notexists(fig, 'traj_y.png')
