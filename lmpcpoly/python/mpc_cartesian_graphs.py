import time
import traceback
from pdb import pm
import matplotlib.pyplot as plt
import matplotlib as mpl
import aux

import sys
import os

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)
###################################################################
## add mpcpp dir to path
import os
import sys

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
buildabspath = os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    '../../../../'  # workspace build/
    + 'build/lmpcpoly')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
print('import mpcpp...')
import pylmpcpoly as mpcpp

print('finished import mpcpp...')
###################################################################

# mpl.rcParams['font.family'] = 'sans-serif'
# mpl.rcParams['font.sans-serif'] = ['Arial']
mpl.rcParams['font.family'] = 'Avenir'
mpl.rcParams['text.usetex'] = True
# mpl.rcParams['figure.autolayout'] = True
mpl.rcParams['font.size'] = 20
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 5

import numpy as np

np.set_printoptions(
    edgeitems=4,
    linewidth=130,
    # suppress=True,
    threshold=1000,
    precision=5)
import scipy as sp
import pinocchio as pin
import csv


##########################################################################
def stringToMotion(mystr):
    return pin.Motion(np.array([float(x) for x in mystr.split()]))


pose0_log = stringToMotion(
    '0.81813072  0.73563557 -0.32793589  2.87568122 -1.19176581 -0.10189045')

start_pose = pin.exp(pose0_log)

objl = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.6, 0.5, -0.2]))
    start_pose.translation + np.array([-.05, -0.1, -.3]))
# start_pose.translation + np.array([-.05, -2.1, -.3]))
objr = pin.SE3(
    start_pose.rotation,
    # start_pose.translation + np.array([-0.1, 0.5, -0.2]))
    start_pose.translation + np.array([-.05, 0.1, -.3]))
# start_pose.translation + np.array([-.05, 20.1, -.3]))
# start_pose.translation + np.array([-.05, 8.1, -.3]))

print('pos0:', objl.translation)
print('posf:', objr.translation)

#######################################################
#######################################################

ndt = 50
solveparams = mpcpp.SolveParameters()
solveparams.u_scaling_factor = 1
# solveparams.ud_scaling_factor = 0.05
# solveparams.ud_scaling_factor = 0.5
solveparams.ud_scaling_factor = 1
# solveparams.udd_scaling_factor = 1e-4 # 0.65
# solveparams.udd_scaling_factor = 1e-3 # 6.5
solveparams.udd_scaling_factor = 1
# solveparams.u_scaling_factor = 1
# solveparams.ud_scaling_factor = 1
# solveparams.udd_scaling_factor = 1
# umax = np.hstack([1.7, 1.7, 1.7, 2.5, 2.5, 2.5])
umax = np.hstack([1, 1, 1, 2, 2, 2])
# udmax = np.hstack([5, 5, 5, 10, 10, 10])
udmax = np.hstack([0.65, 0.65, 0.65, 1.25, 1.25, 1.25])
# uddmax = np.hstack([6500, 6500, 6500, 12500, 12500, 12500])
uddmax = np.hstack([100, 100, 100, 300, 300, 300]) * 5
solveparams.u = mpcpp.VectorLimits(-umax, umax)
solveparams.ud = mpcpp.VectorLimits(-udmax, udmax)
solveparams.udd = mpcpp.VectorLimits(-uddmax, uddmax)

print('Create mpc.MPCProblem...')
params = mpcpp.HorizonParameters(
    # H=300,
    # H=20,
    H=30,
    ndt=ndt,
)
solver = 'osqp'
mpcprob = mpcpp.Planifier(params, solver=solver)
mpcprob.use_origin_dlog = True
mpcprob.wreg = 100
# mpcprob.wreg = 20
# mpcprob.wreg = 10
mpcprob.wreg = 1
# mpcprob.wreg = 1e-1
# mpcprob.wreg = 1e-2
# mpcprob.wreg = 1e-3
mpcprob.wreg = 0
simulation_duration = 0.7  # [s]
simulation_duration = .097  # [s]
# simulation_duration = .105  # [s]
simulation_duration = .205  # [s]
# simulation_duration = .3  # [s]
# simulation_duration = .8  # [s]
simulation_duration = 1.8  # [s]
# mpc_update_delay = 2
# simulation_duration = 3  # [s]
# simulation_duration = .005  # [s]
# simulation_duration = 3*params.hdt
simulation_duration = mpcprob.input_horizon_duration() - params.dt
mpc_update_delay = 0.02
# mpc_update_delay = 0.1
# mpc_update_delay = .3
# mpc_update_delay = .5
# mpc_update_delay = 3
# mpc_update_delay = 0.01
mpc_update_delay = mpcprob.input_horizon_duration()
# mpc_update_delay = simulation_duration/2
mpc_update_delay = simulation_duration

mpcprob.set_compute_time(1)
# mpcprob.set_compute_time(0.1)
mpcprob.update_solve_parameters(solveparams)
print('Create mpcpp.MPCProblem...')

pose_desired = aux.History(objl.homogeneous)
# pose_desired = aux.History(start_pose.homogeneous)
pose_target = aux.History(objr.homogeneous)

vel_desired = aux.History(pin.Motion.Zero().vector, t0=-params.dt)
acc_desired = aux.History(pin.Motion.Zero().vector, t0=-params.dt)
jerk_desired = aux.History(pin.Motion.Zero().vector, t0=-params.dt)

pose_real = aux.History(pose_desired.last())

t = 0
t_mpc_update = -1  # so that it forces update first time
while t < simulation_duration:

    if t > t_mpc_update:
        t_mpc_update = t + mpc_update_delay
        print('t_mpc_update: {:.2f}s'.format(t_mpc_update))
        start = mpcpp.TimedPose()
        start.t = t
        start.pose = pose_real.last()
        goal = mpcpp.Goal()
        goal.pose = pose_target.initial()
        goal.u0 = vel_desired.last()
        goal.ud0 = acc_desired.last()
        goal.start = start
        horizon = mpcprob.solve(goal, solveparams)
        print('update horizon t: {:.2f}s'.format(t))

    pose_desired.add(t, horizon.pose(t + params.dt))
    pose_target.add(t, pose_target.last())
    vel_prev = horizon.twist(t)
    # vel_prev2 = horizon.u(t - params.dt)

    # udd(k=0) = (ud(k=0) - ud(k=-1)) / dt
    #          = (u(k=0) - 2 u(k=-1) + u(k=-2)) / (dt*dt)
    if vel_desired.len() >= 2:
        jerk_desired.add(
            # t, (vel_prev - 2 * vel_desired.at(-1) + vel_desired.at(-2)) /
            t,
            (horizon.twist(t + params.dt) - 2 * vel_prev + vel_desired.at(-1))
            / (params.dt * params.dt))
    if vel_desired.len() > 1:
        acc_desired.add(t, (vel_prev - vel_desired.last()) / params.dt)
    vel_desired.add(t, vel_prev)

    if t >= params.dt:
        pose_real.add(t, pose_real.last() @ pin.exp(vel_prev * params.dt))
        # pose_real.add(t, (pin.SE3(pose_real.last())*pin.exp(velk)).homogeneous)

    t += params.dt

# t = params.dt
# # t = 0
# acc_desired = aux.History(pin.Motion.Zero().vector, t0=-params.dt)
# while t < simulation_duration:
#     acc_desired.add(
#         t, (vel_desired.at_t(t) - vel_desired.at_t(t - params.dt)) / params.dt)
#     t += params.dt

# t = params.dt
# # t = 0
# jerk_desired = aux.History(pin.Motion.Zero().vector, t0=-params.dt)
# while t < simulation_duration:
#     # jerk_desired.add(
#     #     t, (vel_desired.at_t(t + params.dt) - 2 * vel_desired.at_t(t) +
#     #         vel_desired.at_t(t - params.dt)) / (params.dt * params.dt))
#     jerk_desired.add(
#         t, (acc_desired.at_t(t) - acc_desired.at_t(t - params.dt)) / params.dt)
#     # jerk_desired.add(
#     #     t, (acc_desired.at_t(t + params.dt) - acc_desired.at_t(t)) / params.dt)
#     t += params.dt

#######################################################
#######################################################
# rows = 3

# fig.suptitle('linear')
position_real = aux.History.fromHomogeneous(pose_real)
position_desired = aux.History.fromHomogeneous(pose_desired)
position_target = aux.History.fromHomogeneous(pose_target)
# jerk_desired = None
aux.plot_xyz(0, position_real, position_desired, position_target, vel_desired,
             acc_desired, jerk_desired)
plt.show()

# aux.save_xyz(position_real, position_desired, position_target, vel_desired,
#              acc_desired, jerk_desired)
# plt.show()
