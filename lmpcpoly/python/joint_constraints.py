# from mpl_toolkits.mplot3d.art3d import Poly3DCollection
# from mpl_toolkits.mplot3d import Axes3D
import copy
import sys

import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt

# from scipy.spatial import ConvexHull
np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)

###################################################################
## add mpcpp dir to path
# import os
# import sys

# srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
# print('abspath(__file__):', srcabspath)

# buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
#                             'build')
# print('buildabspath:', buildabspath)
# sys.path.append(buildabspath)
# sys.path.append(buildabspath + '.note')
# print('import mpcpp...')
# import pylmpcpoly as mpcpp

# print('finished import mpcpp...')
###################################################################

PAPER_LIMITS = False
PAPER_LIMITS = True

LOWER_BOUND_PLOT = False
LOW_RANGE_PLOT = True

# LOWER_BOUND_PLOT = True
# LOW_RANGE_PLOT = False

LINEWIDTH = 3

ncolors = 1
ncolors_taylor = 1
ncolors_rub = 2
ncolors_pret = 1


def colorFader(
        c1,
        c2,
        mix=0
):  #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1 = np.array(mpl.colors.to_rgb(c1))
    c2 = np.array(mpl.colors.to_rgb(c2))
    return mpl.colors.to_hex((1 - mix) * c1 + mix * c2)


# c1='#1f77b4' #blue
# c2='green' #green
# n=500

# fig, ax = plt.subplots(figsize=(8, 5))
# for x in range(n+1):
#     ax.axvline(x, color=colorFader(c1,c2,x/n), linewidth=4)
# plt.show()

colors_mix = np.linspace(0, 1, ncolors)
colors_over = [colorFader('navy', 'darkgreen', mix) for mix in colors_mix][:-1]
colors_mid = ['green']
colors_under = [colorFader('darkgreen', 'darkred', mix)
                for mix in colors_mix][1:]
colors = colors_over + colors_mid + colors_under

colors_taylor = [
    colorFader('saddlebrown', 'mediumorchid', mix)
    for mix in np.linspace(0, 1, ncolors_taylor)
]

colors_rub = [
    colorFader('magenta', 'pink', mix)
    for mix in np.linspace(0, 1, ncolors_rub)
]

colors_pret = [
    colorFader('cyan', 'dodgerblue', mix)
    for mix in np.linspace(0, 1, ncolors_pret)
]

# print(colors_over)
# print(colors_under)
# fig, ax = plt.subplots(figsize=(8, 5))
# for k, c in enumerate(colors):
#     ax.axvline(k, color=c, linewidth=4)
# plt.show()


class JointLimits:

    def __init__(self, njoint=0):
        q_max_panda = np.array(
            [2.8973, 1.7628, 2.8973, -0.0698, 2.8973, 3.7525, 2.8973])
        q_min_panda = np.array(
            [-2.8973, -1.7628, -2.8973, -3.0718, -2.8973, -0.0175, -2.8973])
        v_max_panda = np.array(
            [2.1750, 2.1750, 2.1750, 2.1750, 2.6100, 2.6100, 2.6100])
        a_max_panda = np.array([15, 7.5, 10, 12.5, 15, 20, 20])
        j_max_panda = np.array([7500, 3750, 5000, 6250, 7500, 10000, 10000])

        if PAPER_LIMITS:
            q_max_panda[0], q_min_panda[0] = 1, -1
            v_max_panda[0] = 5
            a_max_panda[0] = 15
            # a_max_panda[0] = 50
            # j_max_panda[0] = 200
            j_max_panda[0] = 500
            # j_max_panda[0] *= 500

        self.q_max = q_max_panda[njoint]
        self.q_min = q_min_panda[njoint]
        self.v_max = v_max_panda[njoint]
        self.a_max = a_max_panda[njoint]
        self.j_max = j_max_panda[njoint]


class PlotLimits(JointLimits):

    def __init__(self,
                 njoints=0,
                 label=None,
                 pattern='dashed',
                 color='black',
                 lims=None):
        super().__init__(njoints)
        self.label = label
        self.color = color
        self.pattern = pattern

        if lims is not None:
            self.j_max = lims.j_max
            self.a_max = lims.a_max
            self.v_max = lims.v_max
            self.q_max = lims.q_max
            self.q_min = lims.q_min

    def compat_vfromq(self, q0):
        return [-self.vmax, self.v_max]

    def plot(self, ax, a0=0, N=1000, alpha=1):
        ################################################################
        # box limits
        # print('v max:', self.v_max)
        # print('q min, max:', self.q_min, self.q_max)
        qpoints = np.linspace(self.q_min, self.q_max, N)
        vpoints = np.linspace(-self.v_max, self.v_max, N)
        if LOW_RANGE_PLOT:
            vpoints = np.linspace(-self.v_max, self.v_max, N)
            qpoints = np.linspace(self.q_max - (self.q_max - self.q_min) / 1.8,
                                  self.q_max, N)
        style = {
            'color': self.color,
            'linewidth': LINEWIDTH,
            'linestyle': self.pattern,
            'alpha': alpha,
            # 'marker': 'o',
        }

        vmin, vmax = [], []
        for q0 in qpoints:
            vminmax_q0 = self.compat_vfromq(q0)
            vmin.append(np.max([vminmax_q0[0], -self.v_max]))
            vmax.append(np.min([vminmax_q0[1], self.v_max]))
        vmin = np.array(vmin)
        vmax = np.array(vmax)

        # if LOWER_BOUND_PLOT:
        #     ax.plot(vmin, qpoints, **style)
        # ax.plot(vmax, qpoints, label=self.label, **style)
        if LOWER_BOUND_PLOT:
            ax.plot(qpoints, vmin, **style)
        ax.plot(qpoints, vmax, label=self.label, **style)
        ax.set_xlabel('[v]', fontsize=20)
        ###########################################################
        ax.set_ylabel('[q]', fontsize=20, rotation=0)


class FirstTaylorLimits(PlotLimits):

    def __init__(self, T=0.015, pattern='dotted', *args, **kwargs):
        super().__init__(*args, pattern=pattern, **kwargs)
        self.T = T

    def compat_vfromq(self, q0):
        return [(self.q_min - q0) / self.T, (self.q_max - q0) / self.T]


class RubrechtLimits(FirstTaylorLimits):
    # appendix: https://hal.science/hal-00504202v1/document
    # thesis: https://theses.hal.science/tel-00654514v1/document
    # page 31/46 eq 2.31/32
    # https://hal.science/hal-00719855/document

    def __init__(self, T, label, pattern='dotted', *args, **kwargs):
        label = 'rubrecht ' + label
        super().__init__(*args, T=T, label=label, pattern=pattern, **kwargs)

    def compat_vfromq(self, q0):
        aM = self.a_max
        am = -aM
        qM = self.q_max
        qm = self.q_min

        s1 = -np.sqrt(-2 * am * (qM - q0)) / (am * self.T)
        vmax = ((qM - q0) - 0.5 * (s1**2 - s1) * am *
                (self.T**2)) / ((s1 + 1) * self.T)

        s2 = np.sqrt(-2 * aM * (qm - q0)) / (aM * self.T)
        vmin = ((qm - q0) - 0.5 * (s2**2 - s2) * aM *
                (self.T**2)) / ((s2 + 1) * self.T)
        print('s1:', s1, 'vmin', vmin)
        print('s2:', s2, 'vmax', vmax)
        return [vmin, vmax]


class PreteLimits(PlotLimits):
    # https://hal.science/hal-01356989v3/document

    def __init__(self, label, pattern='dotted', *args, **kwargs):
        label = 'prete ' + label
        super().__init__(*args, label=label, pattern=pattern, **kwargs)

    def compat_vfromq(self, q0):
        vmax = np.sqrt(2 * self.a_max * (self.q_max - q0))
        vmin = -np.sqrt(2 * self.a_max * (q0 - self.q_min))
        return [vmin, vmax]


class JerkBoundLimits(PlotLimits):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def compatibilize(self, v0, a0=0):
        print('-' * 20)
        if np.abs(a0) > self.a_max:
            print('a0:', a0, 'a_max:', self.a_max)
            print('Error: a0 outside of valid range')

        if np.abs(v0) > self.v_max:
            print('v0:', v0, 'v_max:', self.v_max)
            print('Error: v0 outside of valid range')

        if v0 == 0 and a0 == 0:
            return copy.copy(self)

        delta_v = 0
        delta_p = 0

        ##############################################
        # ACCELERATION REVERSING NECESSARY
        am, jm = self.a_max, self.j_max
        if a0 > 0:
            am, jm = -self.a_max, -self.j_max

        if (v0 < 0 and a0 < 0) or (v0 > 0 and a0 > 0):
            # print('acceleration reversing', v0, a0)
            tr = -a0 / jm
            if tr < 0 and np.abs(jm) < 20000:
                print('WARNING: tr NEGATIVE:', tr, 'a0:', a0, 'jm:', jm)
            delta_v += a0 * tr + jm * np.square(tr) / 2
            delta_p -= v0 * tr + a0 * np.square(tr) / 2 + jm * (tr**3) / 6
            # delta_v += (a0**2) / (2 * jm)
            # delta_p += v0 * tr + 2 * (a0**3) / (6 * (jm**2))
            a0 = 0
            v0 += delta_v

        ##############################################
        # DECELERATION STAGE
        if (v0 < 0 and a0 < 0) or (v0 > 0 and a0 > 0):
            sys.exit(
                'CRITICAL ERROR: reverse condition should not happen here',
                'a0:', a0, 'v0:', v0, 'am:', am, 'jm:', jm)

        # TODO: WHYYYY ????
        # a0 = -a0

        am, jm = self.a_max, self.j_max
        if v0 > 0:
            am, jm = -self.a_max, -self.j_max

        # print('-' * 10)
        # print('a_max:', self.a_max)
        # print('a_max^2:', np.square(self.a_max))
        # print('j_max:', self.j_max)
        # print('v0:', v0)
        # condition = np.abs(
        #     (2 * np.square(self.a_max) - np.square(a0)) / (2 * self.j_max))
        max_triang_v0 = (2 * np.square(am) - np.square(a0)) / (2 * jm)
        print('cond:{:.4f}, v0:{:.4f}'.format(max_triang_v0, v0))
        # TODO: not working
        # if max_triang_v0 > v0:
        if False:
            # if np.abs(max_triang_v0) >= np.abs(v0):
            # if False:
            ####################
            # triangular deceleration
            print('triangular v0:', v0)
            # ad = np.sqrt(((-v0) * 2 * jm + np.square(a0)) / 2)
            ad = np.sqrt((v0 * 2 * jm + np.square(a0)) / 2)
            print('ad:{:.2f}'.format(ad))
            if np.isnan(ad):
                print('ad nan:', ad, 'v0:', v0)
                print('ad:', ad)
                print('a0:', a0)
                print('v0:', v0)
                print('a_max:', am)
                print('j_max:', jm)
            t1 = (ad - a0) / jm
            t3 = ad / jm
            eps = 1e-5
            if t1 + eps < 0 or t3 + eps < 0:
                print('CRITICAL ERROR: triangular negative time')
                print('a0:', a0)
                print('v0:', v0)
                print('a_max:', am)
                print('j_max:', jm)
                print('t1:{:.5f}, t3:{:.5f}'.format(t1, t3))
                # sys.exit('CRITICAL ERROR: triangular negative time')

            # stage
            # delta_v += (np.square(ad) - np.square(a0)) / (
            #     2 * self.j_max) + np.square(ad) / (2 * self.j_max)
            # print('triang delta_v:', delta_v)

            delta_p += v0 * t1 + a0 * (t1**2) / 2 + jm * (t1**3) / 6
            delta_p += ad * (t3**2) / 2 - jm * (t3**3) / 6

            # delta_p += v0 * t1 + a0 * np.square(t1) / 2 + self.j_max * (t1** 3) / 6
            # delta_p += ad * (t3**2) / 2 - self.j_max * (t3**3) / 6
            # print('t1:{:.3f}, t3:{:.3f}'.format(t1, t3))
            # print('triang delta_p:', delta_p)
            ####################
        else:
            # trapezoidal deceleration
            t1 = (am - a0) / jm
            t3 = am / jm
            t_stop = -v0 / am + (2 * np.square(am) - 2 * a0 * am +
                                 np.square(a0)) / (2 * jm * am)
            t2_stop = t_stop - t1 - t3
            # t2 = t2_stop

            # t2 = v0 / am + ((a0**2) - 2 * (am**2)) / (2 * jm * am)
            # print(-am*t3/2, - a0*t1, - (am-a0)*t1/2)
            # t2 = v0 * t1 + ((a0**2) - 2 * (am**2)) / (2 * jm * am)
            t2 = (v0 - am * t3 / 2 - a0 * t1 - (am - a0) * t1 / 2) / am

            eps = 1e-5
            if t1 + eps < 0 or t2 + eps < 0 or t3 + eps < 0:
                print('CRITICAL ERROR: TAP negative time')
                print('t_stop:', t_stop)
                print('t2_stop:', t2_stop)
                print('a0:', a0)
                print('v0:', v0)
                print('a_max:', am)
                print('j_max:', jm)
                print('t1:{:.5f}, t2:{:.5f}, t3:{:.5f}'.format(t1, t2, t3))
                # sys.exit('CRITICAL ERROR: TAP negative time')

            # stage
            # delta_v += (np.square(self.a_max) - np.square(a0)) / (2 * self.j_max)
            # delta_v += t2 * self.a_max + (np.square(self.a_max)) / (2 * self.j_max)

            delta_p += v0 * t1 + a0 * np.square(t1) / 2 + jm * (
                t1**3) / 6 + am * (t2**2) / 2
            delta_p += am * (t3**2) / 2 - jm * (t3**3) / 6

            # integral of a(t)
            # delta_v += a0 *t1 + self.j_max*np.square(t1)/2 + self.a_max*(t2)
            # delta_v += +self.a_max*(t3)-self.j_max*np.square(t3)/2
            # area of a(t)
            # delta_v += (self.a_max-a0) *t1/2 + a0*t1
            # delta_v += self.a_max*(t2)
            # delta_v += self.a_max*(t3)/2
        # print('delta_v', delta_v)

        if np.isnan(delta_p):
            # print('delta_p nan', 'v0:', v0, 'a_max:', self.a_max, 'j_max:',
            #       self.j_max)
            delta_p = 0

        lim = copy.copy(self)
        # lim.v_max = lim.v_max + delta_v
        # lim.q_min = lim.q_min + delta_p
        # lim.q_max = lim.q_max + delta_p

        lim.q_min = np.clip(lim.q_min + delta_p, lim.q_min, lim.q_max)
        lim.q_max = np.clip(lim.q_max + delta_p, lim.q_min, lim.q_max)
        lim.v_max = np.min([lim.v_max - delta_v, lim.v_max])

        # if v0 < 0:
        #     q_min = lim.q_min + delta_p
        #     lim.q_min = np.max([q_min, lim.q_min])
        # else:
        #     q_max = lim.q_max - delta_p
        #     lim.q_max = np.min([q_max, lim.q_max])
        # lim.v_max = np.min([v_max, lim.v_max])
        return lim

    def plot(self, ax, a0=0, N=1000, alpha=1):
        ################################################################
        # box limits
        # print('v max:', self.v_max)
        # print('q min, max:', self.q_min, self.q_max)
        qpoints = np.linspace(self.q_min, self.q_max, N)
        vpoints = np.linspace(-self.v_max, self.v_max, N)
        if LOW_RANGE_PLOT:
            vpoints = np.linspace(0, self.v_max, N)
        style = {
            'color': self.color,
            'linewidth': LINEWIDTH,
            'linestyle': self.pattern,
            'alpha': alpha,
            # 'marker': 'o',
        }
        # ax.plot(self.q_min * np.ones_like(vpoints), vpoints, **style)
        # ax.plot(self.q_max * np.ones_like(vpoints), vpoints, **style)

        # ax.plot(qpoints, -self.v_max * np.ones_like(qpoints), **style)
        # ax.plot(qpoints, self.v_max * np.ones_like(qpoints), **style)
        vpoints_compat = []
        qpoints_max_compat = []
        qpoints_min_compat = []
        for v0 in vpoints:
            lim = self.compatibilize(v0, a0)
            vpoints_compat.append(lim.v_max)
            qpoints_max_compat.append(lim.q_max)
            qpoints_min_compat.append(lim.q_min)

        vpoints_compat = np.array(vpoints_compat)
        qpoints_min_compat = np.array(qpoints_min_compat)
        qpoints_max_compat = np.array(qpoints_max_compat)

        # if LOWER_BOUND_PLOT:
        #     ax.plot(vpoints, qpoints_min_compat, **style)
        # ax.plot(vpoints, qpoints_max_compat, label=self.label, **style)
        if LOWER_BOUND_PLOT:
            ax.plot(qpoints_min_compat, vpoints, **style)
        ax.plot(qpoints_max_compat, vpoints, label=self.label, **style)
        ax.set_xlabel('[q]', fontsize=20)
        ############################################################
        ax.set_ylabel('[v]', fontsize=20, rotation=0)


base_compat = JerkBoundLimits()
base_compat.label = '$\infty$ A and J max'


def create_jerkbound_trajs(label_append=''):
    maxmin = .5
    cap_factor = [
        1
    ] if len(colors) <= 1 else np.linspace(maxmin, -maxmin, len(colors)) + 1
    compats = []
    for k, color in enumerate(colors):
        factor = cap_factor[k]
        compat = copy.copy(base_compat)
        compat.label = '{:.1f}x Amax {}'.format(factor, label_append)
        compat.a_max *= factor
        compat.color = color
        compats.append(compat)
    return compats


def create_comparing_trajs(label_append=''):
    compats = []
    Tmax, Tmin = 0.06, 0.02
    Ts = np.linspace(Tmax, Tmin, len(colors_taylor))
    for k, color in enumerate(colors_taylor):
        T = Ts[k]
        print('taylor:', k, color, T)
        compats.append(
            FirstTaylorLimits(
                T=T,
                label='1st Taylor ($q_M-q/\Delta t$),T={:.1f}ms'.format(T *
                                                                        1000),
                color=color,
                lims=base_compat))

    Ts = np.linspace(Tmax, Tmin, len(colors_rub))
    for k, color in enumerate(colors_rub):
        T = Ts[k]
        compats.append(
            RubrechtLimits(T=T,
                           label='T={:.1f}ms'.format(T * 1000),
                           color=color,
                           lims=base_compat))

    for k, color in enumerate(colors_pret):
        compats.append(PreteLimits(label='', color=color, lims=base_compat))

    base_compat.a_max *= 1000000
    base_compat.j_max *= 10000000000
    compats.append(base_compat)
    return compats


fig = plt.figure()
ax = fig.gca()
N = 1000
alphas = [0.7, 1, 0.5]
accs = [0, -14.9]
accs = [0, 14.9, -14.9]
for ka, a0 in enumerate(accs):
    alpha = alphas[ka]
    for compat in create_jerkbound_trajs(label_append='$a_0={}$'.format(a0)):
        compat.plot(
            ax,
            a0=a0,
            alpha=alpha,
            N=N,
        )

alpha = 0.8
for compat in create_comparing_trajs(label_append='$a_0={}$'.format(a0)):
    compat.plot(
        ax,
        a0=a0,
        alpha=alpha,
        N=N,
    )

test = JerkBoundLimits()
result = test.compatibilize(v0=1.4, a0=-14.9)
print('q:', result.q_min, result.q_max, 'vmax:', result.v_max)

ax.legend(loc='upper center',
          bbox_to_anchor=(0.5, 1.09),
          ncol=4,
          fancybox=True,
          shadow=True)
ax.grid(color='gray', linestyle='dashed', dashes=(5, 20))
ax.set_title('phase graph [v] vs [q]'.format(a0))
plt.show()
