import numpy as np
###################################################################
## add mpcpp dir to path
import os
import sys

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
                            'build')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
sys.path.append(buildabspath + '.note')
print('import mpcpp...')
import pylmpcpoly as mpcpp

print('finished import mpcpp...')
###################################################################

restime = mpcpp.test_vec([np.arange(6)] * 2)
print('mpcpp.test_vec: time:', restime.total_ns, 'ns')
print('mpcpp.test_vec: result:', restime.result)

a = mpcpp.MPCAccHorizon()
print(a.p)
