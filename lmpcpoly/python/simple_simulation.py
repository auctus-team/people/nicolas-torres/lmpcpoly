import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from matplotlib.patches import Patch
import aux
# import pycapacity as pc
import pycapacity.algorithms as pca
import pycapacity.visual as pcv
# from pycapacity.algorithms import *

###################################################################
## add mpcpp dir to path
import os
import sys

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
                            'build')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
sys.path.append(buildabspath + '.note')
print('import mpcpp...')
import pylmpcpoly as mpcpp

print('finished import mpcpp...')
###################################################################

import matplotlib as mpl

mpl.rcParams['font.family'] = 'Avenir'
# mpl.rcParams['text.usetex'] = True
mpl.rcParams['font.size'] = 14.5
mpl.rcParams['axes.linewidth'] = 1
mpl.rcParams['lines.markersize'] = 5
# mpl.rcParams['text.latex.preamble'] = [
#     r'\usepackage{amsmath}', r'\usepackage{amssymb}'
# ]
mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.serif'] = 'Computer Modern'


def rotx(th):
    return np.vstack([
        np.array([1, 0, 0]),
        np.array([0, np.cos(th), -np.sin(th)]),
        np.array([0, np.sin(th), np.cos(th)]),
    ])


def roty(th):
    return np.vstack([
        np.array([np.cos(th), 0, np.sin(th)]),
        np.array([0, 1, 0]),
        np.array([-np.sin(th), 0, np.cos(th)]),
    ])


def rotz(th):
    return np.vstack([
        np.array([np.cos(th), -np.sin(th), 0]),
        np.array([np.sin(th), np.cos(th), 0]),
        np.array([0, 0, 1]),
    ])


class Axes:
    axes = []

    @classmethod
    def add(cls, ax):
        cls.axes.append(ax)

    @classmethod
    def grid(cls):
        for ax in cls.axes:
            ax.grid()


def setup_ax(ax, botvisible=True):
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['top'].set_visible(False)
    ax.spines['bottom'].set_visible(botvisible)
    # ax.grid(b=True)
    # ax.grid()
    Axes.add(ax)
    return ax


# show_desired = False
show_desired = True

rows = 6
columns = 1

fig1, axes1 = plt.subplots(
    rows,
    columns,
    num=0,
    # sharex='col',
    sharex='all',
    sharey='row',
    gridspec_kw={
        # 'hspace': 0.18,
        'hspace': 0.05,
        'wspace': 0
    },
    squeeze=True,
)

axes1 = axes1[None].T

########################################################
########################################################
np.random.seed(5)
amax = None
# J = np.random.rand(3,7)
J = np.vstack([
    np.array([
        -5.74194e-05, 0.188164, -5.7407e-05, 0.127869, 2.28401e-06, 0.209619,
        -1.11022e-16
    ]),
    np.array([
        0.55544, -3.14204e-05, 0.555514, -1.28982e-06, 0.209445, -2.90809e-05,
        0
    ]),
    np.array(
        [-0, -0.555439, -5.94415e-08, 0.473065, 6.31248e-05, 0.0889043, 0]),
    np.array([
        0, 0.000166984, -0.000395831, -1.03165e-05, 0.999998, -1.09051e-05,
        0.00430988
    ]),
    np.array([0, 1, 6.60974e-08, -1, -1.03166e-05, -1, 0.000301341]),
    np.array(
        [1, 2.22045e-16, 1, 6.20138e-08, -0.00195248, -0.000301391,
         -0.999991]),
])
qd = mpcpp.VectorLimits()
qd.max = np.array([2.1750, 2.1750, 2.1750, 2.1750, 2.6100, 2.6100, 2.6100])
qd.min = -qd.max
acc_con = mpcpp.hps_lmi(J[:3, :], qd.min, qd.max)
A, b = np.vstack([acc_con.A,
                  -acc_con.A]), np.hstack([acc_con.b.max,
                                           -acc_con.b.min])[None].T

########################################################
########################################################
# amax = 10
# amax = 3
# # amax = 2.5
# # amax = 1
# acc_con = mpcpp.LinearInequalityConstraints()
# acc_con.A = np.eye(3)
# acc_con.b.max = amax * np.ones(acc_con.A.shape[0])
# # acc_con.b.min = acc_con.A.shape[0] * [-np.inf]
# acc_con.b.min = -acc_con.b.max

# A = np.vstack([
#     -acc_con.A,
#     acc_con.A,
# ])
# b = np.hstack([acc_con.b.max, acc_con.b.max])[None].T
# # #-------------------------------------------------------
# normal = np.array([1, 1, -1])
# Aplane = np.vstack([
#     normal,
# ])
# A = np.vstack([A, Aplane])
# # plane_factor=0.8
# plane_factor = 0.2
# bplane = plane_factor * amax * np.ones(Aplane.shape[0])
# b = np.vstack([
#     b,
#     bplane[None].T,
# ])
# acc_con.A = np.vstack([acc_con.A, Aplane])
# acc_con.b.max = np.hstack([
#     acc_con.b.max,
#     bplane,
# ])
# acc_con.b.min = np.hstack([
#     acc_con.b.min,
#     -np.inf * np.ones(bplane.shape[0]),
# ])
#-------------------------------------------------------
#-------------------------------------------------------
# normal = np.array([1, 1, -1])
# normal2 = np.array([1, 1, 1])
# Aplane = np.vstack([normal, normal2])
# A = np.vstack([A, Aplane])
# plane_factor = 0.8
# # bplane = plane_factor*amax*np.ones(Aplane.shape[0])
# bplane = np.array([
#     plane_factor * amax,
#     0.3 * amax,
# ])
# b = np.vstack([
#     b,
#     bplane[None].T,
# ])
# acc_con.A = np.vstack([acc_con.A, Aplane])
# acc_con.b.max = np.hstack([
#     acc_con.b.max,
#     bplane,
# ])
# acc_con.b.min = np.hstack([
#     acc_con.b.min,
#     -np.inf * np.ones(bplane.shape[0]),
# ])

# print('A:', A.shape, '\n', A)
# print('b:', b.shape, '\n', b.T)
########################################################
########################################################
########################################################
########################################################

H = 3
# H = 10
H = 50
H = 100
H = 200
# dt = 20e-3
dt = 20e-3
# dt = 50e-3
initial = mpcpp.InitialConditions()
# target_position = 0.1*np.ones(3)
target_position = np.array([1.2, 1., 0])
initial.p = np.array([0, -0.2, 0])
# target_position = np.array([0.554195, -0.00106819, 0.323162])
# initial.p = np.array([0.554148, -0.00119126, 0.623014])
# initial.v = np.array([3, -2, 0])
# initial.v = np.array([-1, 0, 0])
initial.a = np.array([-1, 1, 0])
# target_position = np.random.rand(3)
# initial.p = np.random.rand(3)
print('mpcpp.MPCParams()')
params = mpcpp.MPCParams(
    w_terminal=1000,
    # w_a=1e-4,
    w_a=1,
)
print('mpcpp.MPCAcc()')
mpc = mpcpp.MPCAcc(
    H,
    dt,
    mpc_params=params,
    # solver='osqp',
    # solver='qpoases',
    solver='qpmad',
    # compute_time=15,
    compute_time=50,
    jerk_enabled=True,
)

print('mpcpp.solve()')
# mpc.update_pmax(2 * np.ones(3))
# mpc.update_vmax(1.7*np.ones(3));
# mpc.update_amax(amax * np.ones(3))
# mpc.update_jmax(100 * np.ones(3))

mpc.con_v.update_mode(2)  # only polytope
mpc.con_v.update_firststep_polytope(acc_con)
mpc.con_v.update_horizon_polytope(acc_con)
# mpc.update_v_polytope(acc_con, only_first_step=True)

# mpc.update_a_polytope(acc_con, only_first_step=True)
# mpc.update_a_polytope(acc_con, only_first_step=False)
mpc.solve(initial, target_position)

# p = mpc.horizon.p.reshape(H, 3).T
# v = mpc.horizon.v.reshape(H, 3).T
# a = mpc.horizon.a.reshape(H, 3).T
p = np.array([mpc.horizon.p_at(i * dt) for i in np.arange(H)]).T
v = np.array([mpc.horizon.v_at(i * dt) for i in np.arange(H)]).T
a = np.array([mpc.horizon.a_at(i * dt) for i in np.arange(H)]).T
j = np.array([mpc.horizon.j_at(i * dt) for i in np.arange(H)]).T

df = {}
df['/lmpcpp_moveit/target_pose/pose/position/x'] = target_position[
    0] * np.ones(p.shape[1])
df['/lmpcpp_moveit/target_pose/pose/position/y'] = target_position[
    1] * np.ones(p.shape[1])
df['/lmpcpp_moveit/target_pose/pose/position/z'] = target_position[
    2] * np.ones(p.shape[1])
df['/lmpcpp_moveit/desired_pose/pose/position/x'] = p[0, :]
df['/lmpcpp_moveit/desired_pose/pose/position/y'] = p[1, :]
df['/lmpcpp_moveit/desired_pose/pose/position/z'] = p[2, :]
df['/lmpcpp_moveit/desired_velocity_se3/twist/linear/x'] = v[0, :]
df['/lmpcpp_moveit/desired_velocity_se3/twist/linear/y'] = v[1, :]
df['/lmpcpp_moveit/desired_velocity_se3/twist/linear/z'] = v[2, :]
df['/lmpcpp_moveit/desired_velocity_max/twist/linear/x'] = mpc.con_v.firststep_box_max(
)[0]
df['/lmpcpp_moveit/desired_velocity_max/twist/linear/y'] = mpc.con_v.firststep_box_max(
)[1]
df['/lmpcpp_moveit/desired_velocity_max/twist/linear/z'] = mpc.con_v.firststep_box_max(
)[2]
df['/lmpcpp_moveit/desired_velocity_min/twist/linear/x'] = -mpc.con_v.firststep_box_max(
)[0]
df['/lmpcpp_moveit/desired_velocity_min/twist/linear/y'] = -mpc.con_v.firststep_box_max(
)[1]
df['/lmpcpp_moveit/desired_velocity_min/twist/linear/z'] = -mpc.con_v.firststep_box_max(
)[2]
df['/lmpcpp_moveit/desired_acceleration_se3/twist/linear/x'] = a[0, :]
df['/lmpcpp_moveit/desired_acceleration_se3/twist/linear/y'] = a[1, :]
df['/lmpcpp_moveit/desired_acceleration_se3/twist/linear/z'] = a[2, :]
df['/lmpcpp_moveit/desired_jerk_se3/twist/linear/x'] = j[0, :]
df['/lmpcpp_moveit/desired_jerk_se3/twist/linear/y'] = j[1, :]
df['/lmpcpp_moveit/desired_jerk_se3/twist/linear/z'] = j[2, :]
df = pd.DataFrame.from_dict(df)

df['time'] = [i * dt for i in np.arange(H)]
df.set_index('time', inplace=True)

coords_color = ['firebrick', 'forestgreen', 'steelblue']
coords = ['x', 'y', 'z']
legends = []
legend_labels = []
for icoord in np.arange(3):
    coord = coords[icoord]
    coord_color = coords_color[icoord]
    print('icoord:', icoord, 'coord:', coord)
    col = 0

    lw = 1.5
    lw_des = 2.5 * lw
    max_color = 'brown'
    max_alpha = 0.2
    des_alpha = 0.5

    def plot_des(df, ax, color):
        if show_desired:
            df.plot(ax=ax, alpha=des_alpha, color=color, linewidth=lw_des)

    ###################################################################################
    # position
    row = icoord
    ax = setup_ax(axes1[row, col], botvisible=False)
    # ax.set_title(coord)
    # shay = ax.get_shared_y_axes()
    # shay.remove(ax)
    topic = '/lmpcpp_moveit/target_pose/pose/position/'
    df[topic + coord].plot(ax=ax, linestyle='dashed', color='k', linewidth=lw)
    # topic = '/panda/velocity_control/current_pose_velqp/pose/position/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_pose/pose/position/'
    plot_des(df[topic + coord], ax, color=coord_color)
    if col == 0:
        title = aux.deriv_title(coord) + ' ' + aux.deriv_unit('m', '')
        ax.set_ylabel(title)

    # velocity
    row = 3
    ax = setup_ax(axes1[row, col], botvisible=False)
    # topic = '/lmpcpp_moveit/current_velocity_se3/twist/linear/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_velocity_se3/twist/linear/'
    plot_des(df[topic + coord], ax, color=coord_color)

    topic = '/lmpcpp_moveit/desired_velocity_max/twist/linear/'
    df[topic + coord].plot(ax=ax,
                           linestyle='dashed',
                           color='gray',
                           linewidth=0.8 * lw)
    topic = '/lmpcpp_moveit/desired_velocity_min/twist/linear/'
    df[topic + coord].plot(ax=ax,
                           linestyle='dashed',
                           color='gray',
                           linewidth=0.8 * lw)
    # if icoord == 0:
    #     topic = '/lmpcpp_moveit/max_velocity_se3/twist/linear/'
    #     ax.fill_between(df.index,
    #                     df[topic + coord],
    #                     color=max_color,
    #                     alpha=max_alpha)
    #     topic = '/lmpcpp_moveit/min_velocity_se3/twist/linear/'
    #     ax.fill_between(df.index,
    #                     df[topic + coord],
    #                     color=max_color,
    #                     alpha=max_alpha)
    if col == 0:
        ax.set_ylabel('Velocity ' + aux.deriv_unit('m', 'd'))

    # acceleration
    row += 1
    ax = setup_ax(axes1[row, col], botvisible=False)
    # topic = '/lmpcpp_moveit/current_acceleration_se3/twist/linear/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_acceleration_se3/twist/linear/'
    plot_des(df[topic + coord], ax, color=coord_color)
    ax.set_xlabel('t [s]')
    if col == 0:
        ax.set_ylabel('Acceleration ' + aux.deriv_unit('m', 'dd'))

    # jerk
    row += 1
    ax = setup_ax(axes1[row, col], botvisible=True)
    # topic = '/lmpcpp_moveit/current_jerk_se3/twist/linear/'
    # df[topic + coord].plot(ax=ax, linewidth=lw, color=coord_color)
    topic = '/lmpcpp_moveit/desired_jerk_se3/twist/linear/'
    plot_des(df[topic + coord], ax, color=coord_color)
    ax.set_xlabel('t [s]')
    if col == 0:
        ax.set_ylabel('Jerk ' + aux.deriv_unit('m', 'ddd'))

    coord_des_label = aux.deriv_title(coord, supind='des')
    if show_desired:
        legends.append(
            Line2D([0], [0],
                   color=coord_color,
                   lw=lw_des,
                   alpha=des_alpha,
                   label=coord_des_label))
    # legends.append(Line2D([0], [0], color=coord_color, lw=lw_des, label=coord))

legends.append(
    Patch(facecolor=max_color,
          edgecolor=max_color,
          alpha=max_alpha,
          label='max velocity'))
ax.legend(handles=legends,
          loc='lower center',
          bbox_to_anchor=(0.5, -.55),
          fancybox=True,
          shadow=True,
          ncol=len(legends))

Axes.grid()

plt.suptitle('$w_a=${} $w_t=${}'.format(params.w_a, params.w_terminal))

# fpath = 'result_linear.png'
# print('saving {}...'.format(fpath))
# fig1.savefig(
#     fpath,
#     dpi=300,
#     bbox_inches='tight',
# )

# J = np.random.rand(3, 4)
# qd = mpcpp.VectorLimits.Ones(4)
# hp = mpcpp.HPS(qd, J, nu=3, rows=6)

################################################
# J = np.array([[0.43907966, 0.13791354, 0.40789784],
#               [0.76624451, 0.71389517, 0.89473673],
#               [0.58893548, 0.45893844, 0.05423775]])
# qd = mpcpp.VectorLimits.Ones(3)
# hp = mpcpp.HPS(qd, J, nu=3, rows=3)

# print(J @ np.ones(3))

# A = np.vstack([hp.A, -hp.A])
# b = np.hstack([hp.b.max, -hp.b.min])[None].T
# vertex, face_indices = pca.hsapce_to_vertex(A, b)
# faces = pca.face_index_to_vertex(vertex, face_indices)

# fig = plt.figure()
# ax = pcv.plot_polytope_faces(plt=plt,
#                              faces=faces,
#                              face_color='lightsalmon',
#                              edge_color='orangered',
#                              label='polytope')
# pcv.plot_polytope_vertex(ax=ax, vertex=vertex, color='gray')

# plt.grid()
# plt.show()

import pycapacity.algorithms as pca
import pycapacity.visual as pcv

########################################################
vertex, face_indices = pca.hsapce_to_vertex(A, b)
faces = pca.face_index_to_vertex(vertex, face_indices)
fig = plt.figure()
ax = pcv.plot_polytope_faces(plt=plt,
                             faces=faces,
                             alpha=0.3,
                             face_color='lightsalmon',
                             edge_color='orangered',
                             label='polytope')
pcv.plot_polytope_vertex(ax=ax, vertex=vertex, color='gray')
########################################################

########################################################
# fig = plt.figure()
# ax = fig.add_subplot(111,projection='3d')
# lim = 30
# for p in np.arange(0, lim, lim/20):
#     ax.scatter(p, p, 0, color='green')
# xx, yy = np.meshgrid(np.arange(-lim,lim, lim/10), np.arange(-lim/2,lim/2, lim/10))
# # x + y -z = 15
# normal = np.array([1, 1, -1])
# z = (plane_factor*amax + normal[0]*xx +normal[1]*yy) / normal[2]
# z = (normal[0]*xx +normal[1]*yy) / normal[2]
# zorigin = 15/normal[2]
# print('zinter:', zorigin)
# ax.scatter(0, 0, zorigin, color='blue')
# # xy11 = 15 +
# ax.plot_surface(xx, yy, z, alpha=0.5)
########################################################

lim = amax if amax else 2
for kk in np.arange(0, lim, lim / 10):
    ax.scatter(kk, kk, 0, color='gray')
ax.scatter(0, 0, 0, color='black')
ax.set_xlabel('X', fontsize=15)
ax.set_ylabel('Y', fontsize=15)
ax.set_zlabel('Z', fontsize=15)
plt.grid()

########################################################
# position
p1, p2, p3 = p[0, :], p[1, :], p[2, :]
ax.plot(p1, p2, p3, color='blue', marker='o', label='Position')
ax.scatter(target_position[0],
           target_position[1],
           target_position[2],
           color='k',
           marker='x',
           s=100,
           label='Target')
########################################################
# velocity
vec1, vec2, vec3 = v[0, :], v[1, :], v[2, :]
ax.plot(vec1, vec2, vec3, color='green', marker='o', label='Velocity')
# for k,ak in enumerate(a.T):
#     print('k:', k, 'ak:', ak.T)
########################################################
# acceleration
vec1, vec2, vec3 = a[0, :], a[1, :], a[2, :]
accline = ax.plot(vec1,
                  vec2,
                  vec3,
                  color='red',
                  marker='o',
                  label='Acceleration')
# ax.scatter(vec1, vec2, vec3, color='red')
########################################################

# legends =  [plt.Line2D([0, 0.1], [0, 0], linestyle='-', marker='o', color='w', label='acceleration',
#                           markerfacecolor='r', markersize=10),
#                    plt.Line2D([0], [0], marker='o', color='w', label='velocity',
#                               markerfacecolor='g', markersize=10),
#                    plt.Line2D([0], [0], marker='o', color='w', label='position',
#                               markerfacecolor='b', markersize=10),
#                    plt.Line2D([0], [0], marker='X', color='w', label='target',
#                               markerfacecolor='k', markersize=10),
# ]

# plt.legend(handles = legends, loc='upper right')
plt.legend(loc='upper right')

plt.tight_layout()
plt.show()
