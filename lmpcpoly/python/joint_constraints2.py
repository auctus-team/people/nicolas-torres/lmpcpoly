# from mpl_toolkits.mplot3d.art3d import Poly3DCollection
# from mpl_toolkits.mplot3d import Axes3D
import copy

import matplotlib as mpl
import numpy as np
from matplotlib import pyplot as plt

# from scipy.spatial import ConvexHull
np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)

###################################################################
## add mpcpp dir to path
# import os
# import sys

# srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
# print('abspath(__file__):', srcabspath)

# buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
#                             'build')
# print('buildabspath:', buildabspath)
# sys.path.append(buildabspath)
# sys.path.append(buildabspath + '.note')
# print('import mpcpp...')
# import pylmpcpoly as mpcpp

# print('finished import mpcpp...')
###################################################################

LOWER_BOUND_PLOT = False
LOW_RANGE_PLOT = True
# LOWER_BOUND_PLOT = True
# LOW_RANGE_PLOT = False

LINEWIDTH = 3


def colorFader(
        c1,
        c2,
        mix=0
):  #fade (linear interpolate) from color c1 (at mix=0) to c2 (mix=1)
    c1 = np.array(mpl.colors.to_rgb(c1))
    c2 = np.array(mpl.colors.to_rgb(c2))
    return mpl.colors.to_hex((1 - mix) * c1 + mix * c2)


# c1='#1f77b4' #blue
# c2='green' #green
# n=500

# fig, ax = plt.subplots(figsize=(8, 5))
# for x in range(n+1):
#     ax.axvline(x, color=colorFader(c1,c2,x/n), linewidth=4)
# plt.show()

ncolors = 2
colors_mix = np.linspace(0, 1, ncolors)
colors_over = [colorFader('navy', 'darkgreen', mix) for mix in colors_mix][:-1]
colors_mid = ['green']
colors_under = [colorFader('darkgreen', 'darkred', mix) for mix in colors_mix][1:]
colors = colors_over + colors_mid + colors_under

ncolors_taylor = 3
colors_taylor = [colorFader('saddlebrown', 'mediumorchid', mix) for mix in np.linspace(0, 1, ncolors_taylor)]

# print(colors_over)
# print(colors_under)
# fig, ax = plt.subplots(figsize=(8, 5))
# for k, c in enumerate(colors):
#     ax.axvline(k, color=c, linewidth=4)
# plt.show()


class JointLimits:

    def __init__(self, njoint=0):
        q_max_panda = np.array(
            [2.8973, 1.7628, 2.8973, -0.0698, 2.8973, 3.7525, 2.8973])
        q_min_panda = np.array(
            [-2.8973, -1.7628, -2.8973, -3.0718, -2.8973, -0.0175, -2.8973])
        v_max_panda = np.array(
            [2.1750, 2.1750, 2.1750, 2.1750, 2.6100, 2.6100, 2.6100])
        a_max_panda = np.array([15, 7.5, 10, 12.5, 15, 20, 20])
        j_max_panda = np.array([7500, 3750, 5000, 6250, 7500, 10000, 10000])

        self.q_max = q_max_panda[njoint]
        self.q_min = q_min_panda[njoint]
        self.v_max = v_max_panda[njoint]
        self.a_max = a_max_panda[njoint]
        self.j_max = j_max_panda[njoint]

class FirstTaylorLimits(JointLimits):

    def __init__(self, T=0.015, label=None, pattern = 'dashed', color='black', njoints=0, lims=None):
        super().__init__(njoints)
        self.label = label
        self.color = color
        self.T = T
        self.pattern = pattern

        if lims is not None:
            self.j_max = lims.j_max
            self.a_max = lims.a_max
            self.v_max = lims.v_max
            self.q_max = lims.q_max
            self.q_min = lims.q_min


    def plot(self, ax, a0=0, N=1000, alpha=1):
        ################################################################
        # box limits
        # print('v max:', self.v_max)
        # print('q min, max:', self.q_min, self.q_max)
        qpoints = np.linspace(self.q_min, self.q_max, N)
        vpoints = np.linspace(-self.v_max, self.v_max, N)
        if LOW_RANGE_PLOT:
            vpoints = np.linspace(0, self.v_max, N)
            qpoints = np.linspace(self.q_max - (self.q_max-self.q_min)/18, self.q_max, N)
        style = {
            'color': self.color,
            'linewidth': LINEWIDTH,
            'linestyle': self.pattern,
            'alpha': alpha,
            # 'marker': 'o',
        }

        vmin, vmax = [], []
        for q0 in qpoints:
            vmax.append(np.min([(self.q_max-q0)/self.T, self.v_max]))
            vmin.append(np.max([(self.q_min-q0)/self.T, -self.v_max]))
        vmin = np.array(vmin)
        vmax = np.array(vmax)

        if LOWER_BOUND_PLOT:
            ax.plot(vmin, qpoints, **style)
        ax.plot(vmax, qpoints, label=self.label, **style)
        ax.set_xlabel('[v]', fontsize=20)
        ################################################################

        ax.set_ylabel('[q]', fontsize=20, rotation=0)


class CompatibleLimits(JointLimits):

    def __init__(self, label=None, pattern='dashed', color='black', njoints=0):
        super().__init__(njoints)
        self.label = label
        self.color = color
        self.pattern = pattern

    def compatibilize(self, v0, a0=0):
        v0_original = v0
        delta_v = 0
        delta_p = 0

        ##############################################
        # ACCELERATION REVERSING NECESSARY
        need_acc_reversing = False
        if (v0 < 0 and a0 < 0) or (v0 > 0 and a0 > 0):
            v0, a0 = -np.abs(v0), -np.abs(a0)
            # print('acceleration reversing', v0, a0)
            tr = a0 / self.j_max
            delta_v += a0 * tr + self.j_max * np.square(tr) / 2
            delta_p += v0 * tr + a0 * np.square(
                tr) / 2 + self.j_max * (tr**3) / 6
            a0 = 0
            v0 += delta_v
        else:
            v0, a0 = -np.abs(v0), np.abs(a0)

        ##############################################
        # DECELERATION STAGE
        # print('-' * 10)
        # print('a_max:', self.a_max)
        # print('a_max^2:', np.square(self.a_max))
        # print('j_max:', self.j_max)
        # print('v0:', v0)
        condition = np.abs(
            (2 * np.square(self.a_max) - np.square(a0)) / (2 * self.j_max))
        if condition >= np.abs(v0):
            ####################
            # triangular deceleration
            # print('triangular v0:', v0)
            ad = np.sqrt(((-v0) * 2 * self.j_max + np.square(a0)) / 2)
            if np.isnan(ad):
                print('ad nan:', ad, 'v0:', v0)
            t1 = (ad - a0) / self.j_max
            t3 = ad / self.j_max

            # stage
            # delta_v -= (np.square(ad) - np.square(a0)) / (
            #     2 * self.j_max) + np.square(ad) / (2 * self.j_max)
            # print('triang delta_v:', delta_v)

            delta_p += v0 * t1 + a0 * np.square(t1) / 2 + self.j_max * (t1**
                                                                        3) / 6
            delta_p += +self.a_max * (t3**2) / 2 - self.j_max * (t3**3) / 6
            # print('triang delta_p:', delta_p)
            ####################
        else:
            # trapezoidal deceleration
            t1 = (self.a_max - a0) / self.j_max
            t3 = self.a_max / self.j_max
            t_stop = v0 / self.a_max + (
                2 * np.square(self.a_max) - 2 * a0 * self.a_max +
                np.square(a0)) / (2 * self.j_max * self.a_max)
            t2 = t_stop - t1 - t3
            # print('t1:{:.3f}, t2:{:.3f}, t3:{:.3f}'.format(t1, t2, t3))

            # stage
            # delta_v += (np.square(self.a_max) - np.square(a0)) / (2 * self.j_max)
            # delta_v += t2 * self.a_max + (np.square(self.a_max)) / (2 * self.j_max)

            # integral of a(t)
            # delta_v += a0 *t1 + self.j_max*np.square(t1)/2 + self.a_max*(t2)
            # delta_v += +self.a_max*(t3)-self.j_max*np.square(t3)/2
            delta_p += v0 * t1 + a0 * np.square(t1) / 2 + self.j_max * (
                t1**3) / 6 + self.a_max * (t2**2) / 2
            delta_p += +self.a_max * (t3**2) / 2 - self.j_max * (t3**3) / 6
            # area of a(t)
            # delta_v += (self.a_max-a0) *t1/2 + a0*t1
            # delta_v += self.a_max*(t2)
            # delta_v += self.a_max*(t3)/2
        # print('delta_v', delta_v)

        if np.isnan(delta_p):
            # print('delta_p nan', 'v0:', v0, 'a_max:', self.a_max, 'j_max:',
            #       self.j_max)
            delta_p = 0

        lim = copy.copy(self)
        q_min = lim.q_min + delta_p
        q_max = lim.q_max - delta_p
        v_max = lim.v_max - delta_v
        if v0_original < 0:
            q_max = lim.q_max
        else:
            q_min = lim.q_min

        lim.q_max = np.min([q_max, lim.q_max])
        lim.q_min = np.max([q_min, lim.q_min])
        lim.v_max = np.min([v_max, lim.v_max])
        return lim

    def plot(self, ax, a0=0, N=1000, alpha=1):
        ################################################################
        # box limits
        # print('v max:', self.v_max)
        # print('q min, max:', self.q_min, self.q_max)
        qpoints = np.linspace(self.q_min, self.q_max, N)
        vpoints = np.linspace(-self.v_max, self.v_max, N)
        if LOW_RANGE_PLOT:
            vpoints = np.linspace(0, self.v_max, N)
        style = {
            'color': self.color,
            'linewidth': LINEWIDTH,
            'linestyle': self.pattern,
            'alpha': alpha,
            # 'marker': 'o',
        }
        # ax.plot(self.q_min * np.ones_like(vpoints), vpoints, **style)
        # ax.plot(self.q_max * np.ones_like(vpoints), vpoints, **style)

        # ax.plot(qpoints, -self.v_max * np.ones_like(qpoints), **style)
        # ax.plot(qpoints, self.v_max * np.ones_like(qpoints), **style)
        vpoints_compat = []
        qpoints_max_compat = []
        qpoints_min_compat = []
        for v0 in vpoints:
            lim = self.compatibilize(v0, a0)
            vpoints_compat.append(lim.v_max)
            qpoints_max_compat.append(lim.q_max)
            qpoints_min_compat.append(lim.q_min)

        vpoints_compat = np.array(vpoints_compat)
        qpoints_min_compat = np.array(qpoints_min_compat)
        qpoints_max_compat = np.array(qpoints_max_compat)

        if LOWER_BOUND_PLOT:
            ax.plot(vpoints, qpoints_min_compat, **style)
        ax.plot(vpoints, qpoints_max_compat, label=self.label, **style)
        ax.set_xlabel('[v]', fontsize=20)
        ################################################################

        ax.set_ylabel('[q]', fontsize=20, rotation=0)


base_compat = CompatibleLimits()
base_compat.label = '$\infty$ A and J max'
maxmin = .5
cap_factor = np.linspace(maxmin, -maxmin, len(colors)) + 1
compats = []
for k, color in enumerate(colors):
    factor = cap_factor[k]
    compat = copy.copy(base_compat)
    compat.label = '{:.1f}% Amax'.format(factor * 100)
    compat.a_max *= factor
    compat.color = color
    compats.append(compat)

Ts = np.linspace(0.1, 0.01, len(colors_taylor))
pattern = 'dotted'
for k, color in enumerate(colors_taylor):
    T=Ts[k]
    compats.append(FirstTaylorLimits(
        T=T,
        label='1st Taylor {:.1f}ms'.format(T*1000), color=color,
        pattern = pattern,
                                    lims=base_compat))

fig = plt.figure()
ax = fig.gca()
a0 = 0
# a0 = 10
# a0 = 10
# a0 = 50
# a0 = 100
alpha = 0.7
base_compat.a_max *= 1000000
base_compat.j_max *= 10000000000
compats.append(base_compat)

for compat in compats:
    compat.plot(
        ax,
        a0=a0,
        alpha=alpha,
        # N=4,
    )

ax.legend(loc='upper center',
          bbox_to_anchor=(0.5, 1.09),
          ncol=4,
          fancybox=True,
          shadow=True)
ax.grid(color='gray', linestyle='dashed', dashes=(5, 20))
plt.show()
