#!/usr/bin/env python

###################################################################
## add mpcpp dir to path
import os
import sys

import numpy as np
import pinocchio as pin
import rospy
import tf.transformations
from franka_msgs.msg import FrankaState
from geometry_msgs.msg import (Point32, Polygon, PolygonStamped, Pose,
                               PoseStamped)
from interactive_markers.interactive_marker_server import (
    InteractiveMarkerFeedback, InteractiveMarkerServer)
from jsk_recognition_msgs.msg import PolygonArray
from sensor_msgs.msg import JointState, PointCloud
from std_msgs.msg import Header
from visualization_msgs.msg import (InteractiveMarker,
                                    InteractiveMarkerControl, Marker)

# from visualization_msgs.msg import Marker
# from std_msgs.msg import ColorRGBA
# from geometry_msgs.msg import Point
# from geometry_msgs.msg import Vector3

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)

buildabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)), '..',
                            'build')
print('buildabspath:', buildabspath)
sys.path.append(buildabspath)
sys.path.append(buildabspath + '.note')
print('import mpcpp...')
import pylmpcpoly as mpcpp

print('finished import mpcpp...')
###################################################################

br = tf.TransformBroadcaster()
marker_pose = PoseStamped()
pose_pub = None
# [[min_x, max_x], [min_y, max_y], [min_z, max_z]]
position_limits = [[-0.6, 0.6], [-0.5, 1.2], [0.10, 0.8]]


def create_vertex_msg(vertices, position, frame, scaling_factor=5):
    pointcloud_massage = PointCloud()

    print('vertices.shape', vertices.shape)
    for i in range(vertices.shape[1]):
        point = Point32()
        point.x = vertices[0, i] / scaling_factor + position[0]
        point.y = vertices[1, i] / scaling_factor + position[1]
        point.z = vertices[2, i] / scaling_factor + position[2]
        pointcloud_massage.points.append(point)

    # polytop stamped message
    pointcloud_massage.header = Header()
    pointcloud_massage.header.frame_id = frame
    pointcloud_massage.header.stamp = rospy.Time.now()
    return pointcloud_massage


def create_polytopes_msg(faces, position, frame, scaling_factor=5):
    polygonarray_message = PolygonArray()
    polygonarray_message.header = Header()
    polygonarray_message.header.frame_id = frame
    polygonarray_message.header.stamp = rospy.Time.now()
    for face_polygon in faces:
        polygon_massage = Polygon()
        for i in range(face_polygon.shape[1]):
            point = Point32()
            point.x = face_polygon[0, i] / scaling_factor + position[0]
            point.y = face_polygon[1, i] / scaling_factor + position[1]
            point.z = face_polygon[2, i] / scaling_factor + position[2]
            polygon_massage.points.append(point)

        # polytope stamped message
        polygon_stamped = PolygonStamped()
        polygon_stamped.polygon = polygon_massage
        polygon_stamped.header = Header()
        polygon_stamped.header.frame_id = frame
        polygon_stamped.header.stamp = rospy.Time.now()
        polygonarray_message.polygons.append(polygon_stamped)
        polygonarray_message.likelihood.append(1.0)
    return polygonarray_message


def model_data():
    if model_data.model is None or model_data.data is None:
        rospy.logwarn('model_data()...')
        robot_description = rospy.get_param("/robot_description")
        rospy.logwarn('build robot model...')
        model_data.model = pin.buildModelFromXML(robot_description)
        model_data.data = model_data.model.createData()

    return model_data.model, model_data.data


model_data.model = None
model_data.data = None


def jacobian(q=None, frame_id=None):

    model, data = model_data()

    if frame_id is None:
        frame_id = model.getFrameId(model.names[-1])
    if q is None:
        q = pin.neutral(model)
    pin.computeJointJacobians(model, data, q)
    return pin.getFrameJacobian(model, data, frame_id,
                                pin.LOCAL_WORLD_ALIGNED)[:3, :]


def fk(q=None, frame_id=None):
    model, data = model_data()
    if frame_id is None:
        frame_id = model.getFrameId(model.names[-1])
    if q is None:
        q = pin.neutral(model)
    pin.framesForwardKinematics(model, data, q)
    return data.oMf[frame_id]


def velocity_polytope(q=None):

    model, data = model_data()

    qd = mpcpp.VectorLimits(-model.velocityLimit, model.velocityLimit)
    point_cloud = mpcpp.vertices_from_lmi(jacobian(q), qd.min, qd.max)
    tripol = mpcpp.point_cloud_to_triangle_polygon(point_cloud)
    frame = 'world'

    pose = fk(q)

    polygon = create_polytopes_msg(tripol.faces,
                                   pose.translation,
                                   frame,
                                   scaling_factor=5)
    pointcloud = create_vertex_msg(tripol.vertices,
                                   pose.translation,
                                   frame,
                                   scaling_factor=5)

    return pointcloud, polygon


if __name__ == '__main__':
    # nodename = 'polytope_node' + str(np.random.randint(0, 99999))
    nodename = 'polytope_node'
    rospy.logwarn('starting ' + nodename)
    rospy.init_node(nodename)

    listener = tf.TransformListener()
    link_name = 'world'

    pose_pub = rospy.Publisher('/interactive_marker',
                               PoseStamped,
                               queue_size=10)

    rospy.logwarn('initialize publisher...')
    pub_vertices = rospy.Publisher(nodename + 'velocity_polytope_vertices',
                                   PointCloud,
                                   queue_size=10)
    pub_polygon = rospy.Publisher(nodename + 'velocity_polytope_polygon',
                                  PolygonArray,
                                  queue_size=10)

    rate = rospy.Rate(10)

    while not rospy.is_shutdown():
        rospy.loginfo("publish")
        joints = rospy.wait_for_message('/joint_states', JointState)
        q = np.array(joints.position)
        vertices, polygon = velocity_polytope(q=q)
        pub_vertices.publish(vertices)
        pub_polygon.publish(polygon)
        rate.sleep()
