#!/usr/bin/env python
import geometry_msgs.msg
import numpy as np
import rospy
import tf2_ros
import visualization_msgs.msg
from scipy.spatial.transform import Rotation

n = np.array([1, 0, 1])
n = n / np.linalg.norm(n)
d = 1.1


def equal_tf(p1, p2):
    if p1.rotation.x != p2.rotation.x:
        return False
    if p1.rotation.y != p2.rotation.y:
        return False
    if p1.rotation.z != p2.rotation.z:
        return False
    if p1.rotation.w != p2.rotation.w:
        return False
    if p1.translation.x != p2.translation.x:
        return False
    if p1.translation.y != p2.translation.y:
        return False
    if p1.translation.z != p2.translation.z:
        return False
    return True


def publish_marker(n, d):
    m = visualization_msgs.msg.Marker()
    m.header.stamp = rospy.Time.now()
    m.header.frame_id = 'world'
    m.id = 0
    m.type = m.CUBE
    pos = n * d

    u, s, v = np.linalg.svd(n.reshape(1, -1))
    m.pose.position.x = pos[0]
    m.pose.position.y = pos[1]
    m.pose.position.z = pos[2]

    ro = Rotation.from_matrix(-v)
    orientation = ro.as_quat()
    m.pose.orientation.x = orientation[0]
    m.pose.orientation.y = orientation[1]
    m.pose.orientation.z = orientation[2]
    m.pose.orientation.w = orientation[3]

    m.scale.x = 0.01
    m.scale.y = 2
    m.scale.z = 2

    m.color.a = 0.2
    m.color.r = 1.0
    m.color.g = 0.0
    m.color.b = 0.0

    # publish marker
    global marker_publisher
    marker_publisher.publish(m)


def publish_tf(pos, name, br, orientation):
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = 'world'
    t.child_frame_id = name
    t.transform.translation.x = pos[0]
    t.transform.translation.y = pos[1]
    t.transform.translation.z = pos[2]

    t.transform.rotation.x = orientation[0]
    t.transform.rotation.y = orientation[1]
    t.transform.rotation.z = orientation[2]
    t.transform.rotation.w = orientation[3]

    br.sendTransform(t)
    # if publish_tf.last is not None:
    #     if not equal_tf(publish_tf.last, t.transform):
    #         br.sendTransform(t)
    # publish_tf.last = t.transform

    # publish marker
    global pose_publisher
    posemsg = geometry_msgs.msg.PoseStamped()
    posemsg.header = t.header
    # posemsg.header.frame_id = 'world_optitrack'
    posemsg.pose.orientation = t.transform.rotation
    posemsg.pose.position.x = pos[0]
    posemsg.pose.position.y = pos[1]
    posemsg.pose.position.z = pos[2]
    pose_publisher.publish(posemsg)


publish_tf.last = None


def predictor():
    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)

    br = tf2_ros.TransformBroadcaster()

    rate = rospy.Rate(50)  # 10hz

    # try:
    #     trans = tfBuffer.lookup_transform('world', 'panda_link8', rospy.Time(0))
    #     trans.header.stamp
    #     panda_orientation = trans.transform.rotation
    # except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
    #         tf2_ros.ExtrapolationException):
    #     rospy.logwarn_throttle(1, 'no lego found')
    #     ro = Rotation.from_matrix(np.array([[1,0,0], [0,-1,0], [0,0,-1]]))
    #     panda_orientation = ro.as_quat()
    #     continue

    ro = Rotation.from_matrix(np.array([[1, 0, 0], [0, -1, 0], [0, 0, -1]]))
    panda_orientation = ro.as_quat()

    dt = 0.01
    p_old = np.array([0, 0, 0])
    v_old = np.array([0, 0, 0])
    g = np.array([0, 0, -9.81])
    t_last = 0
    a = np.zeros(3)
    x, y = 0, 0

    while not rospy.is_shutdown():
        publish_marker(n, d)

        try:
            trans = tfBuffer.lookup_transform('world', 'lego1', rospy.Time(0))
            trans.header.stamp
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException,
                tf2_ros.ExtrapolationException):
            rospy.logwarn_throttle(1, 'INTERSECTOR: no lego found')
            continue

        t_c = trans.header.stamp.to_sec()
        if t_c == t_last:
            continue
        dt = t_c - t_last
        t_last = t_c
        p = np.array([
            trans.transform.translation.x, trans.transform.translation.y,
            trans.transform.translation.z
        ])
        v = (p - p_old) / dt
        a = (v - v_old) / dt
        v_old = v
        p_old = p

        if np.linalg.norm(a - g) < 15 and v[0] < 0:

            np0 = n @ p
            nv0 = n @ v
            na0 = n @ g

            try:
                t = (-nv0 + np.sqrt(nv0 * nv0 - 2 * na0 * (np0 - d))) / na0
                t2 = (-nv0 - np.sqrt(nv0 * nv0 - 2 * na0 * (np0 - d))) / na0
            except:
                continue

            if t < 0:
                t = t2

            if not np.isfinite(t):
                continue

            p = p + v * t + g * t * t / 2
            # print('p:', p)
            fc, fs = 15, 100
            alpha = 1 - (1 / fc) / (1 / fc + 1 / fs)
            x = np.clip(alpha * p[0] + (1 - alpha) * x, 0, .5)
            y = np.clip(alpha * p[1] + (1 - alpha) * y, 0, 1)
            z = (d - x * n[0] - y * n[1]) / n[2]
            p = np.array([x, y, z])
            publish_tf(p, 'intersection', br, panda_orientation)
        rate.sleep()


if __name__ == '__main__':
    rospy.init_node('intersector', anonymous=True)
    global marker_publisher, pose_publisher
    marker_publisher = rospy.Publisher('intersector_marker',
                                       visualization_msgs.msg.Marker,
                                       queue_size=10)
    pose_publisher = rospy.Publisher('intersector_pose',
                                     geometry_msgs.msg.PoseStamped,
                                     queue_size=10)

    try:
        predictor()
    except rospy.ROSInterruptException:
        pass
