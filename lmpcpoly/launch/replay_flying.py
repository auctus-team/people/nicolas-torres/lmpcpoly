#!/usr/bin/env python
import csv
import datetime
import os
import sys

import geometry_msgs.msg
import numpy as np
import rospy
import tf2_ros
import visualization_msgs.msg
from scipy.spatial.transform import Rotation

srcabspath = os.path.join(os.path.abspath(os.path.dirname(__file__)))
print('abspath(__file__):', srcabspath)
csvpath = os.path.join(srcabspath, 'flying.csv')
print('csvpath:', csvpath)

TIME, SEQ, STAMP = '__time', '/vrpn_client_node/lego1/pose/header/seq', '/vrpn_client_node/lego1/pose/header/stamp'
PITCH, ROLL, YAW = '/vrpn_client_node/lego1/pose/pose/orientation/pitch_deg', '/vrpn_client_node/lego1/pose/pose/orientation/roll_deg', '/vrpn_client_node/lego1/pose/pose/orientation/yaw_deg'
QW, QX, QY, QZ = '/vrpn_client_node/lego1/pose/pose/orientation/w', '/vrpn_client_node/lego1/pose/pose/orientation/x', '/vrpn_client_node/lego1/pose/pose/orientation/y', '/vrpn_client_node/lego1/pose/pose/orientation/z'
PX, PY, PZ = '/vrpn_client_node/lego1/pose/pose/position/x', '/vrpn_client_node/lego1/pose/pose/position/y', '/vrpn_client_node/lego1/pose/pose/position/z'


def item2PoseStamped(item, frameid='world_optitrack'):
    posemsg = geometry_msgs.msg.PoseStamped()
    posemsg.header.frame_id = frameid
    posemsg.header.stamp = rospy.Time.now()
    posemsg.pose.orientation.w = float(item[QW])
    posemsg.pose.orientation.x = float(item[QX])
    posemsg.pose.orientation.y = float(item[QY])
    posemsg.pose.orientation.z = float(item[QZ])
    posemsg.pose.position.x = float(item[PX]) + .5
    posemsg.pose.position.y = float(item[PY])
    posemsg.pose.position.z = float(item[PZ])
    return posemsg


def publish_tf(name, br, posemsg):
    pos = [
        posemsg.pose.position.x, posemsg.pose.position.y,
        posemsg.pose.position.z
    ]
    orientation = posemsg.pose.orientation
    t = geometry_msgs.msg.TransformStamped()

    t.header.stamp = rospy.Time.now()
    t.header.frame_id = 'world_optitrack'
    t.child_frame_id = name
    t.transform.translation.x = pos[0]
    t.transform.translation.y = pos[1]
    t.transform.translation.z = pos[2]

    t.transform.rotation.x = orientation.x
    t.transform.rotation.y = orientation.y
    t.transform.rotation.z = orientation.z
    t.transform.rotation.w = orientation.w

    br.sendTransform(t)


if __name__ == '__main__':
    nodename = 'flying_publisher'
    rospy.init_node(nodename)

    tfBuffer = tf2_ros.Buffer()
    listener = tf2_ros.TransformListener(tfBuffer)
    br = tf2_ros.TransformBroadcaster()

    data = None
    with open(csvpath, newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        deltas = []
        prevtime = None
        data = list(reader)
        # for item in list(reader)[:5]:
        # for item in list(reader):
        #     unix_timestamp = float(item[TIME])
        #     rostime = rospy.Time.from_sec(unix_timestamp).to_sec()
        #     print(rospy.Time.from_sec(unix_timestamp), item[PX], item[PY],item[PZ],)
        #     datetime_obj = datetime.datetime.fromtimestamp(unix_timestamp)
        #     date_string = datetime_obj.strftime('%Y-%m-%d %H:%M:%S.%f')
        #     print(date_string)

        #     if prevtime is None:
        #         prevtime = rostime
        #     else:
        #         delta = rostime - prevtime
        #         deltas.append(delta)
        #         print('delta:', delta)
        #         prevtime = rostime
        # deltas = np.array(deltas)
        # print('mean deltas: {:.2f} s'.format(deltas.mean()))
        # print('freq: {:.3f} Hz'.format(1/deltas.mean()))

    rospy.logwarn('%s: READY', nodename)

    pose_publisher = rospy.Publisher('/vrpn_client_node/lego1',
                                     geometry_msgs.msg.PoseStamped,
                                     queue_size=10)

    r = rospy.Rate(100)
    cur_idx = 0
    while not rospy.is_shutdown():
        posemsg = item2PoseStamped(data[cur_idx])
        pose_publisher.publish(posemsg)
        publish_tf(name='lego1', br=br, posemsg=posemsg)
        publish_tf(name='lego', br=br, posemsg=posemsg)
        r.sleep()
        cur_idx += 1
        if cur_idx >= len(data):
            cur_idx = 0
