#!/usr/bin/bash

set -xe
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
latex_dir=${SCRIPT_DIR}/../../latex
pythonmpc_dr=${SCRIPT_DIR}
cmakebuild_dir=${pythonmpc_dr}/build

make -C ${cmakebuild_dir} test_mpclib -j
build/bin/test_mpclib || gdb build/bin/test_mpclib -ex 'run'
