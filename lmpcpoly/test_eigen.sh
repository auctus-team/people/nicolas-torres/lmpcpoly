#!/usr/bin/bash

set -xe
SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
latex_dir=${SCRIPT_DIR}/../../latex
pythonmpc_dr=${SCRIPT_DIR}
cmakebuild_dir=${pythonmpc_dr}/build
execf=build/bin/test_eigen

make -C ${cmakebuild_dir} test_eigen -j
${execf} || gdb ${execf} -ex 'run'
