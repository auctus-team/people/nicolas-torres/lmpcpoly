#include <Eigen/Dense>
// #include <Eigen/Geometry>
#include <Eigen/src/Core/util/Constants.h>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <thread>

// #include "pinocchio/fwd.hpp"
// #include "pinocchio/parsers/urdf.hpp"
// #include "pinocchio/algorithm/joint-configuration.hpp"
// #include "pinocchio/algorithm/kinematics.hpp"
// #include "pinocchio/algorithm/jacobian.hpp"
// #include "pinocchio/algorithm/aba.hpp"
// #include "pinocchio/algorithm/rnea.hpp"
// #include "pinocchio/algorithm/crba.hpp"
// #include "pinocchio/algorithm/frames.hpp"
// #include "pinocchio/multibody/model.hpp"
// #include "pinocchio/algorithm/model.hpp"
// #include "pinocchio/algorithm/center-of-mass.hpp"

#include "pinocchio/spatial/explog.hpp"
#include "pinocchio/spatial/se3.hpp"
#include <methods.hpp>
#include <mpc.hpp>
#include <mpcacc.hpp>
#include <mpcacc_tan.hpp>

using namespace std;
using namespace lmpcpoly;

constexpr double PI = 3.14159265359;
constexpr int m = 2; // % Must be 2 or 3 for plotting
constexpr int n = 5; //% Must be greater than m
using mymatrix = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
using mycolvectorn = Eigen::Matrix<double, n, 1>;
using mycolvectorm = Eigen::Matrix<double, m, 1>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

Eigen::Vector3d skew_cross3(Eigen::Vector3d x, Eigen::Vector3d y) {
  Eigen::Vector3d res;
  res(0) = -x(2) * y(1) + x(1) * y(2);
  res(1) = x(2) * y(0) - x(0) * y(2);
  res(2) = -x(1) * y(0) + x(0) * y(1);
  return res;
}

pinocchio::Motion brse3(pinocchio::Motion x, pinocchio::Motion y,
                        float factor) {
  return pinocchio::Motion(skew_cross3(x.angular(), y.linear()) * factor -
                               skew_cross3(y.angular(), x.linear()) * factor,
                           skew_cross3(x.angular(), y.angular()) * factor);
}

Vector6d brse3(Vector6d x, Vector6d y) {
  auto lin1 = x.head<3>();
  auto ang1 = x.tail<3>();
  auto lin2 = y.head<3>();
  auto ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

std::vector<pinocchio::Motion> random_motions(long long N) {
  std::vector<pinocchio::Motion> motions;
  long long i = 0;
  while (i < N) {
    // auto so3 = Eigen::Vector3d::Random()*PI;
    // if (so3.norm()<= PI){
    //   auto pos = Eigen::Vector3d::Random();
    //   motions.push_back(pinocchio::Motion(pos,so3));
    //   i++;
    // }

    auto rand = pinocchio::Motion(Vector6d::Random());
    rand.angular() = rand.angular() * PI;
    // rand.angular() = rand.angular() * 0.5;
    // rand.linear() = rand.linear() * 2; // increase linear range
    // rand.linear() = rand.linear() * .3;
    if (rand.angular().norm() <= PI / 2) {
      // if (rand.angular().norm() <= PI / 6) {
      motions.push_back(rand);
      i++;
    }
  }
  return motions;
}

int main() {

  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Ones(4, 4);
    Eigen::MatrixXd Atri = A.triangularView<Eigen::UpLoType::Lower>();
    std::cout << "A" << std::endl << A << std::endl;
    std::cout << "A.triangularView<Lower>" << std::endl << Atri << std::endl;
  }
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Identity(4, 4) -
                        block_rcircshift(Eigen::MatrixXd::Identity(4, 4), 2);
    std::cout << "A" << std::endl << A << std::endl;
  }

  // {
  //   unsigned int nz = 30, nu = 3, nj = nz - nu;
  //   double T = 1e-3;
  //   Eigen::MatrixXd Inz = Eigen::MatrixXd::Identity(nz, nz);
  //   Eigen::MatrixXd Lj((rshift(Inz, nu) - Inz).block(0, 0, nj, nz) / T);
  //   std::cout << "Inz: " << Inz.rows() << " x " << Inz.cols() << std::endl;
  //   std::cout << "Lj: " << Lj.rows() << " x " << Lj.cols() << std::endl
  //             << Lj << std::endl;
  // }

  {
    unsigned int H = 4, nu = 3;
    Eigen::MatrixXd A = rshift(Eigen::MatrixXd::Identity(H * nu, H * nu), nu) -
                        Eigen::MatrixXd::Identity(H * nu, H * nu);
    std::cout << "A" << std::endl << A << std::endl;
    std::cout << "A.block(..., :-nu,:)" << std::endl
              << A.block(0, 0, A.rows() - nu, A.cols()) << std::endl;
  }

  // {
  //   Eigen::MatrixXd J = Eigen::MatrixXd::Identity(3, 3);
  //   lmpcpoly::VectorLimits qd = lmpcpoly::VectorLimits::Ones(3);
  //   lmpcpoly::print_mat("J", J);
  //   lmpcpoly::Hd hd = lmpcpoly::hps(J, qd);
  //   lmpcpoly::print_mat("H", hd.H);
  //   lmpcpoly::print_mat("d", hd.d);
  // }

  {
    lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpoases");
    Eigen::Vector3d t1, t2;
    t1 << 1, 1, 1;
    t2 << 0, 0, 0;
    mpc.solve(lmpcpoly::InitialConditions(), t1);
    mpc.solve(lmpcpoly::InitialConditions(), t2);
  }
  // {
  //   lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpoases");
  //   Eigen::Vector3d t1, t2;
  //   t1 << 1, 1, 1;
  //   mpc.solve(lmpcpoly::InitialConditions(), t1,
  //             {Eigen::Matrix3d::Random(), lmpcpoly::VectorLimits::Ones(3)});
  // }
  // {
  //   lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpoases");
  //   Eigen::Vector3d t1, t2;
  //   t1 << 1, 1, 1;
  //   mpc.update_pmax(3 * Eigen::Vector3d::Ones());
  //   print_mat("mpc.plim.min", mpc.plim.min);
  //   print_mat("mpc.plim.max", mpc.plim.max);
  //   mpc.solve(lmpcpoly::InitialConditions(), t1);
  // }

  {
    pinocchio::SE3 t1(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero()),
        t2(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Ones());
    lmpcpoly::MPCAccTangent mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpoases");
    mpc.solve(lmpcpoly::InitialConditionsTangent(), t1);
    std::cout << "------------- second solve()" << std::endl;
    mpc.solve(lmpcpoly::InitialConditionsTangent(), t2);
  }

  {
    pinocchio::SE3 t1(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero()),
        t2(Eigen::Matrix3d::Identity(), Eigen::Vector3d::Ones());
    LinearInequalityConstraints pol;
    pol.A = Eigen::MatrixXd::Identity(4, 6);
    pol.A.bottomRows(1) = 0.3 * Eigen::VectorXd::Ones(6).transpose();
    pol.b.min = -Eigen::VectorXd::Ones(4);
    pol.b.max = Eigen::VectorXd::Ones(4);

    lmpcpoly::MPCAccTangent mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpmad");
    mpc.con_v.update_firststep_polytope(pol);
    mpc.con_v.update_horizon_polytope(pol);
    mpc.solve(lmpcpoly::InitialConditionsTangent(), t1);
    std::cout << "------------- second solve()" << std::endl;
  }

  {
    Eigen::Vector3d t1, t2;
    t1 << 1, 1, 1;
    t2 << 0, 0, 0;
    lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpmad");
    LinearInequalityConstraints pol;
    pol.A = Eigen::MatrixXd::Identity(4, 3);
    pol.A.bottomRows(1) = 0.3 * Eigen::VectorXd::Ones(3).transpose();
    pol.b.min = -Eigen::VectorXd::Ones(4);
    pol.b.max = 1.5 * Eigen::VectorXd::Ones(4);
    mpc.con_v.update_mode(2);
    mpc.con_v.update_firststep_polytope(pol);
    mpc.con_v.update_horizon_polytope(pol);
    lmpcpoly::InitialConditions initial;
    initial.v0 = Eigen::Vector3d::Zero();
    initial.v0 << 0.1, -0.5, 0.8;
    std::cout << "-------------" << std::endl;
    print_mat("initial.v0", initial.v0);
    mpc.solve(initial, t1);
    std::cout << "-------------" << std::endl;
    std::cout << "------------- second solve()" << std::endl;
  }

  {
    Eigen::Vector3d t1, t2;
    t1 << 1, 1, 1;
    t2 << 0, 0, 0;
    lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpmad");
    LinearInequalityConstraints pol;
    pol.A = Eigen::MatrixXd::Identity(4, 3);
    pol.A.bottomRows(1) = 0.3 * Eigen::VectorXd::Ones(3).transpose();
    pol.b.min = -Eigen::VectorXd::Ones(4);
    pol.b.max = Eigen::VectorXd::Ones(4);
    mpc.con_a.update_mode(2);
    mpc.con_a.update_firststep_polytope(pol);
    mpc.con_a.update_horizon_polytope(pol);
    lmpcpoly::InitialConditions initial;
    initial.v0 = 0.1 * Eigen::Vector3d::Ones();
    mpc.solve(initial, t1);
    std::cout << "------------- second solve()" << std::endl;
  }

  {
    Eigen::Vector3d t1, t2;
    t1 << 1, 1, 1;
    t2 << 0, 0, 0;
    lmpcpoly::MPCAcc mpc(3, 20e-3, lmpcpoly::MPCParams(), "qpmad");
    Eigen::MatrixXd J(3, 7);
    J << -5.74194e-05, 0.188164, -5.7407e-05, 0.127869, 2.28401e-06, 0.209619,
        -1.11022e-16, //
        0.55544, -3.14204e-05, 0.555514, -1.28982e-06, 0.209445, -2.90809e-05,
        0, //
        -0, -0.555439, -5.94415e-08, 0.473065, 6.31248e-05, 0.0889043, 0;
    lmpcpoly::VectorLimits qd(lmpcpoly::VectorLimits::Ones(7));
    qd.max << 2.1750, 2.1750, 2.1750, 2.1750, 2.6100, 2.6100, 2.6100;
    qd.min = -qd.max;
    auto pol = lmpcpoly::hps_lmi(J, qd.min, qd.max);
    std::cout << "lmpcpoly::hps_lmi done" << std::endl;

    mpc.con_v.update_firststep_polytope(pol);
    mpc.con_v.update_horizon_polytope(pol);
    lmpcpoly::InitialConditions initial;
    initial.v0 = 0.1 * Eigen::Vector3d::Ones();
    mpc.solve(initial, t1);
    std::cout << "------------- second solve()" << std::endl;
  }

  std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << "test_mpclib DONE" << std::endl;

  exit(EXIT_SUCCESS);
}
