#include "polycap_extended.hpp"
#include "polycap.hpp"

// #include <pcl/ModelCoefficients.h>
// #include <pcl/filters/passthrough.h>
// #include <pcl/filters/project_inliers.h>
// #include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
// #include <pcl/sample_consensus/method_types.h>
// #include <pcl/sample_consensus/model_types.h>
// #include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/convex_hull.h>

#include <iostream>

using namespace lmpcpoly;

Hd lmpcpoly::hps(const Eigen::MatrixXd J, const VectorLimits qd) {

  Hd hd;
  hyper_plane_shift_method(hd.H, hd.d, J, qd.min, qd.max);

  return hd;
}

// hyper plane shifting with linear matrix inequalities output
// (instead of half planes)
LinearInequalityConstraints lmpcpoly::hps_lmi(Eigen::MatrixXd A,
                                              Eigen::VectorXd x_min,
                                              Eigen::VectorXd x_max) {
  // lmpcpoly::TickTock temp("hps_lmi");
  LinearInequalityConstraints lmi;

  // print_mat("lmi.A", lmi.A); // debug
  // print_mat("lmi.b.min", lmi.b.min); // debug
  // print_mat("lmi.b.max", lmi.b.max); // debug

  // shape(A) = (m,n)
  // find all combinations of m-1 out of n indexes
  std::vector<std::vector<int>> combinations;
  {
    // lmpcpoly::TickTock temp("CombinationRepetition");
    CombinationRepetition(combinations, A.cols(), A.rows() - 1);
  }
  lmi.A = Eigen::MatrixXd::Zero(combinations.size(), A.rows());
  lmi.b = VectorLimits::Ones(combinations.size());

  // int combi = 0;                               // debug
  // for (auto const &comb : combinations) {      // debug
  //   std::cout << "combi:" << combi++ << "\t>"; // debug
  //   for (auto const &c : comb) {               // debug
  //     std::cout << " " << c;                   // debug
  //   }                                          // debug
  //   std::cout << std::endl;                    // debug
  // }                                            // debug

  unsigned int row = 0;
  // for all combinations of indexes
  // int it = 0; // debug
  for (auto const &comb : combinations) {
    // construct the matrix containing the combination of rows
    // with indexes given by the combination
    // std::cout << "---------------------" << std::endl; // debug
    // std::cout << "it:" << it++ << std::endl; // debug

    //////////////////////////////////////////////////////////////
    // Eigen::MatrixXd W = A(Eigen::all, comb); // TODO: Eigen 3.4
    // std::cout << "A.rows():" << A.rows() << "   comb.size():" << comb.size()
    //           << std::endl;
    Eigen::MatrixXd W(A.rows(), comb.size());
    for (unsigned int i = 0; i < comb.size(); ++i) {
      // std::cout << "i:" << i << std::endl; //debug
      W.col(i) = A.col(comb[i]);
    }
    // lmpcpoly::print_mat("W", W); // debug
    //////////////////////////////////////////////////////////////

    // calculate the svd of this matrix
    // TODO: JacobiSVD seems ~10us faster than BDCSVD
    // it is also expected to be better for small matrices (like jacobians)
    // https://eigen.tuxfamily.org/dox/group__SVD__Module.html
    // also, it seems to fix an issue when compiling in a conda env
    // Eigen::BDCSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);

    // find the null space projection matrix
    // (should be V.T but here is U.T - TODO find why)
    Eigen::MatrixXd V = svd.matrixU().transpose();
    // print_mat("V", V); // debug

    // use singular values to determine if the matrix W is singular
    Eigen::VectorXd s = svd.singularValues();
    // print_mat("s", s); // debug

    // if (s(Eigen::last) != 0) { // TODO: Eigen 3.4
    if (s(s.size() - 1) != 0) {

      lmi.A.conservativeResize(row + 1, A.rows());
      lmi.b.min.conservativeResize(row + 1, 1);
      lmi.b.max.conservativeResize(row + 1, 1);

      // use the last vector of the null space projector
      // as the normal of the half-space

      // Eigen::VectorXd c = V(Eigen::last, Eigen::all); // TODO: Eigen 3.4
      Eigen::VectorXd c = V.row(V.rows() - 1);
      // print_mat("c", c); // debug
      // std::cout << "row:" << row << std::endl; //debug
      // print_mat("lmi.A", lmi.A);                   // debug
      // print_mat("lmi.A.row(row)", lmi.A.row(row)); // debug
      lmi.A.row(row) = c.transpose();

      // Calculate maximal distances of the half-planes
      // with normal vecotr c form the origin
      Eigen::VectorXd I = c.transpose() * A;
      double d_positive(0.0f), d_negative(0.0f);
      // to find the max you can sum only positive values
      // and min only negative ones
      for (int i = 0; i < I.size(); i++) {
        if (I[i] > 0) {
          d_positive += I[i] * x_max[i];
          d_negative -= +I[i] * x_min[i];
        } else {
          d_positive += I[i] * x_min[i];
          d_negative -= I[i] * x_max[i];
        }
      }
      lmi.b.min(row) = -d_negative;
      lmi.b.max(row) = d_positive;
      row++;
    }
    // else {
    //   // W is singular
    //   std::stringstream ss;
    //   ss << "Error: lmpcpoly::hps_lmi W is SINGULAR" << std::endl;
    //   ss << "DEBUG INFO" << std::endl;
    //   ss << str_mat("W", W);
    //   ss << str_mat("V", V);
    //   ss << "singular values of W:" << std::endl;
    //   ss << str_mat("s", s);
    //   ss << str_mat("A", A);
    //   ss << str_mat("x_min", x_min);
    //   ss << str_mat("x_max", x_max);
    //   ss << "lmpcpoly::hps_lmi W is SINGULAR";
    //   print_error(ss);
    //   std::abort();
    // }
  }

  // print_mat("lmi.A", lmi.A); // debug
  return lmi;
}

LinearInequalityConstraints
lmpcpoly::compute_Jqd_halfplane(const VectorLimits qd, const Eigen::MatrixXd J,
                                const unsigned int nu,
                                const unsigned int Jqd_polytope_rows) {

  // polytope capacity
  // double polytope_horizon = 250*mpcprob.dt; // [s]
  // double polytope_horizon = compute_time_limit() + 0.050; // [s]
  // double polytope_horizon = delta_time + 0.050; // [s]
  // ROS_ERROR_STREAM_THROTTLE(5, "polytope_horizon: " <<
  // polytope_horizon*1000<<"ms"); double polytope_horizon = 1; // [s]

  // ROS_INFO_STREAM("model.velocityLimit.cwiseMin");

  // pinocchio::Data::Matrix6x J;
  // TODO: fix ref_pose
  // J = ref_pose.toActionMatrix() * J;

  Eigen::MatrixXd H;
  Eigen::VectorXd d;

  // ROS_INFO_STREAM("hyper_plane_shift_method");
  hyper_plane_shift_method(H, d, J, qd.min, qd.max);

  // ROS_INFO_STREAM("mpcprob.H: " << mpcprob.H);
  // print_sizes(H, "H");
  // print_sizes(d, "d");

  if (H.cols() != nu) {
    std::cout << "H.cols() != nu: " << H.cols() << " != " << nu << std::endl;
  }
  if (H.rows() != 2 * Jqd_polytope_rows) {
    std::cout << "H.rows() != 2*Jqd_polytope_rows: " << H.rows()
              << " != " << 2 * Jqd_polytope_rows << std::endl;
  }

  Eigen::MatrixXd Hf(Eigen::MatrixXd::Zero(Jqd_polytope_rows, H.cols()));
  Eigen::VectorXd ld(Eigen::VectorXd::Zero(Jqd_polytope_rows));
  Eigen::VectorXd ud(Eigen::VectorXd::Zero(Jqd_polytope_rows));
  // print_sizes(Hf, "setting Hf");
  // print_sizes(ld, "setting ld");
  // print_sizes(ud, "setting ud");
  for (unsigned int row = 0; row < Jqd_polytope_rows; row++) {
    Hf.block(row, 0, 1, Hf.cols()) = H.block(2 * row, 0, 1, H.cols());
    ud(row) = d(2 * row);
    ld(row) = -d(2 * row + 1);
  }

  // Eigen::MatrixXd A_lin(Eigen::MatrixXd::Zero(
  //     A_lin_acc.rows() + (params.H - 1) * Hf.rows(), A_lin_acc.cols()));
  // Eigen::MatrixXd HH(Eigen::MatrixXd::Zero(Jq_polytope_rows,
  // A_lin.cols()));
  // // print_sizes(HH, "setting HH");
  // HH.block(0, params.nx_hat, Hf.rows(), Hf.cols()) = Hf;

  return {Hf, {ld, ud}};
}

TrianglePolygon
lmpcpoly::point_cloud_to_triangle_polygon(const Eigen::MatrixXd points) {
  Eigen::MatrixXd point_mat(points);
  if (point_mat.rows() != 3) {
    point_mat = point_mat.transpose();
  }
  assert(point_mat.rows() == 3);

  pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>),
      cloud_hull(new pcl::PointCloud<pcl::PointXYZ>);

  for (unsigned int col = 0; col < point_mat.cols(); ++col) {
    Eigen::Vector3d vec(point_mat.col(col));
    // std::cout << "point:" << vec << std::endl;
    cloud->push_back(pcl::PointXYZ(vec(0), vec(1), vec(2)));
  }

  pcl::ConvexHull<pcl::PointXYZ> chull;
  std::vector<pcl::Vertices> vertices;
  chull.setInputCloud(cloud);
  chull.reconstruct(*cloud_hull, vertices);

  TrianglePolygon tripol;
  tripol.vertices = cloud_hull->getMatrixXfMap().topRows(3).cast<double>();

  for (auto const &v : vertices) {
    auto const &vertex = v.vertices;
    Eigen::Vector3d vec3;
    vec3 << vertex[0], vertex[1], vertex[2];
    tripol.indices.push_back(vec3);

    Eigen::Matrix3d mat3;
    mat3 << Eigen::Vector3d(
        cloud_hull->at(vertex[0]).getVector3fMap().cast<double>()),
        Eigen::Vector3d(
            cloud_hull->at(vertex[1]).getVector3fMap().cast<double>()),
        Eigen::Vector3d(
            cloud_hull->at(vertex[2]).getVector3fMap().cast<double>());
    tripol.faces.push_back(mat3);
  }

  return tripol;
}

Eigen::MatrixXd lmpcpoly::vertices_from_lmi(const Eigen::MatrixXd &A,
                                            const Eigen::VectorXd &x_min,
                                            const Eigen::VectorXd &x_max) {
  unsigned int dof(A.cols());
  assert(x_min.rows() == A.cols());
  assert(x_max.rows() == A.cols());
  assert(A.rows() == 3);
  unsigned int ncombs = pow(2, dof);
  Eigen::MatrixXd mul(dof, ncombs);
  Eigen::VectorXd col(dof);
  for (unsigned int i = 0; i < ncombs; ++i) {
    for (unsigned int j = 0; j < dof; ++j) {
      col[j] = (((1 << j) & i) >> j) ? x_max[j] : x_min[j];
    }
    // std::cout << "vec:" << col.transpose() << std::endl;
    mul.col(i) = col;
  }
  return A * mul;
}

// ----------------------------------------------------------------------------

RobotPolytope::RobotPolytope() {
  std::thread(&RobotPolytope::run, this).detach();
}

RobotPolytope::~RobotPolytope() { stop = true; }

void RobotPolytope::update(Eigen::MatrixXd A, VectorLimits lim) {
  std::lock_guard<std::mutex> guard(_mutex);
  _A = A;
  _lim = lim;
}

LinearInequalityConstraints RobotPolytope::polytope_lmi(Eigen::VectorXd bias,
                                                        bool use_bias) {
  _mutex_lmi.lock();
  LinearInequalityConstraints con = _con;
  _mutex_lmi.unlock();
  if (bias.rows() && use_bias) {
    Eigen::VectorXd Abias = con.A * bias;
    con.b.min += Abias;
    con.b.max += Abias;
  }
  return con;
}

void RobotPolytope::run() {
  Eigen::MatrixXd A;
  VectorLimits lim;
  LinearInequalityConstraints con;

  // wait until first update
  while (!A.cols()) {
    {
      std::lock_guard<std::mutex> guard(_mutex);
      A = _A;
      lim = _lim;
    }
    std::this_thread::sleep_for(std::chrono::microseconds(500));
  }

  // continuosly compute the lmi
  while (!stop) {
    {
      std::lock_guard<std::mutex> guard(_mutex);
      A = _A;
      lim = _lim;
    }
    con = lmpcpoly::hps_lmi(A, lim.min, lim.max);
    {
      std::lock_guard<std::mutex> guard(_mutex_lmi);
      _con = con;
    }
    std::this_thread::sleep_for(std::chrono::microseconds(sleep_us));
  }
}
