#include "methods.hpp"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>

using namespace lmpcpoly;

std::string lmpcpoly::str_val(std::string str, double val) {
  return str + ":" + std::to_string(val);
}

void lmpcpoly::print_error(const std::stringstream &ss) {
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << ss.str();
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
  std::cout << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << std::endl;
}

std::string lmpcpoly::str_size(std::string str, Eigen::MatrixXd mat) {
  std::stringstream ss;
  ss << str << " " << mat.rows() << " x " << mat.cols();
  return ss.str();
}

void lmpcpoly::print_size(std::string str, Eigen::MatrixXd mat) {
  std::cout << str_size(str, mat) << std::endl;
}

std::string lmpcpoly::str_mat(std::string str, Eigen::MatrixXd mat) {
  std::stringstream ss;
  ss << str_size(str, mat) << std::endl;
  if (mat.cols() == 1) {
    ss << mat.transpose() << std::endl;
  } else {
    ss << mat << std::endl;
  }
  return ss.str();
}

void lmpcpoly::print_mat(std::string str, Eigen::MatrixXd mat) {
  std::cout << str_mat(str, mat) << std::endl;
}

Eigen::MatrixXd lmpcpoly::rshift(Eigen::MatrixXd toShift, unsigned int shift) {
  // right circular shift
  unsigned int cols = toShift.cols();
  unsigned int rows = toShift.rows();
  Eigen::MatrixXd shifted(Eigen::MatrixXd::Zero(rows, cols));
  shifted.block(0, shift, rows, shift) = toShift.block(0, 0, rows, shift);
  return shifted;
}

Eigen::MatrixXd lmpcpoly::block_rcircshift(Eigen::MatrixXd toShift,
                                           unsigned int shift) {
  // right circular shift
  unsigned int cols = toShift.cols();
  unsigned int rows = toShift.rows();
  Eigen::MatrixXd shifted(rows, cols);
  shifted << toShift.block(0, cols - shift, rows, shift),
      toShift.block(0, 0, rows, cols - shift);
  return shifted;
}

void VectorLimits::repeat(unsigned int n) {
  unsigned int rows = min.rows();

  min.conservativeResize(n * rows);
  max.conservativeResize(n * rows);
  for (unsigned int h = 0; h < n; ++h) {
    min.segment(rows * h, rows) = min.segment(0, rows);
    max.segment(rows * h, rows) = max.segment(0, rows);
  }
}

VectorLimits VectorLimits::Ones(unsigned int n) {
  return {-Eigen::VectorXd::Ones(n), Eigen::VectorXd::Ones(n)};
}

VectorLimits VectorLimits::Constant(unsigned int n, double value) {
  return {-value * Eigen::VectorXd::Ones(n), value * Eigen::VectorXd::Ones(n)};
}

VectorLimits lmpcpoly::qdmax_from_qqlims(const VectorLimits q,
                                         const VectorLimits qd,
                                         const Eigen::VectorXd current_q,
                                         const double polytope_horizon) {
  return {(-qd.min).cwiseMin((q.min - current_q) / polytope_horizon),
          qd.max.cwiseMin((q.max - current_q) / polytope_horizon)};
}
template <>
void LinearInequalityConstraints::lmi_to_horizon_step(unsigned int H) {
  // from | A | x < b
  // to
  // | A  0  0| x1 < b
  if ((b.min.rows() != A.rows()) || (b.max.rows() != A.rows())) {
    std::cerr << "Error: LinearInequalityConstraints::lmi_to_horizon_step "
                 "inequalities wrong size (for min or max)"
              << std::endl;
    std::cerr << "it should be:" << A.rows() << " x 1" << std::endl;
    std::cerr << "but this was provided (min):" << str_mat("b.min", b.min)
              << std::endl;
    std::cerr << "but this was provided (max):" << str_mat("b.max", b.max)
              << std::endl;
    std::abort();
  }

  unsigned int Acols = A.cols();
  A.conservativeResize(A.rows(), H * Acols);
  A.rightCols((H - 1) * Acols).setZero();
}

template <> void LinearInequalityConstraints::lmi_to_horizon(unsigned int H) {
  // from | A | x < b
  // to
  // | A  0  0| x1 < b
  // | 0  A  0| x2 < b
  // | 0  0  A| x3 < b

  if ((b.min.rows() != A.rows()) || (b.max.rows() != A.rows())) {
    std::cerr << "Error: LinearInequalityConstraints::lmi_to_horizon_step "
                 "inequalities wrong size (for min or max)"
              << std::endl;
    std::cerr << "it should be:" << A.rows() << " x 1" << std::endl;
    std::cerr << "but this was provided (min):" << str_mat("b.min", b.min)
              << std::endl;
    std::cerr << "but this was provided (max):" << str_mat("b.max", b.max)
              << std::endl;
    std::abort();
  }

  unsigned int Acols = A.cols();
  unsigned int Arows = A.rows();
  lmi_to_horizon_step(H);
  A.conservativeResize(H * Arows, H * Acols);
  A.bottomRows((H - 1) * Arows).setZero();
  for (unsigned int kk = 1; kk < H; ++kk) {
    A.middleRows(kk * Arows, Arows) =
        block_rcircshift(A.middleRows((kk - 1) * Arows, Arows), Acols);
  }
  b.repeat(H);
}
template <>
void LinearInequalityConstraints::combine(
    const std::vector<LinearConstraintsTempl> &lics) {
  unsigned int cols = lics.front().A.cols();
  for (const auto &lic : lics) {
    if (cols != lic.A.cols()) {
      std::cerr << "Error: LinearInequalityConstraints::combine "
                   "inequalities wrong size (for A)"
                << std::endl;
      std::cerr << "it should be:" << cols
                << " but provided is:" << lic.A.cols() << std::endl;
      std::abort();
    }
    cols = lic.A.cols();
  }

  unsigned int total_rows = 0;
  for (const auto &lic : lics) {
    if ((lic.b.max.rows() != lic.b.min.rows()) ||
        (lic.b.max.rows() != lic.A.rows())) {
      std::cerr
          << "Error: LinearInequalityConstraints::combine "
             "one of the inequalities wrong size (rows of A and bmin/bmax)"
          << std::endl;
      std::cerr << "all rows size should be equal (A=bmin=bmax): "
                << lic.A.rows() << " = " << lic.b.min.rows() << " = "
                << lic.b.max.rows() << std::endl;
      std::abort();
    }
    total_rows += lic.A.rows();
  }
  A.conservativeResize(total_rows, cols);
  b.min.conservativeResize(total_rows, 1);
  b.max.conservativeResize(total_rows, 1);

  unsigned int current_row = 0;
  for (const auto &lic : lics) {
    A.middleRows(current_row, lic.A.rows()) = lic.A;
    b.min.segment(current_row, lic.A.rows()) = lic.b.min;
    b.max.segment(current_row, lic.A.rows()) = lic.b.max;
    current_row += lic.A.rows();
  }
}

Eigen::VectorXd lmpcpoly::jexp_dist(const Eigen::Matrix4d cur_pose_mat,
                                    const Eigen::Matrix4d obs_pose_mat) {
  pinocchio::SE3 cur_pose(cur_pose_mat);
  pinocchio::SE3 obs_pose(obs_pose_mat);
  pinocchio::SE3 obs_pose_with_curR(cur_pose.rotation(),
                                    obs_pose.translation());
  auto obs_pose_with_curR_log = pinocchio::log6(obs_pose_with_curR);

  Eigen::MatrixXd jexp(6, 6);
  pinocchio::Jexp6(obs_pose_with_curR_log, jexp);

  return jexp * (obs_pose_with_curR_log.toVector() -
                 pinocchio::log6(cur_pose).toVector());
}

LinearInequalityConstraints
lmpcpoly::mindistance_obstacle(const Eigen::Matrix4d obs_pose_mat,
                               const double r) {
  pinocchio::SE3 cur_pose(obs_pose_mat);
  // NOTE: obstacle pose must be aligned with the starting pose of the robot
  auto pose_obs_zeroR = pinocchio::SE3(obs_pose_mat);
  auto pose_obs_zeroR_log = pinocchio::log6(pose_obs_zeroR);

  // JEXP
  // Eigen::MatrixXd jexp(6, 6);
  // pinocchio::Jexp6(pose_obs_zeroR_log, jexp);
  // // take only linear constraints
  // LinearInequalityConstraints cons;
  // cons.A = jexp.block(2, 0, 1, 6);
  // std::cout << "jexp * pose_obs_zeroR_log + r:"
  //           << (jexp * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
  //                  Eigen::VectorXd::Constant(1, r)
  //           << std::endl;
  // // TODO: is there a better way to establish max?
  // cons.b.max = Eigen::VectorXd::Constant(1, 100);
  // cons.b.min = (jexp * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
  //              Eigen::VectorXd::Constant(1, r);

  // JLOG
  Eigen::MatrixXd jlog(6, 6);
  Eigen::VectorXd rz(6);
  rz << 0, 0, r, 0, 0, 0;
  pinocchio::Jlog6(pose_obs_zeroR, jlog);
  Eigen::MatrixXd iden6(Eigen::MatrixXd::Identity(6, 6));
  // take only linear constraints
  LinearInequalityConstraints cons;
  cons.A = iden6.block(2, 0, 1, 6);
  std::cout << "iden6 * pose_obs_zeroR_log + r:"
            << (iden6 * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
                   (jlog * rz).block(2, 0, 1, 1)
            << std::endl;
  // TODO: is there a better way to establish max?
  cons.b.max = Eigen::VectorXd::Constant(1, 100);
  cons.b.min = (iden6 * pose_obs_zeroR_log.toVector()).block(2, 0, 1, 1) +
               (jlog * rz).block(2, 0, 1, 1);

  return cons;
}

SquareMatrix6d lmpcpoly::dlog(const Vector6d &x) {
  // const auto &lin = x.head<3>();
  // const auto &ang = x.tail<3>();
  // SquareMatrix6d dexp0(SquareMatrix6d::Zero());
  // pinocchio::skew(ang, dexp0.block<3, 3>(0, 0));
  // pinocchio::skew(lin, dexp0.block<3, 3>(0, 3));
  // // pinocchio::skew(ang, dexp0.block<3, 3>(3, 3));
  // dexp0.block<3, 3>(3, 3) = dexp0.block<3, 3>(0, 0);
  // return SquareMatrix6d::Identity() + dexp0 / 2 + dexp0 * dexp0 / 12;

  Eigen::MatrixXd temp(6, 6);
  pinocchio::Jlog6(pinocchio::exp6(x), temp);
  // pinocchio::Jexp6(pinocchio::Motion(x), temp);
  return temp;
}

Vector6d lmpcpoly::brse3(const Vector6d &x, const Vector6d &y) {
  const auto &lin1 = x.head<3>();
  const auto &ang1 = x.tail<3>();
  const auto &lin2 = y.head<3>();
  const auto &ang2 = y.tail<3>();

  Vector6d res;
  res << ang1.cross(lin2) - ang2.cross(lin1), ang1.cross(ang2);
  return res;
}

// SquareMatrix6d dexp2(const Vector6d &x) {
//   Eigen::MatrixXd temp(6, 6);
//   pinocchio::Hlog6(pinocchio::exp6(x), temp);
//   // pinocchio::Jexp6(pinocchio::Motion(x), temp);
//   return temp;
// }

double
lmpcpoly::tock_ms(std::chrono::time_point<std::chrono::steady_clock> start) {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
             std::chrono::steady_clock::now() - start)
             .count() /
         1000000.;
}

std::chrono::time_point<std::chrono::steady_clock> lmpcpoly::precise_now() {
  return std::chrono::steady_clock::now();
}

///////////////////////////////////////////////////////
// Delay
Delay::Delay(double delay_s) : delay_s(delay_s) {}

Delay::Delay(const Delay &other)
    : start(other.start), delay_s(other.delay_s),
      initialized(other.initialized) {}

void Delay::print(const std::string &str) {
  if (passed()) {
    std::cout << str;
    refresh();
  }
}

bool Delay::passed() {
  if (!initialized) {
    refresh();
    return true;
  }

  if (tock_ms(start) / 1000 > delay_s) {
    return true;
  }
  return false;
}

void Delay::refresh() {
  initialized = true;
  start = precise_now();
}

///////////////////////////////////////////////////////
// TickTock
TickTock::TickTock(std::string msg, bool print)
    : start(precise_now()), _msg(msg), _print(print) {}

TickTock::~TickTock() {
  if (_print) {
    std::cout << "duration " << _msg << " :: " << std::setprecision(3)
              << this->tock_ms() << "ms" << std::endl;
  }
}

double TickTock::tock_ms() { return ::lmpcpoly::tock_ms(start); }

double TickTock::tock_s() { return this->tock_ms() / 1000.; }
void TickTock::disable() { _print = false; }

void TickTock::tock(std::string msg, bool header) {
  if (_print) {
    if (header) {
      std::cout << "duration " << _msg << " :: " << msg << ": "
                << std::setprecision(3) << this->tock_ms() << "ms" << std::endl;
    } else {
      std::cout << "duration " << msg << ": " << std::setprecision(3)
                << this->tock_ms() << "ms" << std::endl;
    }
  }
}

///////////////////////////////////////////////////////
// LPFilter
Eigen::VectorXd LPFilter::filter(Eigen::VectorXd cur, double w0, double dt) {
  if (last.rows() == 0) {
    last.resize(cur.rows(), 1);
    last.setZero();
  }

  if (w0 < 0) {
    return cur;
  }

  double beta = exp(-w0 * dt);
  Eigen::VectorXd filtered = beta * last + (1 - beta) * cur;
  last = filtered;
  return filtered;
}
