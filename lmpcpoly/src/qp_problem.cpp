#include "qp_problem.hpp"
#include <iostream>
#include <osqp.h>
#include <sstream>

#include <qpOASES/MessageHandling.hpp>

using namespace lmpcpoly;

//////////////////////////////////////////////////////
// QPProblem
QPProblem::QPProblem(unsigned int nz) : nz(nz) {}

double QPProblem::compute_time_limit() { return compute_time; };

void QPProblem::set_compute_time(double tt) { compute_time = tt; }

void QPProblem::update_cost_function(const Eigen::MatrixXd &a,
                                     const Eigen::MatrixXd &Hreg) {
  _cost_a = a;
  _Hreg = Hreg;
}

void QPProblem::mark_initialized() { initialized = true; }

//////////////////////////////////////////////////////
// OSQPProblem
OSQPProblem::OSQPProblem(unsigned int nz) : QPProblem(nz), P(nullptr, &c_free) {

  // std::cout << "OSQPProblem"<<std::endl;

  // TickTock temp("osqp_set_default_settings");
  osqp_set_default_settings(&osqp_settings);
  // osqp_settings.polish = false;
  osqp_settings.polish = true;
  osqp_settings.polish_refine_iter = 100;
  osqp_settings.max_iter = 100000;
  osqp_settings.verbose = false;
  // osqp_settings.verbose = true;
  osqp_settings.eps_abs = 1e-5;    // 1e-5;
  osqp_settings.eps_rel = 1e-5;    // 1e-5;
  osqp_settings.warm_start = true; // NOTE: default is true

  osqp_settings.time_limit = compute_time_s;

  // TODO: remove debug time limits
  // osqp_settings.time_limit = 100; // [s], yes, it's a lot! (for
  // debugging)
  // osqp_settings.time_limit = 200e-3; // [s]
  // osqp_settings.time_limit = 1; // [s]
}
// OSQPProblem::~OSQPProblem(){
// Cleanup
// source: https://osqp.org/docs/examples/setup-and-solve.html#c
// TODO: osqp_cleanup actually calls free for everything so osqp_data
// would need to become a pointer (or just don't call osqp_cleanup)
// https://github.com/osqp/osqp/blob/dc49cc0ab6a27011ccd9f1817f9719a09908b071/src/osqp.c#L659
// std::cout << "osqp_cleanup(osqp_workspace)" << std::endl;
// osqp_cleanup(osqp_workspace);
// if (osqp_data.A) {
//   std::cout << "free(osqp_data.A)" << std::endl;
//   c_free(osqp_data.A);
// }
// if (osqp_data.P) {
//   std::cout << "free(osqp_data.P)" << std::endl;
//   c_free(osqp_data.P);
// }
// }

void OSQPProblem::set_compute_time(double tt) {
  QPProblem::set_compute_time(tt);
  osqp_settings.time_limit = tt;
}

void OSQPProblem::update_cost_function(const Eigen::MatrixXd &a,
                                       const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  // TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    Q.noalias() = 2 * a.transpose() * a + Hreg;

    // NOTE: it seems this is actually slower
    // Q.setZero();
    // Q.selfadjointView<Eigen::Lower>().rankUpdate(a.transpose());
    // Q.selfadjointView<Eigen::Upper>().rankUpdate(Q.transpose());

    // Q += 2 * w_reg * Eigen::MatrixXd::Identity(nz, nz);
    // Q += 2 * w_reg * mpcmat.Fu_underline.transpose() * mpcmat.Fu_underline;

    // print_size("update_cost_function::Q:", Q);
  }

  // Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_row_ind;
  // Eigen::Matrix<c_int, Eigen::Dynamic, 1> P_col_ind;
  // Eigen::Matrix<double, Eigen::Dynamic, Eigen::Dynamic, Eigen::RowMajor> U(
  //     Q.rows(), Q.cols());
  {
    // TickTock temp("update_cost_function:: sparse osqp_data.P");
    U.resize(Q.rows(), Q.cols());
    U = Q.triangularView<Eigen::UpLoType::Upper>();
    Q_sparse = U.sparseView();
    Q_sparse.makeCompressed();
    P_row_ind = Eigen::Map<Eigen::VectorXi>(Q_sparse.innerIndexPtr(),
                                            Q_sparse.nonZeros())
                    .cast<c_int>();
    P_col_ind = Eigen::Map<Eigen::VectorXi>(Q_sparse.outerIndexPtr(),
                                            Q_sparse.outerSize() + 1)
                    .cast<c_int>();
    // if (!P) {
    // std::cout << "free(P)" << std::endl;
    // c_free(P);
    // }

    if (P) {
      if (Q_sparse.nonZeros() != P->p[P->n]) {
        // sparsity changed, need to call osqp_setup again
        initialized = false;
        std::cout << "osqp::sparsity changed!" << std::endl;
      }
    }

    P.reset(csc_matrix(Q_sparse.rows(), Q_sparse.cols(), Q_sparse.nonZeros(),
                       Q_sparse.valuePtr(), P_row_ind.data(),
                       P_col_ind.data()));

    if (initialized) {
      // this is assuming sparsity doesn't change, if it does,
      // we'd need to call osqp_setup again
      osqp_update_P(osqp_workspace, Q_sparse.valuePtr(), OSQP_NULL,
                    Q_sparse.nonZeros());
    }
  }
}

int OSQPProblem::solve(const Eigen::MatrixXd cost_a, const Eigen::MatrixXd Hreg,
                       const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                       Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int OSQPProblem::solve(const Eigen::MatrixXd cost_b, const Eigen::MatrixXd Alin,
                       Eigen::VectorXd Alin_min, Eigen::VectorXd Alin_max) {

  // std::cout << "-> nz:" << nz << std::endl;
  // lmpcpoly::print_size("cost_b.transpose():", cost_b.transpose());
  // lmpcpoly::print_size("cost_a:", _cost_a);
  // lmpcpoly::print_size("cost_b^T * _cost_a:", cost_b.transpose() * _cost_a);
  // Eigen::VectorXd g(Eigen::VectorXd::Zero(nz));
  Eigen::VectorXd g;
  // lmpcpoly::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }

  // setup QP in OSQP
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> A_row_ind;
  Eigen::Matrix<c_int, Eigen::Dynamic, 1> A_col_ind;

  {
    // TickTock temp("sparse osqp_data.A");
    Alin_sparse = Alin.sparseView();
    Alin_sparse.makeCompressed();
    A_row_ind = Eigen::Map<Eigen::VectorXi>(Alin_sparse.innerIndexPtr(),
                                            Alin_sparse.nonZeros())
                    .cast<c_int>();
    A_col_ind = Eigen::Map<Eigen::VectorXi>(Alin_sparse.outerIndexPtr(),
                                            Alin_sparse.outerSize() + 1)
                    .cast<c_int>();
    osqp_data.A = csc_matrix(Alin_sparse.rows(), Alin_sparse.cols(),
                             Alin_sparse.nonZeros(), Alin_sparse.valuePtr(),
                             A_row_ind.data(), A_col_ind.data());
  }

  Alin_min = Alin_min.cwiseMax(-OSQP_INFTY);
  Alin_max = Alin_max.cwiseMin(OSQP_INFTY);

  if (!initialized) {
    // number of constraints
    // m =
    //    nx_overline  | from A_und X_und + B_und U_und - X_over = 0
    // +  nx           | from x0
    // +  H*nx         | from H state variable bounds (x_min, x_max)
    // +  H*nu         | from H input variable bounds (u_min, u_max)
    // osqp_data.m = nx_overline + nx + H*nx + H*nu;
    {
      // TickTock temp("osqp_data P,q,m,l,u,n");
      osqp_data.P = P.get();
      osqp_data.q = g.data();
      osqp_data.m = Alin.rows();
      osqp_data.l = Alin_min.data();
      osqp_data.u = Alin_max.data();
      osqp_data.n = nz; // number of variables
    }

    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // print_size("Alin_min:", Alin_min);
    // print_size("Alin_max:", Alin_max);
    // std::cout << "Alin_min:" << std::endl << Alin_min.transpose() <<
    // std::endl; std::cout << "Alin_max:" << std::endl << Alin_max.transpose()
    // << std::endl; if (Alin_min.rows() > 180) {
    //   std::cout << "Alin_min.segment(180,10):" << std::endl
    //             << Alin_min.segment(180, 10).transpose() << std::endl;
    //   std::cout << "Alin_max.segment(180,10):" << std::endl
    //             << Alin_max.segment(180, 10).transpose() << std::endl;
    // }
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
    // std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;

    {
      // TickTock temp("osqp_setup");
      c_int ret = osqp_setup(&osqp_workspace, &osqp_data, &osqp_settings);
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_setup() FAILED" << std::endl;
        ss << "Error: osqp_setup() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        ss << "g:" << g.transpose() << std::endl;
        print_error(ss);
        return ret;
      }
    }
    mark_initialized();
  } else {
    {
      // TickTock temp("osqp_update_...");
      c_int ret = osqp_update_lin_cost(osqp_workspace, g.data());
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_update_lin_cost() FAILED" << std::endl;
        ss << "Error: osqp_update_lin_cost() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        ss << str_mat("g:", g) << std::endl;
        print_error(ss);
      }

      ret =
          osqp_update_bounds(osqp_workspace, Alin_min.data(), Alin_max.data());
      if (0 != ret) {
        std::stringstream ss;
        ss << "Error: osqp_update_bounds() FAILED" << std::endl;
        ss << "Error: osqp_update_bounds() FAILED" << std::endl;
        ss << "ret:" << ret << std::endl;
        for (unsigned int i = 0; i < Alin_min.rows(); ++i) {
          if (Alin_min(i) > Alin_max(i)) {
            ss << "Alin_min(" << i << ")>=Alin_max(" << i << "): ";
            ss << Alin_min(i) << ">" << Alin_max(i) << std::endl;
          }
        }
        // ss << str_mat("Alin_min:", Alin_min) << std::endl;
        // ss << str_mat("Alin_max:", Alin_max) << std::endl;
        print_error(ss);
      }
    }
  }

  c_int ret = 0;
  {
    // TickTock temp("--> osqp_solve");
    osqp_solve(osqp_workspace);
    ret = osqp_workspace->info->status_val;
  }

  if (ret != 1) {
    std::stringstream ss;
    ss << "Error: osqp::solve() FAILED" << std::endl;
    ss << "Error: osqp::solve() FAILED" << std::endl;
    ss << "ret:" << ret << std::endl;
    print_error(ss);

    std::cout << "osqp::status:" << std::string(osqp_workspace->info->status)
              << std::endl;
    std::cout << "osqp::status_val:" << osqp_workspace->info->status_val
              << std::endl;
    std::cout << "osqp::status_polish:" << osqp_workspace->info->status_polish
              << std::endl;
    std::cout << "osqp::iter:" << osqp_workspace->info->iter << std::endl;
    std::cout << "osqp::setup_time:" << osqp_workspace->info->setup_time
              << std::endl;
    std::cout << "osqp::solve_time:" << osqp_workspace->info->solve_time
              << std::endl;
    std::cout << "osqp::update_time:" << osqp_workspace->info->update_time
              << std::endl;
    std::cout << "osqp::polish_time:" << osqp_workspace->info->polish_time
              << std::endl;
    std::cout << "osqp::run_time:" << osqp_workspace->info->run_time
              << std::endl;
  }
  return ret != 1;
}

Eigen::VectorXd OSQPProblem::result() {
  // Eigen::VectorXd result(nz);
  // result = Eigen::Map<Eigen::VectorXd>(osqp_workspace->solution->x, nz, 1);
  // return result;
  return Eigen::Map<Eigen::VectorXd>(osqp_workspace->solution->x, nz, 1);
}

//////////////////////////////////////////////////////
// QPOasesProblem

QPOasesProblem::QPOasesProblem(unsigned int nz)
    : QPProblem(nz), bounds{Eigen::VectorXd::Zero(nz),
                            Eigen::VectorXd::Zero(nz)} {
  // std::cout << "QPOasesProblem" << std::endl;
  // bounds.min.Constant(-qpOASES::INFTY);
  // bounds.max.Constant(qpOASES::INFTY);

  // options.setToReliable();
  // options.setToDefault();
  options.setToMPC();
  // options.enableRegularisation = qpOASES::BT_TRUE;
  // options.enableFlippingBounds = qpOASES::BT_FALSE;
  // options.initialStatusBounds = qpOASES::ST_INACTIVE;
  // options.numRefinementSteps = 1e3;
  // options.enableCholeskyRefactorisation = 1;
}

// void update_variable_bounds(VectorLimits varbounds) {
//   bounds = varbounds;
// }

void QPOasesProblem::update_cost_function(const Eigen::MatrixXd &a,
                                          const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  // TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    H.noalias() = 2 * a.transpose() * a + Hreg;
  }
}

int QPOasesProblem::solve(const Eigen::MatrixXd cost_a,
                          const Eigen::MatrixXd Hreg,
                          const Eigen::MatrixXd cost_b,
                          const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                          Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int QPOasesProblem::solve(const Eigen::MatrixXd cost_b,
                          const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                          Eigen::VectorXd Alin_max) {

  Eigen::VectorXd g;
  // lmpcpoly::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }

  return solve_Hg(H, g, Alin, Alin_min, Alin_max);
}

int QPOasesProblem::solve_Hg(const Eigen::MatrixXd H_, const Eigen::MatrixXd g,
                             const Eigen::MatrixXd Alin,
                             Eigen::VectorXd Alin_min,
                             Eigen::VectorXd Alin_max) {

  if (n_ineq != Alin.rows()) {
    initialized = false;
  }

  MatrixXdRowmajor A(Alin);
  qpOASES::returnValue ret;
  int nWSR = max_iterations;
  qpOASES::real_t cputime = compute_time;
  if (!initialized) {
    n_ineq = Alin.rows();
    solver.reset(new qpOASES::SQProblem(nz, Alin.rows(), qpOASES::HST_POSDEF));

    solver->setOptions(options);
    // solver->setPrintLevel(qpOASES::PL_NONE);
    solver->setPrintLevel(qpOASES::PL_LOW);
    // solver->setPrintLevel(qpOASES::PL_MEDIUM);
    // solver->setPrintLevel(qpOASES::PL_HIGH);
    // solver->setPrintLevel(qpOASES::PL_DEBUG_ITER);

    // ret =
    //     solver->init(H.data(), g.data(), Alin.data(), bounds.min.data(),
    //                  bounds.max.data(), Alin_min.data(), Alin_max.data(),
    //                  nWSR);

    ret = solver->init(H_.data(), g.data(), A.data(), nullptr, nullptr,
                       Alin_min.data(), Alin_max.data(), nWSR, &cputime);

    if (ret != qpOASES::RET_INIT_FAILED) {
      mark_initialized();
    }
  } else {
    // ret = solver->hotstart(H.data(), g.data(), Alin.data(),
    // bounds.min.data(),
    //                        bounds.max.data(), Alin_min.data(),
    //                        Alin_max.data(), nWSR);
    ret = solver->hotstart(H_.data(), g.data(), A.data(), nullptr, nullptr,
                           Alin_min.data(), Alin_max.data(), nWSR, &cputime);
  }

  // if (qpOASES::SUCCESSFUL_RETURN != ret) {
  //   std::stringstream ss;
  //   ss << "Error: qpOASES FAILED, ret:" << ret << std::endl;
  //   // ss << "qpOASES::SUCCESSFUL_RETURN:" << qpOASES::SUCCESSFUL_RETURN <<
  //   std::endl;
  //   // ss << "qpOASES::RET_MAX_NWSR_REACHED:" <<
  //   qpOASES::RET_MAX_NWSR_REACHED << std::endl;
  //   // ss << "qpOASES::RET_INIT_FAILED:" << qpOASES::RET_INIT_FAILED <<
  //   std::endl;
  //   // ss << "qpOASES::RET_HOTSTART_FAILED:" << qpOASES::RET_HOTSTART_FAILED
  //   //    << std::endl;
  //   // ss << "nWSR:" << nWSR << std::endl;
  //   ss << std::string(
  //             qpOASES::getGlobalMessageHandler()->getErrorCodeMessage(ret))
  //      << std::endl;

  //   for (unsigned int i = 0; i < Alin_min.rows(); ++i) {
  //     if (Alin_min(i) > Alin_max(i)) {
  //       ss << "Alin_min(" << i << ")>=Alin_max(" << i << "): ";
  //       ss << Alin_min(i) << ">" << Alin_max(i) << std::endl;
  //     }
  //   }
  //   // ss << str_mat("Alin_min:", Alin_min) << std::endl;
  //   // ss << str_mat("Alin_max:", Alin_max) << std::endl;
  //   print_error(ss);
  // }

  // if (ret == 61) {
  //   std::stringstream ss;

  //   ss << "Error: qpOASES FAILED, ret:" << ret << std::endl;
  //   for (unsigned int i = 0; i < Alin_min.rows(); ++i) {
  //     ss << "Alin_min(" << i << ")>=Alin_max(" << i << "): ";
  //     ss << Alin_min(i) << ">" << Alin_max(i) << std::endl;
  //   }
  //   ss << str_mat("Alin_min:", Alin_min) << std::endl;
  //   ss << str_mat("Alin_max:", Alin_max) << std::endl;
  //   print_error(ss);
  // }

  if ((ret == qpOASES::RET_MAX_NWSR_REACHED) && (nWSR > 0)) {
    std::cout << "Warning: qpOASES RET_MAX_NWSR_REACHED but nWSR > 0 (cputime "
                 "probably exceeded)"
              << std::endl;
    ret = qpOASES::SUCCESSFUL_RETURN;
  }

  if ((ret == qpOASES::RET_INIT_FAILED) ||
      (ret == qpOASES::RET_HOTSTART_FAILED)) {
    std::stringstream ss;
    ss << "Error: qpOASES::solve() FAILED" << std::endl;
    ss << "Error: qpOASES::solve() FAILED" << std::endl;
    ss << "ret:" << (int)ret << std::endl;
    ss << "ret==qpOASES::RET_MAX_NWSR_REACHED:" << std::boolalpha
       << (ret == qpOASES::RET_MAX_NWSR_REACHED) << std::endl;
    print_error(ss);
  }

  return ret;
}

Eigen::VectorXd QPOasesProblem::result() {
  Eigen::VectorXd result(nz);
  solver->getPrimalSolution(result.data());
  return result;
}

//////////////////////////////////////////////////////
// QPMadProblem

#ifdef qpmad_FOUND
QPMadProblem::QPMadProblem(unsigned int nz)
    : QPProblem(nz), bounds{Eigen::VectorXd::Zero(nz),
                            Eigen::VectorXd::Zero(nz)},
      x{Eigen::VectorXd::Zero(nz)} {
  // std::cout << "QPMadProblem" << std::endl;
}

void QPMadProblem::update_cost_function(const Eigen::MatrixXd &a,
                                        const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  // TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    H.noalias() = 2 * a.transpose() * a + Hreg;
    // Eigen::MatrixXd U = 2 * a.transpose() * a + Hreg;
    // H = U.triangularView<Eigen::UpLoType::Upper>();
  }
}

int QPMadProblem::solve(const Eigen::MatrixXd cost_a,
                        const Eigen::MatrixXd Hreg,
                        const Eigen::MatrixXd cost_b,
                        const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                        Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int QPMadProblem::solve(const Eigen::MatrixXd cost_b,
                        const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                        Eigen::VectorXd Alin_max) {

  Eigen::VectorXd g;
  // lmpcpp::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }

  qpmad::Solver::ReturnStatus ret;
  if (!initialized) {
    solver.reset(new qpmad::Solver);
    // TODO: set max_iterations
    mark_initialized();
  }

  try {
    qpmad::SolverParameters params;
    // params.hessian_type_ = qpmad::SolverParameters::HESSIAN_LOWER_TRIANGULAR;
    // // default
    // params.tolerance_ = 1e-3; // no effect? // default: 1e-12
    // params.max_iter_ = 100; // 100 is already too much // default: infinity
    // Eigen::VectorXd empty;
    // ret =
    //     solver->solve(x, H, g, empty, empty, Alin, Alin_min, Alin_max,
    //     params);
    ret = solver->solve(x, H, g, Alin, Alin_min, Alin_max);
  } catch (const std::runtime_error &e) {
    if (delay2.passed()) {
      std::cout << "Error: qpMad:" << e.what() << std::endl;
      ret = (qpmad::Solver::ReturnStatus)-2;
      warn_counter2 = 0;
      delay2.refresh();
    } else {
      warn_counter2++;
    }
  }

  if (ret != qpmad::Solver::OK) {
    if (delay.passed()) {
      std::stringstream ss;
      ss << "Error: qpMad::solve() FAILED" << std::endl;
      ss << "Error: qpMad::solve() FAILED" << std::endl;
      ss << "ret:" << (int)ret << std::endl;
      ss << "counter:" << (int)warn_counter << std::endl;
      print_error(ss);
      warn_counter = 0;
      delay.refresh();
    } else {
      warn_counter++;
    }
  }

  return ret;
}

Eigen::VectorXd QPMadProblem::result() { return x; }

#endif

//////////////////////////////////////////////////////
// ProxQPProblem

#ifdef proxsuite_FOUND
ProxQPProblem::ProxQPProblem(unsigned int nz)
    : QPProblem(nz), bounds{Eigen::VectorXd::Zero(nz),
                            Eigen::VectorXd::Zero(nz)} {
  // std::cout<<"ProxQPProblem()"<<std::endl;
}

void ProxQPProblem::update_cost_function(const Eigen::MatrixXd &a,
                                         const Eigen::MatrixXd &Hreg) {
  QPProblem::update_cost_function(a, Hreg);

  // TickTock temp("complete update_cost_function");
  {
    // TickTock temp("update_cost_function:: --> QP matrix Q");
    // Eigen::MatrixXd U = 2 * a.transpose() * a + Hreg;
    // H = U.triangularView<Eigen::UpLoType::Upper>();
    H.noalias() = 2 * a.transpose() * a + Hreg;
  }
}

int ProxQPProblem::solve(const Eigen::MatrixXd cost_a,
                         const Eigen::MatrixXd Hreg,
                         const Eigen::MatrixXd cost_b,
                         const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                         Eigen::VectorXd Alin_max) {
  update_cost_function(cost_a, Hreg);
  return solve(cost_b, Alin, Alin_min, Alin_max);
}

int ProxQPProblem::solve(const Eigen::MatrixXd cost_b,
                         const Eigen::MatrixXd Alin, Eigen::VectorXd Alin_min,
                         Eigen::VectorXd Alin_max) {

  Eigen::VectorXd g;
  // lmpcpp::print_size("g:", g);
  {
    // TickTock temp("QP matrix g");
    // g.noalias() = -2 * cost_b.transpose() * _cost_a;
    // TODO: WTF, why is it inverted in the refactoring?
    g.noalias() = -2 * _cost_a.transpose() * cost_b;
  }
  if (n_ineq != Alin.rows()) {
    initialized = false;
  }

  if (n_ineq != Alin.rows()) {
    initialized = false;
  }

  if (!initialized) {
    n_ineq = Alin.rows();
    proxsuite::proxqp::dense::isize n_eq(0);
    solver.reset(new proxsuite::proxqp::dense::QP<double>(nz, n_eq, n_ineq));
    // solver->settings.initial_guess =
    // proxsuite::proxqp::InitialGuessStatus::NO_INITIAL_GUESS;
    solver->settings.initial_guess =
        proxsuite::proxqp::InitialGuessStatus::WARM_START_WITH_PREVIOUS_RESULT;
    // solver->settings.max_iter = max_iterations;
    solver->init(H, g, proxsuite::nullopt, proxsuite::nullopt, Alin, Alin_min,
                 Alin_max);

    mark_initialized();
  } else {
    solver->update(H, g, proxsuite::nullopt, proxsuite::nullopt, Alin, Alin_min,
                   Alin_max);
  }

  solver->solve();

  // https://simple-robotics.github.io/proxsuite/namespaceproxsuite_1_1proxqp.html#ae3b64c0293312c54fdfa991580ed70fd
  if ((solver->results.info.status !=
       proxsuite::proxqp::QPSolverOutput::PROXQP_SOLVED)) {
    if (solver->results.info.status ==
        proxsuite::proxqp::QPSolverOutput::PROXQP_MAX_ITER_REACHED) {
      std::cout << "Warning: proxsuite MAX_ITER_REACHED" << std::endl;
      // TODO should it be success?
      return (int)proxsuite::proxqp::QPSolverOutput::PROXQP_SOLVED;
    }

    std::stringstream ss;
    ss << "Error: proxsuite::proxqp::solve() FAILED" << std::endl;
    ss << "Error: proxsuite::proxqp::solve() FAILED" << std::endl;
    ss << "status:" << (int)solver->results.info.status << std::endl;
    ss << "PROXQP_MAX_ITER_REACHED:"
       << (int)proxsuite::proxqp::QPSolverOutput::PROXQP_MAX_ITER_REACHED
       << std::endl;
    ss << "PROXQP_PRIMAL_INFEASIBLE :"
       << (int)proxsuite::proxqp::QPSolverOutput::PROXQP_PRIMAL_INFEASIBLE
       << std::endl;
    ss << "PROXQP_DUAL_INFEASIBLE :"
       << (int)proxsuite::proxqp::QPSolverOutput::PROXQP_DUAL_INFEASIBLE
       << std::endl;
    ss << "PROXQP_NOT_RUN:"
       << (int)proxsuite::proxqp::QPSolverOutput::PROXQP_NOT_RUN << std::endl;
    print_error(ss);
  }
  return (int)solver->results.info.status;
}

Eigen::VectorXd ProxQPProblem::result() { return solver->results.x; }

#endif
