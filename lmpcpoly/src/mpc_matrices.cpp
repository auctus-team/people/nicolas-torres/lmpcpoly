#include "mpc_matrices.hpp"
#include "methods.hpp"
#include <vector>

using namespace lmpcpoly;

// #define DEBUG
#ifdef DEBUG
#define debug_lincons(str, Acols, cur_row, cur_height)                         \
  std::cout << str << " Acols:" << Acols << " cur_height:" << cur_height       \
            << " cur_row:" << cur_row << "->" << (cur_row + cur_height)        \
            << std::endl;
#else
#define debug_lincons(str, Acols, cur_row, cur_height)                         \
  do {                                                                         \
  } while (0)
#endif

///////////////////////////////////////////////////////
// CostPonderation
CostPonderation::CostPonderation(double linear_weight, double angular_weight)
    : linear_weight(linear_weight), angular_weight(angular_weight),
      vec(build_vector(linear_weight, angular_weight)) {
  // const double onedeg = 17e-3;   // [rad]
  // const double onemm = 5 * 1e-3; //  [m]
  // const double lin_w=onedeg/onemm;
  // const double lin_w=onemm/onedeg;
  // const double lin_w = 10000;
}

Eigen::VectorXd CostPonderation::build_vector(double linear_weight,
                                              double angular_weight) {
  Eigen::VectorXd myvec(6);
  myvec << linear_weight, linear_weight, linear_weight, angular_weight,
      angular_weight, angular_weight;

  return myvec;
}

///////////////////////////////////////////////////////
// MPCMatrices

MPCMatrices::MPCMatrices(HorizonParameters params)
    : params(params),
      Fx_underline(Eigen::MatrixXd::Identity(params.nx_underline, params.nz)),
      Fx_overline(block_rcircshift(
          Eigen::MatrixXd::Identity(params.nx_overline, params.nz), params.nx)),
      Fu_underline(Fu(0, params.H - 1)),
      Fud((Fu(1, params.H - 1) - Fu(0, params.H - 2)) / params.hdt),
      Fudd((Fu(2, params.H - 1) - 2 * Fu(1, params.H - 2) +
            Fu(0, params.H - 3)) /
           (params.hdt * params.hdt)),
      // Fudd(((Fu(2, params.H - 1) - Fu(1, params.H - 2)) / params.hdt -
      //       (Fu(1, params.H - 2) - Fu(0, params.H - 3)) / params.hdt) /
      //      params.hdt),
      X_overline_iden(
          -Eigen::MatrixXd::Identity(params.nx_overline, params.nx_overline)),
      A(Eigen::MatrixXd::Identity(params.nx, params.nx)), // A = np.eye(nx)
      // private members
      initial_u{Eigen::VectorXd::Zero(params.nu)},
      initial_ud{Eigen::VectorXd::Zero(params.nu)},
      initial_state{Eigen::MatrixXd::Identity(params.nx, params.nz),
                    Eigen::VectorXd::Zero(params.nx)},
      noninitial_equality{Eigen::MatrixXd::Zero(params.nx_overline, params.nz),
                          Eigen::VectorXd::Zero(params.nx_overline)},
      u_lims{Eigen::VectorXd::Zero(params.nu),
             Eigen::VectorXd::Zero(params.nu)},
      u_cons{Fu_underline,
             {Eigen::VectorXd::Zero(params.nu_underline),
              Eigen::VectorXd::Zero(params.nu_underline)}},
      ud_lims{Eigen::VectorXd::Zero(params.nu),
              Eigen::VectorXd::Zero(params.nu)},
      ud_cons{Fud,
              {Eigen::VectorXd::Zero(params.nud_all),
               Eigen::VectorXd::Zero(params.nud_all)}},
      udd_lims{Eigen::VectorXd::Zero(params.nu),
               Eigen::VectorXd::Zero(params.nu)},
      // TODO WHY ndt?????
      udd_cons{params.ndt * Fudd,
               {Eigen::VectorXd::Zero(params.nudd_all),
                Eigen::VectorXd::Zero(params.nudd_all)}} {
  // TODO: need to construct Alin/max/min
  Eigen::VectorXd ud_max = Eigen::VectorXd::Zero(6);
  // NOTE : biggest cube inside R-sphere=> L=2R/sqrt(3)
  ud_max << 13, 13, 13, 25, 25, 25;
  // ud_max = 2*ud_max/sqrt(3);
  // Eigen::VectorXd ud_min = -Eigen::VectorXd::Constant(6, OSQP_INFTY);
  Eigen::VectorXd ud_min(-ud_max);
  ud_lims = {ud_min, ud_max};

  Eigen::VectorXd udd_max = Eigen::VectorXd::Zero(6);
  udd_max << 6500, 6500, 6500, 12500, 12500, 12500;
  Eigen::VectorXd udd_min(-udd_max);

  Eigen::VectorXd u_max = Eigen::VectorXd::Zero(6);
  u_max << 1.7, 1.7, 1.7, 2.5, 2.5, 2.5;
  // u_max = 2*u_max/sqrt(3);
  Eigen::VectorXd u_min(-u_max);
  resize_if_necessary();
  if (params.nu == 6) {
    // TODO if non-default u size, then these should be initialized manually
    std::cout << __LINE__ << ":" << __FUNCTION__
              << "initialize u & ud limits from default values" << std::endl;
    update_u_limits({u_min, u_max});
    update_ud_limits({ud_min, ud_max});
    update_udd_limits({udd_min, udd_max});
  }

  // sanity checks
  assert(initial_state.A.rows() == initial_state.b.rows());
  assert(noninitial_equality.A.rows() == noninitial_equality.b.rows());

  assert(u_cons.A.rows() == u_cons.b.max.rows());
  assert(u_cons.A.rows() == u_cons.b.min.rows());
  assert(u_cons.b.max.cols() == 1);
  assert(u_cons.b.min.cols() == 1);

  // print_size("ud_cons.A:", ud_cons.A);
  // print_size("ud_cons.b.max:", ud_cons.b.max);
  // print_size("ud_cons.b.min:", ud_cons.b.min);

  // print_size("u_cons.A:", u_cons.A);
  // print_size("u_cons.b.max:", u_cons.b.max);
  // print_size("u_cons.b.min:", u_cons.b.min);

  assert(ud_cons.A.rows() == ud_cons.b.max.rows());
  assert(ud_cons.A.rows() == ud_cons.b.min.rows());
  assert(ud_cons.b.max.cols() == 1);
  assert(ud_cons.b.min.cols() == 1);

  // column checks
  assert(initial_state.A.cols() == initial_state.A.cols());
  assert(initial_state.A.cols() == noninitial_equality.A.cols());
  assert(initial_state.A.cols() == u_cons.A.cols());
  assert(initial_state.A.cols() == ud_cons.A.cols());

  update_Qreg();
  update_cost_ponderation();
  update_extra_constraints();
}

// ****************************************************
// public methods
Eigen::MatrixXd MPCMatrices::Fu(unsigned int ind0, unsigned int indf) const {
  unsigned int delta_ind = indf - ind0;

  if (indf <= ind0) {
    std::stringstream ss;
    ss << __LINE__ << ":MPCMatrices::Fu  Error:";
    ss << "ind0 cannot be lower or equal to indf" << std::endl;
    ss << "ind0:" << ind0 << std::endl;
    ss << "indf:" << indf << std::endl;
    ss << "delta_ind:" << delta_ind << std::endl;
    print_error(ss);
  }

  if (params.H < indf) {
    std::stringstream ss;
    ss << __LINE__ << ":MPCMatrices::Fu  Error:";
    ss << "indf cannot be higher than H" << std::endl;
    ss << "ind0:" << ind0 << std::endl;
    ss << "indf:" << indf << std::endl;
    ss << "delta_ind:" << delta_ind << std::endl;
    ss << "H:" << params.H << std::endl;
    print_error(ss);
  }

  // std::stringstream ss;
  // ss << __LINE__ << "-----------------------------" << std::endl;
  // ss << "ind0:" << ind0 << std::endl;
  // ss << "indf:" << indf << std::endl;
  // ss << "delta_ind:" << delta_ind << std::endl;
  // ss << "H:" << params.H << std::endl;
  // ss << __LINE__ << "-----------------------------" << std::endl;
  // std::cout << ss.str();

  return block_rcircshift(
      Eigen::MatrixXd::Identity((delta_ind + 1) * params.nu, params.nz),
      params.nx_hat + ind0 * params.nu);
}

VectorLimits MPCMatrices::u_max() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return ulim_eff();
  // return {u_cons.b.min.segment(0, params.nu),
  //         u_cons.b.max.segment(0, params.nu)};
}

VectorLimits MPCMatrices::ud_max() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return udlim_eff();
  // return {u_cons.b.min.segment(0, params.nu),
  //         u_cons.b.max.segment(0, params.nu)};
}

VectorLimits MPCMatrices::udd_max() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return uddlim_eff();
}

void MPCMatrices::update_wreg(double w) {
  std::lock_guard<std::mutex> guard(_mutex);
  _wreg = w;
  update_Qreg();
}

double MPCMatrices::wreg() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _wreg;
}

void MPCMatrices::update_initial_u(Eigen::VectorXd u) {
  std::lock_guard<std::mutex> guard(_mutex);
  initial_u = u;
  update_u_constraints();
}

void MPCMatrices::update_initial_ud(Eigen::VectorXd ud) {
  std::lock_guard<std::mutex> guard(_mutex);
  initial_ud = ud;
  update_u_constraints();
  update_ud_constraints();
}

void MPCMatrices::update_u_scaling(double scaling) {
  std::lock_guard<std::mutex> guard(_mutex);
  u_scaling_factor = scaling;
  update_u_constraints();
}

void MPCMatrices::update_u_limits(VectorLimits lim) {
  std::lock_guard<std::mutex> guard(_mutex);
  u_lims = lim;
  update_u_constraints();
}

void MPCMatrices::update_ud_scaling(double scaling) {
  std::lock_guard<std::mutex> guard(_mutex);
  ud_scaling_factor = scaling;
  update_u_constraints(); // depends on ud_scaling_factor
  update_ud_constraints();
}

void MPCMatrices::update_ud_limits(VectorLimits lim) {
  std::lock_guard<std::mutex> guard(_mutex);
  ud_lims = lim;
  update_u_constraints(); // depends on ud_lims
  update_ud_constraints();
}

void MPCMatrices::update_udd_scaling(double scaling) {
  std::lock_guard<std::mutex> guard(_mutex);
  udd_scaling_factor = scaling;
  update_u_constraints(); // depends on udd_lims
  update_ud_constraints();
  update_udd_constraints();
}

void MPCMatrices::update_udd_limits(VectorLimits lim) {
  std::lock_guard<std::mutex> guard(_mutex);
  udd_lims = lim;
  update_u_constraints(); // depends on udd_lims
  update_ud_constraints();
  update_udd_constraints();
}

void MPCMatrices::update_initial_state(Eigen::VectorXd x0) {
  std::lock_guard<std::mutex> guard(_mutex);
  assert(initial_state.b.rows() == x0.rows());
  assert(initial_state.b.cols() == x0.cols());
  initial_state.b = x0;
  update_nonextra_constraints();
}

CostFunctionLS MPCMatrices::cost_function() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return {cost.a, cost.b, Qreg};
}

LinearInequalityConstraints MPCMatrices::linear_constraints() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return {current_cons.A, current_cons.b};
}

void MPCMatrices::update_extra_linear_constraints(
    LinearInequalityConstraints constraints) {
  std::lock_guard<std::mutex> guard(_mutex);
  extra_cons = constraints;
  update_extra_constraints();
}

void MPCMatrices::update_linear_horizon_matrices(const Eigen::VectorXd x0,
                                                 const Eigen::VectorXd xf
                                                 __attribute__((unused))) {
  std::lock_guard<std::mutex> guard(_mutex);
  // NOTE: this is for:
  // X_overline = A_underline X_underline + B_underline U_underline
  // TODO: think about adding u0 (for AeqU_1)
  // B = hdt * dexp0
  Eigen::MatrixXd B = params.hdt * dlog(x0);

  // auto x0exp = pinocchio::exp6(x0);
  // auto xfexp = pinocchio::exp6(xf);
  for (unsigned int step = 0; step < params.H; step += 1) {
    unsigned int row = step * params.nx;
    unsigned int xk_col = step * params.nx;
    unsigned int uk_col = params.nx_hat + step * params.nu;

    // std::cout << "step:" << step << " row:" << row << " xk_col:" << xk_col
    //           << " uk_col:" << uk_col << " xk1_col:" << xk_col + params.nx
    //           << std::endl;

    // double alpha = step / (3 * params.H);
    // auto xk = pinocchio::log6(pinocchio::SE3::Interpolate(x0exp, xfexp,
    // alpha))
    //               .toVector();
    // Eigen::MatrixXd B = params.hdt * dlog(xk);
    noninitial_equality.A.block(row, xk_col, params.nx, params.nx) = A;
    noninitial_equality.A.block(row, uk_col, params.nx, params.nu) = B;
    noninitial_equality.A.block(row, xk_col + params.nx, params.nx, params.nx) =
        -Eigen::MatrixXd::Identity(params.nx, params.nx);
  }
}

void MPCMatrices::update_cost_ponderation(const CostPonderation _ponderation) {
  std::lock_guard<std::mutex> guard(_mutex);
  // Eigen::MatrixXd a = mpcprob.Fx_overline;
  ponderation = _ponderation;

  Eigen::MatrixXd ponder_mat(
      Eigen::MatrixXd::Identity(params.nx_overline, params.nz));
  for (unsigned int i = 0; i < params.nx_overline; i += params.nx) {
    ponder_mat(i, i) = ponderation.vec[0];
    ponder_mat(i + 1, i + 1) = ponderation.vec[1];
    ponder_mat(i + 2, i + 2) = ponderation.vec[2];
    ponder_mat(i + 3, i + 3) = ponderation.vec[3];
    ponder_mat(i + 4, i + 4) = ponderation.vec[4];
    ponder_mat(i + 5, i + 5) = ponderation.vec[5];
  }

  // terminal cost
  // double terminal_factor = 10;
  // double i = params.nx_overline - params.nx;
  // ponder_mat(i, i) = ponderation.vec[0];
  // ponder_mat(i + 1, i + 1) = terminal_factor * ponderation.vec[1];
  // ponder_mat(i + 2, i + 2) = terminal_factor * ponderation.vec[2];
  // ponder_mat(i + 3, i + 3) = terminal_factor * ponderation.vec[3];
  // ponder_mat(i + 4, i + 4) = terminal_factor * ponderation.vec[4];
  // ponder_mat(i + 5, i + 5) = terminal_factor * ponderation.vec[5];

  cost.a = Eigen::MatrixXd(block_rcircshift(ponder_mat, params.nx));
}

void MPCMatrices::update_cost_b(const Eigen::VectorXd x0,
                                const Eigen::VectorXd xf) {

  std::lock_guard<std::mutex> guard(_mutex);
  // std::cout<< "a ("<<a.cols()<<" x "<<a.rows()<<")"<<std::endl;
  // std::cout << "mpcprob.nz - mpcprob.nu_underline - nx:"<< mpcprob.nz -
  //   mpcprob.nu_underline - nx<<std::endl;
  // std::cout << "mpcprob.nx_overline - nx:" << mpcprob.nx_overline - nx
  //           << std::endl;
  // std::cout << std::endl
  //           << a.block(mpcprob.nx_overline - nx,mpcprob.nz -
  //                          mpcprob.nu_underline - nx,
  //                      nx, nx)
  //           << std::endl;

  // a.block(mpcprob.nx_overline - nx, mpcprob.nz - mpcprob.nu_underline -
  // nx, nx, nx) *= 3;

  // b = ref_se3_horizon_extended.reshape(H * 6, order = 'F')
  // TODO: need to construct horizon
  // Eigen::VectorXd b = Eigen::VectorXd::Zero(params.H * params.nx); // b
  // std::cout << "-----------------------------------"
  // << std::endl;
  // auto myx0(x0);
  // myx0 << 0.848715, 0.710175, - 0.333284, 2.90236, - 1.20235, - 0;
  // auto x0exp = pinocchio::exp6(x0);
  // auto x0exp = pinocchio::exp6(myx0);
  // auto xfexp = pinocchio::exp6(xf);

  // ROS_WARN_STREAM("x0:" << x0.transpose());
  // ROS_WARN_STREAM("xf:" << xf.transpose());

  // auto delta = x0exp.actInv(xfexp);
  // auto delta= pinocchio::SE3::Interpolate(xfexp, x0exp, 0);
  // auto deltalog = pinocchio::log6(delta);
  // ROS_WARN_STREAM("deltalog: " << deltalog);
  // ROS_WARN_STREAM("delta: " << delta);

  // print_size("cost.a:", cost.a);
  // std::cout << "cost.a.rows():" << cost.a.rows() << std::endl;
  // std::cout << "params.nx_overline:" << params.nx_overline << std::endl;
  // std::cout << "params.nx_hat:" << params.nx_hat << std::endl;
  // std::cout << "params.ny:" << params.ny << std::endl;
  // std::cout << "(params.H - 1) * params.ny:" << (params.H - 1) * params.ny
  //           << std::endl;

  assert(cost.a.rows() == params.H * params.ny);
  cost.b = Eigen::VectorXd::Zero(params.H * params.nx);
  for (double i = 0; i < params.H; ++i) {
    // time-optimal
    if (i == 0) {
      cost.b.segment(params.ny * i, params.ny) =
          x0.cwiseProduct(ponderation.vec);
    } else {
      cost.b.segment(params.ny * i, params.ny) =
          xf.cwiseProduct(ponderation.vec);
    }
  }
}

void MPCMatrices::print_matrices() const {
  std::cout << str_matrices() << std::endl;
}

std::string MPCMatrices::str_matrices() const {
  std::stringstream ss;
  ss << str_mat("Fx_underline:", Fx_underline) << std::endl;
  ss << str_mat("Fx_overline:", Fx_overline) << std::endl;
  ss << str_mat("Fu_underline:", Fu_underline) << std::endl;
  ss << str_mat("Fud:", Fud) << std::endl;
  ss << str_mat("Fudd:", Fudd) << std::endl;

  ss << str_mat("initial_u", initial_u) << std::endl;
  ss << str_mat("u_lims.max", u_lims.max) << std::endl;
  ss << str_mat("u_lims.min", u_lims.min) << std::endl;
  ss << str_mat("ud_lims.max", ud_lims.max) << std::endl;
  ss << str_mat("ud_lims.min", ud_lims.min) << std::endl;

  ss << str_mat("initial_state.A", initial_state.A) << std::endl;
  ss << str_mat("initial_state.b", initial_state.b) << std::endl;

  ss << str_mat("noninitial_equality.A", noninitial_equality.A) << std::endl;
  ss << str_mat("noninitial_equality.b", noninitial_equality.b) << std::endl;

  ss << str_mat("u_cons.A", u_cons.A) << std::endl;
  ss << str_mat("u_cons.b.min", ulim_eff().min) << std::endl;
  ss << str_mat("u_cons.b.max", ulim_eff().max) << std::endl;

  ss << str_mat("ud_cons.A", ud_cons.A) << std::endl;
  ss << str_mat("ud_cons.b.min", udlim_eff().min) << std::endl;
  ss << str_mat("ud_cons.b.max", udlim_eff().max) << std::endl;

  ss << str_mat("udd_cons.A", udd_cons.A) << std::endl;
  ss << str_mat("udd_cons.b.min", uddlim_eff().min) << std::endl;
  ss << str_mat("udd_cons.b.max", uddlim_eff().max) << std::endl;

  ss << str_mat("current_cons.A", current_cons.A) << std::endl;
  ss << str_mat("current_cons.b.min", current_cons.b.min) << std::endl;
  ss << str_mat("current_cons.b.max", current_cons.b.max);

  return ss.str();
}

// ****************************************************
VectorLimits MPCMatrices::ulim_eff() const {
  return {u_scaling_factor * u_lims.min, u_scaling_factor * u_lims.max};
}

VectorLimits MPCMatrices::udlim_eff() const {
  return {ud_scaling_factor * ud_lims.min, ud_scaling_factor * ud_lims.max};
}

VectorLimits MPCMatrices::uddlim_eff() const {
  return {udd_scaling_factor * udd_lims.min, udd_scaling_factor * udd_lims.max};
}

void MPCMatrices::update_Qreg() {
  Eigen::MatrixXd freg = Fu_underline;
  Qreg = 2 * _wreg * (freg.transpose() * freg);

  // // ud
  // Eigen::MatrixXd freg2 = Fu(1, params.H - 1) - Fu(0, params.H - 2);
  // // udd
  // Eigen::MatrixXd freg3 =
  //     Fu(2, params.H - 1) - 2 * Fu(1, params.H - 2) + Fu(0, params.H - 3);
  // Qreg = 2 * _wreg *
  //        (freg.transpose() * freg + 10 * freg2.transpose() * freg2 +
  //         100 * freg3.transpose() * freg3);
}

unsigned int MPCMatrices::nonextra_rows() const {
  //////////////////////////////////////////////////////////////
  // without udd
  // unsigned int A_rows = initial_state.A.rows() + noninitial_equality.A.rows()
  // +
  //                       u_cons.A.rows() + ud_cons.A.rows();
  // // A_rows == b_rows
  // assert(A_rows == initial_state.b.rows() + noninitial_equality.b.rows() +
  //                      u_cons.b.max.rows() + ud_cons.b.max.rows());

  //////////////////////////////////////////////////////////////
  // with udd
  unsigned int A_rows = initial_state.A.rows() + noninitial_equality.A.rows() +
                        u_cons.A.rows() + ud_cons.A.rows() + udd_cons.A.rows();
  // A_rows == b_rows
  assert(A_rows == initial_state.b.rows() + noninitial_equality.b.rows() +
                       u_cons.b.max.rows() + ud_cons.b.max.rows() +
                       udd_cons.b.max.rows());
  return A_rows;
}

unsigned int MPCMatrices::total_rows() const {
  return nonextra_rows() + extra_cons.A.rows();
}

void MPCMatrices::update_initial_state_constraints() {
  unsigned int Acols = initial_state.A.cols();
  unsigned int cur_row = 0;
  unsigned int cur_height = initial_state.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // initial state
  current_cons.A.block(cur_row, 0, cur_height, Acols) = initial_state.A;
  current_cons.b.max.segment(cur_row, cur_height) = initial_state.b;
  current_cons.b.min.segment(cur_row, cur_height) = initial_state.b;
}

// void MPCMatrices::update_u_polytope_constraints() {
//   resize_if_necessary();

//   unsigned int Acols = initial_state.A.cols();

//   unsigned int cur_row = nonextra_rows();
//   unsigned int cur_height = extra_cons.A.rows();

//   // extra constraints (like polytopes)
//   if (u_polytope.A.cols() != 0) {
//     u_polytope.A.block(cur_row, 0, cur_height, Acols) = extra_cons.A;
//     u_polytope.b.max.segment(cur_row, cur_height) = extra_cons.b.max;
//     u_polytope.b.min.segment(cur_row, cur_height) = extra_cons.b.min;
//   }
// }

void MPCMatrices::update_noninitial_equality_constraints() {
  unsigned int Acols = initial_state.A.cols();
  unsigned int cur_row = initial_state.A.rows();
  unsigned int cur_height = noninitial_equality.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // non initial equality constraints
  current_cons.A.block(cur_row, 0, cur_height, Acols) = noninitial_equality.A;
  current_cons.b.max.segment(cur_row, cur_height) = noninitial_equality.b;

  current_cons.b.min.segment(cur_row, cur_height) = noninitial_equality.b;
}

void MPCMatrices::update_u_constraints() {
  // Eigen::VectorXd umax = ulim_eff().max.cwiseMin(initial_u);
  // Eigen::VectorXd umin = ulim_eff().min.cwiseMax(initial_u);

  // assert there's continuity with initial_u
  // Eigen::VectorXd umax =
  //     ulim_eff().max.cwiseMin(udlim_eff().max * params.dt + initial_u);
  // Eigen::VectorXd umin =
  //     ulim_eff().min.cwiseMax(udlim_eff().min * params.dt + initial_u);

  // take into account jerk limits
  // assert there's continuity with initial_u and initial_ud
  Eigen::VectorXd udmax =
      udlim_eff().max.cwiseMin(uddlim_eff().max * params.dt + initial_ud);
  Eigen::VectorXd udmin =
      udlim_eff().min.cwiseMax(uddlim_eff().min * params.dt + initial_ud);
  Eigen::VectorXd umax =
      udmax.cwiseMin(udlim_eff().max * params.dt + initial_u);
  Eigen::VectorXd umin =
      udmin.cwiseMax(udlim_eff().min * params.dt + initial_u);

  std::stringstream ss;
  ss << "Warning: initial_u incompatible";
  ss << "   <<=============" << std::endl;
  ss << "Warning: initial_u incompatible";
  ss << "   <<=============" << std::endl;
  ss << "inds: ";
  bool initial_bounds_error = false;

  for (unsigned int i = 0; i < params.nu; ++i) {
    if (umax(i) < initial_u(i)) {
      initial_bounds_error = true;
      ss << i << "> ";
    }
    if (umin(i) > initial_u(i)) {
      initial_bounds_error = true;
      ss << i << "< ";
    }
  }
  ss << std::endl;

  // relax constraints if initial_u is already violating
  if (initial_bounds_error) {
    ss << str_mat("   initial_u:", initial_u) << std::endl;
    ss << "before/after:" << std::endl;
    ss << str_mat("      umax:", umax) << std::endl;
    umax = ulim_eff().max.cwiseMax(initial_u + udlim_eff().min * params.hdt);
    ss << str_mat("      umax:", umax) << std::endl;

    ss << str_mat("      umin:", umin) << std::endl;
    umin = ulim_eff().min.cwiseMin(initial_u + udlim_eff().max * params.hdt);
    ss << str_mat("      umin:", umin) << std::endl;
    print_error(ss);

    for (unsigned int i = 0; i < params.H; ++i) {
      u_cons.b.max.segment(params.nu * i, params.nu) = umax;
      u_cons.b.min.segment(params.nu * i, params.nu) = umin;
      umax = ulim_eff().max.cwiseMax(umax + udlim_eff().min * params.hdt);
      umin = ulim_eff().min.cwiseMin(umin + udlim_eff().max * params.hdt);
    }
  } else {
    // no need to relax constraints if they're compatible
    u_cons.b.max.segment(0, params.nu) = umax;
    u_cons.b.min.segment(0, params.nu) = umin;
    for (unsigned int i = 1; i < params.H; ++i) {
      u_cons.b.max.segment(params.nu * i, params.nu) = ulim_eff().max;
      u_cons.b.min.segment(params.nu * i, params.nu) = ulim_eff().min;
    }
  }

  // actually update global linear constraints
  unsigned int Acols = initial_state.A.cols();
  unsigned int cur_row = initial_state.A.rows() + noninitial_equality.A.rows();
  unsigned int cur_height = u_cons.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // u
  current_cons.A.block(cur_row, 0, cur_height, Acols) = u_cons.A;
  current_cons.b.max.segment(cur_row, cur_height) = u_cons.b.max;
  current_cons.b.min.segment(cur_row, cur_height) = u_cons.b.min;
}

void MPCMatrices::update_ud_constraints() {
  // TODO DANGER!! it seems ddq_d is too noisy and makes
  // initial_ud NOT trust-worthy
  // take into account jerk limits
  // Eigen::VectorXd udmax =
  //     udlim_eff().max.cwiseMin(uddlim_eff().max * params.dt + initial_ud);
  // Eigen::VectorXd udmin =
  //     udlim_eff().min.cwiseMax(uddlim_eff().min * params.dt + initial_ud);

  // only for when initial_u is used as exact constraint limits
  // double dt = params.hdt;
  // Eigen::VectorXd ud = initial_u / dt - initial_ud;
  // ud.setZero();
  // Eigen::VectorXd udmax = udlim_eff().max.cwiseMin(ud);
  // Eigen::VectorXd udmin = udlim_eff().min.cwiseMax(ud);

  Eigen::VectorXd udmax = udlim_eff().max;
  Eigen::VectorXd udmin = udlim_eff().min;

  std::stringstream ss;
  ss << "Warning: initial_ud incompatible";
  ss << "   <<=============" << std::endl;
  ss << "Warning: initial_ud incompatible";
  ss << "   <<=============" << std::endl;
  ss << "inds: ";
  bool initial_bounds_error = false;

  for (unsigned int i = 0; i < params.nu; ++i) {
    if (udmax(i) < initial_ud(i)) {
      initial_bounds_error = true;
      ss << i << "> ";
    }
    if (udmin(i) > initial_ud(i)) {
      initial_bounds_error = true;
      ss << i << "< ";
    }
  }
  ss << std::endl;

  ////////////////////////////////////////////////////////
  // TODO DEBUG CODE
  if (initial_bounds_error) {
    ss << str_mat("   initial_ud:", initial_ud) << std::endl;
    ss << "before/after:" << std::endl;
    ss << str_mat("        udmax:", udmax) << std::endl;
    ss << str_mat("        udmin:", udmin) << std::endl;
    print_error(ss);
  }
  initial_bounds_error = false;
  ////////////////////////////////////////////////////////

  // relax constraints if initial_ud is already violating
  if (initial_bounds_error) {
    ss << str_mat("   initial_ud:", initial_ud) << std::endl;
    ss << "before/after:" << std::endl;
    ss << str_mat("     udmax:", udmax) << std::endl;
    udmax =
        udlim_eff().max.cwiseMax(initial_ud + uddlim_eff().min * params.hdt);
    ss << str_mat("     udmax:", udmax) << std::endl;

    ss << str_mat("     udmin:", udmin) << std::endl;
    udmin =
        udlim_eff().min.cwiseMin(initial_ud + uddlim_eff().max * params.hdt);
    ss << str_mat("     udmin:", udmin) << std::endl;
    print_error(ss);

    for (unsigned int i = 0; i < params.H - 1; ++i) {
      ud_cons.b.max.segment(params.nu * i, params.nu) = udmax;
      ud_cons.b.min.segment(params.nu * i, params.nu) = udmin;
      udmax = udlim_eff().max.cwiseMax(udmax + uddlim_eff().min * params.hdt);
      udmin = udlim_eff().min.cwiseMin(udmin + uddlim_eff().max * params.hdt);
    }

  } else {
    // no need to relax constraints if they're compatible
    ud_cons.b.max.segment(0, params.nu) = udmax;
    ud_cons.b.min.segment(0, params.nu) = udmin;
    for (unsigned int i = 1; i < params.H - 1; ++i) {
      ud_cons.b.max.segment(params.nu * i, params.nu) = udlim_eff().max;
      ud_cons.b.min.segment(params.nu * i, params.nu) = udlim_eff().min;
    }
  }

  unsigned int Acols = initial_state.A.cols();
  unsigned int cur_row =
      initial_state.A.rows() + noninitial_equality.A.rows() + u_cons.A.rows();
  unsigned int cur_height = ud_cons.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // ud
  current_cons.A.block(cur_row, 0, cur_height, Acols) = ud_cons.A;
  current_cons.b.max.segment(cur_row, cur_height) = ud_cons.b.max;
  current_cons.b.min.segment(cur_row, cur_height) = ud_cons.b.min;
}

void MPCMatrices::update_udd_constraints() {
  for (unsigned int i = 0; i < params.H - 2; ++i) {
    udd_cons.b.max.segment(params.nu * i, params.nu) = uddlim_eff().max;
    udd_cons.b.min.segment(params.nu * i, params.nu) = uddlim_eff().min;
  }

  unsigned int Acols = initial_state.A.cols();
  unsigned int cur_row = initial_state.A.rows() + noninitial_equality.A.rows() +
                         u_cons.A.rows() + ud_cons.A.rows();
  unsigned int cur_height = udd_cons.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // udd
  current_cons.A.block(cur_row, 0, cur_height, Acols) = udd_cons.A;
  current_cons.b.max.segment(cur_row, cur_height) = udd_cons.b.max;
  current_cons.b.min.segment(cur_row, cur_height) = udd_cons.b.min;
}

void MPCMatrices::update_nonextra_constraints() {
  update_initial_state_constraints();
  update_noninitial_equality_constraints();
  update_u_constraints();
  update_ud_constraints();
  update_udd_constraints();
}

void MPCMatrices::resize_if_necessary() {
  assert(initial_state.A.cols() == noninitial_equality.A.cols());
  assert(initial_state.A.cols() == u_cons.A.cols());
  assert(initial_state.A.cols() == ud_cons.A.cols());
  assert(initial_state.A.cols() == udd_cons.A.cols());

  assert(initial_state.b.cols() == noninitial_equality.b.cols());
  assert(initial_state.b.cols() == u_cons.b.max.cols());
  assert(initial_state.b.cols() == ud_cons.b.max.cols());
  assert(initial_state.b.cols() == udd_cons.b.max.cols());
  assert(initial_state.b.cols() == 1);

  if (extra_cons.A.cols() != 0) {
    assert(extra_cons.b.max.rows() != 0);
    assert(extra_cons.b.min.rows() != 0);
    assert(initial_state.A.cols() == extra_cons.A.cols());
    assert(initial_state.b.cols() == extra_cons.b.max.cols());
    assert(initial_state.b.cols() == extra_cons.b.min.cols());
  }

  if (current_cons.A.rows() != total_rows()) {
    current_cons.A.resize(total_rows(), initial_state.A.cols());
    current_cons.b.max.resize(total_rows(), 1);
    current_cons.b.min.resize(total_rows(), 1);

    current_cons.A.setZero();
    current_cons.b.max.setZero();
    current_cons.b.min.setZero();
    update_nonextra_constraints();
  }
}

void MPCMatrices::update_extra_constraints() {
  resize_if_necessary();

  unsigned int Acols = initial_state.A.cols();

  unsigned int cur_row = nonextra_rows();
  unsigned int cur_height = extra_cons.A.rows();
  debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);

  // extra constraints (like polytopes)
  if (extra_cons.A.cols() != 0) {
    debug_lincons(__FUNCTION__, Acols, cur_row, cur_height);
    current_cons.A.block(cur_row, 0, cur_height, Acols) = extra_cons.A;
    current_cons.b.max.segment(cur_row, cur_height) = extra_cons.b.max;
    current_cons.b.min.segment(cur_row, cur_height) = extra_cons.b.min;
  }
}
