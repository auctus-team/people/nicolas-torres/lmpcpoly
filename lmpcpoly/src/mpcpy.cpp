#include <Eigen/Core>
#include <Eigen/Dense>
#include <chrono>
#include <tuple>
#include <vector>
// #include <iostream>

#include <pybind11/eigen.h>
#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include "horizon.hpp"
#include "methods.hpp"
#include "mpc.hpp"
#include "mpcacc.hpp"
#include "polycap_extended.hpp"
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <pinocchio/spatial/skew.hpp>

#include <cmath>
#include <limits>

// #include <qpOASES.hpp>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

using Vector6d = Eigen::Matrix<double, 6, 1>;
using RefVector6d = const Eigen::Ref<Vector6d>;

struct ResTime {
  ResTime() : result(Vector6d::Zero()) {}
  void tick() { startt = std::chrono::steady_clock::now(); }
  void tock() {
    endd = std::chrono::steady_clock::now();
    total_ns =
        std::chrono::duration_cast<std::chrono::nanoseconds>(endd - startt)
            .count();
  }

  double total_ns;
  std::chrono::time_point<std::chrono::steady_clock> startt;
  std::chrono::time_point<std::chrono::steady_clock> endd;
  Vector6d result;
};

ResTime test_vec(std::vector<Vector6d> vectors) {

  ResTime restime;
  restime.tick();
  for (const auto &vec : vectors) {
    restime.result += vec;
    lmpcpoly::print_mat("vec", vec);
    for (unsigned int i = 0; i < vec.rows(); ++i) {
      double elem = vec(i);
      std::cout << "elem:" << elem << std::endl;
      // std::cout << "elem==qpOASES::INFTY:" << (elem == qpOASES::INFTY)
      //           << std::endl;
      // std::cout << "elem==-qpOASES::INFTY:" << (elem == -qpOASES::INFTY)
      // << std::endl;
      std::cout << "elem==FP_INFINITE:" << (elem == FP_INFINITE) << std::endl;
      std::cout << "elem==-FP_INFINITE:" << (elem == -FP_INFINITE) << std::endl;
      std::cout << "elem==1e20:" << (elem == 1e20) << std::endl;
      std::cout << "elem==-1e20:" << (elem == -1e20) << std::endl;
      std::cout << "elem==std::numeric_limits<double>::infinity():"
                << (elem == std::numeric_limits<double>::infinity())
                << std::endl;
      std::cout << "elem==-std::numeric_limits<double>::infinity():"
                << (elem == -std::numeric_limits<double>::infinity())
                << std::endl;
    }
  }
  // std::cout << "qpOASES::INFTY:" << qpOASES::INFTY << std::endl;
  // std::cout << "-qpOASES::INFTY:" << -qpOASES::INFTY << std::endl;
  // std::cout << "qpOASES::INFTY==1e12:" << (qpOASES::INFTY == 1e12) <<
  // std::endl; std::cout << "qpOASES::INFTY==1e20:" << (qpOASES::INFTY == 1e20)
  // << std::endl; std::cout << "-qpOASES::INFTY==-1e20:" << (-qpOASES::INFTY ==
  // -1e20)
  //           << std::endl;
  restime.tock();
  return restime;
};

namespace py = pybind11;
using namespace lmpcpoly;

PYBIND11_MODULE(pylmpcpoly, m) {
  // MPCParams
  py::class_<MPCParams>(m, "MPCParams")
      .def(py::init<double, double>(), py::arg("w_a") = 1e-6,
           py::arg("w_terminal") = 10)
      .def_readonly("w_a", &MPCParams::w_a)
      .def_readonly("w_terminal", &MPCParams::w_terminal);

  // MPCAccHorizon
  py::class_<MPCAccHorizon>(m, "MPCAccHorizon")
      .def(py::init<unsigned int, double>(), py::arg("H"), py::arg("T"))
      .def_readonly("p", &MPCAccHorizon::p)
      .def_readonly("v", &MPCAccHorizon::v)
      .def_readonly("a", &MPCAccHorizon::a)
      .def_readonly("j", &MPCAccHorizon::j)
      .def("duration", &MPCAccHorizon::duration)
      .def("p_at", &MPCAccHorizon::p_at, py::arg("t"))
      .def("v_at", &MPCAccHorizon::v_at, py::arg("t"))
      .def("a_at", &MPCAccHorizon::a_at, py::arg("t"))
      .def("j_at", &MPCAccHorizon::j_at, py::arg("t"));

  // StateSpaceAcc
  py::class_<StateSpaceAcc>(m, "StateSpaceAcc")
      .def(py::init<double, unsigned int>(), py::arg("T"), py::arg("H"))
      .def_readonly("As", &StateSpaceAcc::As)
      .def_readonly("Aps", &StateSpaceAcc::Aps)
      .def_readonly("A", &StateSpaceAcc::A)
      .def_readonly("B", &StateSpaceAcc::B)
      .def_readonly("Ap", &StateSpaceAcc::Ap)
      .def_readonly("Ap", &StateSpaceAcc::Bp);

  // InitialConditions
  py::class_<InitialConditions>(m, "InitialConditions")
      .def(py::init<>())
      .def_readwrite("t", &InitialConditions::t)
      .def_readwrite("p", &InitialConditions::p0)
      .def_readwrite("v", &InitialConditions::v0)
      .def_readwrite("a", &InitialConditions::a0)
      .def_readwrite("j", &InitialConditions::j0);

  // LinearInequalityConstraints
  py::class_<LinearInequalityConstraints>(m, "LinearInequalityConstraints")
      .def(py::init())
      .def(py::init<Eigen::MatrixXd, VectorLimits>(), py::arg("A"),
           py::arg("b"))
      // .def(py::init<int, double>(), py::arg("n"), py::arg("value"))
      .def_readwrite("A", &LinearInequalityConstraints::A)
      .def_readwrite("b", &LinearInequalityConstraints::b);

  // HorizonBoxConstraints
  py::class_<HorizonBoxConstraints>(m, "HorizonBoxConstraints")
      .def(py::init<unsigned int, unsigned int, const Eigen::VectorXd &,
                    const Eigen::MatrixXd &, unsigned int>(),
           py::arg("horizon_constraint_steps"), py::arg("horizon_optim_steps"),
           py::arg("max"), py::arg("L") = Eigen::MatrixXd(), py::arg("nu") = 3)
      .def_readonly("L", &HorizonBoxConstraints::L)
      .def_readonly("nu", &HorizonBoxConstraints::nu)
      .def_readonly("horizon_optim_steps",
                    &HorizonBoxConstraints::horizon_optim_steps)
      .def_readonly("horizon_constraint_steps",
                    &HorizonBoxConstraints::horizon_constraint_steps)
      .def("update_bias", &HorizonBoxConstraints::update_bias)
      .def("update_box_max", &HorizonBoxConstraints::update_box_max)
      .def("update_firststep_box_max",
           &HorizonBoxConstraints::update_firststep_box_max)
      .def("lmi", &HorizonBoxConstraints::lmi)
      .def("firststep_box_max", &HorizonBoxConstraints::firststep_box_max)
      .def("horizon_box_limits", &HorizonBoxConstraints::horizon_box_limits);

  py::class_<PolytopeConstraints1, HorizonBoxConstraints>(
      m, "PolytopeConstraints1")
      .def("update_mode", &PolytopeConstraints1::update_mode)
      .def("update_firststep_polytope",
           &PolytopeConstraints1::update_firststep_polytope)
      .def("update_horizon_polytope",
           &PolytopeConstraints1::update_horizon_polytope);

  py::class_<PolytopeConstraints2, PolytopeConstraints1>(
      m, "PolytopeConstraints2");
  py::class_<PolytopeConstraints3, PolytopeConstraints1>(
      m, "PolytopeConstraints3");

  // using PositionConstraints = PolytopeConstraints2;
  // using VelocityConstraints = PolytopeConstraints3;
  // using AccelerationConstraints = PolytopeConstraints1;
  // using JerkConstraints = PolytopeConstraints2;
  m.attr("PositionConstraints") = m.attr("PolytopeConstraints2");
  m.attr("VelocityConstraints") = m.attr("PolytopeConstraints3");
  m.attr("AccelerationConstraints") = m.attr("PolytopeConstraints1");
  m.attr("JerkConstraints") = m.attr("PolytopeConstraints2");

  // MPCAcc
  py::class_<MPCAcc> mpcacc(m, "MPCAcc");
  mpcacc
      .def(py::init<unsigned int, double, MPCParams, std::string, double,
                    bool>(),
           py::arg("H"), py::arg("T"), py::arg("mpc_params") = MPCParams(),
           py::arg("solver") = "qpoases", py::arg("compute_time") = 0.01,
           py::arg("jerk_enabled") = false)
      // .def("update_pmax", &MPCAcc::update_pmax, py::arg("max"))
      // .def("update_vmax", &MPCAcc::update_vmax, py::arg("max"))
      // .def("update_amax", &MPCAcc::update_amax, py::arg("max"))
      // .def("update_jmax", &MPCAcc::update_jmax, py::arg("max"))
      // .def("update_p_polytope", &MPCAcc::update_p_polytope,
      // py::arg("polytope"),
      //      py::arg("only_first_step") = true)
      // .def("update_v_polytope", &MPCAcc::update_v_polytope,
      // py::arg("polytope"),
      //      py::arg("only_first_step") = true)
      // .def("update_a_polytope", &MPCAcc::update_a_polytope,
      // py::arg("polytope"),
      //      py::arg("only_first_step") = true)
      // .def("update_j_polytope", &MPCAcc::update_j_polytope,
      // py::arg("polytope"), py::arg("only_first_step")=true)
      .def("solve", &MPCAcc::solve, py::arg("initial_conditions"),
           py::arg("target_position"))
      .def_readonly("horizon", &MPCAcc::horizon)
      .def_readonly("H", &MPCAcc::H)
      .def_readonly("nz", &MPCAcc::nz)
      .def_readonly("nj", &MPCAcc::nj)
      .def_readonly("Apsx0_over", &MPCAcc::Apx0_over)
      .def_readonly("Avx0_over", &MPCAcc::Avx0_over)
      .def_readonly("con_p", &MPCAcc::con_p)
      .def_readonly("con_v", &MPCAcc::con_v)
      .def_readonly("con_a", &MPCAcc::con_a)
      .def_readonly("con_j", &MPCAcc::con_j)
      .def_readonly("ss", &MPCAcc::ss)
      .def_readonly("Lp", &MPCAcc::Lp)
      .def_readonly("Lv", &MPCAcc::Lv)
      .def_readonly("La", &MPCAcc::La)
      .def_readonly("Lj", &MPCAcc::Lj)
      .def_readonly("C", &MPCAcc::C);

  // ResTime
  py::class_<ResTime>(m, "ResTime")
      .def(py::init<>())
      .def_readonly("total_ns", &ResTime::total_ns)
      .def_readwrite("result", &ResTime::result);

  // HorizonParameters
  py::class_<HorizonParameters>(m, "HorizonParameters")
      .def(py::init<unsigned int, unsigned int, unsigned int, unsigned int,
                    unsigned int, double>(),
           py::arg("H"), py::arg("ndt") = 15, py::arg("nx") = 6,
           py::arg("nu") = 6, py::arg("ny") = 6, py::arg("dt") = double(1e-3))
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def("state_horizon_duration", &HorizonParameters::state_horizon_duration)
      .def("input_horizon_duration", &HorizonParameters::input_horizon_duration)
      .def("__repr__", &HorizonParameters::str)
      // TODO fix repr (should return string)
      // .def("__repr__", &HorizonParameters::str)
      // .def("__repr__",
      //      [](const HorizonParameters &params) { return params.str(); })
      // .def("__str__", &HorizonParameters::str)
      .def_readonly("H", &HorizonParameters::H)
      .def_readonly("nx", &HorizonParameters::nx)
      .def_readonly("nu", &HorizonParameters::nu)
      .def_readonly("ny", &HorizonParameters::ny)
      .def_readonly("ndt", &HorizonParameters::ndt)
      .def_readonly("dt", &HorizonParameters::dt)
      .def_readonly("nx_underline", &HorizonParameters::nx_underline)
      .def_readonly("nx_overline", &HorizonParameters::nx_overline)
      .def_readonly("nx_hat", &HorizonParameters::nx_hat)
      .def_readonly("nu_underline", &HorizonParameters::nu_underline)
      .def_readonly("nud_all", &HorizonParameters::nud_all)
      .def_readonly("nudd_all", &HorizonParameters::nudd_all)
      .def_readonly("nz", &HorizonParameters::nz)
      .def_readonly("hdt", &HorizonParameters::hdt);

  // Horizon
  py::class_<Horizon>(m, "Horizon")
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def(py::init<Horizon>(), py::arg("Horizon"))
      .def("update", &Horizon::update, py::arg("result"), py::arg("goal"),
           py::arg("pullpose"), py::arg("solve_time"))
      .def("t_horizon", &Horizon::t_horizon, py::arg("t"))
      .def("solve_time", &Horizon::solve_time)
      .def("result", &Horizon::result)
      .def("x", py::overload_cast<double>(&Horizon::x, py::const_),
           py::arg("t"))
      .def("pose", py::overload_cast<double>(&Horizon::pose, py::const_),
           py::arg("t"))
      .def("twist", &Horizon::twist, py::arg("t"))
      .def("u", &Horizon::u, py::arg("t"))
      .def("u0", &Horizon::u0)
      .def("ud0", &Horizon::ud0)
      .def("sample_horizon", &Horizon::sample_horizon, py::arg("samples") = 10)
      .def("dump_result", &Horizon::dump_result, py::arg("filepath"))
      .def("dump_pose_reference", &Horizon::dump_pose_reference,
           py::arg("filepath") = 10)
      .def_readonly("params", &Horizon::params);

  // VectorLimits
  py::class_<VectorLimits>(m, "VectorLimits")
      .def(py::init())
      .def(py::init<Eigen::VectorXd, Eigen::VectorXd>(), py::arg("min"),
           py::arg("max"))
      .def_static("Ones", &VectorLimits::Ones, py::arg("size"))
      .def_readwrite("min", &VectorLimits::min)
      .def_readwrite("max", &VectorLimits::max);

  // CostPonderation
  py::class_<CostPonderation>(m, "CostPonderation")
      .def(py::init())
      .def(py::init<double, double>(), py::arg("linear_weight"),
           py::arg("angular_weight"))
      .def_static("build_vector", &CostPonderation::build_vector,
                  py::arg("linear_weight"), py::arg("angular_weight"))
      .def_readonly("linear_weight", &CostPonderation::linear_weight)
      .def_readonly("angular_weight", &CostPonderation::angular_weight)
      .def_readonly("vec", &CostPonderation::vec);

  // CostFunctionLS
  py::class_<CostFunctionLS>(m, "CostFunctionLS")
      .def(py::init())
      .def(py::init<Eigen::MatrixXd, Eigen::VectorXd, Eigen::MatrixXd>(),
           py::arg("a"), py::arg("b"), py::arg("Hreg"))
      .def_readwrite("a", &CostFunctionLS::a)
      .def_readwrite("b", &CostFunctionLS::b)
      .def_readwrite("Hreg", &CostFunctionLS::Hreg);

  // Hd
  py::class_<Hd>(m, "Hd")
      .def(py::init())
      .def_readwrite("H", &Hd::H)
      .def_readwrite("d", &Hd::d);

  // TrianglePolygon
  py::class_<TrianglePolygon>(m, "TrianglePolygon")
      .def(py::init())
      .def_readwrite("vertices", &TrianglePolygon::vertices)
      .def_readwrite("faces", &TrianglePolygon::faces)
      .def_readwrite("indices", &TrianglePolygon::indices);

  // hyperplane shifting
  m.def("HPS", &lmpcpoly::compute_Jqd_halfplane, py::arg("qd"), py::arg("J"),
        py::arg("nu"), py::arg("rows"));
  m.def("hps", &lmpcpoly::hps, py::arg("J"), py::arg("qd"));
  m.def("hps_lmi", &lmpcpoly::hps_lmi, py::arg("A"), py::arg("min"),
        py::arg("max"));
  m.def("point_cloud_to_triangle_polygon",
        &lmpcpoly::point_cloud_to_triangle_polygon, py::arg("points_matrix"));
  m.def("vertices_from_lmi", &lmpcpoly::vertices_from_lmi, py::arg("A"),
        py::arg("min"), py::arg("max"));

  // MPCMatrices
  py::class_<MPCMatrices>(m, "MPCMatrices")
      .def(py::init<HorizonParameters>(), py::arg("HorizonParameters"))
      .def("Fu", &MPCMatrices::Fu, py::arg("ind0"), py::arg("indf"))
      .def("u_max", &MPCMatrices::u_max)
      .def("ud_max", &MPCMatrices::ud_max)
      .def("udd_max", &MPCMatrices::udd_max)
      .def_property("w_reg", &MPCMatrices::update_wreg, &MPCMatrices::wreg)
      .def("update_initial_u", &MPCMatrices::update_initial_u, py::arg("u_1"))
      .def("update_u_scaling", &MPCMatrices::update_u_scaling,
           py::arg("factor"))
      .def("update_ud_scaling", &MPCMatrices::update_ud_scaling,
           py::arg("factor"))
      .def("update_udd_scaling", &MPCMatrices::update_udd_scaling,
           py::arg("factor"))
      .def("update_u_limits", &MPCMatrices::update_u_limits, py::arg("limits"))
      .def("update_ud_limits", &MPCMatrices::update_ud_limits,
           py::arg("limits"))
      .def("update_udd_limits", &MPCMatrices::update_udd_limits,
           py::arg("limits"))
      .def("update_initial_state", &MPCMatrices::update_initial_state,
           py::arg("x0"))
      .def("cost_function", &MPCMatrices::cost_function)
      .def("linear_constraints", &MPCMatrices::linear_constraints)
      .def("update_extra_linear_constraints",
           &MPCMatrices::update_extra_linear_constraints,
           py::arg("constraints"))
      .def("update_linear_horizon_matrices",
           &MPCMatrices::update_linear_horizon_matrices, py::arg("x0"),
           py::arg("xf"))
      .def("update_cost_ponderation", &MPCMatrices::update_cost_ponderation,
           py::arg("ponderation") = CostPonderation())
      .def("update_cost_b", &MPCMatrices::update_cost_b, py::arg("x0"),
           py::arg("xf"))
      .def("__repr__", &MPCMatrices::str_matrices)
      .def_readonly("params", &MPCMatrices::params)
      .def_readonly("Fx_underline", &MPCMatrices::Fx_underline)
      .def_readonly("Fx_overline", &MPCMatrices::Fx_overline)
      .def_readonly("Fu_underline", &MPCMatrices::Fu_underline)
      .def_readonly("Fud", &MPCMatrices::Fud)
      .def_readonly("Fudd", &MPCMatrices::Fudd)
      .def_readonly("X_overline_iden", &MPCMatrices::X_overline_iden)
      .def_readonly("A", &MPCMatrices::A);

  // SolveParamters
  py::class_<SolveParameters>(m, "SolveParameters")
      .def(py::init())
      .def_readwrite("u", &SolveParameters::u)
      .def_readwrite("ud", &SolveParameters::ud)
      .def_readwrite("udd", &SolveParameters::udd)
      .def_readwrite("u_scaling_factor", &SolveParameters::u_scaling_factor)
      .def_readwrite("ud_scaling_factor", &SolveParameters::ud_scaling_factor)
      .def_readwrite("udd_scaling_factor",
                     &SolveParameters::udd_scaling_factor);

  // TimedPose
  py::class_<TimedPose>(m, "TimedPose")
      .def(py::init())
      .def_readwrite("t", &TimedPose::t)
      .def_readwrite("pose", &TimedPose::pose);

  // Goal
  py::class_<Goal>(m, "Goal")
      .def(py::init())
      .def_readwrite("start", &Goal::start)
      .def_readwrite("pose", &Goal::pose)
      .def_readwrite("u0", &Goal::u0)
      .def_readwrite("ud0", &Goal::ud0);

  // Planifier
  py::class_<Planifier> planifier_class(m, "Planifier");

  // Planifier::State
  py::enum_<Planifier::State>(planifier_class, "State", py::arithmetic())
      .value("OK", Planifier::State::OK)
      .value("Computing", Planifier::State::Computing)
      .value("Failed", Planifier::State::Failed)
      .value("NonInitialized", Planifier::State::NonInitialized)
      .value("Waiting", Planifier::State::Waiting)
      .value("Ending", Planifier::State::Ending)
      .export_values();

  planifier_class
      .def(py::init<HorizonParameters, std::string>(),
           py::arg("HorizonParameters"),
           py::arg("solver") = lmpcpoly_default_solver)
      .def(py::init<Planifier>(), py::arg("Planifier"))
      .def("status", &Planifier::status)
      .def("solve", &Planifier::solve, py::arg("goal"),
           py::arg("solve_parameters"))
      .def("update_cost_ponderation", &Planifier::update_cost_ponderation,
           py::arg("cost_ponderation"))
      .def("update_solve_parameters", &Planifier::update_solve_parameters,
           py::arg("solve_parameters"))
      .def("set_compute_time", &Planifier::set_compute_time, py::arg("t"))
      .def_property("use_origin_dlog", &Planifier::use_origin_dlog,
                    &Planifier::set_use_origin_dlog)
      .def("compute_time_limit", &Planifier::compute_time_limit)
      .def("print_matrices", &Planifier::print_matrices)
      .def("u_max", &Planifier::u_max)
      .def("ud_max", &Planifier::ud_max)
      .def("udd_max", &Planifier::udd_max)
      .def_property("wreg", &Planifier::get_wreg, &Planifier::set_wreg)
      .def("t_horizon", &Planifier::t_horizon, py::arg("t"))
      .def("horizon_duration", &Planifier::horizon_duration)
      .def("input_horizon_duration", &Planifier::input_horizon_duration)
      // TODO add update_cartvel_polytope
      // TODO add update_obstacle
      .def_readonly("params", &Planifier::params)
      .def_readonly("solver", &Planifier::solver)
      .def_readonly("mpcmat", &Planifier::mpcmat)
      // TODO fix
      // .def_readonly("qpprob", &Planifier::qpprob)
      .def_readonly("polytope_linconstraints",
                    &Planifier::polytope_linconstraints)
      .def_readonly("obstacle_linconstraints",
                    &Planifier::obstacle_linconstraints)
      .def("test", &Planifier::test, py::arg("x0"), py::arg("A"))
      .def("test2", &Planifier::test2, py::arg("x0"), py::arg("A"));

  m.doc() = R"pbdoc(
           Linear MPC library for pose trajectory planification.
    )pbdoc";

  m.def("test_vec", &test_vec, R"pbdoc(
        test sum std::vector of Vector6d
    )pbdoc");

#ifdef VERSION_INFO
  m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
  m.attr("__version__") = "dev";
#endif
}
