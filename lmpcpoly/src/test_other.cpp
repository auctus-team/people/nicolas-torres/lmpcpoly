#include <Eigen/Dense>
// #include <Eigen/Geometry>
#include <Eigen/src/Core/util/Constants.h>
#include <atomic>
#include <chrono>
#include <cmath>
#include <cstdlib>
#include <iomanip>
#include <iostream>
#include <memory>
#include <methods.hpp>
#include <thread>

using namespace std;
using namespace lmpcpoly;

constexpr double PI = 3.14159265359;
constexpr int m = 2; // % Must be 2 or 3 for plotting
constexpr int n = 5; //% Must be greater than m
using mymatrix = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
using mycolvectorn = Eigen::Matrix<double, n, 1>;
using mycolvectorm = Eigen::Matrix<double, m, 1>;
using Vector6d = Eigen::Matrix<double, 6, 1>;

struct mythread {
  mythread(bool async = true) : _async(async), _stop(false) {}

  void ensure() {
    if (_async && !_thread) {
      cout << "async: launch thread..." << endl;
      _thread.reset(new std::thread(&mythread::run_thread, this));
      _thread->detach();
    } else {
      run();
    }
  }

  void stop() { _stop = true; }

  void run_thread() {
    while (!_stop) {
      if (_delay.passed()) {
        run();
        _delay.refresh();
      }
    }

    cout << "stop received, end." << endl;
  }

  void run() { cout << "run()..." << endl; }

  std::unique_ptr<std::thread> _thread;
  lmpcpoly::Delay _delay = lmpcpoly::Delay(0.3);
  std::atomic<bool> _async;
  std::atomic<bool> _stop;
};

int main() {

  {
    cout << "------------------------------------" << endl;
    cout << "async test (threaded continuous execution)" << endl;
    bool async = true;
    // bool async = false;
    mythread t(async);
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    double wait = 1;
    lmpcpoly::Delay delay = lmpcpoly::Delay(wait);
    cout << "waiting " << wait << "s..." << endl;
    t.ensure();
    delay.refresh();
    while (!delay.passed()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    cout << "send stop " << endl;
    t.stop();
    delay.refresh();
    while (!delay.passed()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }
  {
    cout << "------------------------------------" << endl;
    cout << "no thread test (single execution)" << endl;
    bool async = false;
    mythread t(async);
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    double wait = 1;
    lmpcpoly::Delay delay = lmpcpoly::Delay(wait);
    cout << "waiting " << wait << "s..." << endl;
    t.ensure();
    delay.refresh();
    while (!delay.passed()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    cout << "send stop " << endl;
    t.stop();
    delay.refresh();
    while (!delay.passed()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
  }

  std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << __FILE__ << " DONE" << std::endl;

  exit(EXIT_SUCCESS);
}
