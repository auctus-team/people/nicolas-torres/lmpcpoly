
#include "mpcacc_tan.hpp"

#include <atomic>
#include <chrono>
#include <limits>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

// #include "horizon.hpp"
#include "methods.hpp"
// #include "mpc_matrices.hpp"
// #include "poses.hpp"
#include "qp_problem.hpp"
#include <fstream>
#include <future>
#include <iostream>
#include <memory>

#include <limits>
#include <qpOASES.hpp>

using namespace lmpcpoly;

MPCAccTangentHorizon::MPCAccTangentHorizon(unsigned int H, double T)
    : _H(H), _T(T) {}

void MPCAccTangentHorizon::update(double t0, const pinocchio::SE3 &pullpose,
                                  const Eigen::VectorXd &p,
                                  const Eigen::VectorXd &v,
                                  const Eigen::VectorXd &a,
                                  const Eigen::VectorXd &j) {
  this->pull_pose = pullpose;
  this->p = p;
  this->v = v;
  this->a = a;
  this->j = j;
  _t0 = t0;
}

double MPCAccTangentHorizon::duration() { return _H * _T; }
bool MPCAccTangentHorizon::after_horizon(double t) {
  return t > _t0 + duration();
}
bool MPCAccTangentHorizon::before_horizon(double t) { return t < _t0; }

std::vector<pinocchio::SE3>
MPCAccTangentHorizon::sample_horizon(unsigned int samples) {
  if (samples < 5) {
    samples = 5;
  }
  double step_dt = duration() / (samples - 1);
  std::vector<pinocchio::SE3> ret;
  for (unsigned int i = 0; i < samples; ++i) {
    double t = _t0 + i * step_dt;
    // std::cout << "step_dt:" << step_dt << "s  t:" << t << "s  i:" << i
    //           << std::endl;
    ret.push_back(p_at(t));
  }
  return ret;
}

double MPCAccTangentHorizon::t_horizon(double t) { return t - _t0; }

pinocchio::SE3 MPCAccTangentHorizon::p_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::p_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return p_at(duration());
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::p_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return p_at(_t0);
  }
  if (p.rows() == 0) {
    return {Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero()};
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Vector6d pk, pk1;
  pk = p.segment(6 * st.idx, 6);
  pk1 = p.segment(6 * (st.idx + 1), 6);
  // print_mat("pk", pk);
  // print_mat("pk1", pk1);
  // print_mat("st.alpha * (pk1 - pk)", st.alpha * (pk1 - pk));

  pinocchio::SE3 pose = pinocchio::SE3::Interpolate(
      pinocchio::exp6(pk), pinocchio::exp6(pk1), st.alpha);
  return pull_pose.act(pose);
}
Vector6d MPCAccTangentHorizon::v_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::v_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::v_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (v.rows() == 0) {
    return Vector6d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Vector6d pk, pk1;
  pk = v.segment(6 * st.idx, 6);
  pk1 = v.segment(6 * (st.idx + 1), 6);

  return pk + st.alpha * (pk1 - pk);
}
Vector6d MPCAccTangentHorizon::a_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::a_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::a_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (a.rows() == 0) {
    return Vector6d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Vector6d pk, pk1;
  pk = a.segment(6 * st.idx, 6);
  // pk1 = a.segment(6 * (st.idx + 1), 6);

  // return pk + st.alpha * (pk1 - pk);
  return pk;
}
Vector6d MPCAccTangentHorizon::j_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::j_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccTangentHorizon::j_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Vector6d::Zero();
  }
  if (j.rows() == 0) {
    return Vector6d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Vector6d pk, pk1;
  pk = j.segment(6 * st.idx, 6);
  // pk1 = j.segment(6 * (st.idx + 1), 6);
  // return pk + st.alpha * (pk1 - pk);
  return pk;
}

StateSpaceAccTangent::StateSpaceAccTangent(double T, unsigned int H)
    : T(T), A(Eigen::MatrixXd::Identity(2 * nu, 2 * nu)) {
  A.block(0, nu, nu, nu) = T * Eigen::MatrixXd::Identity(nu, nu);
  Ap = A.block(0, 0, nu, 2 * nu);
  Av = A.block(nu, 0, nu, 2 * nu);

  Bp = (T * T / 2) * Eigen::MatrixXd::Identity(nu, nu);
  Bv = T * Eigen::MatrixXd::Identity(nu, nu);
  B.resize(2 * nu, nu);
  B.block(0, 0, nu, nu) = Bp;
  B.block(nu, 0, nu, nu) = Bv;

  // print_mat("A", A);
  // print_mat("B", B);

  As.push_back(Eigen::MatrixXd::Identity(A.rows(), A.cols()));
  Aps.push_back(As.back().block(0, 0, nu, 2 * nu));
  Avs.push_back(As.back().block(nu, 0, nu, 2 * nu));
  As.push_back(A);
  Aps.push_back(Ap);
  Avs.push_back(Av);

  compute_As(H);
}

void StateSpaceAccTangent::compute_As(unsigned int h) {
  // std::cout << "StateSpaceAccTangent::compute_As::h:" << h
  //           << "  As.size():" << As.size() << std::endl;
  if (h > As.size()) {
    for (unsigned int n = As.size(); n < h + 1; ++n) {
      // std::cout << "StateSpaceAccTangent::compute_As::n:" << n << std::endl;
      As.push_back(As.back() * A);
      Aps.push_back(As.back().block(0, 0, nu, 2 * nu));
      Avs.push_back(As.back().block(nu, 0, nu, 2 * nu));
      // std::cout << "A^" << n << " added:" << std::endl
      //           << As.back() << std::endl;
    }
  }
}

// Lp = |Bp                        |
//      |ApBp     Bp               |
//      |Ap^2*Bp  ApBp     Bp      |
//      |Ap^3*Bp  Ap^2*Bp  ApBp  Bp|
Eigen::MatrixXd StateSpaceAccTangent::Lp(unsigned int h) {
  unsigned int nz = h * B.cols();
  Eigen::MatrixXd lp = T * T * Eigen::MatrixXd::Identity(nz, nz) / 2;
  for (unsigned int col = 0; col < nz; col += nu) {
    unsigned int exp = 0;
    // std::cout << "===================" << std::endl;
    // std::cout << "col:" << col << std::endl;
    // std::cout << "row:" << col + nu << std::endl;
    // std::cout << "lim: row < " << nu * H << std::endl;
    for (unsigned int row = col; row < nz - 1; row += nu) {
      // std::cout << "col:" << col << "  row:" << row << "  exp:" << exp
      //           << std::endl;
      // std::stringstream ss;
      // ss << "Aps[" << exp << "]";
      // print_mat(ss.str(), Aps[exp]);
      // print_mat("Aps[exp] * ss.B", Aps[exp] * B);
      // print_mat("Lp", Lp);
      lp.block(row, col, nu, nu) = Aps[exp++] * B;
    }
  }
  // print_mat("lp", lp);
  return lp;
}
// Lv = |Bp            |
//      |Bp  Bp        |
//      |Bp  Bp  Bp    |
//      |Bp  Bp  Bp  Bp|
Eigen::MatrixXd StateSpaceAccTangent::Lv(unsigned int h) {
  unsigned int nz = h * B.cols();
  Eigen::MatrixXd lv(T * Eigen::MatrixXd::Identity(nz, nz));
  for (unsigned int col = 0; col < nz; col += nu) {
    // std::cout << "===================" << std::endl;
    // std::cout << "col:" << col << std::endl;
    // std::cout << "row:" << col + nu << std::endl;
    // std::cout << "lim: row < " << nu * H << std::endl;
    for (unsigned int row = col; row < nz - 1; row += nu) {
      // std::cout << "col:" << col << "  row:" << row << "  exp:" << exp
      //           << std::endl;
      // print_mat("Aps[exp] * ss.B", Aps[exp] * ss.B);
      lv.block(row, col, nu, nu) = Bv;
    }
  }
  // print_mat("lv", lv);
  return lv;
}

Eigen::MatrixXd StateSpaceAccTangent::cost_a(unsigned int h) {
  Eigen::MatrixXd lp(this->Lp(h));
  Eigen::MatrixXd lv(this->Lv(h));
  Eigen::MatrixXd mat(lp.rows() + lv.rows(), lp.cols());
  mat.block(0, 0, lp.rows(), lp.cols()) = lp;
  mat.block(lp.rows(), 0, lv.rows(), lp.cols()) = T * lv;
  return mat;
}

MPCAccTangent::MPCAccTangent(unsigned int H, double T, MPCParams params,
                             std::string solver, double compute_time_s,
                             bool jerk_limited_horizon)
    :                                        //
      compute_time_limit_s(compute_time_s),  //
      H(H),                                  //
      nz(nu * H),                            //
      nj(nz - nu),                           //
      T(T),                                  //
      La(Eigen::MatrixXd::Identity(nz, nz)), //
      Lj((rshift(Eigen::MatrixXd::Identity(nz, nz), nu) -
          Eigen::MatrixXd::Identity(nz, nz))
             .block(0, 0, nj, nz) /
         T),                                                            //
      ss(T, H),                                                         //
      Lv(ss.Lv(H)),                                                     //
      Lp(ss.Lp(H)),                                                     //
      jerk_limited_horizon(jerk_limited_horizon),                       //
      cost_a(ss.cost_a(H)),                                             //
      horizon(H, T),                                                    //
      params(params),                                                   //
      Qa(0.5 * T * T * params.w_a * Eigen::MatrixXd::Identity(nz, nz)), //
      con_p(H, H, Eigen::VectorXd::Constant(nu, 1), Lp, nu),            //
      con_v(H, H, Eigen::VectorXd::Constant(nu, 1.5), Lv, nu),          //
      con_a(H, H, Eigen::VectorXd::Constant(nu, 5), La, nu),            //
      con_j(H - 1, H, Eigen::VectorXd::Constant(nu, 100), Lj, nu),      //
      Apx0_over(Eigen::VectorXd::Zero(nz)),                             //
      Avx0_over(Eigen::VectorXd::Zero(nz))                              //
{
  // ****************************************************
  // ****************************************************
  // QP Solver
  if (solver == "osqp") {
    qpprob.reset(new OSQPProblem(nz));
  } else if (solver == "qpoases") {
    qpprob.reset(new QPOasesProblem(nz));

  }
#ifdef qpmad_FOUND
  else if (solver == "qpmad") {
    qpprob.reset(new QPMadProblem(nz));
  }
#endif
#ifdef proxsuite_FOUND
  else if (solver == "proxqp") {
    qpprob.reset(new ProxQPProblem(nz));
  }
#endif
  else {
    const std::string lmpcpoly_default_solver = "qpoases";
    std::stringstream ss;
    ss << "Error: MPCAccTangent::solver '" << solver
       << "' unknown, using default: " << lmpcpoly_default_solver << std::endl;
    ss << "valid values are: osqp, qpoases or (if detected) qpmad, proxqp"
       << std::endl;
    print_error(ss);
    solver = lmpcpoly_default_solver;
    qpprob.reset(new QPOasesProblem(nz));
  }
  qpprob->set_compute_time(compute_time_s);
  std::cout << "MPCAccTangent::solver:" << solver << std::endl;

  // auto cost = mpcmat.cost_function();
  // qpprob->update_cost_function(cost.a, cost.Hreg);
  // ****************************************************
  // ****************************************************
  // initialize linear constraints

  // ****************************************************
  // with pose constraints
  // C.A.resize(3 * nz, nz);
  // C.A.block(0, 0, nz, nz) = Lp;
  // C.A.block(nz, 0, nz, nz) = Lv;
  // C.A.block(2 * nz, 0, nz, nz) = La;
  // ****************************************************
  // without pose constraints (only v and a)
  C.A.resize(2 * nz + nu, nz);
  C.A.setZero();
  C.A.block(0, 0, nz, nz) = Lv;
  C.A.block(nz, 0, nz, nz) = La;
  C.A.block(2 * nz, 0, nu, nu) = Eigen::MatrixXd::Identity(nu, nu);

  // print_mat("C.A", C.A);
}

// void MPCAccTangent::update_pmax(const Vector6d &max) {
//   con_p.update_box_max(max);
// }
// void MPCAccTangent::update_vmax(const Vector6d &max) {
//   con_v.update_box_max(max);
// }
// void MPCAccTangent::update_amax(const Vector6d &max) {
//   con_a.update_box_max(max);
// }
// void MPCAccTangent::update_jmax(const Vector6d &max) {
//   con_j.update_box_max(max);
// }

// void MPCAccTangent::update_p_polytope(const LinearInequalityConstraints pol,
//                                       bool only_first_step) {
//   con_p.LTI_step_to_horizon(pol, only_first_step);
// }
// void MPCAccTangent::update_v_polytope(const LinearInequalityConstraints pol,
//                                       bool only_first_step) {
//   con_v.LTI_step_to_horizon(pol, only_first_step);
// }
// void MPCAccTangent::update_a_polytope(const LinearInequalityConstraints pol,
//                                       bool only_first_step) {
//   con_a.LTI_step_to_horizon(pol, only_first_step);
// }

void MPCAccTangent::update_wreg(double w, Eigen::MatrixXd M) {
  std::lock_guard<std::mutex> guard(_mutex);
  params.w_a = w;
  if (M.rows() == 0) {
    Qa = 0.5 * T * T * params.w_a * Eigen::MatrixXd::Identity(nz, nz);
  } else {
    Eigen::MatrixXd temp = Eigen::MatrixXd::Zero(nz, nz);
    for (unsigned int h = 0; h < H; ++h) {
      temp.block(nu * h, nu * h, nu, nu) = 0.5 * T * T * params.w_a * M;
    }
    // Qa = Lv.transpose() * temp * Lv;
    Qa = temp;
  }
}

void MPCAccTangent::update_wterminal(double w) {
  std::lock_guard<std::mutex> guard(_mutex);
  params.w_terminal = w;
}

void MPCAccTangent::solve(InitialConditionsTangent initial,
                          pinocchio::SE3 target, bool presolve) {
  TickTock temp("MPCAccTangent::solve", verbose);

  if (presolve) {
    _mutex.lock();
    qpprob->set_compute_time(5 * compute_time_s);
    std::cout << "MPCAccTangent::solve presolve" << std::endl;
  } else {
    if (!_mutex.try_lock()) {
      temp.tock("did not solve qp");
      return;
    }
  }

  Eigen::VectorXd target_position =
      pinocchio::log6(initial.pose0.actInv(target)).toVector();

  Eigen::VectorXd target_state_horizon(Eigen::VectorXd::Zero(2 * nz));
  Eigen::VectorXd x0(2 * nu);
  Eigen::VectorXd p0 = pinocchio::Motion::Zero().toVector();
  x0 << p0, initial.v0;
  for (unsigned int h = 0; h < H; ++h) {
    Apx0_over.segment(nu * h, nu) = ss.Aps[h] * x0;
    Avx0_over.segment(nu * h, nu) = ss.Avs[h] * x0;
    target_state_horizon.segment(nu * h, nu) = target_position;
  }

  con_p.update_bias(Apx0_over);
  con_v.update_bias(Avx0_over);

  if (jerk_limited_horizon) {
    C.combine({con_p.lmi(), con_v.lmi(), con_a.lmi(), con_j.lmi()});
  } else {
    C.combine({con_p.lmi(), con_v.lmi(), con_a.lmi()});
  }

  Eigen::VectorXd cost_b(Apx0_over.rows() + Avx0_over.rows());
  cost_b << Apx0_over, Avx0_over;
  Eigen::MatrixXd cost_mat(cost_a);

  // TODO: messing up sampled horizon?
  // double w_linear = 2;
  // for (unsigned int h = 0; h < H; ++h) {
  //   Lpcost.block(h * nu, h * nu, 3, nz) *= w_linear;
  //   Apx0_over_cost.segment(h * nu, 3) *= w_linear;
  //   target_state_horizon.segment(h * nu, 3) *= w_linear;
  //   Qa.block(h * nu, h * nu, 3, 3) =
  //       w_linear * 0.5 * T * T * params.w_a * Eigen::MatrixXd::Identity(nz,
  //       nz);
  // }

  target_state_horizon.segment(nz - nu, nu) *= params.w_terminal;
  cost_mat.block(nz - nu, 0, nu, nz) *= params.w_terminal;
  cost_b.segment(nz - nu, nu) *= params.w_terminal;
  cost_mat.block(2 * nz - nu, 0, nu, nz) *= T * params.w_terminal;
  cost_b.segment(2 * nz - nu, nu) *= T * params.w_terminal;
  // Qa.block(nz - nu, 0, nu, nz) *= params.w_terminal;

  // print_mat("lim.min", lim.min);
  // print_mat("lim.max", lim.max);
  // print_mat("Qa", Qa);
  // print_mat("cost_b", cost_b);
  // print_mat("cost_mat", cost_mat);
  // print_mat("target_state_horizon", target_state_horizon);

  // |a z - b| + |Hreg z|
  // |x_overline - x*| + |Hreg a_underline|
  // |Lx a_underline + Apx0_overline - x*| + |Hreg a_underline|
  // a = Lx      b = x* - Apx0_overline
  temp.tock("before qpprob->update_cost_function(Lpcost, Qa)");
  qpprob->update_cost_function(cost_mat, Qa);
  temp.tock("before solve()");
  auto ret =
      qpprob->solve(target_state_horizon - cost_b, C.A, C.b.min, C.b.max);
  if (!(bool)ret) {
    Eigen::VectorXd j(Eigen::VectorXd::Zero(nz));
    j.segment(0, nu) =
        (qpprob->result().segment(0, nu) - initial.a0) / control_period;
    j.segment(nu, nz - nu) = Lj * qpprob->result();
    horizon.update(initial.t, initial.pose0,
                   Lp * qpprob->result() + Apx0_over, // p
                   Lv * qpprob->result() + Avx0_over, // v
                   qpprob->result(),                  // a
                   j                                  // j
    );
    // _horizon.update(qpprob->result(), goal, ref_pose, temp.tock_s());
    // promise.set_value(std::move(std::make_unique<Horizon>(_horizon)));
  } else {
    if (delay.passed()) {
      std::stringstream ss;
      std::cout << "Error: mpc::qpprob->solve(t=" << initial.t
                << "s) FAILED, ret:" << ret << std::endl;
      ss << "ret:" << ret << std::endl;
      ss << "con_p size:" << con_p.lmi().A.rows() << std::endl;
      ss << "con_v size:" << con_v.lmi().A.rows() << std::endl;
      ss << "con_a size:" << con_a.lmi().A.rows() << std::endl;
      ss << "con_j size:" << con_j.lmi().A.rows() << std::endl;
      for (unsigned int i = 0; i < C.b.min.rows(); ++i) {
        if (C.b.min(i) > C.b.max(i)) {
          ss << "Alin_min(" << i << ")>=Alin_max(" << i << "): ";
          ss << C.b.min(i) << ">" << C.b.max(i) << std::endl;
          ss << "C.A.row(i):" << C.A.row(i) << std::endl;
        }
      }
      ss << "counter:" << (int)warn_counter << std::endl;
      // ss << str_mat("Alin_min:", Alin_min) << std::endl;
      // ss << str_mat("Alin_max:", Alin_max) << std::endl;
      print_error(ss);
      warn_counter = 0;
      delay.refresh();
    } else {
      warn_counter++;
    }
  }

  if (presolve) {
    qpprob->set_compute_time(compute_time_s);
  }
  _mutex.unlock();
}
