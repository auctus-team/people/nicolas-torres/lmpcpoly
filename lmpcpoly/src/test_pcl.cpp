#include "methods.hpp"
#include <pcl/ModelCoefficients.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/project_inliers.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/surface/convex_hull.h>

#include <Eigen/Dense>
#include <chrono>
#include <cstdlib>
#include <iostream>

double tock_ms(std::chrono::time_point<std::chrono::steady_clock> start) {
  return std::chrono::duration_cast<std::chrono::nanoseconds>(
             std::chrono::steady_clock::now() - start)
             .count() /
         1000000.;
}

std::chrono::time_point<std::chrono::steady_clock> precise_now() {
  return std::chrono::steady_clock::now();
}

int main(int argc __attribute__((unused)),
         char **argv __attribute__((unused))) {
  // ---------------------------------------------------------------------
  // ---------------------------------------------------------------------
  // ---------------------------------------------------------------------
  // example code from docs
  // can download the pcd file here:
  // https://pcl.readthedocs.io/projects/tutorials/en/latest/hull_2d.html
  // ---------------------------------------------------------------------
  {
      //   std::cerr << "EXAMPLE FROM DOCS" << std::endl;
      //   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(
      //       new pcl::PointCloud<pcl::PointXYZ>),
      //       cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>),
      //       cloud_projected(new pcl::PointCloud<pcl::PointXYZ>);

      //   pcl::PCDReader reader;
      //   reader.read("table_scene_mug_stereo_textured.pcd", *cloud);
      //   std::cerr << "PointCloud BEFORE filtering has: " << cloud->size()
      //             << " data points" << std::endl;

      //   // Build a filter to remove spurious NaNs
      //   pcl::PassThrough<pcl::PointXYZ> pass;
      //   pass.setInputCloud(cloud);
      //   pass.setFilterFieldName("z");
      //   pass.setFilterLimits(0, 1.1); // for .pcd file
      //   pass.filter(*cloud_filtered);
      //   std::cerr << "PointCloud after filtering has: " <<
      //   cloud_filtered->size()
      //             << " data points." << std::endl;

      //   pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
      //   pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
      //   // Create the segmentation object
      //   pcl::SACSegmentation<pcl::PointXYZ> seg;
      //   // Optional
      //   seg.setOptimizeCoefficients(true);
      //   // Mandatory
      //   seg.setModelType(pcl::SACMODEL_PLANE);
      //   seg.setMethodType(pcl::SAC_RANSAC);
      //   seg.setDistanceThreshold(0.01);

      //   seg.setInputCloud(cloud_filtered);
      //   seg.segment(*inliers, *coefficients);
      //   std::cerr << "PointCloud after segmentation has: "
      //             << inliers->indices.size() << " inliers." << std::endl;

      //   // Project the model inliers
      //   pcl::ProjectInliers<pcl::PointXYZ> proj;
      //   proj.setModelType(pcl::SACMODEL_PLANE);
      //   proj.setInputCloud(cloud_filtered);
      //   proj.setIndices(inliers);
      //   proj.setModelCoefficients(coefficients);
      //   proj.filter(*cloud_projected);
      //   std::cerr << "PointCloud after projection has: " <<
      //   cloud_projected->size()
      //             << " data points." << std::endl;

      //   // Create a Convex Hull representation of the projected inliers
      //   pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(
      //       new pcl::PointCloud<pcl::PointXYZ>);
      //   pcl::ConvexHull<pcl::PointXYZ> chull;
      //   chull.setInputCloud(cloud_projected);
      //   chull.reconstruct(*cloud_hull);

      //   std::cerr << "Convex hull has: " << cloud_hull->size() << " data
      //   points."
      //             << std::endl;

      //   // pcl::PCDWriter writer;
      //   // writer.write("table_scene_mug_stereo_textured_hull.pcd",
      //   *cloud_hull,
      //   // false);
      //   std::cerr << "END EXAMPLE FROM DOCS" << std::endl;
      //   std::cerr << "-----------------------------------------------" <<
      //   std::endl;
      //   std::cerr << "-----------------------------------------------" <<
      //   std::endl;
  } // ---------------------------------------------------------------------
  // ---------------------------------------------------------------------

  {
    std::cerr << "EXAMPLE WITH RANDOM POINTS" << std::endl;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(
        new pcl::PointCloud<pcl::PointXYZ>),
        cloud_filtered(new pcl::PointCloud<pcl::PointXYZ>),
        cloud_projected(new pcl::PointCloud<pcl::PointXYZ>);

    double max_z = 0.7;
    double min_z = 0.;
    int npoints = 1e3;
    for (int i = 0; i < npoints; ++i) {
      // cloud->push_back(pcl::PointXYZ(rand01(), rand01(), rand01()));
      auto vec(Eigen::Vector3d::Random());
      cloud->push_back(pcl::PointXYZ(vec(0), vec(1), vec(2)));
    }
    int under_limit_points = 0;
    for (const auto &point : *cloud) {
      if ((point.z >= min_z) && (point.z <= max_z)) {
        // std::cout << "point:" << point << std::endl;
        under_limit_points++;
      }
    }
    std::cerr << "PointCloud BEFORE filtering has: " << cloud->size()
              << " data points" << std::endl;
    std::cerr << "of which only " << under_limit_points
              << " will pass the filter." << std::endl;

    // Build a filter to remove spurious NaNs
    pcl::PassThrough<pcl::PointXYZ> pass;
    pass.setInputCloud(cloud);
    pass.setFilterFieldName("z");
    // pass.setFilterLimits(0, 1.1); // for .pcd file
    // pass.setNegative(true);
    pass.setFilterLimits(min_z, max_z);
    pass.filter(*cloud_filtered);
    std::cerr << "PointCloud after filtering has: " << cloud_filtered->size()
              << " data points." << std::endl;

    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.01);

    seg.setInputCloud(cloud_filtered);
    seg.segment(*inliers, *coefficients);
    std::cerr << "PointCloud after segmentation has: "
              << inliers->indices.size() << " inliers." << std::endl;

    // Project the model inliers
    pcl::ProjectInliers<pcl::PointXYZ> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);
    proj.setInputCloud(cloud_filtered);
    proj.setIndices(inliers);
    proj.setModelCoefficients(coefficients);
    proj.filter(*cloud_projected);
    std::cerr << "PointCloud after projection has: " << cloud_projected->size()
              << " data points." << std::endl;

    // Create a Convex Hull representation of the projected inliers
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(
        new pcl::PointCloud<pcl::PointXYZ>);
    pcl::ConvexHull<pcl::PointXYZ> chull;
    chull.setInputCloud(cloud_projected);
    chull.reconstruct(*cloud_hull);

    std::cerr << "Convex hull has: " << cloud_hull->size() << " data points."
              << std::endl;

    std::cerr << "END EXAMPLE WITH RANDOM POINTS" << std::endl;
    std::cerr << "-----------------------------------------------" << std::endl;
    std::cerr << "-----------------------------------------------" << std::endl;
  }
  // ---------------------------------------------------------------------
  // ---------------------------------------------------------------------
  {
    std::cerr << "SIMPLE EXAMPLE CONVEX HULL" << std::endl;
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(
        new pcl::PointCloud<pcl::PointXYZ>),
        cloud_projected(new pcl::PointCloud<pcl::PointXYZ>);

    int npoints = 1e2;
    for (int i = 0; i < npoints; ++i) {
      // cloud->push_back(pcl::PointXYZ(rand01(), rand01(), rand01()));
      auto vec(Eigen::Vector3d::Random());
      cloud->push_back(pcl::PointXYZ(vec(0), vec(1), vec(2)));
    }
    std::cerr << "PointCloud has: " << cloud->size() << " data points."
              << std::endl;

    auto start_total = precise_now();
    pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
    pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
    // Create the segmentation object
    pcl::SACSegmentation<pcl::PointXYZ> seg;
    // Optional
    seg.setOptimizeCoefficients(true);
    // Mandatory
    seg.setModelType(pcl::SACMODEL_PLANE);
    seg.setMethodType(pcl::SAC_RANSAC);
    seg.setDistanceThreshold(0.01);

    seg.setInputCloud(cloud);
    seg.segment(*inliers, *coefficients);
    std::cerr << "PointCloud after segmentation has: "
              << inliers->indices.size() << " inliers." << std::endl;
    double segmentation_duration(tock_ms(start_total));

    auto start_projection = precise_now();
    // Project the model inliers
    pcl::ProjectInliers<pcl::PointXYZ> proj;
    proj.setModelType(pcl::SACMODEL_PLANE);
    proj.setInputCloud(cloud);
    proj.setIndices(inliers);
    proj.setModelCoefficients(coefficients);
    proj.filter(*cloud_projected);
    std::cerr << "PointCloud after projection has: " << cloud_projected->size()
              << " data points." << std::endl;
    double projection_duration(tock_ms(start_projection));

    // Create a Convex Hull representation of the projected inliers
    auto start_hull = precise_now();
    {
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(
          new pcl::PointCloud<pcl::PointXYZ>);
      pcl::ConvexHull<pcl::PointXYZ> chull;
      std::vector<pcl::Vertices> vertices;
      chull.setInputCloud(cloud_projected);
      chull.reconstruct(*cloud_hull, vertices);
      for (const auto &point : *cloud_hull) {
        std::cout << "point:" << point << std::endl;
      }
      // std::cout << "points:" << std::endl
      //           << cloud_hull->getMatrixXfMap().transpose().leftCols(3)
      //           << std::endl;
      for (const auto &vertex : vertices) {
        std::cout << "vertex:" << vertex << std::endl;
      }
      std::cerr << "Convex hull has: " << cloud_hull->size() << " data points."
                << std::endl;
    }
    double hull_duration(tock_ms(start_hull));

    double total_duration(tock_ms(start_total));
    std::cerr
        << "Total computation time (segmentation + projection + convex hull): "
        << total_duration << "ms." << std::endl;
    std::cerr << "Segmentation time: " << segmentation_duration << "ms."
              << std::endl;
    std::cerr << "Projection time: " << projection_duration << "ms."
              << std::endl;
    std::cerr << "Convex Hull time: " << hull_duration << "ms." << std::endl;

    std::cerr << "Computation time per point (including all points):"
              << total_duration / cloud->size() << "ms" << std::endl;
    std::cerr << "Computation time per point (only projected points): "
              << total_duration / cloud_projected->size() << "ms" << std::endl;

    std::cerr << "++++++++++++++++++++" << std::endl;

    // Create a Convex Hull representation of the projected inliers
    std::cerr
        << "Computing Convex Hull again without segmentation or projection..."
        << std::endl;
    auto start_hull2 = precise_now();
    {
      pcl::PointCloud<pcl::PointXYZ>::Ptr cloud_hull(
          new pcl::PointCloud<pcl::PointXYZ>);
      std::vector<pcl::Vertices> vertices;
      pcl::ConvexHull<pcl::PointXYZ> chull;
      chull.setInputCloud(cloud);
      chull.reconstruct(*cloud_hull, vertices);
      for (const auto &vertex : vertices) {
        std::cout << "vertex:" << vertex << std::endl;
      }
      std::cerr << "Convex hull has: " << cloud_hull->size() << " data points."
                << std::endl;
    }
    double hull_duration2(tock_ms(start_hull2));

    std::cerr << "Total computation time for Convex Hull (for " << cloud->size()
              << " points): " << hull_duration2 << "ms." << std::endl;

    std::cerr << "END SIMPLE EXAMPLE CONVEX HULL" << std::endl;
    std::cerr << "-----------------------------------------------" << std::endl;
    std::cerr << "-----------------------------------------------" << std::endl;
  }

  return (0);
}
