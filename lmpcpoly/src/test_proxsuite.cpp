#include <iostream>
#include <proxsuite/proxqp/dense/dense.hpp> // load the dense solver backend
#include <proxsuite/proxqp/utils/random_qp_problems.hpp> // used for generating a random convex qp

using namespace proxsuite::proxqp;
using T = double;

void print_size(std::string str, Eigen::MatrixXd mat) {
  std::cout << str << " " << mat.rows() << " x " << mat.cols();
  if (mat.cols() == 1) {
    std::cout << mat.transpose() << std::endl;
  } else {
    std::cout << mat << std::endl;
  }
}

int main() {
  {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Example: Dense simple" << std::endl;
    dense::isize dim = 10;
    dense::isize n_eq(dim / 4);
    dense::isize n_in(dim / 4);
    // generate a random qp
    T sparsity_factor(0.15);
    T strong_convexity_factor(1.e-2);
    dense::Model<T> qp_random = utils::dense_strongly_convex_qp(
        dim, n_eq, n_in, sparsity_factor, strong_convexity_factor);

    dense::QP<T> qp(dim, n_eq, n_in); // create the QP object
    qp.init(qp_random.H, qp_random.g, qp_random.A, qp_random.b, qp_random.C,
            qp_random.l,
            qp_random.u);           // initialize the model
    qp.settings.eps_abs = T(1.E-9); // set accuracy threshold to 1.e-9
    qp.settings.verbose = true;     // print some intermediary results
    qp.solve();                     // solve the problem with previous settings
    // print an optimal solution x,y and z
    std::cout << "optimal x: " << qp.results.x.transpose() << std::endl;
    std::cout << "optimal y: " << qp.results.y.transpose() << std::endl;
    std::cout << "optimal z: " << qp.results.z.transpose() << std::endl;
  }

  {
    std::cout << "------------------------------" << std::endl;
    std::cout << "Example: Dense 0 inequalities" << std::endl;

    dense::isize dim = 10;
    dense::isize n_eq(0);
    dense::isize n_in(0);
    dense::QP<T> qp(dim, n_eq, n_in);
    T strong_convexity_factor(0.1);
    T sparsity_factor(0.15);
    // we generate a qp, so the function used from helpers.hpp is
    // in proxqp namespace. The qp is in dense eigen format and
    // you can control its sparsity ratio and strong convexity factor.
    dense::Model<T> qp_random = utils::dense_strongly_convex_qp(
        dim, n_eq, n_in, sparsity_factor, strong_convexity_factor);

    print_size("qp_random.A", qp_random.A);
    print_size("qp_random.C", qp_random.C);

    qp.init(qp_random.H, qp_random.g, qp_random.A, qp_random.b, qp_random.C,
            qp_random.l,
            qp_random.u); // initialization with zero shape matrices
    // it is equivalent to do qp.init(qp_random.H, qp_random.g,
    // nullopt,nullopt,nullopt,nullopt,nullopt);
    qp.solve();
    // print an optimal solution x,y and z
    std::cout << "optimal x: " << qp.results.x.transpose() << std::endl;
    std::cout << "optimal y: " << qp.results.y.transpose() << std::endl;
    std::cout << "optimal z: " << qp.results.z.transpose() << std::endl;
  }

#ifdef proxsuite_FOUND
  std::cout << "proxsuite_FOUND OK" << std::endl;
#endif
  std::cout << "++++++++++++++++++++++++++++++++++++" << std::endl;
}
