
#include "mpcacc.hpp"

#include <atomic>
#include <chrono>
#include <mutex>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

// #include "horizon.hpp"
#include "methods.hpp"
// #include "mpc_matrices.hpp"
// #include "poses.hpp"
#include "qp_problem.hpp"
#include <fstream>
#include <future>
#include <iostream>
#include <memory>

#include <limits>
#include <qpOASES.hpp>

using namespace lmpcpoly;

void lmpcpoly::convert_infinity(Eigen::VectorXd &a, double val) {
  for (unsigned int i = 0; i < a.rows(); ++i) {
    if (a(i) == std::numeric_limits<double>::infinity()) {
      a(i) = val;
    }
    if (a(i) == -std::numeric_limits<double>::infinity()) {
      a(i) = -val;
    }
  }
}

interp_step lmpcpoly::first_index(double t0, double t, double T,
                                  unsigned int H) {

  // double duration = H * T;
  double t_horizon = t - t0;
  unsigned int idx = t_horizon / T;
  double alpha = (t_horizon - idx * T) / T;
  if (idx >= H - 1) {
    idx = H - 2;
    alpha = 1;
  }
  // std::cout << "idx:" << idx << std::endl;
  // std::cout << "alpha:" << alpha << std::endl;
  // std::cout << "t_horizon:" << t_horizon << std::endl;
  // std::cout << "T:" << T << std::endl;

  return {idx, alpha};
}

MPCAccHorizon::MPCAccHorizon(unsigned int H, double T) : _H(H), _T(T) {}

void MPCAccHorizon::update(double t0, const Eigen::VectorXd &p,
                           const Eigen::VectorXd &v, const Eigen::VectorXd &a,
                           const Eigen::VectorXd &j) {
  this->p = p;
  this->v = v;
  this->a = a;
  this->j = j;
  _t0 = t0;
}

double MPCAccHorizon::duration() { return _H * _T; }
bool MPCAccHorizon::after_horizon(double t) { return t > _t0 + duration(); }
bool MPCAccHorizon::before_horizon(double t) { return t < _t0; }

std::vector<Eigen::Vector3d>
MPCAccHorizon::sample_horizon(unsigned int samples) {
  if (samples < 5) {
    samples = 5;
  }
  double step_dt = duration() / (samples - 1);
  std::vector<Eigen::Vector3d> ret;
  for (unsigned int i = 0; i < samples; ++i) {
    double t = _t0 + i * step_dt;
    // std::cout << "step_dt:" << step_dt << "s  t:" << t << "s  i:" << i
    //           << std::endl;
    ret.push_back(p_at(t));
  }
  return ret;
}

double MPCAccHorizon::t_horizon(double t) { return t - _t0; }

Eigen::Vector3d MPCAccHorizon::p_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::p_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return p_at(duration());
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::p_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return p_at(_t0);
  }
  if (p.rows() == 0) {
    return Eigen::Vector3d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Eigen::Vector3d pk, pk1;
  pk = p.segment(3 * st.idx, 3);
  pk1 = p.segment(3 * (st.idx + 1), 3);
  // print_mat("pk", pk);
  // print_mat("pk1", pk1);
  // print_mat("st.alpha * (pk1 - pk)", st.alpha * (pk1 - pk));

  return pk + st.alpha * (pk1 - pk);
}
Eigen::Vector3d MPCAccHorizon::v_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::v_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::v_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (v.rows() == 0) {
    return Eigen::Vector3d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Eigen::Vector3d pk, pk1;
  pk = v.segment(3 * st.idx, 3);
  pk1 = v.segment(3 * (st.idx + 1), 3);

  return pk + st.alpha * (pk1 - pk);
}
Eigen::Vector3d MPCAccHorizon::a_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::a_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::a_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (a.rows() == 0) {
    return Eigen::Vector3d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Eigen::Vector3d pk, pk1;
  pk = a.segment(3 * st.idx, 3);
  pk1 = a.segment(3 * (st.idx + 1), 3);

  return pk + st.alpha * (pk1 - pk);
}
Eigen::Vector3d MPCAccHorizon::j_at(double t) {
  if (after_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::j_at '" << t << "s";
    ss << "' is after horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (before_horizon(t)) {
    std::stringstream ss;
    ss << "Error: MPCAccHorizon::j_at '" << t << "s";
    ss << "' is before horizon" << std::endl;
    print_error(ss);
    return Eigen::Vector3d::Zero();
  }
  if (j.rows() == 0) {
    return Eigen::Vector3d::Zero();
  }
  interp_step st = first_index(_t0, t, _T, _H);
  Eigen::Vector3d pk, pk1;
  pk = j.segment(3 * st.idx, 3);
  // pk1 = j.segment(3 * (st.idx + 1), 3);
  // return pk + st.alpha * (pk1 - pk);
  return pk;
}

void StateSpaceAcc::compute_As(unsigned int h) {
  // std::cout << "StateSpaceAcc::compute_As::h:" << h
  //           << "  As.size():" << As.size() << std::endl;
  if (h > As.size()) {
    for (unsigned int n = As.size(); n < h + 1; ++n) {
      // std::cout << "StateSpaceAcc::compute_As::n:" << n << std::endl;
      As.push_back(As.back() * A);
      Aps.push_back(As.back().block(0, 0, 3, 6));
      Avs.push_back(As.back().block(3, 0, 3, 6));
      // std::cout << "A^" << n << " added:" << std::endl
      //           << As.back() << std::endl;
    }
  }
}

StateSpaceAcc::StateSpaceAcc(double T, unsigned int H)
    : T(T), A(Eigen::MatrixXd::Identity(6, 6)) {
  A.block(0, 3, 3, 3) = T * Eigen::MatrixXd::Identity(3, 3);
  Ap = A.block(0, 0, 3, 6);
  Av = A.block(3, 0, 3, 6);

  Bp = (T * T / 2) * Eigen::MatrixXd::Identity(3, 3);
  Bv = T * Eigen::MatrixXd::Identity(3, 3);
  B.resize(6, 3);
  B.block(0, 0, 3, 3) = Bp;
  B.block(3, 0, 3, 3) = Bv;

  // print_mat("A", A);
  // print_mat("B", B);

  As.push_back(Eigen::MatrixXd::Identity(A.rows(), A.cols()));
  Aps.push_back(As.back().block(0, 0, 3, 6));
  Avs.push_back(As.back().block(3, 0, 3, 6));
  As.push_back(A);
  Aps.push_back(Ap);
  Avs.push_back(Av);

  compute_As(H);
}

// Lp = |Bp                        |
//      |ApBp     Bp               |
//      |Ap^2*Bp  ApBp     Bp      |
//      |Ap^3*Bp  Ap^2*Bp  ApBp  Bp|
Eigen::MatrixXd StateSpaceAcc::Lp(unsigned int h) {
  unsigned int nz = h * B.cols();
  Eigen::MatrixXd Lp = T * T * Eigen::MatrixXd::Identity(nz, nz) / 2;
  for (unsigned int col = 0; col < nz; col += 3) {
    unsigned int exp = 0;
    // std::cout << "===================" << std::endl;
    // std::cout << "col:" << col << std::endl;
    // std::cout << "row:" << col + 3 << std::endl;
    // std::cout << "lim: row < " << 3 * H << std::endl;
    for (unsigned int row = col; row < nz - 1; row += 3) {
      // std::cout << "col:" << col << "  row:" << row << "  exp:" << exp
      //           << std::endl;
      // std::stringstream ss;
      // ss << "Aps[" << exp << "]";
      // print_mat(ss.str(), Aps[exp]);
      // print_mat("Aps[exp] * ss.B", Aps[exp] * B);
      // print_mat("Lp", Lp);
      Lp.block(row, col, 3, 3) = Aps[exp++] * B;
    }
  }
  // print_mat("Lp", Lp);
  return Lp;
}
// Lv = |Bp            |
//      |Bp  Bp        |
//      |Bp  Bp  Bp    |
//      |Bp  Bp  Bp  Bp|
Eigen::MatrixXd StateSpaceAcc::Lv(unsigned int h) {
  unsigned int nz = h * B.cols();
  Eigen::MatrixXd Lv = T * Eigen::MatrixXd::Identity(nz, nz);
  for (unsigned int col = 0; col < nz; col += 3) {
    // std::cout << "===================" << std::endl;
    // std::cout << "col:" << col << std::endl;
    // std::cout << "row:" << col + 3 << std::endl;
    // std::cout << "lim: row < " << 3 * H << std::endl;
    for (unsigned int row = col; row < nz - 1; row += 3) {
      // std::cout << "col:" << col << "  row:" << row << "  exp:" << exp
      //           << std::endl;
      // print_mat("Aps[exp] * ss.B", Aps[exp] * ss.B);
      Lv.block(row, col, 3, 3) = Bv;
    }
  }
  // print_mat("Lv", Lv);
  return Lv;
}

// ----------------------------------------------------------------------------

MPCAcc::MPCAcc(unsigned int H, double T, MPCParams params, std::string solver,
               double compute_time_s, bool jerk_limited_horizon)
    :                                       //
      compute_time_limit_s(compute_time_s), //
      params(params),                       //
      H(H),                                 //
      nz(3 * H),                            //
      nj(nz - 3),                           //
      T(T),                                 //
      Lj((rshift(Eigen::MatrixXd::Identity(nz, nz), 3) -
          Eigen::MatrixXd::Identity(nz, nz))
             .block(0, 0, nj, nz) /
         T),                                                            //
      La(Eigen::MatrixXd::Identity(nz, nz)),                            //
      ss(T, H),                                                         //
      Lv(ss.Lv(H)),                                                     //
      Lp(ss.Lp(H)),                                                     //
      Qa(0.5 * T * T * params.w_a * Eigen::MatrixXd::Identity(nz, nz)), //
      jerk_limited_horizon(jerk_limited_horizon),                       //
      horizon(H, T),                                                    //
      con_p(H, H, Eigen::Vector3d::Ones(), Lp),                         //
      con_v(H, H, Eigen::Vector3d::Constant(1.5), Lv),                  //
      con_a(H, H, Eigen::Vector3d::Constant(5), La),                    //
      con_j(H - 1, H, Eigen::Vector3d::Constant(100), Lj),              //
      Apx0_over(Eigen::VectorXd::Zero(nz)),                             //
      Avx0_over(Eigen::VectorXd::Zero(nz))                              //
{
  // ****************************************************
  // ****************************************************
  // QP Solver
  if (solver == "osqp") {
    qpprob.reset(new OSQPProblem(nz));
  } else if (solver == "qpoases") {
    qpprob.reset(new QPOasesProblem(nz));
  }
#ifdef qpmad_FOUND
  else if (solver == "qpmad") {
    qpprob.reset(new QPMadProblem(nz));
  }
#endif
#ifdef proxsuite_FOUND
  else if (solver == "proxqp") {
    qpprob.reset(new ProxQPProblem(nz));
  }
#endif
  else {
    const std::string lmpcpoly_default_solver = "qpoases";
    std::stringstream ss;
    ss << "Error: MPCAcc::solver '" << solver
       << "' unknown, using default: " << lmpcpoly_default_solver << std::endl;
    ss << "valid values are: osqp, qpoases or (if detected) qpmad, proxqp"
       << std::endl;
    print_error(ss);
    solver = lmpcpoly_default_solver;
    qpprob.reset(new QPOasesProblem(nz));
  }
  qpprob->set_compute_time(compute_time_s);
  std::cout << "MPCAcc::solver:" << solver << std::endl;
  // auto cost = mpcmat.cost_function();
  // qpprob->update_cost_function(cost.a, cost.Hreg);
  // ****************************************************
  // ****************************************************
}

// void MPCAcc::update_pmax(const Eigen::Vector3d &max) {
//   con_p.update_box_max(max);
// }
// void MPCAcc::update_vmax(const Eigen::Vector3d &max) {
//   con_v.update_box_max(max);
// }
// void MPCAcc::update_amax(const Eigen::Vector3d &max) {
//   con_a.update_box_max(max);
// }
// void MPCAcc::update_jmax(const Eigen::Vector3d &max) {
//   con_j.update_box_max(max);
// }

// void MPCAcc::update_p_polytope(const LinearInequalityConstraints pol,
//                                bool only_first_step) {
//   con_p.LTI_step_to_horizon(pol, only_first_step);
// }
// void MPCAcc::update_v_polytope(const LinearInequalityConstraints pol,
//                                bool only_first_step) {
//   con_v.LTI_step_to_horizon(pol, only_first_step);
// }
// void MPCAcc::update_a_polytope(const LinearInequalityConstraints pol,
//                                bool only_first_step) {
//   con_a.LTI_step_to_horizon(pol, only_first_step);
// }
// void MPCAcc::update_j_polytope(const LinearInequalityConstraints pol,
//                                bool only_first_step) {
//   // TODO
//   // con_j.LTI_step_to_horizon(pol, only_first_step);
// }

void MPCAcc::solve(InitialConditions initial, Eigen::Vector3d target_position) {
  TickTock temp("MPCAcc::solve", verbose);

  // _mutex.lock();

  // mpcmat.update_initial_u(goal.u0);
  // mpcmat.update_initial_ud(goal.ud0);

  // mpcmat.update_linear_horizon_matrices(x0, xf);
  // mpcmat.update_initial_state(x0);
  // mpcmat.update_cost_b(x0, xf);
  // // mpcmat.update_extra_linear_constraints(polytope_linconstraints);
  // // mpcmat.update_extra_linear_constraints(obstacle_linconstraints);

  // CostFunctionLS cost = mpcmat.cost_function();
  // LinearInequalityConstraints linear_constraints =
  //     mpcmat.linear_constraints();
  // _mutex.unlock();
  Eigen::VectorXd target_position_horizon(nz);
  target_position_horizon.setZero();
  // print_mat("target_position", target_position);
  Eigen::VectorXd x0(6);
  x0 << initial.p0, initial.v0;
  // print_mat("x0", x0);
  for (unsigned int h = 0; h < H; ++h) {
    Apx0_over.segment(3 * h, 3) = ss.Aps[h] * x0;
    Avx0_over.segment(3 * h, 3) = ss.Avs[h] * x0;
    target_position_horizon.segment(3 * h, 3) = target_position;
  }
  // print_mat("Apx0_over", Apx0_over);
  // print_mat("Avx0_over", Avx0_over);

  con_p.update_bias(Apx0_over);
  con_v.update_bias(Avx0_over);

  // *********************************************
  // *********************************************
  // ACC constraints

  // print_mat("con_v.lmi().A", con_v.lmi().A);
  // print_mat("con_v.lmi().b.min", con_v.lmi().b.min);
  // print_mat("con_v.lmi().b.max", con_v.lmi().b.max);

  if (jerk_limited_horizon) {
    C.combine({con_p.lmi(), con_v.lmi(), con_a.lmi(), con_j.lmi()});
  } else {
    C.combine({con_p.lmi(), //
               con_v.lmi(), //
               con_a.lmi()});
  }

  Eigen::VectorXd Apx0_over_cost(Apx0_over);
  Eigen::MatrixXd Lpcost(Lp);
  target_position_horizon.segment(nz - 3, 3) *= params.w_terminal;
  Apx0_over_cost.segment(nz - 3, 3) *= params.w_terminal;
  Lpcost.block(nz - 3, 0, 3, nz) *= params.w_terminal;

  // print_mat("lim.min", lim.min);
  // print_mat("lim.max", lim.max);
  // print_mat("Qa", Qa);
  // print_mat("Lpcost", Lpcost);

  // |a z - b| + |Hreg z|
  // |x_overline - x*| + |Hreg a_underline|
  // |Lx a_underline + Apx0_overline - x*| + |Hreg a_underline|
  // a = Lx      b = x* - Apx0_overline
  temp.tock("before qpprob->update_cost_function(Lpcost, Qa)");
  qpprob->update_cost_function(Lpcost, Qa);
  temp.tock("before solve()");
  if (!(bool)qpprob->solve(target_position_horizon - Apx0_over_cost, C.A,
                           C.b.min, C.b.max)) {
    Eigen::VectorXd j(Eigen::VectorXd::Zero(nz));
    j.segment(0, 3) =
        (qpprob->result().segment(0, 3) - initial.a0) / control_period;
    j.segment(3, nz - 3) = Lj * qpprob->result();
    horizon.update(initial.t,
                   Lp * qpprob->result() + Apx0_over, // p
                   Lv * qpprob->result() + Avx0_over, // v
                   qpprob->result(),                  // a
                   j                                  // j
    );
    // _horizon.update(qpprob->result(), goal, ref_pose, temp.tock_s());
    // promise.set_value(std::move(std::make_unique<Horizon>(_horizon)));
  } else {
    if (delay.passed()) {
      std::stringstream ss;
      ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
      ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
      ss << "Error: mpc::qpprob->solve() FAILED" << std::endl;
      ss << "counter:" << (int)warn_counter << std::endl;
      print_error(ss);
      warn_counter = 0;
      delay.refresh();
    } else {
      warn_counter++;
    }
  }
}
