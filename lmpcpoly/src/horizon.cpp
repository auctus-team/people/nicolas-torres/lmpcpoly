#include "horizon.hpp"
#include <pinocchio/spatial/explog.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

using namespace lmpcpoly;

Eigen::VectorXd interpolate_linear(const Eigen::VectorXd &from,
                                   const Eigen::VectorXd &to,
                                   const double &alpha) {
  return from + (to - from) * alpha;
}

Eigen::VectorXd interpolate_se3_slerp(const Eigen::VectorXd &from,
                                      const Eigen::VectorXd &to,
                                      const double &alpha) {
  return pinocchio::log6(pinocchio::SE3::Interpolate(
                             pinocchio::exp6(from), pinocchio::exp6(to), alpha))
      .toVector();
}

void stream_vector(std::ostream &file, const Eigen::VectorXd &vec,
                   double time) {
  file << time;
  for (unsigned int i = 0; i < vec.rows(); ++i) {
    file << ", " << vec[i];
  }
  file << std::endl;
}

///////////////////////////////////////////////////////
// HorizonParameters
HorizonParameters::HorizonParameters(unsigned int H, unsigned int ndt,
                                     unsigned int nx, unsigned int nu,
                                     unsigned int ny, double dt)
    : /*  H: horizon steps
       *  ndt: number of dt, per horizon step
       *  dt: delta time
       *  nx: state variable size
       *  nu: input variable size
       *  ny: output variable size (observer)
       */
      H(H), nx(nx), nu(nu), ny(ny), ndt(ndt), dt(dt),
      /* underline => from 0 -> H-1
       * overline  => from 1 -> H
       * hat       => from 0 -> H (every step)
       * there is only u_underline because only those are needed
       * to generate x_overline
       * X_underline from 0->(H-1): X_underline = [x0 x1 ... x_{H-1}]^T
       * X_overline  from 1->(H):   X_overline  = [x1 x2 ... x_H]^T
       * X_hat       from 0->(H):   X_hat       = [x0 x1 ... x_H]^T
       * NOTE: X_hat = [ x0 X_overline^T ]^T = [ X_underline^T xH ]^T
       * U from 0->(H-1): U_underline = [u0 u1 ... u_{H-1}]^T
       * Z = [ x0 X_overline^T U_underline^T ]^T = [ X_hat^T U_underline^T
       * ]^T = [ X_underline^T xH U_underline^T ]^T
       */
      nx_underline(H * nx), nx_overline(H * nx), nx_hat((H + 1) * nx),
      nu_underline(H * nu), nud_all((H - 1) * nu), nudd_all((H - 2) * nu),
      nz(nx_hat + nu_underline), hdt(ndt * dt) {}
HorizonParameters::HorizonParameters(HorizonParameters const &other)
    : HorizonParameters(other.H, other.ndt, other.nx, other.nu, other.ny,
                        other.dt) {}

void HorizonParameters::print() const { std::cout << str() << std::endl; }

std::string HorizonParameters::str() const {
  std::stringstream ss;
  ss << lmpcpoly::str_val("dt", dt) << std::endl;
  ss << lmpcpoly::str_val("ndt", ndt) << std::endl;
  ss << lmpcpoly::str_val("hdt", hdt) << std::endl;
  ss << lmpcpoly::str_val("H", H) << std::endl;
  ss << lmpcpoly::str_val("nx", nx) << std::endl;
  ss << lmpcpoly::str_val("nu", nu) << std::endl;
  ss << lmpcpoly::str_val("ny", ny) << std::endl;
  ss << lmpcpoly::str_val("nx_hat", nx_hat) << std::endl;
  ss << lmpcpoly::str_val("nx_overline", nx_overline) << std::endl;
  ss << lmpcpoly::str_val("nx_underline", nx_underline) << std::endl;
  ss << lmpcpoly::str_val("nu_underline", nu_underline) << std::endl;
  ss << lmpcpoly::str_val("nud_all", nud_all) << std::endl;
  ss << lmpcpoly::str_val("nudd_all", nudd_all) << std::endl;
  ss << lmpcpoly::str_val("nz", nz);
  return ss.str();
}

double HorizonParameters::state_horizon_duration() const {
  return H * ndt * dt;
}
double HorizonParameters::input_horizon_duration() const {
  return (H - 1) * ndt * dt;
}

///////////////////////////////////////////////////////
// HorizonInterpolationStep
HorizonInterpolationStep::HorizonInterpolationStep(
    const std::string interpolated_variable, const HorizonParameters &params,
    Delay *delay, const double time_start, const double t,
    const unsigned int max_steps)
    : interpolated_variable(interpolated_variable), params(params),
      delay(delay), time(t), time_start(time_start),
      t_horizon(time - time_start), i_start(t_horizon / params.hdt),
      max_steps(max_steps), warning_negative_t(t < 0),
      warning_negative_thorizon(t_horizon < 0),
      warning_outside_horizon(i_start >= max_steps),
      horizon_duration(max_steps * params.hdt) {
  if (warning_negative_t || warning_negative_thorizon) {
    std::string msg = "NEGATIVE TIME REQUESTED!!!!!";
    if (warning_negative_thorizon) {
      msg = "NEGATIVE t_horizon!!!";
    }
    debug(msg);
    i_start = 0;
    i_end = 1;
    alpha = 0;
  } else if (warning_outside_horizon) {
    debug("requested time is outside horizon.");
    i_start = max_steps - 1;
    i_end = max_steps;
    alpha = 1;
  } else {
    i_end = i_start + 1;
    alpha = (t_horizon - i_start * params.hdt) / params.hdt;
    assert(alpha <= 1);
    assert(alpha >= 0);
  }
}

bool HorizonInterpolationStep::negative() {
  return warning_negative_t || warning_negative_thorizon;
}

bool HorizonInterpolationStep::warning() {
  return warning_outside_horizon || warning_negative_t ||
         warning_negative_thorizon;
}

std::string HorizonInterpolationStep::warnstr() {
  std::stringstream ss;
  ss << "outside_horizon:" << std::boolalpha << warning_outside_horizon;
  ss << "  negative_t:" << std::boolalpha << warning_negative_t;
  ss << "  warning_negative_thorizon:" << std::boolalpha
     << warning_negative_thorizon;
  return ss.str();
}

void HorizonInterpolationStep::debug(std::string msg) {
  if (delay->passed() && warning()) {
    std::cout << "------------------------------------" << std::endl;
    std::cout << "interpolating << " << interpolated_variable << " >>"
              << std::endl;
    std::cout << msg << std::endl;
    std::cout << "...................................." << std::endl;
    std::cout << "               i_start:" << i_start << std::endl;
    std::cout << "                 i_end:" << i_end << std::endl;
    std::cout << "                 alpha:" << alpha << std::endl;
    std::cout << "        requested time:" << time << "s" << std::endl;
    std::cout << "            time_start:" << time_start << "s" << std::endl;
    std::cout << "             t_horizon:" << t_horizon << "s" << std::endl;
    std::cout << "                   ndt:" << params.ndt << std::endl;
    std::cout << "                   hdt:" << params.hdt << std::endl;
    std::cout << "                     H:" << params.H << std::endl;
    std::cout << "             max_steps:" << max_steps << std::endl;
    std::cout << "      horizon duration:" << horizon_duration << "s"
              << std::endl;
    std::cout << "       warn negative_t:" << std::boolalpha
              << warning_negative_t << std::endl;
    std::cout << "warn negative_thorizon:" << std::boolalpha
              << warning_negative_thorizon << std::endl;
    std::cout << "  warn outside_horizon:" << std::boolalpha
              << warning_outside_horizon << std::endl;
    std::cout << "...................................." << std::endl;
    warning_sent = true;
    delay->refresh();
  }
}

///////////////////////////////////////////////////////
// Horizon
Horizon::Horizon(HorizonParameters params)
    : params(params), _resultx(Eigen::VectorXd::Zero(params.nz)) {}
// Horizon(Horizon const &other) : Horizon(other.params) {
//   _resultx = other._resultx;
//   _goal = other._goal;
// }

Horizon::Horizon(Horizon const &other)
    : params(other.params), _goal(other._goal),
      _pull_pose_pin(other._pull_pose_pin), _resultx(other._resultx),
      last_b(other.last_b) {}

// ****************************************************
// private

Eigen::Matrix4d Horizon::_pose(double time) const {
  pinocchio::SE3 pose = pinocchio::exp6(_x(time));
  return _pull_pose_pin.act(pose).toHomogeneousMatrix();
}

Eigen::Matrix4d Horizon::_pose(double time, const pinocchio::SE3 from) const {
  pinocchio::SE3 pose = pinocchio::exp6(_x(time));
  return from.act(pose).toHomogeneousMatrix();
}

Eigen::VectorXd Horizon::_x(double time) const {
  auto step = HorizonInterpolationStep("STATE", params, &state_warn_delay,
                                       _goal.start.t, time, params.H);
  if (step.negative()) {
    // return _resultx.segment(0, params.nx);
    return pinocchio::log6(_goal.start.pose).toVector();
  }

  auto posek = _resultx.segment(step.i_start * params.nx, params.nx);
  auto posek1 = _resultx.segment(step.i_end * params.nx, params.nx);
  auto result = interpolate_se3_slerp(posek, posek1, step.alpha);

  if (step.warning_sent) {
    std::cout << "          posek:" << posek.transpose() << std::endl;
    std::cout << "         posek1:" << posek1.transpose() << std::endl;
    std::cout << "         result:" << result.transpose() << std::endl;
    std::cout << "           time:" << time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }

  return result;
}

Eigen::VectorXd Horizon::_u(double time) const {
  auto step = HorizonInterpolationStep("INPUT", params, &input_warn_delay,
                                       _goal.start.t, time, params.H - 1);

  if (step.warning_sent) {
    std::cout << "           time:" << time << std::endl;
    std::cout << "------------------------------------" << std::endl;
  }

  if (step.warning()) {
    std::stringstream ss;
    ss << "--------------------------------------" << std::endl;
    ss << "    Horizon._u ZERO U INPUT           " << std::endl;
    ss << "t_horizon:" << step.t_horizon
       << "s  horizon:" << step.horizon_duration
       << "s  time_start:" << step.time_start << "s  time:" << time
       << std::endl;
    ss << "reason: " << step.warnstr() << std::endl;
    ss << "--------------------------------------" << std::endl;
    input_zeromsg_delay.print(ss.str());
    return Eigen::VectorXd::Zero(params.nu);
  }

  auto uk =
      _resultx.segment(params.nx_hat + step.i_start * params.nu, params.nu);
  auto uk1 =
      _resultx.segment(params.nx_hat + step.i_end * params.nu, params.nu);
  auto result = interpolate_linear(uk, uk1, step.alpha);

  // if (step.warning()) {
  //   std::cout << "          uk:" << uk.transpose() << std::endl;
  //   std::cout << "         uk1:" << uk1.transpose() << std::endl;
  //   std::cout << "      result:" << result.transpose() << std::endl;
  //   std::cout << "------------------------------------" << std::endl;
  //   std::cout << "------------------------------------" << std::endl;
  // }

  return result;
}

void Horizon::update(const Eigen::VectorXd result, const Goal goal,
                     const Eigen::Matrix4d pullpose, const double solve_time) {
  std::lock_guard<std::mutex> guard(_mutex);
  this->_resultx = result;
  this->_goal = goal;
  this->_pull_pose_pin = pinocchio::SE3(pullpose);
  this->_solve_time = solve_time;
}

double Horizon::t_horizon(double t) const {
  std::lock_guard<std::mutex> guard(_mutex);
  return t - _goal.start.t;
}

double Horizon::solve_time() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _solve_time;
}

Eigen::VectorXd Horizon::result() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _resultx;
}

// NOTE redirects to pose() that locks the mutex
Eigen::VectorXd Horizon::x(double time) const {
  return pinocchio::log6(pose(time)).toVector();
}

Eigen::Matrix4d Horizon::pose(double time) const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _pose(time);
}

// NOTE redirects to pose() that locks the mutex
Eigen::VectorXd Horizon::x(double time, const pinocchio::SE3 from) const {
  return pinocchio::log6(pose(time, from)).toVector();
}

Eigen::Matrix4d Horizon::pose(double time, const pinocchio::SE3 from) const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _pose(time, from);
}

// NOTE redirects to u() that locks the mutex
Eigen::VectorXd Horizon::twist(double time) const { return u(time); }
Eigen::VectorXd Horizon::u(double time) const {
  std::lock_guard<std::mutex> guard(_mutex);
  // return (_pull_pose_pin.toActionMatrixInverse() * _u(time));
  // return (_pull_pose_pin.toActionMatrix() * _u(time));
  // pinocchio::SE3 refpose(_pull_pose_pin.rotation(), Eigen::Vector3d::Zero());
  // return refpose.toActionMatrixInverse() * _u(time);
  // return refpose.toActionMatrix() * _u(time);
  return _u(time);
}

Eigen::VectorXd Horizon::u0() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _goal.u0;
}

Eigen::VectorXd Horizon::ud0() const {
  std::lock_guard<std::mutex> guard(_mutex);
  return _goal.ud0;
}

std::vector<Eigen::Matrix4d>
Horizon::sample_horizon(unsigned int samples) const {
  std::lock_guard<std::mutex> guard(_mutex);
  if (samples < 5) {
    samples = 5;
  }
  double step_dt = params.state_horizon_duration() / (samples - 1);
  std::vector<Eigen::Matrix4d> ret;
  for (unsigned int i = 0; i < samples; ++i) {
    double t = _goal.start.t + i * step_dt;
    // std::cout << "step_dt:" << step_dt << "s  t:" << t << "s  i:" << i
    //           << std::endl;
    ret.push_back(_pose(t));
  }
  return ret;
}

void Horizon::dump_result(std::string filename) const {
  std::string input_filename = filename + std::string("_resultx.csv");
  std::string state_filename = filename + std::string("_resultu.csv");
  std::ofstream state_file, input_file;
  input_file.open(state_filename);
  state_file.open(input_filename);
  double tt = 0;
  for (int i = 0; tt < params.state_horizon_duration(); ++i) {
    tt = i * params.dt;
    stream_vector(state_file, x(tt).transpose(), tt);
    stream_vector(input_file, u(tt).transpose(), tt);
  }
  state_file.close();
  std::cout << state_filename << " saved." << std::endl;
  input_file.close();
  std::cout << input_filename << " saved." << std::endl;
}

void Horizon::dump_pose_reference(std::string filename) const {
  std::string input_filename = filename + std::string("_refu.csv");
  std::string state_filename = filename + std::string("_refx.csv");
  std::ofstream state_file, input_file;
  state_file.open(state_filename);
  input_file.open(input_filename);

  if (params.ny != 6) {
    std::cout << "Error: Horizon::dump_pose_reference is made for pose "
                 "trajectories with ny=6"
              << std::endl;
    std::abort();
  }

  for (unsigned int i = 0; i < params.H; ++i) {
    double tt = i * params.hdt;

    Eigen::VectorXd poselogk = last_b.segment(params.ny * i, params.ny);
    pinocchio::SE3 posek = pinocchio::exp6(poselogk);

    Eigen::VectorXd posedk = Eigen::VectorXd::Zero(params.ny);
    if (i < params.H - 1) {
      Eigen::VectorXd poselogk1 =
          last_b.segment(params.ny * (i + 1), params.ny);
      pinocchio::SE3 posek1 = pinocchio::exp6(poselogk1);

      posedk = pinocchio::log6(posek.actInv(posek1)).toVector();
    }

    // if ((i < 2) || (i > H - 2)) {
    //   std::cout << "i:" << i << " poselogk: " << poselogk.transpose()
    //             << std::endl;
    // }
    stream_vector(state_file, poselogk.transpose(), tt);
    stream_vector(input_file, posedk.transpose(), tt);
  }
  state_file.close();
  std::cout << state_filename << " saved." << std::endl;
  input_file.close();
  std::cout << input_filename << " saved." << std::endl;
}
