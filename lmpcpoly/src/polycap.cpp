#include "polycap.hpp"
#include "methods.hpp"

using namespace lmpcpoly;

void lmpcpoly::CombinationRepetitionUtil(
    std::vector<std::vector<int>> &combinations, std::vector<int> &chosen,
    int r, int start, int end) {
  // when the number of choosen indexes is equal to the
  // combinaiton length
  if ((int)chosen.size() == r) {
    combinations.push_back(chosen);
    return;
  }

  // recursive choosing of all the combinations elements
  for (int i = start; i <= end; i++) {
    if (std::find(chosen.begin(), chosen.end(), i) != chosen.end()) {
      // make sure that there is no repetitions
      continue;
    }

    chosen.push_back(i);
    // choose the next element
    CombinationRepetitionUtil(combinations, chosen, r, i, end);
    chosen.pop_back();
  }
  return;
}

void lmpcpoly::CombinationRepetition(
    std::vector<std::vector<int>> &combinations, int n, int r) {
  // Allocate memory
  std::vector<int> chosen;
  // Call the recursive function
  CombinationRepetitionUtil(combinations, chosen, r, 0, n - 1);
}

// ref:
// https://gitlab.inria.fr/auctus-team/people/antunskuric/pycapacity/-/blob/master/pycapacity/algorithms.py#L286
void lmpcpoly::hyper_plane_shift_method(Eigen::MatrixXd &H, Eigen::VectorXd &d,
                                        Eigen::MatrixXd A,
                                        Eigen::VectorXd x_min,
                                        Eigen::VectorXd x_max) {
  lmpcpoly::TickTock temp("hps");

  // shape(A) = (m,n)
  // find all combinations of m-1 out of n indexes
  std::vector<std::vector<int>> combinations;
  CombinationRepetition(combinations, A.cols(), A.rows() - 1);

  // int combi = 0;
  // for (auto const &comb : combinations) {
  //   std::cout << "combi:" << combi++ << "\t>";
  //   for (auto const &c : comb) {
  //     std::cout << " " << c;
  //   }
  //   std::cout << std::endl;
  // }

  // for all combinations of indexes
  for (auto const &comb : combinations) {
    // construct the matrix containing the combination of rows
    // with indexes given by the combination

    //////////////////////////////////////////////////////////////
    // Eigen::MatrixXd W = A(Eigen::all, comb); // TODO: Eigen 3.4
    Eigen::MatrixXd W(A.rows(), comb.size());
    for (unsigned int i = 0; i < comb.size(); ++i) {
      W.col(i) = A.col(comb[i]);
    }
    //////////////////////////////////////////////////////////////

    // calculate the svd of this matrix
    // TODO: JacobiSVD seems ~10us faster than BDCSVD
    // it is also expected to be better for small matrices (like jacobians)
    // https://eigen.tuxfamily.org/dox/group__SVD__Module.html
    // also, it seems to fix an issue when compiling in a conda env
    // Eigen::BDCSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);
    Eigen::JacobiSVD<Eigen::MatrixXd> svd(W, Eigen::ComputeFullU);

    // find the null space projection matrix
    // (should be V.T but here is U.T - TODO find why)
    Eigen::MatrixXd V = svd.matrixU().transpose();

    // use singular values to determine if the matrix W is singular
    Eigen::VectorXd s = svd.singularValues();
    // if (s(Eigen::last) != 0) { // TODO: Eigen 3.4
    if (s(s.size() - 1) != 0) {

      // use the last vector of the null space projector
      // as the normal of the half-space

      // Eigen::VectorXd c = V(Eigen::last, Eigen::all); // TODO: Eigen 3.4
      Eigen::VectorXd c = V.row(V.rows() - 1);

      // check if this normal has allready been added to H
      if (H.size()) {
        double tol = 1e-15;
        double diff = (H.rowwise() - c.transpose()).rowwise().norm().minCoeff();
        if ((diff > tol) || (diff < -tol)) {
          // not added yet so add it
          H.conservativeResize(H.rows() + 2, H.cols());
          H.row(H.rows() - 2) = c;
          H.row(H.rows() - 1) = -c;
        } else {
          // already added this normal
          continue;
        }
      } else {
        // first time adding a normal
        H = c.transpose();
        H.conservativeResize(H.rows() + 1, H.cols());
        H.row(H.rows() - 1) = -c;
      }

      // Calculate maximal distances of the half-planes
      // with normal vecotr c form the origin
      Eigen::VectorXd I = c.transpose() * A;
      double d_positive(0.0f), d_negative(0.0f);
      // to find the max you can sum only positive values
      // and min only negative ones
      for (int i = 0; i < I.size(); i++) {
        if (I[i] > 0) {
          d_positive += I[i] * x_max[i];
          d_negative -= +I[i] * x_min[i];
        } else {
          d_positive += I[i] * x_min[i];
          d_negative -= I[i] * x_max[i];
        }
      }
      // add the distances to the vector d
      d.conservativeResize(d.size() + 2);
      d(d.size() - 2) = d_positive;
      d(d.size() - 1) = d_negative;
    }
  }
}
