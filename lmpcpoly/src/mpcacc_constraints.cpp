#include "mpcacc_constraints.hpp"
#include "methods.hpp"

using namespace lmpcpoly;
// ----------------------------------------------------------------------------
// HorizonBoxConstraints
HorizonBoxConstraints::HorizonBoxConstraints(
    unsigned int horizon_constraint_steps, unsigned int horizon_optim_steps,
    const Eigen::VectorXd &max, const Eigen::MatrixXd &L,
    unsigned int nu)
    : nu(nu),                                             //
      horizon_optim_steps(horizon_optim_steps),           //
      horizon_constraint_steps(horizon_constraint_steps), //
      L(HorizonBoxConstraints::linear_matrix(L, horizon_constraint_steps,
                                             horizon_optim_steps,
                                             nu)), //
      _box_without_bias(HorizonBoxConstraints::horizonized(
          max, horizon_constraint_steps, nu)), //
      _box{L, _box_without_bias},              //
      _lmi{L, _box.b}                          //
      {};

VectorLimits
HorizonBoxConstraints::horizonized(const Eigen::VectorXd &max_,
                                   unsigned int horizon_constraint_steps_,
                                   unsigned int nu_) {
  if (max_.rows() != nu_) {
    std::cerr << "Error: horizonized max vector wrong sizes" << std::endl;
    std::cerr << "it should be:" << nu_ << " x 1" << std::endl;
    std::cerr << "but this was provided:" << str_mat("max_", max_) << std::endl;
    std::abort();
  }
  VectorLimits lim(VectorLimits::Ones(nu_));
  lim.min = -max_;
  lim.max = max_;
  lim.repeat(horizon_constraint_steps_);
  return lim;
}

Eigen::MatrixXd HorizonBoxConstraints::linear_matrix(
    const Eigen::MatrixXd &mat, unsigned int horizon_constraint_steps_,
    unsigned int horizon_optim_steps_, unsigned int nu_) {
  unsigned int Lrows_correct = horizon_constraint_steps_ * nu_;
  unsigned int Lcols_correct = horizon_optim_steps_ * nu_;
  if ((mat.cols() != Lcols_correct) || (mat.rows() != Lrows_correct)) {
    std::cerr << "Error: HorizonBoxConstraints L wrong sizes" << std::endl;
    std::cerr << "it should be:" << Lrows_correct << " x " << Lcols_correct
              << std::endl;
    std::cerr << "but this was provided:" << str_mat("L", mat) << std::endl;
    std::abort();
  }

  return mat;
}

VectorLimits
HorizonBoxConstraints::biased_horizon_limits(VectorLimits box_limits,
                                             Eigen::VectorXd x0_) {
  if (x0_.rows() == 0) {
    return box_limits;
  }

  if ((x0_.rows() != box_limits.min.rows()) ||
      (x0_.rows() != box_limits.max.rows())) {
    std::cerr << "Error: simple_box_limits x0_ vector "
                 "wrong sizes"
              << std::endl;
    std::cerr << "it should be:" << box_limits.min.rows() << " x 1"
              << std::endl;
    std::cerr << "but this was provided:" << str_mat("x0_", x0_) << std::endl;
    std::abort();
  }

  box_limits.min -= x0_;
  box_limits.max -= x0_;

  return box_limits;
}

void HorizonBoxConstraints::update_box_max(Eigen::VectorXd max_) {
  std::lock_guard<std::mutex> guard(_mutex);
  _box_without_bias =
      HorizonBoxConstraints::horizonized(max_, horizon_constraint_steps, nu);
  _box.b = _box_without_bias;
  _lmi.b.min.head(_box.b.min.rows()) = _box.b.min;
  _lmi.b.max.head(_box.b.max.rows()) = _box.b.max;
}

void HorizonBoxConstraints::update_bias(Eigen::VectorXd x0_) {
  std::lock_guard<std::mutex> guard(_mutex);
  _box.b = HorizonBoxConstraints::biased_horizon_limits(_box_without_bias, x0_);
  _lmi.b.min.head(_box.b.min.rows()) = _box.b.min;
  _lmi.b.max.head(_box.b.max.rows()) = _box.b.max;
}

void HorizonBoxConstraints::update_firststep_box_max(
    Eigen::VectorXd first_step_min, Eigen::VectorXd first_step_max) {
  std::lock_guard<std::mutex> guard(_mutex);

  if (first_step_max.rows() != 0) {
    _box.b.max.head(first_step_max.rows()) =
        _box_without_bias.max.head(first_step_max.rows())
            .cwiseMin(first_step_max);
    _lmi.b.max.head(_box.b.max.rows()) = _box.b.max;
  }
  if (first_step_min.rows() != 0) {
    _box.b.min.head(first_step_min.rows()) =
        _box_without_bias.min.head(first_step_min.rows())
            .cwiseMax(first_step_min);
    _lmi.b.min.head(_box.b.min.rows()) = _box.b.min;
  }
}

LinearInequalityConstraints HorizonBoxConstraints::lmi() {
  std::lock_guard<std::mutex> guard(_mutex);
  return _lmi;
}

VectorLimits HorizonBoxConstraints::horizon_box_limits() {
  std::lock_guard<std::mutex> guard(_mutex);
  return _box.b;
}

Eigen::VectorXd HorizonBoxConstraints::firststep_box_max() {
  std::lock_guard<std::mutex> guard(_mutex);
  return _box_without_bias.max.head(nu);
}
// HorizonBoxConstraints
// ----------------------------------------------------------------------------
// PolytopeConstraints1
void PolytopeConstraints1::update_mode(int mode_) {
  std::lock_guard<std::mutex> guard(_mutex);
  mode = mode_;
}

LinearInequalityConstraints PolytopeConstraints1::lmi() {
  std::lock_guard<std::mutex> guard(_mutex);
  if (mode == 0) {
    return _box;
  } else if (mode == 1) {
    // only remove first box step
    unsigned int rows_without_first_box = _lmi.A.rows() - nu;
    return {_lmi.A.bottomRows(rows_without_first_box),
            {_lmi.b.min.tail(rows_without_first_box),
             _lmi.b.max.tail(rows_without_first_box)}};
  }

  unsigned int box_rows = _box.A.rows();
  unsigned int pol_rows = _lmi.A.rows() - box_rows;
  return {_lmi.A.bottomRows(pol_rows),
          {_lmi.b.min.tail(pol_rows), _lmi.b.max.tail(pol_rows)}};
}

void PolytopeConstraints1::update_firststep_polytope(
    LinearInequalityConstraints pol) {
  std::lock_guard<std::mutex> guard(_mutex);
  if ((mode == 1) || (mode == 2)) {
    unsigned int Acols = pol.A.cols();

    if (Acols != nu) {
      std::cerr << "Error: PolytopeConstraints1::update_firststep_polytope "
                   "A matrix "
                   "wrong sizes"
                << std::endl;
      std::cerr << "it should be:  N x " << nu << std::endl;
      std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                << std::endl;
      std::abort();
    }

    unsigned int steps_to_add = 1;
    unsigned int Arows = pol.A.rows();

    unsigned int box_rows = _box.b.max.rows();

    {
      // lmpcpoly::TickTock temp(
      //     "con_a.update_firststep_polytope::conservativeResize");
      _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
                                horizon_optim_steps * Acols);
      _lmi.A.bottomRows(steps_to_add * Arows).setZero();
      // NOTE: no need to set zero
      _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
      _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);
    }

    {
      // lmpcpoly::TickTock temp("con_a.update_firststep_polytope::assignment");
      // add first-step constraints
      _lmi.A.block(L.rows(), 0, Arows, Acols) = pol.A;
      _lmi.b.min.segment(L.rows(), Arows) = pol.b.min;
      _lmi.b.max.segment(L.rows(), Arows) = pol.b.max;
    }
  } else {
    _lmi.A.conservativeResize(_box.A.rows(), _box.A.cols());
    _lmi.b.min.conservativeResize(_box.b.min.rows());
    _lmi.b.max.conservativeResize(_box.b.max.rows());
  }
}

void PolytopeConstraints1::update_horizon_polytope(
    LinearInequalityConstraints pol) {
  std::lock_guard<std::mutex> guard(_mutex);
  if (mode == 2) {
    unsigned int Acols = pol.A.cols();
    if (Acols != nu) {
      std::cerr
          << "Error: PolytopeConstraints1::update_horizon_polytope A matrix "
             "wrong sizes"
          << std::endl;
      std::cerr << "it should be:  N x " << nu << std::endl;
      std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                << std::endl;
      std::abort();
    }

    unsigned int box_rows = _box.b.max.rows();
    unsigned int firststep_rows = _lmi.A.rows() - box_rows;
    unsigned int Arows = pol.A.rows();
    if (Arows != firststep_rows) {
      if (delay.passed()) {
        std::cerr << "Warning: PolytopeConstraints1::update_horizon_polytope "
                     "first step polytope "
                     "is different from horizon polytope"
                  << std::endl;
        std::cerr << "warn_counter:" << warn_counter << std::endl;
        std::cerr << "A should be: " << firststep_rows << " x N" << std::endl;
        std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                  << std::endl;
        Eigen::MatrixXd mat = _lmi.A.block(box_rows, 0, firststep_rows, Acols);
        std::cerr << "first step polytope:" << str_mat("mat", mat) << std::endl;
        warn_counter = 0;
        delay.refresh();
      } else {
        warn_counter++;
      }
      // std::abort();
    }

    unsigned int steps_to_add = horizon_constraint_steps;
    {
      // lmpcpoly::TickTock temp(
      //     "con_a.update_horizon_polytope::conservativeResize");
      _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
                                horizon_optim_steps * Acols);
      unsigned int non_firststep_steps = horizon_constraint_steps - 1;
      _lmi.A.bottomRows(non_firststep_steps * Arows).setZero();
      // NOTE: no need to set zero
      _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
      _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);
    }

    // std::cout << "Arows:" << Arows << std::endl;
    // print_mat("pol.A", pol.A);
    // print_mat("pol.b.min", pol.b.min);
    // print_mat("pol.b.max", pol.b.max);

    {
      // lmpcpoly::TickTock temp("con_a.update_horizon_polytope::for loop");
      // print_mat("_lmi.A", _lmi.A);
      // print_mat("_lmi.b.min", _lmi.b.min);
      // print_mat("_lmi.b.max", _lmi.b.max);
      // NOTE: this assumes first step is already updated
      for (unsigned int kk = 1; kk < steps_to_add; ++kk) {
        // std::cout << "kk:" << kk
        //           << "   box_rows + kk * Arows:" << box_rows + kk * Arows
        //           << "   kk * Acols:" << kk * Acols << std::endl;
        _lmi.A.block(box_rows + kk * Arows, kk * Acols, Arows, Acols) = pol.A;
        _lmi.b.min.segment(box_rows + kk * Arows, Arows) = pol.b.min;
        _lmi.b.max.segment(box_rows + kk * Arows, Arows) = pol.b.max;
      }
    }

    // print_mat("_lmi.A", _lmi.A);
    // print_mat("_lmi.b.min", _lmi.b.min);
    // print_mat("_lmi.b.max", _lmi.b.max);
  }
}

// ----------------------------------------------------------------------------
// PolytopeConstraints2

void PolytopeConstraints2::update_bias(Eigen::VectorXd x0_) {
  // print_mat("_lmi.b.max", _lmi.b.max);
  if ((mode == 0) || (mode == 1)) {
    HorizonBoxConstraints::update_bias(x0_);
  }

  std::lock_guard<std::mutex> guard(_mutex);
  if ((mode == 1) || (mode == 2)) {

    Eigen::VectorXd Ax0_firststep =
        _polytope_firststep_nobias.A *
        x0_.head(_polytope_firststep_nobias.A.cols());

    unsigned int box_rows = _box.b.max.rows();
    _lmi.b.min.segment(box_rows, Ax0_firststep.rows()) =
        _polytope_firststep_nobias.b.min - Ax0_firststep;
    _lmi.b.max.segment(box_rows, Ax0_firststep.rows()) =
        _polytope_firststep_nobias.b.max - Ax0_firststep;
  }

  if (mode == 2) {
    // print_mat("_polytope_horizon_nobias.A", _polytope_horizon_nobias.A);
    // print_mat("_polytope_horizon_nobias.b.min",
    // _polytope_horizon_nobias.b.min);
    // print_mat("_polytope_horizon_nobias.b.max",
    // _polytope_horizon_nobias.b.max); print_mat("x0_", x0_);

    if (_polytope_horizon_nobias.A.rows()) {
      Eigen::VectorXd Ax0_nonfirst =
          _polytope_horizon_nobias.A *
          x0_.tail(_polytope_horizon_nobias.A.cols());
      // print_mat("Ax0_nonfirst", Ax0_nonfirst);

      _lmi.b.min.tail(_polytope_horizon_nobias.b.min.rows()) =
          _polytope_horizon_nobias.b.min -
          _polytope_horizon_nobias.A *
              x0_.head(_polytope_horizon_nobias.A.cols());
      _lmi.b.max.tail(_polytope_horizon_nobias.b.max.rows()) =
          _polytope_horizon_nobias.b.max -
          _polytope_horizon_nobias.A *
              x0_.head(_polytope_horizon_nobias.A.cols());
    }
  }

  // print_mat("_lmi.b.max", _lmi.b.max);

  // if (mode == 2) {
  // unsigned int steps_to_add = 1;
  // unsigned int Arows = pol.A.rows();

  // unsigned int box_rows = _box.b.max.rows();
  // _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
  //                           horizon_optim_steps * Acols);
  // _lmi.A.bottomRows(steps_to_add * Arows).setZero();
  // // NOTE: no need to set zero
  // _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
  // _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);

  // // add first-step constraints
  // _polytope_firststep_nobias = pol;
  // _lmi.A.block(L.rows(), 0, Arows, Acols) = pol.A * L.block(0, 0, nu,
  // nu); _lmi.b.min.segment(L.rows(), Arows) = pol.b.min;
  // _lmi.b.max.segment(L.rows(), Arows) = pol.b.max;
  // }
}

void PolytopeConstraints2::update_firststep_polytope(
    LinearInequalityConstraints pol) {
  std::lock_guard<std::mutex> guard(_mutex);
  if ((mode == 1) || (mode == 2)) {
    // set first step polytope
    unsigned int Acols = pol.A.cols();

    if (Acols != nu) {
      std::cerr << "Error: PolytopeConstraints1::update_firststep_polytope "
                   "A matrix "
                   "wrong sizes"
                << std::endl;
      std::cerr << "it should be:  N x " << nu << std::endl;
      std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                << std::endl;
      std::abort();
    }

    unsigned int steps_to_add = 1;
    unsigned int Arows = pol.A.rows();

    unsigned int box_rows = _box.b.max.rows();
    _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
                              horizon_optim_steps * Acols);
    _lmi.A.bottomRows(steps_to_add * Arows).setZero();
    // NOTE: no need to set zero
    _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
    _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);

    // add first-step constraints
    _polytope_firststep_nobias = pol;
    _lmi.A.block(L.rows(), 0, Arows, Acols) = pol.A * L.block(0, 0, nu, nu);
    _lmi.b.min.segment(L.rows(), Arows) = pol.b.min;
    _lmi.b.max.segment(L.rows(), Arows) = pol.b.max;
  } else {
    // box only mode
    _lmi.A.conservativeResize(_box.A.rows(), _box.A.cols());
    _lmi.b.min.conservativeResize(_box.b.min.rows());
    _lmi.b.max.conservativeResize(_box.b.max.rows());
    _polytope_firststep_nobias = LinearInequalityConstraints();
    _polytope_horizon_nobias = LinearInequalityConstraints();
  }
}

void PolytopeConstraints2::update_horizon_polytope(
    LinearInequalityConstraints pol) {
  std::lock_guard<std::mutex> guard(_mutex);
  if (mode == 2) {
    unsigned int Acols = pol.A.cols();
    if (Acols != nu) {
      std::cerr
          << "Error: PolytopeConstraints2::update_horizon_polytope A matrix "
             "wrong sizes"
          << std::endl;
      std::cerr << "it should be:  N x " << nu << std::endl;
      std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                << std::endl;
      std::abort();
    }

    unsigned int box_rows = _box.b.max.rows();
    unsigned int firststep_rows = _lmi.A.rows() - box_rows;
    unsigned int Arows = pol.A.rows();
    if (Arows != firststep_rows) {
      if (delay.passed()) {
        std::cerr << "Error: PolytopeConstraints2::update_horizon_polytope "
                     "first step polytope "
                     "is different from horizon polytope"
                  << std::endl;
        std::cerr << "warn_counter:" << warn_counter << std::endl;
        std::cerr << "A should be: " << firststep_rows << " x N" << std::endl;
        std::cerr << "but this was provided:" << str_mat("pol.A", pol.A)
                  << std::endl;
        Eigen::MatrixXd mat = _lmi.A.block(box_rows, 0, firststep_rows, Acols);
        std::cerr << "first step polytope:" << str_mat("mat", mat) << std::endl;
        warn_counter = 0;
        delay.refresh();
      } else {
        warn_counter++;
      }
      // std::abort();
    }

    unsigned int steps_to_add = horizon_constraint_steps;
    _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
                              horizon_optim_steps * Acols);
    unsigned int non_firststep_steps = horizon_constraint_steps - 1;
    _lmi.A.bottomRows(non_firststep_steps * Arows).setZero();
    // NOTE: no need to set zero
    _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
    _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);

    // update non first step polytope
    _polytope_horizon_nobias.A = Eigen::MatrixXd::Zero(
        non_firststep_steps * Arows, horizon_optim_steps * Acols);
    _polytope_horizon_nobias.b.min.conservativeResize(
        non_firststep_steps * Arows, 1);
    _polytope_horizon_nobias.b.max.conservativeResize(
        non_firststep_steps * Arows, 1);

    // std::cout << "Arows:" << Arows << std::endl;
    // print_mat("pol.A", pol.A);
    // print_mat("pol.b.min", pol.b.min);
    // print_mat("pol.b.max", pol.b.max);
    // print_mat("_polytope_horizon_nobias.A", _polytope_horizon_nobias.A);
    // print_mat("_polytope_horizon_nobias.b.min",
    // _polytope_horizon_nobias.b.min);
    // print_mat("_polytope_horizon_nobias.b.max",
    // _polytope_horizon_nobias.b.max);

    for (unsigned int kk = 1; kk < steps_to_add; ++kk) {
      // std::cout << "kk:" << kk << "  (kk-1)*Arows:" << (kk - 1) * Arows
      //           << std::endl; // debug
      _polytope_horizon_nobias.A.block((kk - 1) * Arows, kk * Acols, Arows,
                                       Acols) = _polytope_firststep_nobias.A;
      _polytope_horizon_nobias.b.min.segment((kk - 1) * Arows, Arows) =
          pol.b.min;
      _polytope_horizon_nobias.b.max.segment((kk - 1) * Arows, Arows) =
          pol.b.max;
      _lmi.b.min.segment(box_rows + kk * Arows, Arows) = pol.b.min;
      _lmi.b.max.segment(box_rows + kk * Arows, Arows) = pol.b.max;
    }

    // print_mat("_lmi.A", _lmi.A);
    // print_mat("L.bottomRows(_polytope_horizon_nobias.A.cols())",
    //           L.bottomRows(_polytope_horizon_nobias.A.cols()));
    // print_mat("_polytope_horizon_nobias.A", _polytope_horizon_nobias.A);
    // print_mat("_lmi.A.block...",
    //           _lmi.A.block(box_rows + Arows, 0,
    //           _polytope_horizon_nobias.A.rows(),
    //                        _polytope_horizon_nobias.A.cols()));
    // print_mat("_A_non * L.bot",
    //           _polytope_horizon_nobias.A *
    //           L.bottomRows(_polytope_horizon_nobias.A.cols()));

    _lmi.A.block(box_rows + Arows, 0, _polytope_horizon_nobias.A.rows(),
                 _polytope_horizon_nobias.A.cols()) =
        _polytope_horizon_nobias.A *
        L.bottomRows(_polytope_horizon_nobias.A.cols());

    // print_mat("_lmi.A", _lmi.A);
    // print_mat("_lmi.b.min", _lmi.b.min);
    // print_mat("_lmi.b.max", _lmi.b.max);
  }
}

// ----------------------------------------------------------------------------
// PolytopeConstraints3

LinearInequalityConstraints PolytopeConstraints3::lmi() {
  std::lock_guard<std::mutex> guard(_mutex);
  if (mode == 0) {
    return _box;
  } else if (mode == 1) {
    // only remove first box step
    unsigned int rows_without_first_box = _lmi.A.rows() - nu;
    return {_lmi.A.bottomRows(rows_without_first_box),
            {_lmi.b.min.tail(rows_without_first_box),
             _lmi.b.max.tail(rows_without_first_box)}};
  }

  return _lmi;
}

void PolytopeConstraints3::update_bias(Eigen::VectorXd x0_) {
  // print_mat("_lmi.b.max", _lmi.b.max);
  HorizonBoxConstraints::update_bias(x0_);

  std::lock_guard<std::mutex> guard(_mutex);
  if ((mode == 1) || (mode == 2)) {

    Eigen::VectorXd Ax0_firststep =
        _polytope_firststep_nobias.A *
        x0_.head(_polytope_firststep_nobias.A.cols());

    unsigned int box_rows = _box.b.max.rows();
    _lmi.b.min.segment(box_rows, Ax0_firststep.rows()) =
        _polytope_firststep_nobias.b.min - Ax0_firststep;
    _lmi.b.max.segment(box_rows, Ax0_firststep.rows()) =
        _polytope_firststep_nobias.b.max - Ax0_firststep;
  }

  if (mode == 2) {
    // print_mat("_polytope_horizon_nobias.A", _polytope_horizon_nobias.A);
    // print_mat("_polytope_horizon_nobias.b.min",
    // _polytope_horizon_nobias.b.min);
    // print_mat("_polytope_horizon_nobias.b.max",
    // _polytope_horizon_nobias.b.max); print_mat("x0_", x0_);

    if (_polytope_horizon_nobias.A.rows()) {
      Eigen::VectorXd Ax0_nonfirst =
          _polytope_horizon_nobias.A *
          x0_.tail(_polytope_horizon_nobias.A.cols());
      // print_mat("Ax0_nonfirst", Ax0_nonfirst);

      _lmi.b.min.tail(_polytope_horizon_nobias.b.min.rows()) =
          _polytope_horizon_nobias.b.min -
          _polytope_horizon_nobias.A *
              x0_.head(_polytope_horizon_nobias.A.cols());
      _lmi.b.max.tail(_polytope_horizon_nobias.b.max.rows()) =
          _polytope_horizon_nobias.b.max -
          _polytope_horizon_nobias.A *
              x0_.head(_polytope_horizon_nobias.A.cols());
    }
  }

  // print_mat("_lmi.b.max", _lmi.b.max);

  // if (mode == 2) {
  // unsigned int steps_to_add = 1;
  // unsigned int Arows = pol.A.rows();

  // unsigned int box_rows = _box.b.max.rows();
  // _lmi.A.conservativeResize(box_rows + steps_to_add * Arows,
  //                           horizon_optim_steps * Acols);
  // _lmi.A.bottomRows(steps_to_add * Arows).setZero();
  // // NOTE: no need to set zero
  // _lmi.b.min.conservativeResize(box_rows + steps_to_add * Arows, 1);
  // _lmi.b.max.conservativeResize(box_rows + steps_to_add * Arows, 1);

  // // add first-step constraints
  // _polytope_firststep_nobias = pol;
  // _lmi.A.block(L.rows(), 0, Arows, Acols) = pol.A * L.block(0, 0, nu,
  // nu); _lmi.b.min.segment(L.rows(), Arows) = pol.b.min;
  // _lmi.b.max.segment(L.rows(), Arows) = pol.b.max;
  // }
}
