#pragma once

#include "methods.hpp"
#include "poses.hpp"
#include <atomic>
#include <iostream>
#include <mutex>
#include <pinocchio/spatial/explog.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <string>
#include <vector>

namespace lmpcpoly {

struct HorizonParameters {
  HorizonParameters(unsigned int H, unsigned int ndt = 15, unsigned int nx = 6,
                    unsigned int nu = 6, unsigned int ny = 6, double dt = 1e-3);
  HorizonParameters(HorizonParameters const &other);

  void print() const;
  std::string str() const;

  double state_horizon_duration() const;
  double input_horizon_duration() const;

  const unsigned int H;
  const unsigned int nx;
  const unsigned int nu;
  const unsigned int ny;
  const unsigned int ndt;
  const double dt;
  const unsigned int nx_underline;
  const unsigned int nx_overline;
  const unsigned int nx_hat;
  const unsigned int nu_underline;
  const unsigned int nud_all;
  const unsigned int nudd_all;
  const unsigned int nz;
  const double hdt;
};

struct HorizonInterpolationStep {
  std::string interpolated_variable;
  const HorizonParameters params;
  Delay *delay;

  double time;
  double time_start;
  double t_horizon; // time if horizon started at t=0

  unsigned int i_start;
  unsigned int i_end;
  double alpha; // between i_start and i_end
  unsigned int max_steps;

  bool warning_sent = false;
  bool warning_negative_t;
  bool warning_negative_thorizon;
  bool warning_outside_horizon;

  double horizon_duration;

  HorizonInterpolationStep(const std::string interpolated_variable,
                           const HorizonParameters &params, Delay *delay,
                           const double time_start, const double t,
                           const unsigned int max_steps);
  bool negative();

  bool warning();
  std::string warnstr();
  void debug(std::string msg);
};

class Horizon {
public:
  Horizon(HorizonParameters params);

  Horizon(Horizon const &other);

  void update(const Eigen::VectorXd result, const Goal goal,
              const Eigen::Matrix4d pullpose, const double solve_time);

  double t_horizon(double t) const;

  double solve_time() const;

  Eigen::VectorXd result() const;

  // NOTE redirects to pose() that locks the mutex
  Eigen::VectorXd x(double time) const;
  Eigen::Matrix4d pose(double time) const;
  Eigen::VectorXd x(double time, const pinocchio::SE3 from) const;
  Eigen::Matrix4d pose(double time, const pinocchio::SE3 from) const;

  // NOTE redirects to u() that locks the mutex
  Eigen::VectorXd twist(double time) const;
  Eigen::VectorXd u(double time) const;
  Eigen::VectorXd u0() const;
  Eigen::VectorXd ud0() const;

  std::vector<Eigen::Matrix4d> sample_horizon(unsigned int samples = 10) const;

  void dump_result(std::string filename) const;
  void dump_pose_reference(std::string filename) const;

  const HorizonParameters params;

private:
  // private functions will not mutex-lock
  Eigen::Matrix4d _pose(double time) const;
  Eigen::Matrix4d _pose(double time, const pinocchio::SE3 from) const;
  Eigen::VectorXd _x(double time) const;
  Eigen::VectorXd _u(double time) const;

  // using mutex in return functions:
  // ref: https://stackoverflow.com/a/60338091
  mutable std::mutex _mutex;
  Goal _goal;
  pinocchio::SE3 _pull_pose_pin;
  double _solve_time = 0;

  Eigen::VectorXd _resultx;
  // TODO: remove or rename, can't now because it's used in
  // dump_pose_reference()
  Eigen::VectorXd last_b;

  inline static Delay state_warn_delay = Delay(5);
  inline static Delay input_warn_delay = Delay(5);
  inline static Delay input_zeromsg_delay = Delay(1);
};

} // namespace lmpcpoly
