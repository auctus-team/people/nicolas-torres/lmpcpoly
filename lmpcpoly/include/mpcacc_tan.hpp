#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>
// #include <Eigen/src/Core/Matrix.h>
// #include <atomic>
// #include <chrono>
#include <mutex>
// #include <string>
// #include <thread>
// #include <tuple>
// #include <vector>

// #include "horizon.hpp"
#include "methods.hpp"
// #include "mpc_matrices.hpp"
// #include "poses.hpp"
#include "qp_problem.hpp"
// #include <fstream>
// #include <future>
// #include <iostream>
// #include <memory>

// #include <qpOASES.hpp>
#include "mpcacc.hpp"
#include "pinocchio/spatial/se3.hpp"
#include <pinocchio/spatial/explog.hpp>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace lmpcpoly {

using Vector6d = Eigen::Matrix<double, 6, 1>;

class MPCAccTangentHorizon {
public:
  MPCAccTangentHorizon(unsigned int H, double T);
  void update(double t0, const pinocchio::SE3 &pullpose,
              const Eigen::VectorXd &p, const Eigen::VectorXd &v,
              const Eigen::VectorXd &a, const Eigen::VectorXd &j);

  std::vector<pinocchio::SE3> sample_horizon(unsigned int samples = 10);

  double duration();
  bool after_horizon(double t);
  bool before_horizon(double t);

  double t_horizon(double t);

  pinocchio::SE3 p_at(double t);
  Vector6d v_at(double t);
  Vector6d a_at(double t);
  Vector6d j_at(double t);

  Eigen::VectorXd p;
  Eigen::VectorXd v;
  Eigen::VectorXd a;
  Eigen::VectorXd j;
  pinocchio::SE3 pull_pose;

private:
  unsigned int _H;
  double _T;

  double _t0 = 0;
};

struct InitialConditionsTangent {
  double t = 0;
  pinocchio::SE3 pose0{Eigen::Matrix3d::Identity(), Eigen::Vector3d::Zero()};
  Vector6d v0{Vector6d::Zero()};
  Vector6d a0{Vector6d::Zero()};
  Vector6d j0{Vector6d::Zero()};
};

// x = [p v]^T
// u = [a]^T
// x_k+1 = A x_k + B u_k
// p_k+1 = Ap x_k + Bp u_k
struct StateSpaceAccTangent {
  double T;
  const unsigned int nu = 6;

  Eigen::MatrixXd A;
  Eigen::MatrixXd B;
  Eigen::MatrixXd Ap;
  Eigen::MatrixXd Av;
  Eigen::MatrixXd Bp;
  Eigen::MatrixXd Bv;

  // constant but initialized in constructor
  std::vector<Eigen::MatrixXd> As;  // contains A^n for n=0...H
  std::vector<Eigen::MatrixXd> Aps; // contains Ap^n for n=0...H
  std::vector<Eigen::MatrixXd> Avs; // contains Ap^n for n=0...H

  StateSpaceAccTangent(double T, unsigned int H);
  void compute_As(unsigned int h);
  // u_underline = [u0 u1 ... uH-1] (acceleration in the horizon)
  // v_overline = Lv u_underline + v0
  // p_overline = Lp u_underline + [Ap x0 ...]
  Eigen::MatrixXd Lp(unsigned int h);
  Eigen::MatrixXd Lv(unsigned int h);
  Eigen::MatrixXd cost_a(unsigned int h);
};

// least squares cost function ||p(u)-p*|| + ||Qv(u)|| + ||Ru||
// x = [p v]^T
// u = [a]^T
// p_k(u)  = Ap^k x_0 + Ap^(k-1)B u_k-1 + Ap^(k-2)B u_k-2 + ... + B u_0
//         = a [u_0 ... u_H]^T + [Ap x_0 Ap^2 x_0 ... Ap^H x_0]^T
// p(u)-p* = a [u_0 ... u_H]^T + [Ap x_0 Ap^2 x_0 ... Ap^H x_0]^T - [p* p*]^T
//         =                     \___________________ ____________________/
//         =                                         V
//         = a       z         -                     b
//
struct MPCAccTangent {

  MPCAccTangent(unsigned int H, double T, MPCParams params = MPCParams(),
                std::string solver = "qpoases", double compute_time_s = 0.01,
                bool jerk_limited_horizon = false);

  // void update_pmax(const Vector6d &max);
  // void update_vmax(const Vector6d &max);
  // void update_amax(const Vector6d &max);
  // void update_jmax(const Vector6d &max);

  // void update_p_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_v_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_a_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_j_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);

  void update_wreg(double w, Eigen::MatrixXd M = Eigen::MatrixXd());
  void update_wterminal(double w);

  void solve(InitialConditionsTangent initial, pinocchio::SE3 target,
             bool presolve = false);

  // ----------------------------------------
  // COMPLETELY CONSTANT
  const unsigned int nu = 6;
  const double compute_time_limit_s;
  const unsigned int H;  // number of steps in horizon
  const unsigned int nz; // number of optimization variables
  const unsigned int nj; // number of jerk variables
  const double T;        // time step in discrete horizon

  // u_underline = [u0 u1 ... uH-1] (acceleration in the horizon)
  const Eigen::MatrixXd La;
  const Eigen::MatrixXd Lj;
  StateSpaceAccTangent ss;
  const Eigen::MatrixXd Lv; // v_overline = Lv u_underline + v0
  const Eigen::MatrixXd Lp; // p_overline = Lp u_underline + [Ap x0 ...]

  const double control_period = 1e-3;
  const bool jerk_limited_horizon;

  const Eigen::MatrixXd cost_a;

  MPCAccTangentHorizon horizon;

  MPCParams params;
  // ----------------------------------------
  // not constant
  Eigen::MatrixXd Qa;

  bool verbose = true;
  std::unique_ptr<QPProblem> qpprob;

  PositionConstraints con_p;
  VelocityConstraints con_v;
  AccelerationConstraints con_a;
  JerkConstraints con_j;

  Eigen::VectorXd Apx0_over;
  Eigen::VectorXd Avx0_over;

  LinearInequalityConstraints C; // all constraints

  std::mutex _mutex;

  Delay delay = Delay(warn_delay_mpc_s);
  int warn_counter = 0;
};
} // namespace lmpcpoly
