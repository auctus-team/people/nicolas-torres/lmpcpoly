#pragma once
#include "methods.hpp"
#include <Eigen/Dense>
#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>

namespace lmpcpoly {
struct Hd {
  Eigen::MatrixXd H;
  Eigen::VectorXd d;
};

struct TrianglePolygon {
  Eigen::MatrixXd vertices;
  std::vector<Eigen::Matrix3d> faces;
  std::vector<Eigen::Vector3d> indices;
};

Hd hps(const Eigen::MatrixXd J, const VectorLimits qd);

LinearInequalityConstraints hps_lmi(Eigen::MatrixXd A, Eigen::VectorXd x_min,
                                    Eigen::VectorXd x_max);

LinearInequalityConstraints
compute_Jqd_halfplane(const VectorLimits qd, const Eigen::MatrixXd J,
                      const unsigned int nu,
                      const unsigned int Jqd_polytope_rows = 21);

TrianglePolygon point_cloud_to_triangle_polygon(const Eigen::MatrixXd points);

Eigen::MatrixXd vertices_from_lmi(const Eigen::MatrixXd &A,
                                  const Eigen::VectorXd &x_min,
                                  const Eigen::VectorXd &x_max);

class RobotPolytope {
public:
  RobotPolytope();
  ~RobotPolytope();

  void update(Eigen::MatrixXd A, VectorLimits lim);
  LinearInequalityConstraints
  polytope_lmi(Eigen::VectorXd bias = Eigen::VectorXd(), bool use_bias = false);

private:
  void run();

  std::mutex _mutex;
  std::mutex _mutex_lmi;
  LinearInequalityConstraints _con;
  Eigen::MatrixXd _A;
  VectorLimits _lim;
  std::atomic_bool stop = false;
  int sleep_us = 300;
};

} // namespace lmpcpoly
