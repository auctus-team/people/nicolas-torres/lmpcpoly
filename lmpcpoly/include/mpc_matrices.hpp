#pragma once

#include "horizon.hpp"
#include "methods.hpp"
#include <Eigen/Dense>
#include <mutex>

namespace lmpcpoly {

struct CostPonderation {
  CostPonderation(double linear_weight = 1, double angular_weight = 1);
  static Eigen::VectorXd build_vector(double linear_weight,
                                      double angular_weight);
  double linear_weight;
  double angular_weight;
  Eigen::VectorXd vec; // should be size nx=6 for x=log(pose in SE3)
};

// least squares cost function ||ax -b|| + ||Hreg||
struct CostFunctionLS {
  Eigen::MatrixXd a;
  Eigen::VectorXd b;
  Eigen::MatrixXd Hreg;
};

class MPCMatrices {
public:
  MPCMatrices(HorizonParameters params);

  Eigen::MatrixXd Fu(unsigned int ind0, unsigned int indf) const;

  VectorLimits u_max() const;
  VectorLimits ud_max() const;
  VectorLimits udd_max() const;

  void update_wreg(double w);
  double wreg() const;

  void update_initial_u(Eigen::VectorXd u);

  void update_initial_ud(Eigen::VectorXd ud);

  void update_u_scaling(double scaling);
  void update_u_limits(VectorLimits lim);

  void update_ud_scaling(double scaling);
  void update_ud_limits(VectorLimits lim);

  void update_udd_scaling(double scaling);
  void update_udd_limits(VectorLimits lim);

  void update_initial_state(Eigen::VectorXd x0);

  CostFunctionLS cost_function() const;

  LinearInequalityConstraints linear_constraints() const;

  void update_extra_linear_constraints(LinearInequalityConstraints constraints);

  void update_linear_horizon_matrices(const Eigen::VectorXd x0,
                                      const Eigen::VectorXd xf);

  void update_cost_ponderation(
      const CostPonderation _ponderation = CostPonderation());

  void update_cost_b(const Eigen::VectorXd x0, const Eigen::VectorXd xf);

  void print_matrices() const;

  std::string str_matrices() const;

  const HorizonParameters params;
  /* underline => from 0 -> H-1
   * overline  => from 1 -> H
   * hat       => from 0 -> H (every step)
   * X_underline from 0->(H-1): X_underline = [x0 x1 ... x_{H-1}]^T
   * X_overline  from 1->(H):   X_overline  = [x1 x2 ... x_H]^T
   * X_hat       from 0->(H):   X_hat       = [x0 x1 ... x_H]^T
   * NOTE: X_hat = [ x0 X_overline^T ]^T = [ X_underline^T xH ]^T
   * U from 0->(H-1): U_underline = [u0 u1 ... u_{H-1}]^T
   * Z = [ x0 X_overline^T U_underline^T ]^T = [ X_hat^T U_underline^T ]^T
   *   = [ X_underline^T xH U_underline^T ]^T
   */
  const Eigen::MatrixXd Fx_underline; // X_underline = Fx_underline Z
  const Eigen::MatrixXd Fx_overline;  // X_overline = Fx_overline Z
  const Eigen::MatrixXd Fu_underline; // U_underline = Fu_underline Z

  const Eigen::MatrixXd Fud;  // ud from 0->H-1
  const Eigen::MatrixXd Fudd; // udd from 0->H-2
  const Eigen::MatrixXd X_overline_iden;

  const Eigen::MatrixXd A; // as in Xk1 = A Xk + B Uk, A is constant

private:
  // ****************************
  // these private methods don't mutex-lock
  VectorLimits ulim_eff() const;
  VectorLimits udlim_eff() const;
  VectorLimits uddlim_eff() const;

  void update_Qreg();

  unsigned int nonextra_rows() const;

  unsigned int total_rows() const;

  void update_initial_state_constraints();

  // void update_u_polytope_constraints() {}

  void update_noninitial_equality_constraints();

  void update_u_constraints();

  void update_ud_constraints();

  void update_udd_constraints();

  void update_nonextra_constraints();

  void resize_if_necessary();

  void update_extra_constraints();
  // ****************************

  // using mutex in return functions:
  // ref: https://stackoverflow.com/a/60338091
  mutable std::mutex _mutex;

  // ****************************
  // equality constraints
  Eigen::VectorXd initial_u;  // previous input value U(k=-1)
  Eigen::VectorXd initial_ud; // previous input value Ud(k=-1)
  SemiconstLinearEqualityConstraints
      initial_state; // equality constraints for X(k=0)

  // equality constraints for Xk1 = Xk + Uk
  // without initial state(k=0) nor input(k=-1)
  SemiconstLinearEqualityConstraints2
      noninitial_equality; // as in Xk1 = A Xk + B Uk, A is constant
  // ****************************

  double _wreg = 1e-6;  // regularisation task weight cost
  Eigen::MatrixXd Qreg; // regularisation task hessian matrix
  CostFunctionLS cost;
  CostPonderation ponderation;

  // VectorLimits x;

  // ****************************
  // input constraints
  unsigned int iter = 0;
  VectorLimits u_lims;
  double u_scaling_factor = .1;
  SemiconstLinearInequalityConstraints u_cons;

  VectorLimits ud_lims;
  // double ud_scaling_factor = .1;
  double ud_scaling_factor = .01;
  SemiconstLinearInequalityConstraints ud_cons;

  VectorLimits udd_lims;
  double udd_scaling_factor = .01;
  SemiconstLinearInequalityConstraints udd_cons;
  // ****************************

  // ****************************
  // polytope constraints
  // LinearInequalityConstraints u_polytope = {
  //     Eigen::MatrixXd::Zero(0, 0),
  //     {Eigen::VectorXd::Zero(0), Eigen::VectorXd::Zero(0)}};
  // LinearInequalityConstraints ud_polytope = {
  //     Eigen::MatrixXd::Zero(0, 0),
  //     {Eigen::VectorXd::Zero(0), Eigen::VectorXd::Zero(0)}};
  // ****************************

  LinearInequalityConstraints extra_cons = {
      Eigen::MatrixXd::Zero(0, 0),
      {Eigen::VectorXd::Zero(0), Eigen::VectorXd::Zero(0)}};

  // final linear constraints matrices
  LinearInequalityConstraints current_cons;
};

} // namespace lmpcpoly
