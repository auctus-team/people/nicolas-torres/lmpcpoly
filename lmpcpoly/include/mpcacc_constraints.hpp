#pragma once

#include "methods.hpp"
#include <mutex>

namespace lmpcpoly {

constexpr int warn_delay_constraints_s = 20;

class HorizonBoxConstraints {
public:
  HorizonBoxConstraints(unsigned int horizon_constraint_steps,
                        unsigned int horizon_optim_steps,
                        const Eigen::VectorXd &max, const Eigen::MatrixXd &L,
                        unsigned int nu = 3);
  virtual ~HorizonBoxConstraints() = default;

  static VectorLimits horizonized(const Eigen::VectorXd &max_,
                                  unsigned int horizon_constraint_steps_,
                                  unsigned int nu_);

  static Eigen::MatrixXd linear_matrix(const Eigen::MatrixXd &mat,
                                       unsigned int horizon_constraint_steps_,
                                       unsigned int horizon_optim_steps_,
                                       unsigned int nu_ = 3);

  static VectorLimits
  biased_horizon_limits(VectorLimits box_limits,
                        Eigen::VectorXd x0_ = Eigen::VectorXd());

  // ------------------------------------------------------------------
  // update methods
  void update_box_max(Eigen::VectorXd max_);

  virtual void update_bias(Eigen::VectorXd x0_);

  void
  update_firststep_box_max(Eigen::VectorXd first_step_min = Eigen::VectorXd(),
                           Eigen::VectorXd first_step_max = Eigen::VectorXd());

  // ------------------------------------------------------------------
  // some getters
  virtual LinearInequalityConstraints lmi();

  VectorLimits horizon_box_limits();

  Eigen::VectorXd firststep_box_max();
  // ------------------------------------------------------------------

  const unsigned int nu;
  const unsigned int horizon_optim_steps;
  const unsigned int horizon_constraint_steps;

  const Eigen::MatrixXd L;

protected:
  VectorLimits _box_without_bias;
  LinearInequalityConstraints _box; // max and min values for the whole horizon
  LinearInequalityConstraints _lmi;
  std::mutex _mutex;
  bool only_box_constraints = true;
};

// PolytopeConstraints1 are constraints directly related to the optimization
// variables i.e. Acceleration constraints
struct PolytopeConstraints1 : public HorizonBoxConstraints {
  using HorizonBoxConstraints::HorizonBoxConstraints;
  int mode = 0;

  void update_mode(int mode_);

  virtual LinearInequalityConstraints lmi() override;
  virtual void update_firststep_polytope(LinearInequalityConstraints pol);
  virtual void update_horizon_polytope(LinearInequalityConstraints pol);

  Delay delay = Delay(warn_delay_constraints_s);
  int warn_counter = 0;
};

// PolytopeConstraints2 are constraints that relate to the optimization
// variables through a transfer matrix i.e. Position, Velocity and Jerk
struct PolytopeConstraints2 : public PolytopeConstraints1 {
  using PolytopeConstraints1::PolytopeConstraints1;

  LinearInequalityConstraints _polytope_firststep_nobias;
  LinearInequalityConstraints _polytope_horizon_nobias;

  void update_firststep_polytope(LinearInequalityConstraints pol) override;
  void update_horizon_polytope(LinearInequalityConstraints pol) override;
  void update_bias(Eigen::VectorXd x0_) override;

  Delay delay = Delay(warn_delay_constraints_s);
  int warn_counter = 0;
};

// PolytopeConstraints3 same as PolytopeConstraints2 but combines box +
// polytopes simultaneously i.e. Velocity
struct PolytopeConstraints3 : public PolytopeConstraints2 {
  using PolytopeConstraints2::PolytopeConstraints2;

  LinearInequalityConstraints lmi() override;
  void update_bias(Eigen::VectorXd x0_) override;
};

using PositionConstraints = PolytopeConstraints3;
using VelocityConstraints = PolytopeConstraints3;
using AccelerationConstraints = PolytopeConstraints1;
using JerkConstraints = PolytopeConstraints2;

} // namespace lmpcpoly
