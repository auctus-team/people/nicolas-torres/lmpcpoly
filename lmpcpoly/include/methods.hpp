#pragma once
#include <Eigen/Dense>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <string>
// TODO: if print_val was in the .cpp, we could use iosfwd
// #include <iosfwd>
#include <iostream>
#include <vector>

namespace lmpcpoly {

std::string str_val(std::string str, double val);

void print_error(const std::stringstream &ss);

std::string str_size(std::string str, Eigen::MatrixXd mat);
void print_size(std::string str, Eigen::MatrixXd mat);

std::string str_mat(std::string str, Eigen::MatrixXd mat);
void print_mat(std::string str, Eigen::MatrixXd mat);

Eigen::MatrixXd rshift(Eigen::MatrixXd toShift, unsigned int shift);
Eigen::MatrixXd block_rcircshift(Eigen::MatrixXd toShift, unsigned int shift);

struct VectorLimits {
  Eigen::VectorXd min;
  Eigen::VectorXd max;

  void repeat(unsigned int n);
  static VectorLimits Ones(unsigned int n);
  static VectorLimits Constant(unsigned int n, double value);
};

struct SolveParameters {
  VectorLimits u;
  VectorLimits ud;
  VectorLimits udd;
  double u_scaling_factor;
  double ud_scaling_factor;
  double udd_scaling_factor;
};

VectorLimits qdmax_from_qqlims(const VectorLimits q, const VectorLimits qd,
                               const Eigen::VectorXd current_q,
                               const double polytope_horizon = 0.25);

// linear constriants b.min <= Ax <= b.max
template <typename TA, typename Tb> struct LinearConstraintsTempl {
  TA A;
  Tb b;
  void lmi_to_horizon_step(unsigned int H);
  void lmi_to_horizon(unsigned int H);
  void combine(const std::vector<LinearConstraintsTempl> &lics);
  // LinearConstraintsTempl(unsigned int n, double value)
  //     : A(Eigen::MatrixXd::Identity(n, n)),
  //       b(VectorLimits::Constant(n, value)){};
};

using LinearInequalityConstraints =
    LinearConstraintsTempl<Eigen::MatrixXd, VectorLimits>;
using SemiconstLinearInequalityConstraints =
    LinearConstraintsTempl<const Eigen::MatrixXd, VectorLimits>;
// using LinearEqualityConstraints =
//     LinearConstraintsTempl<Eigen::MatrixXd, Eigen::VectorXd>;

using SemiconstLinearEqualityConstraints =
    LinearConstraintsTempl<const Eigen::MatrixXd, Eigen::VectorXd>;
using SemiconstLinearEqualityConstraints2 =
    LinearConstraintsTempl<Eigen::MatrixXd, const Eigen::VectorXd>;

using Vector6d = Eigen::Matrix<double, 6, 1>;
using SquareMatrix6d = Eigen::Matrix<double, 6, 6>;

SquareMatrix6d dlog(const Vector6d &x);

Vector6d brse3(const Vector6d &x, const Vector6d &y);

Eigen::VectorXd jexp_dist(const Eigen::Matrix4d cur_pose_mat,
                          const Eigen::Matrix4d obs_pose_mat);

LinearInequalityConstraints
mindistance_obstacle(const Eigen::Matrix4d obs_pose_mat, const double r);

double tock_ms(std::chrono::time_point<std::chrono::steady_clock> start);

std::chrono::time_point<std::chrono::steady_clock> precise_now();

struct Delay {
  Delay(double delay_s);

  Delay(const Delay &other);

  void print(const std::string &str);

  bool passed();

  void refresh();

  std::chrono::time_point<std::chrono::steady_clock> start;
  double delay_s;
  bool initialized = false;
};

struct TickTock {
  TickTock(std::string msg, bool print = true);
  ~TickTock();
  double tock_s();
  double tock_ms();
  void disable();

  void tock(std::string msg, bool header = true);

  std::chrono::time_point<std::chrono::steady_clock> start;
  std::string _msg;
  bool _print;
};

struct LPFilter {
  Eigen::VectorXd filter(Eigen::VectorXd cur,
                         double w0 = -1,  // [Hz] cutoff freq, negative=disabled
                         double dt = 1e-3 // [s] sample T
  );
  Eigen::VectorXd last = Eigen::VectorXd::Zero(0);
};

struct FakeVelocityScaling {
  double min = 0.01;
  double max = 1;
  double step = 0.03;
  double current = 1;
  unsigned int iter = 0;
  Delay delay = Delay(0.06);

  bool scale() {
    if (delay.passed()) {
      // std::cout << "max:" << max << " min:" << min << " current:" << current
      //           << std::endl;
      if ((iter % 2) == 0) {
        current += step;
      } else {
        current -= step;
      }
      if (current > max) {
        current = max;
        iter++;
      }
      if (current < min) {
        current = min;
        iter++;
      }
      delay.refresh();
      return true;
    }
    return false;
  }
};

} // namespace lmpcpoly
