#pragma once

#include <Eigen/Dense>
#include <Eigen/Sparse>
// #include <Eigen/src/Core/Matrix.h>
// #include <atomic>
// #include <chrono>
#include <mutex>
// #include <string>
// #include <thread>
// #include <tuple>
// #include <vector>

#include "mpcacc_constraints.hpp"
// #include "horizon.hpp"
#include "methods.hpp"
// #include "mpc_matrices.hpp"
// #include "poses.hpp"
#include "qp_problem.hpp"
// #include <fstream>
// #include <future>
// #include <iostream>
// #include <memory>
#include <cstdlib>

// #include <qpOASES.hpp>

#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace lmpcpoly {

constexpr double warn_delay_mpc_s = 2;

struct interp_step {
  unsigned int idx;
  double alpha;
};

void convert_infinity(Eigen::VectorXd &a, double val);
interp_step first_index(double t0, double t, double T, unsigned int H);

class MPCAccHorizon {
public:
  MPCAccHorizon(unsigned int H, double T);
  void update(double t0, const Eigen::VectorXd &p, const Eigen::VectorXd &v,
              const Eigen::VectorXd &a, const Eigen::VectorXd &j);

  std::vector<Eigen::Vector3d> sample_horizon(unsigned int samples = 10);

  double duration();
  bool after_horizon(double t);
  bool before_horizon(double t);

  double t_horizon(double t);

  Eigen::Vector3d p_at(double t);
  Eigen::Vector3d v_at(double t);
  Eigen::Vector3d a_at(double t);
  Eigen::Vector3d j_at(double t);

  Eigen::VectorXd p;
  Eigen::VectorXd v;
  Eigen::VectorXd a;
  Eigen::VectorXd j;

private:
  unsigned int _H;
  double _T;

  double _t0 = 0;
};

struct InitialConditions {
  double t = 0;
  Eigen::Vector3d p0{Eigen::Vector3d::Zero()};
  Eigen::Vector3d v0{Eigen::Vector3d::Zero()};
  Eigen::Vector3d a0{Eigen::Vector3d::Zero()};
  Eigen::Vector3d j0{Eigen::Vector3d::Zero()};
};

struct MPCParams {
  // MPCParams() = default;
  // MPCParams(MPCParams &) = default;
  // MPCParams(const MPCParams &) = default;
  MPCParams(double w_a = 1e-5, double w_terminal = 1000)
      : w_a(w_a), w_terminal(w_terminal){};
  double w_a;
  double w_terminal;
};

// x = [p v]^T
// u = [a]^T
// x_k+1 = A x_k + B u_k
// p_k+1 = Ap x_k + Bp u_k
struct StateSpaceAcc {
  double T;
  Eigen::MatrixXd A;
  Eigen::MatrixXd B;
  Eigen::MatrixXd Ap;
  Eigen::MatrixXd Av;
  Eigen::MatrixXd Bp;
  Eigen::MatrixXd Bv;

  // constant but initialized in constructor
  std::vector<Eigen::MatrixXd> As;  // contains A^n for n=0...H
  std::vector<Eigen::MatrixXd> Aps; // contains Ap^n for n=0...H
  std::vector<Eigen::MatrixXd> Avs; // contains Ap^n for n=0...H

  void compute_As(unsigned int h);
  StateSpaceAcc(double T, unsigned int H);

  // u_underline = [u0 u1 ... uH-1] (acceleration in the horizon)
  // v_overline = Lv u_underline + v0
  // p_overline = Lp u_underline + [Ap x0 ...]
  Eigen::MatrixXd Lp(unsigned int h);
  Eigen::MatrixXd Lv(unsigned int h);
};

// least squares cost function ||p(u)-p*|| + ||Qv(u)|| + ||Ru||
// x = [p v]^T
// u = [a]^T
// p_k(u)  = Ap^k x_0 + Ap^(k-1)B u_k-1 + Ap^(k-2)B u_k-2 + ... + B u_0
//         = a [u_0 ... u_H]^T + [Ap x_0 Ap^2 x_0 ... Ap^H x_0]^T
// p(u)-p* = a [u_0 ... u_H]^T + [Ap x_0 Ap^2 x_0 ... Ap^H x_0]^T - [p* p*]^T
//         =                     \___________________ ____________________/
//         =                                         V
//         = a       z         -                     b
//
struct MPCAcc {
  MPCAcc(unsigned int H, double T, MPCParams params = MPCParams(),
         std::string solver = "qpoases", double compute_time_s = 0.01,
         bool jerk_limited_horizon = false);

  // void update_pmax(const Eigen::Vector3d &max);
  // void update_vmax(const Eigen::Vector3d &max);
  // void update_amax(const Eigen::Vector3d &max);
  // void update_jmax(const Eigen::Vector3d &max);

  // void update_p_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_v_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_a_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);
  // void update_j_polytope(const LinearInequalityConstraints pol,
  //                        bool only_first_step = true);

  void solve(InitialConditions initial, Eigen::Vector3d target_position);

  // ----------------------------------------
  const double compute_time_limit_s;
  const MPCParams params;
  const unsigned int H;  // number of steps in horizon
  const unsigned int nz; // number of optimization variables
  const unsigned int nj; // number of jerk variables
  const double T;        // time step in discrete horizon

  const double control_period = 1e-3;
  // ----------------------------------------
  // u_underline = [u0 u1 ... uH-1] (acceleration in the horizon)
  const Eigen::MatrixXd Lj;
  const Eigen::MatrixXd La;
  StateSpaceAcc ss;
  const Eigen::MatrixXd Lv; // v_overline = Lv u_underline + v0
  const Eigen::MatrixXd Lp; // p_overline = Lp u_underline + [Ap x0 ...]

  const Eigen::MatrixXd Qa;
  const bool jerk_limited_horizon;

  MPCAccHorizon horizon;

  // ----------------------------------------
  // not constant
  bool verbose = true;
  std::unique_ptr<QPProblem> qpprob;

  PositionConstraints con_p;
  VelocityConstraints con_v;
  AccelerationConstraints con_a;
  JerkConstraints con_j;

  Eigen::VectorXd Apx0_over;
  Eigen::VectorXd Avx0_over;

  LinearInequalityConstraints C; // all constraints

  std::mutex _mutex;

  Delay delay = Delay(warn_delay_mpc_s);
  int warn_counter = 0;
};
} // namespace lmpcpoly
