# Lmpcpoly


### test python code

```bash
mkdir build/ && cd build
cmake ..
make -j
python ../polyhedron.py # assumes pycapacity
python ../simple_simulation.py
```

### visualize polytopes

```bash
cd lmpcpoly
roslaunch ./launch/one_panda.launch
python rviz_triangle.py # this requires having compiled lmpcpoly
```
