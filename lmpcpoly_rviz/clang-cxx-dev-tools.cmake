# Additional targets to perform clang-format
# Get all project files
file(GLOB_RECURSE
     ALL_CXX_SOURCE_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "src/*.cpp"
     )

file(GLOB_RECURSE
     ALL_HEADER_FILES
     RELATIVE
     "${CMAKE_SOURCE_DIR}" "src/*.h"
     )
message("cpps: ${ALL_CXX_SOURCE_FILES}")

# Exclude all files in 3rd dirs.
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "3rd/*")
list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "build*")
# list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "#*")
# list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "^.*")
# list(FILTER ALL_CXX_SOURCE_FILES EXCLUDE REGEX "^/src/.")

# Find clang-format
find_program(clang_format_binpath NAMES clang-format clang-format-10 PATHS "/usr/bin/")

# Adding clang-format target if executable is found
add_custom_target(
    clang-format
    ALL
    COMMAND cd ${CMAKE_SOURCE_DIR} && ${clang_format_binpath}
    -i
    ${ALL_CXX_SOURCE_FILES}
    ${ALL_HEADER_FILES}
    )

if (${TIDY})
     message("CLANG-TIDY ENABLED")
     set(ALL_VAR ALL)
endif()

find_program(clang_tidy_binpath NAMES clang-tidy-10 clang-tidy PATHS "/usr/bin/")
add_custom_target(
     clang-tidy
     ${ALL_VAR}
     COMMAND
     cd ${CMAKE_SOURCE_DIR} && ${clang_tidy_binpath}
     ${ALL_CXX_SOURCE_FILES}
     -p ${CMAKE_BINARY_DIR}
     )
# get_target_property(MY_PROJECT_SOURCES my_project SOURCES)
function(foo HI)
  message("I am foo!! ${HI}")
endfunction()

foo("asdf")

function(format_target TARGET_NAME)
  get_target_property(target_sources ${TARGET_NAME} SOURCES)
  get_target_property(target_includes ${TARGET_NAME} INCLUDE_DIRECTORIES)
  message("******************************************************")
  message("format_target(${TARGET_NAME}):")
  message("           sources: ${target_sources}")
  message("PROJECT_SOURCE_DIR: ${PROJECT_SOURCE_DIR}")
  # message("   target_includes: ${target_includes}")
  list(FILTER target_includes INCLUDE REGEX "${PROJECT_SOURCE_DIR}.*")
  message("   target_includes: ${target_includes}")

  # glob headers from include dirs
  set(headers_paths)
  foreach(f ${target_includes})
    file(GLOB_RECURSE
      f_headers
      "${f}" "*.hpp" "*.h"
      )
    message("       f_headers: ${f_headers}")
    list(APPEND headers_paths ${f_headers})
  endforeach()
  message("           headers: ${headers_paths}")

  # find absolute paths from .cpp files
  set(cpp_paths)
  foreach(f ${target_sources})
    get_filename_component(fabs ${f}
      REALPATH BASE_DIR "${CMAKE_SOURCE_DIR}")
    list(APPEND cpp_paths ${fabs})
  endforeach()
  message("              cpps: ${cpp_paths}")

  # add format target
  # add_custom_target(
  #   clang-format-${TARGET_NAME}
  #   ALL
  #   COMMAND cd ${CMAKE_BINARY_DIR} && ${clang_format_binpath}
  #   -i
  #   ${target_sources}
  #   ${headers_paths}
  #   )
  message("******************************************************")
endfunction()


