/*********************************************************************
 * Software License Agreement (BSD License)
 *
 *  Copyright (c) 2017, PickNik Consulting
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above
 *     copyright notice, this list of conditions and the following
 *     disclaimer in the documentation and/or other materials provided
 *     with the distribution.
 *   * Neither the name of PickNik Consulting nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 *  FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 *  INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 *  BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 *  ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 *  POSSIBILITY OF SUCH DAMAGE.
 *********************************************************************/

/* Author: Dave Coleman
   Desc:   Rviz display panel for controlling and debugging ROS applications
*/

#include <cstdio>
#include <cstdlib>

#include <QGroupBox>
#include <QHBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QPainter>
#include <QSpinBox>
#include <QTimer>

#include <QIcon>
#include <QStyle>

#include "robot_control.h"
#include <ros/node_handle.h>

#include <ros/master.h>

#include <QApplication>
#include <QStyle>

#include <cmath>
#include <ios>
#include <sstream>

namespace rviz_robot_plugins {

// std::string run(const char *cmd) {
//   char buffer[128];
//   std::string result = "";
//   FILE *pipe = popen(cmd, "r");
//   if (!pipe)
//     throw std::runtime_error("popen() failed!");
//   try {
//     while (fgets(buffer, sizeof buffer, pipe) != NULL) {
//       result += buffer;
//     }
//   } catch (...) {
//     pclose(pipe);
//     throw;
//   }
//   pclose(pipe);
//   return result;
// }

const int QLABEL_WIDTH = 42;

double proportional_value(long int max, long int val, double realMax) {
  double delta = (max - 1);
  return (val / delta) * realMax;
}

long int proportional_int(long int max, double val, double realMax) {
  double delta = (max - 1);
  return (val * delta) / realMax;
}

double proportional_value_min(long int min, long int max, long int val,
                              double realMin, double realMax) {
  double delta = (max - min);
  return (val / delta) * (realMax - realMin);
}

long int proportional_int_min(long int min, long int max, double val,
                              double realMin, double realMax) {
  double delta = (max - min);
  return (val * delta) / (realMax - realMin);
}

void PushButton::add_custom_slider(std::string title, int slider_max,
                                   int slider_interval, double current_value,
                                   double real_max, double *msg_variable,
                                   QVBoxLayout *layout) {
  QString qtitle(QString::fromStdString(title));
  QHBoxLayout *tmpH1 = new QHBoxLayout();
  QLabel *tmpLabelTitle = new QLabel(qtitle);
  QLabel *tmpLabel = new QLabel("0");
  tmpLabel->setFixedWidth(QLABEL_WIDTH);
  QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
  tmpSlider->setMinimum(1);
  tmpSlider->setMaximum(slider_max);
  tmpSlider->setValue(proportional_int(slider_max, current_value, real_max));
  tmpSlider->setTickInterval(slider_interval);
  tmpSlider->setTickPosition(QSlider::TicksBelow);
  tmpH1->addWidget(tmpLabelTitle);
  tmpH1->addWidget(tmpSlider);
  tmpH1->addWidget(tmpLabel);
  layout->addLayout(tmpH1);
  auto updateLambda = [=, this]() {
    double realVal =
        proportional_value(slider_max, tmpSlider->value(), real_max);
    tmpLabel->setText(QString::number(realVal));
    *msg_variable = realVal;
  };
  updateLambda();
  QObject::connect(tmpSlider, &QSlider::valueChanged, this, [=, this]() {
    updateLambda();
    this->configure_mpc_client.call(this->configure_msg);
  });
}

void PushButton::add_custom_scientific_slider(std::string title,
                                              double current_value,
                                              double *msg_variable,
                                              QVBoxLayout *layout) {
  QString qtitle(QString::fromStdString(title));
  QHBoxLayout *tmpH1 = new QHBoxLayout();
  QLabel *tmpLabelTitle = new QLabel(qtitle);
  QLabel *tmpLabel = new QLabel("0");
  tmpLabel->setFixedWidth(QLABEL_WIDTH);
  QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
  tmpSlider->setMinimum(-12);
  tmpSlider->setMaximum(12);
  tmpSlider->setValue(int(log10(current_value)));
  tmpSlider->setTickInterval(1);
  tmpSlider->setTickPosition(QSlider::TicksBelow);
  tmpH1->addWidget(tmpLabelTitle);
  tmpH1->addWidget(tmpSlider);
  tmpH1->addWidget(tmpLabel);
  layout->addLayout(tmpH1);

  auto updateLambda = [=]() {
    double realVal = exp10(tmpSlider->value());
    tmpLabel->setText(QString::number(tmpSlider->value()));
    *msg_variable = realVal;
  };
  updateLambda();
  QObject::connect(tmpSlider, &QSlider::valueChanged, this, [=, this]() {
    updateLambda();
    this->configure_mpc_client.call(this->configure_msg);
  });
}

PushButton::PushButton(QWidget *parent) : rviz::Panel(parent) {
  // ros services
  const std::string start_service_str = "/start_mpc";
  push_button_client = nh.serviceClient<moveit_trajectory_interface::MPCFollow>(
      start_service_str);

  const std::string configure_service_str = "/configure_mpc";
  configure_mpc_client =
      nh.serviceClient<moveit_trajectory_interface::MPCConfigure>(
          configure_service_str);
  const std::string getconfigure_service_str = "/get_configure_mpc";
  get_configure_mpc_client =
      nh.serviceClient<moveit_trajectory_interface::MPCConfigure>(
          getconfigure_service_str);

  unsigned int timeout_ms = 30000;
  ROS_INFO_STREAM("lmpcpoly rviz plugin wait for services...");
  if (!ros::service::waitForService(start_service_str, timeout_ms)) {
    ROS_FATAL_STREAM(start_service_str << " NOT READY");
  }
  ROS_INFO_STREAM(start_service_str << " OK");

  if (!ros::service::waitForService(configure_service_str, timeout_ms)) {
    ROS_FATAL_STREAM(configure_service_str << " NOT READY");
  }
  ROS_INFO_STREAM(configure_service_str << " OK");

  if (!ros::service::waitForService(getconfigure_service_str, timeout_ms)) {
    ROS_FATAL_STREAM(getconfigure_service_str << " NOT READY");
  }
  ROS_INFO_STREAM(getconfigure_service_str << " OK");

  moveit_trajectory_interface::MPCConfigure current_configuration;
  get_configure_mpc_client.call(current_configuration);
  ROS_INFO_STREAM(getconfigure_service_str << "call OK");
  // std::cout << "current_"
  //              "configuration:"
  //           << std::endl;

  // std::cout << "max_velocity_linear     "
  //           << current_configuration.response.max_velocity_linear <<
  //           std::endl;
  // std::cout << "max_acceleration_linear "
  //           << current_configuration.response.max_acceleration_linear
  //           << std::endl;
  // std::cout << "max_jerk_linear         "
  //           << current_configuration.response.max_jerk_linear << std::endl;
  // std::cout << "max_velocity_angular    "
  //           << current_configuration.response.max_velocity_angular <<
  //           std::endl;
  // std::cout << "max_acceleration_angular"
  //           << current_configuration.response.max_acceleration_angular
  //           << std::endl;
  // std::cout << "max_jerk_angular        "
  //           << current_configuration.response.max_jerk_angular << std::endl;
  // std::cout << "marker_update_delay     "
  //           << current_configuration.response.marker_update_delay <<
  //           std::endl;
  // std::cout << "mpc_w_reg               "
  //           << current_configuration.response.mpc_w_reg << std::endl;
  // std::cout << "mpc_w_terminal          "
  //           << current_configuration.response.mpc_w_terminal << std::endl;
  // std::cout << "kp_linear               "
  //           << current_configuration.response.kp_linear << std::endl;
  // std::cout << "kp_angular              "
  //           << current_configuration.response.kp_angular << std::endl;
  // std::cout << "kd_factor_linear        "
  //           << current_configuration.response.kd_factor_linear << std::endl;
  // std::cout << "kd_factor_angular       "
  //           << current_configuration.response.kd_factor_angular << std::endl;
  // std::cout << "ff_acc_factor           "
  //           << current_configuration.response.ff_acc_factor << std::endl;
  // std::cout << "Emax                    " <<
  // current_configuration.response.Emax
  //           << std::endl;
  // std::cout << "use_polytope            "
  //           << current_configuration.response.use_polytope << std::endl;

  auto *layout = new QVBoxLayout;
  layout->setContentsMargins(0, 0, 0, 0);
  layout->setSpacing(5);
  layout->setAlignment(Qt::AlignHCenter);
  // layout->addStretch(10);

  // {
  //   QLabel *tmpLabel = new QLabel("MPC Settings");
  //   tmpLabel->setAlignment(Qt::AlignCenter);
  //   tmpLabel->setStyleSheet("color: gray");
  //   auto *hlayout = new QHBoxLayout;
  //   hlayout->setAlignment(Qt::AlignHCenter);
  //   hlayout->addWidget(tmpLabel);
  //   hlayout->setContentsMargins(0, 0, 0, 0);
  //   hlayout->setSpacing(0);
  //   layout->addLayout(hlayout);
  // }

  {
    auto *hlayout = new QHBoxLayout;
    hlayout->setAlignment(Qt::AlignHCenter);

    // -----------------------------------------------
    // -----------------------------------------------
    // mpc mode combo: position, tangent
    {
      mpc_mode_combo = new QComboBox;
      QStringList mpc_modes;
      const std::string PositionStr = "Position";
      const std::string TangentStr = "Tangent";
      mpc_modes << QString::fromStdString(PositionStr);
      mpc_modes << QString::fromStdString(TangentStr);

      mpc_mode_combo->clear();
      mpc_mode_combo->addItems(mpc_modes);
      if (current_configuration.response.tangent_mode) {
        mpc_mode_combo->setCurrentText(QString::fromStdString(TangentStr));
      } else {
        mpc_mode_combo->setCurrentText(QString::fromStdString(PositionStr));
      }
      connect(mpc_mode_combo, qOverload<int>(&QComboBox::currentIndexChanged),
              [=, this](__attribute__((unused)) int ix) {
                this->configure_msg.request.tangent_mode =
                    mpc_mode_combo->currentText().toStdString() == TangentStr;
                this->configure_mpc_client.call(this->configure_msg);
              });
      hlayout->addWidget(mpc_mode_combo);
    }
    // -----------------------------------------------
    // -----------------------------------------------

    topic_combo = new QComboBox;
    hlayout->addWidget(topic_combo);
    auto update_topics = [=, this]() {
      ros::master::V_TopicInfo topic_infos;
      ros::master::getTopics(topic_infos);
      QStringList pose_topics;
      // std::cout<<"Pose topics found:"<<std::endl;
      const std::string Pose = "PoseStamped";
      for (auto const &topic : topic_infos) {
        if (topic.datatype.find(Pose) != std::string::npos) {
          pose_topics << QString::fromStdString(topic.name);
          // std::cout << topic.name << "->" << topic.datatype << std::endl;
        }
      }

      std::string default_topic = "";
      const std::string Marker = "marker";
      for (auto const &topic : pose_topics) {
        if (topic.toStdString().find(Marker) != std::string::npos) {
          default_topic = topic.toStdString();
          break;
        }
      }
      const std::string Lego = "lego";
      if (default_topic.empty()) {
        for (auto const &topic : pose_topics) {
          if (topic.toStdString().find(Lego) != std::string::npos) {
            default_topic = topic.toStdString();
            break;
          }
        }
      }

      topic_combo->clear();
      topic_combo->addItems(pose_topics);
      if (!default_topic.empty()) {
        topic_combo->setCurrentText(QString::fromStdString(default_topic));
      }
    };

    update_topics();

    QToolButton *refresh_button = new QToolButton;
    refresh_button->setIcon(
        QApplication::style()->standardIcon(QStyle::SP_BrowserReload));
    connect(refresh_button, &QToolButton::clicked, [=]() { update_topics(); });
    refresh_button->setIconSize(QSize(20, 20));
    hlayout->addWidget(refresh_button);

    push_button = new QToolButton;
    push_button->setIcon(
        QApplication::style()->standardIcon(QStyle::SP_ArrowForward));
    push_button->setIconSize(QSize(20, 20));
    hlayout->addWidget(push_button);

    {
      QLabel *tmpLabel = new QLabel("OFF");
      tmpLabel->setFixedWidth(QLABEL_WIDTH);
      tmpLabel->setAlignment(Qt::AlignCenter);
      const std::string ON = "ON";
      const std::string ONbg =
          "QLabel { background-color : red; color : black; }";
      const std::string OFF = "OFF";
      const std::string OFFbg =
          "QLabel { background-color : green; color : black; }";
      tmpLabel->setStyleSheet(QString::fromStdString(OFFbg));
      connect(push_button, &QToolButton::clicked, this, [=, this]() {
        if (tmpLabel->text().toStdString() == ON) {
          tmpLabel->setText(QString::fromStdString(OFF));
          tmpLabel->setStyleSheet(QString::fromStdString(OFFbg));
          follow_msg.request.flag = false;
          push_button->setIcon(
              QApplication::style()->standardIcon(QStyle::SP_ArrowForward));
        } else {
          tmpLabel->setText(QString::fromStdString(ON));
          follow_msg.request.flag = true;
          tmpLabel->setStyleSheet(QString::fromStdString(ONbg));
          push_button->setIcon(
              QApplication::style()->standardIcon(QStyle::SP_BrowserStop));
        }
        follow_msg.request.topic = topic_combo->currentText().toStdString();
        push_button_client.call(follow_msg);
      });
      hlayout->addWidget(tmpLabel);
    }

    layout->addLayout(hlayout);
  }

  // ----------------------------------------------------------------------------
  // ----------------------------------------------------------------------------

  add_custom_slider("Marker Delay", 1000, 1, 0.01, 20,
                    &this->configure_msg.request.marker_update_delay, layout);

  add_custom_scientific_slider("w reg 1e^",
                               current_configuration.response.mpc_w_reg,
                               &this->configure_msg.request.mpc_w_reg, layout);

  add_custom_scientific_slider(
      "w term 1e^", current_configuration.response.mpc_w_terminal,
      &this->configure_msg.request.mpc_w_terminal, layout);

  const int small_line_length = 4;
  const int big_line_length = 10;
  {
    QFrame *line = new QFrame(this);
    line->setFrameShape(QFrame::HLine); // Horizontal line
    // line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(big_line_length);
    layout->addWidget(line);
  }

  // ------------------------------------------------
  // ----------------------------------------------------------------------------
  // ----------------------------------------------------------------------------
  {
    unsigned int vertical_line_width = 2;

    auto *hlayout = new QHBoxLayout;
    // hlayout->setSizeConstraint(QLayout::SetMaximumSize);

    QLabel *tmpLabelTitle = new QLabel("Constraints:");
    hlayout->addWidget(tmpLabelTitle);

    constraint_mode_combo = new QComboBox;
    hlayout->addWidget(constraint_mode_combo);

    QToolButton *refresh_button = new QToolButton;
    refresh_button->setIcon(
        QApplication::style()->standardIcon(QStyle::SP_MediaPlay));
    refresh_button->setIconSize(QSize(20, 20));

    QLabel *refresh_label = new QLabel();
    // QIcon iconOK(
    //     QApplication::style()->standardIcon(QStyle::SP_DialogApplyButton));
    // QPixmap pixmapOK = iconOK.pixmap(iconOK.actualSize(QSize(16, 16)));
    // QIcon iconNOK(QApplication::style()->standardIcon(
    //     QStyle::SP_DialogApplyButton, QIcon::Disabled));
    // QPixmap pixmapNOK = iconNOK.pixmap(iconNOK.actualSize(QSize(16, 16)));
    refresh_label->setPixmap(
        QApplication::style()->standardPixmap(QStyle::SP_DialogApplyButton));

    QStringList constraint_modes;
    const std::string OnlyBoxStr = "Only Box";
    const std::string HybridStr = "Box + First Step Polytope";
    const std::string OnlyPolytopeStr = "Only Polytopes";
    constraint_modes << QString::fromStdString(OnlyBoxStr);
    constraint_modes << QString::fromStdString(HybridStr);
    constraint_modes << QString::fromStdString(OnlyPolytopeStr);

    auto mode_to_int = [=, this](std::string option) -> int {
      if (option == HybridStr) {
        return 1;
      } else if (option == OnlyPolytopeStr) {
        return 2;
      } else {
        return 0;
      }
    };

    constraint_mode_combo->clear();
    constraint_mode_combo->addItems(constraint_modes);
    constraint_mode_combo->setCurrentText(QString::fromStdString(OnlyBoxStr));
    connect(constraint_mode_combo,
            qOverload<int>(&QComboBox::currentIndexChanged),
            [=](__attribute__((unused)) int ix) {
              refresh_label->setPixmap(QApplication::style()->standardPixmap(
                  QStyle::SP_DialogCancelButton));
            });

    auto update_mode = [=, this]() {
      this->configure_msg.request.constraints_mode =
          mode_to_int(constraint_mode_combo->currentText().toStdString());
      this->configure_mpc_client.call(this->configure_msg);
      refresh_label->setPixmap(
          QApplication::style()->standardPixmap(QStyle::SP_DialogApplyButton));
    };
    connect(refresh_button, &QToolButton::clicked, [=]() { update_mode(); });
    hlayout->addWidget(refresh_button);
    hlayout->addWidget(refresh_label);
    {
      QFrame *line = new QFrame(this);
      line->setFrameShape(QFrame::VLine);
      line->setFrameShadow(QFrame::Sunken);
      line->setLineWidth(vertical_line_width);
      hlayout->addWidget(line);
    }

    // --------------------------------------------------
    // jerk combo
    {
      QLabel *tmpLabelTitle2 = new QLabel("Jerk Limits:");
      hlayout->addWidget(tmpLabelTitle2);
      jerk_mode_combo = new QComboBox;
      hlayout->addWidget(jerk_mode_combo);
      const std::string JerkFristStepStr = "First Step";
      const std::string JerkHorizonStr = "Horizon";
      QStringList jerk_modes;
      jerk_modes << QString::fromStdString(JerkFristStepStr);
      jerk_modes << QString::fromStdString(JerkHorizonStr);
      jerk_mode_combo->clear();
      jerk_mode_combo->addItems(jerk_modes);
      if (current_configuration.response.limit_jerk_horizon) {
        jerk_mode_combo->setCurrentText(QString::fromStdString(JerkHorizonStr));
      } else {
        jerk_mode_combo->setCurrentText(
            QString::fromStdString(JerkFristStepStr));
      }
      connect(jerk_mode_combo, qOverload<int>(&QComboBox::currentIndexChanged),
              [=, this](__attribute__((unused)) int ix) {
                this->configure_msg.request.limit_jerk_horizon =
                    jerk_mode_combo->currentText().toStdString() ==
                    JerkHorizonStr;
                this->configure_mpc_client.call(this->configure_msg);
              });
      hlayout->addWidget(jerk_mode_combo);
      {
        QFrame *line = new QFrame(this);
        line->setFrameShape(QFrame::VLine);
        line->setFrameShadow(QFrame::Sunken);
        line->setLineWidth(vertical_line_width);
        hlayout->addWidget(line);
      }
    }
    // --------------------------------------------------
    // --------------------------------------------------
    // bias combo
    {
      QLabel *tmpLabelTitle2 = new QLabel("Bias:");
      hlayout->addWidget(tmpLabelTitle2);
      bias_mode_combo = new QComboBox;
      hlayout->addWidget(bias_mode_combo);
      const std::string BiasNoStr = "No";
      const std::string BiasYesStr = "Yes";
      QStringList bias_modes;
      bias_modes << QString::fromStdString(BiasNoStr);
      bias_modes << QString::fromStdString(BiasYesStr);
      bias_mode_combo->clear();
      bias_mode_combo->addItems(bias_modes);
      if (current_configuration.response.polytope_with_bias) {
        bias_mode_combo->setCurrentText(QString::fromStdString(BiasYesStr));
      } else {
        bias_mode_combo->setCurrentText(QString::fromStdString(BiasNoStr));
      }
      connect(bias_mode_combo, qOverload<int>(&QComboBox::currentIndexChanged),
              [=, this](__attribute__((unused)) int ix) {
                this->configure_msg.request.polytope_with_bias =
                    bias_mode_combo->currentText().toStdString() == BiasYesStr;
                this->configure_mpc_client.call(this->configure_msg);
              });
      hlayout->addWidget(bias_mode_combo);
    }
    // --------------------------------------------------

    hlayout->setAlignment(Qt::AlignHCenter);
    layout->addLayout(hlayout);
  }

  {
    QFrame *line = new QFrame(this);
    line->setFrameShape(QFrame::HLine); // Horizontal line
    line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(small_line_length);
    layout->addWidget(line);
  }

  // ----------------------------------------------------------------------------

  // slider
  {
    QLabel *tmpLabelTitle = new QLabel("Box Limits");
    QHBoxLayout *tmpH1 = new QHBoxLayout();
    // tmpH1->addWidget(tmpLabelTitle, 0, Qt::AlignTop);
    tmpH1->addWidget(tmpLabelTitle);
    tmpH1->setAlignment(Qt::AlignHCenter);
    tmpH1->setSpacing(0);
    tmpH1->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(tmpH1);
  }

  add_custom_slider("Lin Vel", 1000, 1,
                    // current_configuration.response.max_velocity_linear, 1.7,
                    current_configuration.response.max_velocity_linear, 3,
                    &this->configure_msg.request.max_velocity_linear, layout);
  add_custom_slider(
      "Lin Acc", 10000, 1,
      // current_configuration.response.max_acceleration_linear, 13,
      current_configuration.response.max_acceleration_linear, 30,
      &this->configure_msg.request.max_acceleration_linear, layout);
  add_custom_slider("Lin Jer", 20000, 1,
                    // current_configuration.response.max_jerk_linear, 6500,
                    current_configuration.response.max_jerk_linear, 20000,
                    &this->configure_msg.request.max_jerk_linear, layout);

  add_custom_slider("Ang Vel", 1000, 1,
                    current_configuration.response.max_velocity_angular, 2.5,
                    &this->configure_msg.request.max_velocity_angular, layout);
  add_custom_slider("Ang Acc", 1000, 1,
                    current_configuration.response.max_acceleration_angular, 25,
                    &this->configure_msg.request.max_acceleration_angular,
                    layout);
  add_custom_slider("Ang Jer", 12500, 1,
                    current_configuration.response.max_jerk_angular, 6500,
                    &this->configure_msg.request.max_jerk_angular, layout);

  {
    QFrame *line = new QFrame(this);
    line->setFrameShape(QFrame::HLine); // Horizontal line
    line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(small_line_length);
    layout->addWidget(line);
  }
  {
    QLabel *tmpLabelTitle = new QLabel("Polytope Limits");
    QHBoxLayout *tmpH1 = new QHBoxLayout();
    // tmpH1->addWidget(tmpLabelTitle, 0, Qt::AlignTop);
    tmpH1->addWidget(tmpLabelTitle);
    tmpH1->setAlignment(Qt::AlignHCenter);
    tmpH1->setSpacing(0);
    tmpH1->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(tmpH1);
  }

  // add_custom_slider("Emax", 100000, 1, current_configuration.response.Emax,
  // 20,
  //                   &this->configure_msg.request.Emax, layout);
  add_custom_slider("Qd %", 10001, 1, current_configuration.response.qd_factor,
                    1, &this->configure_msg.request.qd_factor, layout);
  add_custom_slider("Qdd %", 10001, 1,
                    current_configuration.response.qdd_factor, 1,
                    &this->configure_msg.request.qdd_factor, layout);
  add_custom_slider("Qddd %", 1000001, 1,
                    current_configuration.response.qddd_factor, 1,
                    &this->configure_msg.request.qddd_factor, layout);
  // add_custom_slider("Tau %", 1001, 1,
  // current_configuration.response.tau_factor,
  //                   1, &this->configure_msg.request.tau_factor, layout);

  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------
  // -------------------------------------------------------------------------------------------

  {
    QFrame *line = new QFrame(this);
    line->setFrameShape(QFrame::HLine); // Horizontal line
    // line->setFrameShadow(QFrame::Sunken);
    line->setLineWidth(big_line_length);
    layout->addWidget(line);
  }

  {
    QLabel *tmpLabelTitle = new QLabel("Controller Parameters");
    QHBoxLayout *tmpH1 = new QHBoxLayout();
    // tmpH1->addWidget(tmpLabelTitle, 0, Qt::AlignTop);
    tmpH1->addWidget(tmpLabelTitle);
    tmpH1->setAlignment(Qt::AlignHCenter);
    tmpH1->setSpacing(0);
    tmpH1->setContentsMargins(0, 0, 0, 0);
    layout->addLayout(tmpH1);
  }

  add_custom_slider("QP Kp Lin", 2000, 1,
                    current_configuration.response.kp_linear, 2000,
                    &this->configure_msg.request.kp_linear, layout);

  add_custom_slider("QP Kp Ang", 2000, 1,
                    current_configuration.response.kp_angular, 2000,
                    &this->configure_msg.request.kp_angular, layout);

  add_custom_slider("QP *√Kp Lin", 1000, 1,
                    current_configuration.response.kd_factor_linear, 3,
                    &this->configure_msg.request.kd_factor_linear, layout);

  add_custom_slider("QP *√Kp Ang", 1000, 1,
                    current_configuration.response.kd_factor_angular, 3,
                    &this->configure_msg.request.kd_factor_angular, layout);

  add_custom_slider("QP ACC FF *", 1000, 1,
                    current_configuration.response.ff_acc_factor, 1,
                    &this->configure_msg.request.ff_acc_factor, layout);

  // ------------------------------------------------------
  // PROPORTIONAL GAIN SLIDERS
  // auto update_gains = [=]() {
  //   std::stringstream command(run("rosservice call /get_gains_service {}"));
  //   command << "rosservice call /set_gains_service " << gain_kp_linear << " "
  //           << gain_kp_angular;
  //   std::cout << command.str() << std::endl;
  //   std::cout << run(command.str().c_str()) << std::endl;
  // };
  // {
  //   std::cout << "execute get_gains:" << std::endl;
  //   std::stringstream sstream(run("rosservice call /get_gains_service {}"));

  //   std::vector<std::string> words;
  //   std::string line;
  //   while (std::getline(sstream, line, '\n')) {
  //     std::stringstream to_split(line);
  //     std::string word;
  //     while (std::getline(to_split, word, ' ')) {
  //       words.push_back(word);
  //     }
  //   }
  //   // int i = 0;
  //   // for (const auto &str : words) {
  //   //   std::cout << i++ << ": " << str << std::endl;
  //   // }

  //   gain_kp_linear = std::stol(words[1]);
  //   gain_kp_angular = std::stol(words[3]);
  //   std::cout << "kp linear:" << gain_kp_linear << std::endl;
  //   std::cout << "kp angular:" << gain_kp_angular << std::endl;
  // }

  // {
  //   QString qtitle(QString::fromStdString("QP Kp Lin"));
  //   QHBoxLayout *tmpH1 = new QHBoxLayout();
  //   QLabel *tmpLabelTitle = new QLabel(qtitle);
  //   QLabel *tmpLabel = new QLabel("0");
  //   QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);

  //   tmpSlider->setMinimum(1);
  //   tmpSlider->setMaximum(2000);
  //   tmpSlider->setValue(gain_kp_linear);
  //   tmpSlider->setTickInterval(1);
  //   tmpSlider->setTickPosition(QSlider::TicksBelow);
  //   tmpH1->addWidget(tmpLabelTitle);
  //   tmpH1->addWidget(tmpSlider);
  //   tmpH1->addWidget(tmpLabel);
  //   layout->addLayout(tmpH1);
  //   auto updateLambda = [=]() {
  //     gain_kp_linear = tmpSlider->value();
  //     tmpLabel->setText(QString::number(gain_kp_linear));
  //   };
  //   updateLambda();
  //   QObject::connect(tmpSlider, &QSlider::valueChanged, this, [=]() {
  //     updateLambda();
  //     update_gains();
  //   });
  // }
  // {
  //   QString qtitle(QString::fromStdString("QP Kp Ang"));
  //   QHBoxLayout *tmpH1 = new QHBoxLayout();
  //   QLabel *tmpLabelTitle = new QLabel(qtitle);
  //   QLabel *tmpLabel = new QLabel("0");
  //   QSlider *tmpSlider = new QSlider(Qt::Horizontal, this);
  //   tmpSlider->setMinimum(1);
  //   tmpSlider->setMaximum(2000);
  //   tmpSlider->setValue(gain_kp_angular);
  //   tmpSlider->setTickInterval(1);
  //   tmpSlider->setTickPosition(QSlider::TicksBelow);
  //   tmpH1->addWidget(tmpLabelTitle);
  //   tmpH1->addWidget(tmpSlider);
  //   tmpH1->addWidget(tmpLabel);
  //   layout->addLayout(tmpH1);
  //   auto updateLambda = [=]() {
  //     gain_kp_angular = tmpSlider->value();
  //     tmpLabel->setText(QString::number(gain_kp_angular));
  //   };
  //   updateLambda();
  //   QObject::connect(tmpSlider, &QSlider::valueChanged, this, [=]() {
  //     updateLambda();
  //     update_gains();
  //   });
  // }

  // ------------------------------------------------------

  // ------------------------------------------------------

  {
    auto *hlayout = new QHBoxLayout;
    hlayout->setAlignment(Qt::AlignHCenter);
    // ---------------------------------------------------
    // recover robot button
    QPushButton *push_button2 = new QPushButton(this);
    push_button2->setText("Recover Robot");
    connect(push_button2, &QPushButton::clicked, this, [=]() {
      std::string command =
          "rostopic pub -1 /panda/franka_control/error_recovery/goal "
          "franka_msgs/ErrorRecoveryActionGoal {}";
      std::cout << command << std::endl;
      std::cout << system(command.c_str()) << std::endl;
    });
    hlayout->addWidget(push_button2);
    layout->addLayout(hlayout);
    // ---------------------------------------------------
  }

  // Vertical layout
  setLayout(layout);

  ROS_WARN_STREAM("lmpcpoly rviz plugin READY");
}

void PushButton::save(rviz::Config config) const { rviz::Panel::save(config); }

void PushButton::load(const rviz::Config &config) { rviz::Panel::load(config); }
} // namespace rviz_robot_plugins

#include <pluginlib/class_list_macros.h>
PLUGINLIB_EXPORT_CLASS(rviz_robot_plugins::PushButton, rviz::Panel)
